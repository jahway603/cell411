#!/usr/bin/env node
const flavor = process.env.PARSE_FLAVOR || "geocache";
const ParseServer = require("parse-server").ParseServer;
const express = require("express");
const fs = require("fs");
const path = require("path");
const pwd=process.env.PWD;


async function main() {
  global.config=await loadConfig();

  await setupExpress();


  const startTime=new Date().getTime();
  setTimeout(checkFiles,5000);
  function checkFiles() {
    const list = [ './index.js', './cloud/main.js' ];
    for(var i=0;i<list.length;i++){
      const file=list[i];
      if(!file.endsWith(".js"))
        continue;
      const stat=fs.statSync(file);
      if(stat.mtime.getTime()>startTime){
        console.warn("Exiting due to cloud code update");
        process.exit(0);
      };
    };
    setTimeout(checkFiles,5000);
  };
}

/// FUNCTIONS DOWN HERE.


async function citiesHandler(req,res) {
  const query = Parse.Query("city");
  const cities = readFully(query);
  return res.status(200).send(JSON.stringify({cities}));
};
async function reverseGeocodeHandler(req, res) {
  try {
    const query=req.query;
    const request={};
    request.params=query;
    console.dump(request);
    const result=await reverseGeocode(request);
    return res.status(200).send( JSON.stringify(result,null,2) );
  } catch ( err ) {
    console.error(err);
    return res.status(400).send( err );
  };
};
async function geocodeHandler(req, res) {
  try {
    const query=req.query;
    const request={};
    request.params=query;
    console.dump(request);
    const result=await geocode(request);
    return res.status(200).send( JSON.stringify(result,null,2) );
  } catch ( err ) {
    console.error(err);
    return res.status(400).send( err );
  };
};
async function die(msg) {
  console.error(msg);
  process.exit(1);
};
async function loadConfig() {
  const home=process.env.HOME;
  const configFile = home+"/.parse/config-"+flavor+".json";
  const configText = fs.readFileSync(configFile);
  const config = JSON.parse(configText);
  const env=process.env;

  config || await die("failed to read: "+configFile);

  if(env['PARSE_SERVER_LOGS_FOLDER']){
    console.log("overriding conf log folder from environment");
    config.logsFolder=env['PROCESS_SERVER_LOGS_FOLDER'];
  } else if(Object.prototype.hasOwnProperty(config,"logFolder")) {
    env['PARSE_SERVER_LOGS_FOLDER']=config.logsFolder;
  };

  return config;
};
async function setupExpress() {
  const app = express();
  app.use(config.mountPath, new ParseServer(config));
  app.get('/cities', citiesHandler);
  app.get('/reverseGeocode', reverseGeocodeHandler);
  app.get('/geocode', geocodeHandler);
  const httpServer = require('http').createServer(app);
  httpServer.listen(config.port, function () {
    console.log('geocache server running on port ' + 
      config.port + '.');
  });
};
main().then((res)=>{console.log("main returned");})
.catch((err)=>{console.error("error:",err)});
