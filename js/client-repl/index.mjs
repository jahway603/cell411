console.log('started');
import repl from 'repl';

const replConfig = {
  prompt: '> ',
  useColors: true,
  ignoreUndefined: true,
  preview: true
};
// replConfig.eval=function replEval(text,repl)
// {
//   console.log("\n\n\n",{text,repl: JSON.stringify(repl)});
// };
const replServer = repl.start(replConfig);
replServer.setupHistory(process.env.HOME+'/.parse/cli-history', ()=>{});
replServer.defineCommand('sayhello', {
  help: 'Say hello',
  action(name) {
    this.clearBufferedCommand();
    console.log(`Hello, ${name}!`);
    this.displayPrompt();
  }
});
replServer.defineCommand('saybye', function saybye() {
  console.log('Goodbye!');
  this.close();
});
