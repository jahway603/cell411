import 'parse-global';
import util from 'util';
import net from 'net';
const flavor = process.env.PARSE_FLAVOR;
if(!flavor) {
  console.err("must set variable PARSE_FLAVOR");
  process.exit(1);
};
await loadParseConfig();
const initOptions = {
};
const pgp_cred=await readJson(configDir+'/pg_admin.json');
const pgp = (await import('pg-promise')).default(initOptions);
pgp_cred.database=flavor;
const db = pgp(pgp_cred);
// Helper for linking to external query files:
function sql(file) {
  const fullPath = process.env.PWD+"/"+file; // generating full path;
  return new pgp.QueryFile(fullPath, {minify: false});
}
const cols = sql("sql/cols.sql");
const columns = sql("sql/columns.sql");

function q(id) {
  if(id == 'user')
    return '"user"';
  if(id.match(/^[a-z_]+$/)){
    return id;
  } else {
    return '"'+id+'"';
  };
};
async function tableToSql(table_name, cols) {
  var sql = `create table ${q(table_name)} (`;
  for(var i=0;i<cols.length;i++){
    if(i>0)
      sql = sql + ',';
    const col = cols[i];
    sql = sql + `\n  ${q(col.column_name)} ${col.data_type}`;    
  }
  sql = sql + `\n);`;
  return sql;
};
const skip = {
"_Hooks" : 1,
"_Idempotency": 1,
"_JobSchedule": 1,
"_JobStatus": 1,
"_PushStatus": 1,
"numsrc": 1,
"spatial_ref_sys": 1,
  "_GlobalConfig": 1,
};
async function dbdump(){
  var columns;
  const tables = {};
  columns = await db.many(cols, [ flavor ] ).then((x)=>{return x;});
  for(var i=0;i<columns.length;i++){
    const col = columns[i];
    const { table_name, ordinal_position } = col;
    if(skip[table_name])
      continue;
    if(table_name.substr(0,3)=="rej")
      continue;
    delete col.table_name;
    delete col.ordinal_position;

    if(col.data_type == 'ARRAY'){
      col.data_type='text[]';
    };
    if(tables[table_name]==null)
      tables[table_name]=[];
    tables[table_name][ordinal_position-1]=col;
  };
  const table_names = Object.keys(tables);
  table_names.sort();
  for(var i=0;i<table_names.length;i++){
    const table_name = table_names[i];
    console.log(await tableToSql(table_name, tables[table_name]));
  };
};

await dbdump().then(()=>pgp.end());


