select      table_name,
            column_name,
            ordinal_position,
            data_type
   from     information_schema.columns c
    where   ( c.table_catalog, c.table_schema, c.table_name ) in
   (select    t.table_catalog, 
              t.table_schema, 
              t.table_name
         from information_schema.tables t
        where t.table_schema = 'public'
          and t.table_catalog = $1
          and t.table_type = 'BASE TABLE'
        );




