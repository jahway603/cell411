set -e
node dbdump.mjs > out.sql
psql -c 'drop database test';
psql -c 'create database test';
psql test < out.sql 
