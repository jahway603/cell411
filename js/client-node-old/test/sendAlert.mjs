import 'cell411'

async function main() {
  const user = await parseLogin();
  const sessionToken = user.getSessionToken();
  const skey = { sessionToken };
  const locinfo = await Parse.Cloud.run("geocode", {address:"keene, nh", type: "city"});
  const location = locinfo.location;
  console.dump(location);
  const alert = {
    problemType: "Crime",
    location: location,
  };
  const audience = [
    "global", "allFriends", "allCells"
  ];
  const params = { alert, audience };
  console.dump(skey);
  const res = await Parse.Cloud.run("sendAlert", params, skey);
  const alertId = res.alert;
  const alertObj = await new Parse.Query("Alert").get(alertId);
  console.dump(alertObj);
}
main()
