#!/usr/bin/env node

await import('parse-global');
await import('cell411');
await initializeParse();
const user = await parseLogin();
const username=user.get("username");
const sessionToken=await user.getSessionToken();
//  var _CoreManager = await import ("parse/CoreManager.js");
//   const lqc= _CoreManager.default.getLiveQueryController();
//   
//   const def=await lqc.getDefaultLiveQueryClient();
//   
function event(event,...args){
  console.dump({event,args});
};
async function runAndAttach(query) {
  const results={};
  const subscription = await query.subscribe(sessionToken);
  const names=[ 'open','create','update','delete','enter','leave' ];
  for(var i=0;i<names.length;i++){
    const name=names[i];
    subscription.on(name,function(...args) { event(name,args); });
  };
  return results;
}
//   async function createOrQuery(table,value,...cols){
//     var q1 = new Parse.Query(table);
//     q1.equalTo(cols.shift(),value);
//     console.dump({q1});
//     if(cols.length) {
//       var q2 = createOrQuery(table,value,cols);
//       console.dump(q1,q2);
//       q1 = Parse.Query.or(q1,q2);
//     };
//     return q1;
//   };
   async function alertQuery(func, key, val){
     const query = new Parse.Query("Request");
     (query[func])(key,val);
     return query;
   };
//   async function show(query){
//     console.dump(await findFully(query));
//   };
//   async function watch(query){
//     return await runAndAttach(query);
//   };
async function allRelated() {
  var query = new Parse.Query(Request);
  query.containedIn("owner",[user,user.id]);
  query.containedIn("sentTo",[user,user.id]);
  const subscription = await query.subscribe(sessionToken);
  console.dump(subscription);
  subscription.on('open',console.log);
  subscription.on('create',console.log);
//     await runAndAttach(query);
//     await show(query);
//     await watch(query);
//     update.containedBy("audience",[user.id]);
//     update.limit(10);
//     console.dump(await update.find());
//     update.containedIn("audience",[user.id]);
//     console.dump(await update.find());
//     update.containedIn("audience",[user]);
};
await allRelated(user);
