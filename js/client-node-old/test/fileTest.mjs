#!node

const parse_login=await import("../parse-login.mjs");
const user=await parseLogin();
const fname="urls.txt";
const FileType = Parse.Object.extend("FileType");

async function createFileTypeObjs() {
  const text = fs.readFileSync(fname).toString();
  const urls = text.split(/\n/);
  const thumbnails = [];
  const avatars = [];
  for(var i=0;i<urls.length;i++){
    const url = urls[i];
    if(url.match(/thumb/)){
      thumbnails.push(url);
    } else {
      avatars.push(url);
    }
  }
  console.log({thumbnails,avatars});
  const length = Math.max(avatars.length,thumbnails.length);
  for(var i=0;i<length;i++) {
    console.log(i);
    const fileType = new FileType();
    const data1 = { uri: thumbnails[i] };
    const data2 = { uri: avatars[i] };

    if(data1 && data1.uri ) {
      console.dump({data1});
      var name1="avatar.png";
      const file1 = new Parse.File(name1,data1,"image/png");
      fileType.set("file1",file1);
    };

    if(data2 && data2.uri) {
      console.dump({data2});
      var name2="thumbnail.png";
      const file2 = new Parse.File(name2,data2,"image/png");
      fileType.set("file2",file2);
    };

    fileType.save(null,{useMasterKey: true});
  };
}
async function getMD5Promise(file){
  if(file!=null) {
    if(!fs.existsSync("cache/"+file.name())) {
      console.log("+ file.getData()");
      const base64 = await file.getData();
      const data = Buffer.from(base64,'base64');
      fs.writeFileSync("cache/"+file.name(),data);
    };
    const md5 = getMD5Sum("cache/"+file.name());
    return {file, fileName: "cache/"+file.name(), md5};
  }
};
async function readFileTypeObjs() {
  const query = new Parse.Query("FileType");
  const fileTypes = await findFully(query);
  const promises=[];
  for(var i=0;i<fileTypes.length;i++){
    console.log("fileTypes["+i+"]");
    const fileType = fileTypes[i];
    promises.push(getMD5Promise(fileType.get("file1")));
    promises.push(getMD5Promise(fileType.get("file2")));
  };
  await Promise.all(promises);
  const results=[];
  for(var i=0;i<promises.length;i++){
    const res = await promises[i];
    results.push(res);
  };
}
async function main(argv) {
  if(fs.existsSync(fname)) {
    return await createFileTypeObjs();
  } else {
    return await readFileTypeObjs();
  };
}
main(process.argv);
