cell () 
{ 
  export -f cell
  if test -z "$SUBSHELL" ; then
    SUBSHELL=true bash -c cell
  else
    cd "$dir";
    local PARSE_FLAVOR=parse PARSE_USER=${PARSE_USER}
    test -z "$PARSE_USER" && PARSE_USER=dev1
    export PARSE_FLAVOR PARSE_USER
    node cell.mjs
  fi
}
