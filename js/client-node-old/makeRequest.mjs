#!node
await import("cell411");
Parse.Object.enableSingleInstance();
initializeParse();
//global.user = await parseLogin();
const query=new Parse.Query("_User");
//const cskey = { sessionToken: user.getSessionToken() };
const mkey = { useMasterKey: true };
global.util = await import('util');

const users = await new Parse.Query("_User").containedIn("objectId", [ "dev1", "dev2" ]).find();
console.dump(users);
const req = new Request();
req.set("owner",users[0]);
req.set("sentTo",users[1]);
req.set("status","PENDING");
console.dump({req});
const x = await req.save(null,mkey);
console.dump({req});
console.dump({x});
