#!node

global.res=global.res;
const cell411 = await import("cell411");
import md5 from 'md5';
import { EventEmitter, errorMonitor } from 'events';
await import("parse-global/pg-connect.js");
await import("pg-promise");

const net = await import("net");
async function main() {
  console.log("main: enter");
  const fileName=process.argv[1]+".out";
  const user = await parseLogin();
  const query = new Parse.Query("ChatMsg");
  const sub = await query.subscribe(user.getSessionToken());
  const bareEvents = [ 'open', 'close', 'error' ];
  const objEvents = [ 'create', 'enter', 'leave', 'update', 'delete' ];
  const objevs = {
  };
  const events = [];
  for(var ev of objEvents ) {
    objevs[ev]=1;
    events.push(ev);
  };
  for(var ev of bareEvents ) {
    objevs[ev]=0;
    events.push(ev);
  };
  const evList = [];
  console.log(sub.eventNames());
  for(var event of events){
    const ev=event;
    sub.on(ev,(...args)=>{
      if(objevs[ev]){
        console.log({ev,id:args[0].id});
      } else {
        console.log({ev});
      }
      evList.push({ev,args});
      writeJson(fileName,evList);
    });
  };

  console.log("main: exit");
};
await main();
