#!node
await import("cell411");
Parse.Object.enableSingleInstance();
global.user = await parseLogin();
const query=new Parse.Query("_User");
const mkey = { useMasterKey: true };
const forward=false;
const reverse=true;
const child_process = await import("child_process");
global.util = await import('util');
const process =await import('process');
const { stdin: input, stderr: output } = process;
const readline=await import ('node:readline/promises');
const prompt="\nCell411 Tui> ";
const rl=readline.createInterface({
  input,
  output,
  prompt
});
async function end() {
  rl.close();
};

async function randomCell(rev) {
  var cellQ;
  if(rev) {
    cellQ = new Parse.Query("PublicCell");
    cellQ.equalTo("owner",user);
  } else {
    const excludeQ = Parse.Query.or(
      new Parse.Query("PublicCell").equalTo("owner",user),
      new Parse.Query("PublicCell").equalTo("members",user),
    );
    const revQ = new Parse.Query("PublicCell");
    revQ.doesNotMatchKeyInQuery("objectId","objectId",excludeQ);
    cellQ=revQ;
  };
  while(true) {
    const count=await cellQ.count();
    var cell;
    if(count==0){
      cell = new PublicCell();
      cell.set("owner",user);
      cell.set("description", "automatically created");
      cell.set("Category",3);
      cell.set("name","the cell nobody knows about");
      console.dump(cell);
      await cell.save(null,{mkey});
      console.dump(cell);
      continue;
    };
    const skip = Math.round(Math.random()*count);
    console.log({count,skip});
    cellQ.skip(skip);
    cellQ.limit(1);
    const cells=await cellQ.find();
    cell=cells[0];
    if(cell)
      return cell;
    console.dump(cellQ);
  };
};
async function cellRequest(rev) {
  const mark=makeTimer("cellRequest");
  mark("starting");
  var cell=await randomCell(rev);
  const params={
    type: "CellJoinRequest",
    objectId: cell.id
  };
  const other = await randomUser(rev,cell.relation("members").query(),mark);
  var res =await Parse.Cloud.run("sendRequest",params,mkey);
  var req = res.request;
  var owner = req.get("owner").toString();
  var sentTo = req.get("sentTo").toString();
  cell = req.get("cell").toString();
  req=req.toString();
  console.log({req,owner,sentTo,cell});
}
//   async function getOneRequestQuery(rev){
//     return new Parse.Query("Request");
//   };
//   async function getRequestQuery(friend,rev){
//     const userKey = rev ? "sentTo" : "owner";
//     const otherKey = rev ? "owner" : "sentTo";
//     const requestQ = await getOneRequestQuery(rev);
//     requestQ.equalTo(userKey,user);
//     requestQ.containedIn("status",[ "PENDING", "RESENT" ]);
//     requestQ.include(otherKey);
//     if(friend){
//       requestQ.doesNotExist("cell");
//     } else {
//       requestQ.exists("cell");
//     };
//     return await requestQ.find();
//   };
//   async function getFriendRequests(rev){
//     return await getRequestQuery(true,rev);
//   };
//   async function getCellRequests(rev){
//     return await getRequestQuery(false,rev);
//   };
//   async function acceptCellRequest(rev) {
//     const reqs = await getCellRequests(rev);
//     console.log(`found ${reqs.length} reqs ${rev?"for":"from"} you`);
//     var i=0;
//     if(reqs.length>1){
//       i=Math.random()*i;
//     };
//     const req=reqs[i];
//     if(reqs.length>0) {
//       const owner=req.get("owner");
//       owner.fetch();
//       const sentTo=req.get("sentTo");
//       sentTo.fetch();
//       const cell=req.get("cell");
//       cell.fetch();
//       const status=req.get("status");
//       console.dump({owner,sentTo});
//       console.log(`accepting request from ${owner.get('username')} to ${sentTo.get('username')}`);
//       console.log({owner,sentTo,cell,status});
//       return await acceptRequest(reqs[i]);
//     } else {
//       return { message: "no reqs found" };
//     };
//   };
//   async function acceptFriendRequest(rev) {
//     const reqs = await getFriendRequests(rev);
//     console.log(`found ${reqs.length} reqs ${rev?"for":"from"} you`);
//     var i=0;
//     if(reqs.length>1){
//       i=Math.random()*i;
//     };
//     const req=reqs[i];
//     if(reqs.length>0) {
//       const owner=req.get("owner");
//       owner.fetch();
//       const sentTo=req.get("sentTo");
//       sentTo.fetch();
//       const status=req.get("status");
//       console.dump({owner,sentTo});
//       console.log(`accepting request from ${owner.get('username')} to ${sentTo.get('username')}`);
//       console.log({owner,sentTo,status});
//       return await acceptRequest(reqs[i]);
//     } else {
//       return { message: "no reqs found" };
//     };
//   };
//   async function acceptRequest(request){
//     const sentTo=request.get("sentTo");
//     const skey={sessionToken: null};
//     if(sentTo.id !== user.id){
//       sentTo.set("password","asdf");
//       await sentTo.save(null,mkey);
//       await sentTo.logIn();
//       const sessionToken=sentTo.getSessionToken();
//       console.dump({sentTo});
//       skey.sessionToken=sessionToken;;
//     } else {
//       console.dump({user});
//       const sessionToken=cskey.sessionToken;
//       skey.sessionToken=sessionToken;
//     };
//     console.dump({skey});
//     const cell=request.get("cell");
//     const params={
//       objectId:request.id,
//       type: (cell==null?"FriendApprove":"CellJoinApprove")
//     };
//     console.log(params);
//     const res = await Parse.Cloud.run("sendRequestResponse",params,skey);
//     return res;
//   };
//   async function jiggleAll(rev) {
//     const requests = await getFriendRequests(rev);
//     for(var i=0;i<requests.length;i++){
//       requests[i].set("status","RESENT");
//       await requests[i].save(null,mkey);
//       requests[i].set("status","PENDING");
//       await requests[i].save(null,mkey);
//     };
//   };
function time() {
  return new Date().getTime();
};
function makeTimer(tag){
  if(tag==null||tag==""){
    tag="Unnamed";
  };
  const start=time();
  const mark=function mark(comment){
    var elapsed=""+(time()-start);
    while(elapsed.length<5){
      elapsed=" "+elapsed;
    };
    console.log(elapsed,tag," ",comment);
  };
  mark("start");
  return mark;
};
async function randomUser(rev,excludeQ,mark) {
  mark("got count");
  const otherQ = await new Parse.Query(Parse.User);
  otherQ.exists("email");
  otherQ.exists("username");
  if(excludeQ)
    otherQ.doesNotMatchKeyInQuery("objectId","objectId",excludeQ);
  otherQ.notEqualTo("objectId",user.id);
  var count=await otherQ.count(mkey);
  var skip=Math.random()*count;
  otherQ.skip(skip);
  otherQ.limit(1);
  mark("running query");
  const newFriends=await findFully(otherQ);
  mark("query done");
  if(newFriends==null || newFriends.length==0) {
    mark("failed");
    console.dump({count,skip,otherQ});
    console.log( "Failed to find unfriended user" );
    return;
  };
  const newFriend = newFriends[0];
  if(rev) {
    mark("setting newFriend's password");
    newFriend.set("password","asdf");
    await newFriend.save(null,mkey);
    mark("logging in");
  };
  return newFriend;
};
//   async function spam(rev) {
//     const mark=makeTimer("spamRequest");
//     const newFriend = await randomUser(rev,null,mark);
//     console.dump({newFriend});
//     if(rev){
//       newFriend.relation("spamUsers").add(user);
//       await newFriend.save(null,mkey);
//     } else {
//       user.relation("spamUsers").add(newFriend);
//       await user.save(null,mkey);
//     }
//   };
async function requestClear() {
  console.log("requestClear()");
  const requestQuery1 = new Parse.Query("Request").equalTo("sentTo",user.id);
  const requestQuery2 = new Parse.Query("Request").equalTo("owner",user.id);
  const requestQuery = Parse.Query.or(requestQuery1,requestQuery2);
  const requests = await requestQuery.find();
  if(requests.length) {
    for(let i=0;i<requests.length;i++){
      console.log("destroy: "+requests[i].id);
      await requests[i].destroy();
    };
  } else {
    console.log("No requests to destroy");
  };
}
async function friendClear() {
  const friendR = user.relation("friends")
  const friendQ = friendR.query();
  const friends = await friendQ.find();
  for(var i=0;i<friends.length;i++){
    console.log(friends[i]);
    friendR.remove(friends[i]);
  };
  await user.save(null,mkey);
}
async function friendRequest(rev) {
  //  const promise=new Promise(async (resolve,reject)=>{
  const mark=makeTimer("friendRequest");
  var i=0;
  const friendQ = user.relation("friends").query();
  const requestQuery1 = new Parse.Query("Request").equalTo("sentTo",user.id);
  const requestUsers1 = new Parse.Query("_User").matchesKeyInQuery("objectId","owner",requestQuery1); 
  const requestQuery2 = new Parse.Query("Request").equalTo("owner",user.id);
  const requestUsers2 = new Parse.Query("_User").matchesKeyInQuery("objectId","sentTo",requestQuery2); 
  const selfQuery = new Parse.Query("_User").equalTo("objectId",user.id);
  const excludeQuery = Parse.Query.or(requestUsers1,requestUsers2,friendQ,selfQuery);
  console.dump(await excludeQuery.find());
  const newFriend = await randomUser(rev,excludeQuery,mark);
  if(!newFriend)
    return;
  const params={ type: "FriendRequest" };
  if(rev) {
    params.objectId=user.id;
    params.user=newFriend.id;
  } else {
    params.objectId=newFriend.id;
    params.user=user.id;
  };
  mark("running function");
  const res = await Parse.Cloud.run("sendRequest",params,mkey);
  mark("done");
  console.log({res});
  return (res);
}
function pad(i) {
  var res=""+i;
  while(res.length<5){
    res=" "+res;
  };
  return res;
};
function menu() {
  console.log("MENU: ");
  for(var i of Object.keys(options)) {
    console.log(`${pad(i)}) ${options[i].text}`);
  };
};
//   function selfDump() {
//     console.dump(user);
//   };
async function makeRequest(owned,cell,active){
  const query = new Parse.Query("Request");
  if(owned) {
    query.equalTo("owner",user);
  } else {
    query.equalTo("sentTo",user);
  };
  if(cell) {
    query.exists("cell");
  } else {
    query.doesNotExist("cell");
  };
  const activeStates = [ "PENDING", "RESENT" ];
  if(active) {
    query.containedIn("status",activeStates);
  } else {
    query.notContainedIn("status",activeStates);
  };
  return query.count().then((count)=>{
    return { owned, cell, active, count };
  });
};
async function countRequests() {
  const promises=[];
  promises.push(makeRequest(false  ,false  ,false  ));
  promises.push(makeRequest(false  ,false  ,true   ));
  promises.push(makeRequest(false  ,true   ,false  ));
  promises.push(makeRequest(false  ,true   ,true   ));
  promises.push(makeRequest(true   ,false  ,false  ));
  promises.push(makeRequest(true   ,false  ,true   ));
  promises.push(makeRequest(true   ,true   ,false  ));
  promises.push(makeRequest(true   ,true   ,true   ));
  const result = await Promise.all(promises);
  console.table(result);
  return "that's it";
};
const options={
  m:   {  text:  "menu",                    func:  ()=>{  return  menu();                      }  },
  q:   {  text:  "exit",                    func:  ()=>{  return  end();                       }  },
  d:   {  text:  "count_requests",          func:  ()=>{  return  countRequests();             }  },
  a:   {  text:  "Send Alert",              func:  ()=>{  return  sendAlert();                 }  },
  A:   {  text:  "Kill Alerts",             func:  ()=>{  return  killAlerts();                }  },
  sd:  {  text:  "self-dump",               func:  ()=>{  return  selfDump();                  }  },
  fo:  {  text:  "Send_Friend_Request",     func:  ()=>{  return  friendRequest(false);        }  },
  fi:  {  text:  "Receive_Friend_Request",  func:  ()=>{  return  friendRequest(true);         }  },
  cc:  {  text:  "Create_Cell",             func:  ()=>{  return  createCell();                }  },
  cf:  {  text:  "Clear_Friends",           func:  ()=>{  return  friendClear();               }  },
  cr:  {  text:  "Clear_Requests",          func:  ()=>{  return  requestClear();              }  },
  so:  {  text:  "Spam_A_User",             func:  ()=>{  return  spam(false);        }  },
  si:  {  text:  "User_Spam_You",           func:  ()=>{  return  spam(true);         }  },
  go:  {  text:  "Send_Cell_Request",       func:  ()=>{  return  cellRequest(false);          }  },
  gi:  {  text:  "Receive_Cell_Request",    func:  ()=>{  return  cellRequest(true);           }  },
  aO:  {  text:  "Accept_Out_Cell",         func:  ()=>{  return  acceptCellRequest(true);     }  },
  aI:  {  text:  "Accept_In_Cell",          func:  ()=>{  return  acceptCellRequest(false);    }  },
  ao:  {  text:  "Accept_Out_Friend",       func:  ()=>{  return  acceptFriendRequest(true);   }  },
  ai:  {  text:  "Accept_In_Friend",        func:  ()=>{  return  acceptFriendRequest(false);  }  },
  ji:  {  text:  "Jiggle_One_Incoming",     func:  ()=>{  return  jiggleOne(false);            }  },
  jo:  {  text:  "Jiggle_One_Outgoing",     func:  ()=>{  return  jiggleOne(true);             }  },
};
options['?']=options['m'];
options['h']=options['m'];
//   
async function command(txt) {
  console.log(`calling options[${txt}].func()`);
  if(options[txt]){
    var func = options[txt].func;
    console.log({func,end});
    var p=await func();
  } else {
    console.log("No command for: "+txt);
  };
  if(options[txt]!=options['q']){
    rl.prompt();
  };
};
//   async function sendAlert() {
//     const params={};
//     const query = new Parse.Query("ProblemType");
//     const types = await findFully(query);
//     const i = Math.floor(Math.random()*types.length);
//     const name=types[i].id;
//   
//     params.alert={};
//      
//     params.alert.location=new Parse.GeoPoint(42.927950,-72.275078);
//     params.alert.problemType=name;
//     params.audience=[];
//     params.audience.push("global");
//     params.audience.push("allFriends");
//     params.audience.push("allCells");
//     const res = await Parse.Cloud.run("sendAlert",params,{sessionToken:user.getSessionToken()});
//     console.dump(res);
//     const alert = res['alert'];
//     if(alert!=null){
//       const query = new Parse.Query("Alert");
//       const alertObj = await query.get(alert);
//       console.dump(alertObj);
//     }
//   }
//   async function killAlerts() {
//     const query = new Parse.Query("Alert");
//     const limit = 10;
//     query.limit(limit);
//     while(true){
//       const tmp = await query.find();
//       const ids = [];
//       if(tmp.length==0)
//         break;
//       for(var i=0;i<tmp.length;i++){
//         const alert = tmp[i];
//         console.log(alert.id);
//         await alert.destroy(mkey);
//       };
//     }
//   }

//   async function fixCommand(){
//     fs.writeFileSync("tempfile.txt",last); 
//     child_process.execSync("vi tempfile.txt",{stdio: [ 'inherit', 'inherit', 'inherit' ]} );
//     line=fs.readFileSync("tempfile.txt").toString();
//     for(const word of line.trim().split(/ +/)){
//       await command(word);
//     } 
//   }
rl.on('close',()=>{
  console.log("bye, now!");
  process.exit(0);
});
rl.on('line',async (line)=>{
  for(const word of line.trim().split(/ +/)){
    await command(word);
  };
});
console.log("The setup thread fell off the end of the script, but that's how it "
  +"goes in javascript.\n\n");
setTimeout(async ()=>{
  await command("m");
},100);
