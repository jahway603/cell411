#!node

const parse_login=await import("cell411");
const user=await parseLogin();
async function populateUser(user,username,password){
  user.set("username",username);
  user.set("email",username);
  user.set("password",password);
  var parts = user.get("username").split("@");
  if(parts.length==2){
    user.set("firstName",parts[0]);
    user.set("lastName","user");
  } else if ( parts.length == 1 ) {
    throw new Error("username must be an email address");
  } else {
    throw new Error("Unexpected parts: "+parts);
  };
  user.set('newPublicCellAlert', true);
  user.set('patrolMode', true);
  if(user.id==null) {
    // gotta get an objectId
    user=await user.save(null,{useMasterKey:true});
  };
  var acl = user.getACL();
  var dirty = false;
  acl=new Parse.ACL();
  dirty=true;
  if(!acl.getPublicReadAccess()) {
    acl.setPublicReadAccess(dirty=true);
  };
  if(!acl.getWriteAccess(user.id)) {
    acl.setWriteAccess(user.id,(dirty=true));
  };
  if(acl.getPublicWriteAccess()) {
    acl.setPublicWriteAccess(!(dirty=true));
  }
  if(dirty) { 
    user.setACL(acl);
    user=await user.save(null,{useMasterKey: true})
  };
};
async function main(argv) {
  const regex=/tes.*@copblock.app$/;
  var start = await new Parse.Query("_User").matches("username",regex).count();
  console.log({start});
  for(var count=start;count<start+10;count++){
    const user = new Parse.User();
    var countStr = ""+count;
    while(countStr.length<4){
      countStr="0"+countStr;
    };
    await populateUser(user,"test"+countStr+"@copblock.app",process.env.PARSE_PASSWORD || "aa");
  }
  var finish = await new Parse.Query("_User").matches("username",regex).count();
  console.log({finish});
}
main(process.argv);
