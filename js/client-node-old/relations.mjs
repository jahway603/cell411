await import('cell411-mjs');
console.dump=function(obj) { console.log(JSON.stringify(obj,null,2)); };
async function main() {
  await initializeParse();
  const user = await parseLogin();
  const sessionToken=user.getSessionToken();
  const rows = await  Parse.Cloud.run("relations",null,{sessionToken});
  console.dump(rows);
}
main()
