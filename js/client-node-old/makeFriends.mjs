#!node
const parse_login_im=await import("cell411");
global.user=global.user;

async function main() {
  const user = await parseLogin();
  console.log("login done");
  const eq1 = user.relation("friends").query();
  const eq2 = user.relation("spamUsers").query();
  const eq3 = new Parse.Query("_User").equalTo("spamUsers",user);
//     console.dump({eq1:await eq1.find()});
//     console.dump({eq2:await eq2.find()});
//     console.dump({eq3:await eq3.find()});
  const excludeQ = Parse.Query.or(eq1,eq2,eq3);
//     const excludes = await excludeQ.find();
//     console.log("excluded");
//     for(var i=0;i<excludes.length;i++){
//       console.log("  "+excludes[i].id);
//     };
  const query = new Parse.Query("_User");
  query.notEqualTo("objectId",user.id);
  query.doesNotMatchKeyInQuery("objectId", "objectId", excludeQ );
  query.ascending("objectId");
  const friends = await query.find(); 
  if(friends.length==0)
    return;
  var num=Math.min(friends.length,10); 
  for(var i=0;i<num;i++){
    const index = Math.floor(friends.length*Math.random());
    const friend = friends[index];
    friend.relation("friends").add(user);
    friend.save(null,{useMasterKey: true});//{sessionToken: user.getSessionToken()});
    user.relation("friends").add(friend);
  }
  user.save(null,{useMasterKey: true});
}
main();
