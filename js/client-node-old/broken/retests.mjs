#!node
await import("./parse-login.mjs");
Parse.Object.enableSingleInstance();
global.currentUser = await parseLogin();
const skey = { sessionToken: currentUser.getSessionToken() };
const mkey = { useMasterKey: true };
global.util = await import('util');

async function main(args){
  var users;
  var lengthen=false;
  for(var i=0;i<args.length;i++){
    if(args[i]==="+"){
      lengthen=true;
    }
  };
  if(lengthen){
    users = await new Parse.Query("_User").equalTo("lastName","paul").limit(3).find();
  } else {
    users = await new Parse.Query("_User").notEqualTo("lastName","paul").limit(3).find();
  };
  for(var i=0;i<users.length;i++) {
    const user = users[i];
    console.log(user);
    setTimeout(()=>{
      console.log(user);
      if(lengthen) {
        user.set("lastName","paul ("+user.id+")");
      } else {
        user.set("lastName","paul");
      }
      console.log(user.get("lastName"));
      user.save(null,mkey);
    }, i*1000);
  };
};
main(process.argv);
