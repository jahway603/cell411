#!node
const parse_login_im=await import("./parse-login.mjs");
global.user=global.user;

async function main() {
  const user = await parseLogin();
  console.log("login done");
  const query = user.relation("friends").query();
  const friends = await query.find(); 
  for(var i=0;i<friends.length;i++){
    const friend = friends[i];
    console.log(friend.id);
    friend.relation("friends").remove(user);
    friend.save(null,{useMasterKey: true});//{sessionToken: user.getSessionToken()});
    user.relation("friends").remove(friend);
    user.save(null,{useMasterKey: true});//{sessionToken: user.getSessionToken()});
  }
}
main();
