#!node
await import("pg-promise");
await import("parse-global");
await import("./pg-connect.js");
await import("./parse-login.mjs");
global.user=await parseLogin();
const pgp_cred=await readJson(configDir+'/pg_admin.json');
const db = pgp(pgp_cred);

async function query(sql,args) {
  const arr=await db.any(sql,args);
  for(var i=0;i<arr.length;i++){
    arr[i]=arr[i].x;
  }
  return arr;
};
async function main() {
  var results;
  const skel={ id: 'dev1' };
//     results = await query('select "relatedId" as x from "_Join:friends:_User" where "owningId" = '+"'dev1'");
//     skel.id=user.id;
//     skel.friends=results;
//     results = await query('select "relatedId" as x from "_Join:spamUsers:_User" where "owningId" = '+"'dev1'");
//     skel.spamUsers=results;
//     results = await query('select "owningId" as x from "_Join:spamUsers:_User" where "relatedId" = '+"'dev1'");
//     skel.revSpamUsers=results;
  skel.owner={};
  skel.owner.PublicCell=await query('select "objectId" as x from $1:name where $2:name = $3 ', [ "PublicCell", "owner", "dev2" ] );
  skel.owner.PrivateCell=await query('select "objectId" as x from $1:name where $2:name = $3 ', [ "PrivateCell", "owner", "dev2" ]);
  skel.owner.Alert= await query('select "objectId" as x from $1:name where $2:name = $3 ', [ "Alert", "owner", "dev2" ]);
  skel.owner.ChatMsg= await query('select "objectId" as x from $1:name where $2:name = $3 ', [ "ChatMsg", "owner", "dev2" ]);
  skel.owner.Request= await query('select "objectId" as x from $1:name where $2:name = $3 ', [ "Request", "owner", "dev2" ]);
  skel.owner.Request= await query('select "objectId" as x from $1:name where $2:name = $3 ', [ "Request", "sentTo", "dev2" ]);
  //skel.owner.Response= await query('select "objectId" as x from $1:name where $2:name = $3 ', [ "Response", "owner", "dev2" ]);
  console.log("here");
  skel.owner.Response= await query('select "objectId" as x from $1:name where $2:name =  $3  ', [ "Response", "owner", "dev2" ]);

  console.dump(skel);
}
main().finally(()=>{
  pgp.end();
});
