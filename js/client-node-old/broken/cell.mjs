#!node
await import("./parse-login.mjs");
const inspect = (await import('object-inspect')).default;

Parse.Object.enableSingleInstance();

global.user = await parseLogin();
global.util = await import('util');
global.rl = await getReadLine();

const skey = { sessionToken: user.getSessionToken() };
const mkey = { useMasterKey: true, sessionToken: user.getSessionToken() };
const schema = await makeSchema();

const alias = {
 user: "_User",
 pubcell: "PublicCell",
 pricell: "PrivateCell",
 alert: "Alert"
};

async function makeSchema() {
  const arr = await Parse.Schema.all();
  const obj = {};
  for(var i=0;i<arr.length;i++) {
    var type = arr[i];
    const className = type.className;
    delete type.className;
    obj[className]=type;
  }
  return obj;
}
async function getSchema(className){
  if(alias[className])
    className=alias[className];
  return schema[className];
}
async function getFields(className){
  const schema = getSchema(className);
  return schema.fields;
};

var done=false;
const cmdtab = {
  quit: function quit(args){
    if(args.length)
      throw new Error("usage: quit");
    done=true;
  },
  create: function create(args){
    if(!args.length)
      throw new Error("usage: create <type>");
    const type=args.shift();
    console.log(`create a ${type}`);
    const fields = getFields(type);
    console.dump({fields});
  }
};

main().catch((err)=>{
  console.log(inspect(err));
}).finally(()=>{
  console.log("finally\n");
  rl.close();
});

async function command() {
  console.log(await rl.question("what up?"));
  for await (const line of rl)
  {
    var words = await split(line);
    var name=words.shift();
    var args=words;
    var func=cmdtab[name];
    var doit=function(){ this.func(this.args); };
    console.log({words,name,args,func,doit});
    return {name,args,func,doit};
  }
}

async function main() {
  while(!done){
    try {
      const cmd=await command();
      if(cmd!=null && cmd.func!=null)
        cmd.doit();
    } catch (err) {
      var stack=err.stack;
      console.log({stack});
      err=inspect(err);
      console.log({err});
    }
  }
}


async function split(line){
  const words = line.split(/\s+/);
  for(var i=0;i<words.length;i++){
    const word=words[i];
    if(!word.length){
      words.splice(i,1); 
    }
  }
  return words;
}
