#!node
await import('./parse-login.mjs');

async function main() {
  console.log("main: enter");
  const user = await parseLogin();
  const sessionToken=user.getSessionToken();
  const options={sessionToken};
  {
    const query = new Parse.Query("Alert").containedIn("audience",["dev1"]);
    const alerts = await findFully(query);
    console.log(alerts);
  }
  {
    const user = await new Parse.Query("_User").get("dev1");
    console.dump(user);
    const query = new Parse.Query("Alert").containedIn("audience",[user]);
    const alerts = await findFully(query);
    console.log(alerts);
  }
};
main();
