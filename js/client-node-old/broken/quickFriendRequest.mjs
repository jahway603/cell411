#!node

const parse_login=await import("./parse-login.mjs");
const user=await parseLogin();
async function populateUser(user,username,password){
  user.set("username",username);
  user.set("email",username);
  user.set("password",password);
  var parts = user.get("username").split("@");
  if(parts.length==2){
    user.set("firstName",parts[0]);
    user.set("lastName","user");
  } else if ( parts.length == 1 ) {
    throw new Error("username must be an email address");
  } else {
    throw new Error("Unexpected parts: "+parts);
  };
  user.set('newPublicCellAlert', true);
  user.set('patrolMode', true);
  if(user.id==null) {
    // gotta get an objectId
    user=await user.save(null,{useMasterKey:true});
  };
  var acl = user.getACL();
  var dirty = false;
  acl=new Parse.ACL();
  dirty=true;
  if(!acl.getPublicReadAccess()) {
    acl.setPublicReadAccess(dirty=true);
  };
  if(!acl.getWriteAccess(user.id)) {
    acl.setWriteAccess(user.id,(dirty=true));
  };
  if(acl.getPublicWriteAccess()) {
    acl.setPublicWriteAccess(!(dirty=true));
  }
  if(dirty) { 
    user.setACL(acl);
    user=await user.save(null,{useMasterKey: true})
  };
};
async function main(argv) {
  const user = new Parse.User();
  console.log(user);
  console.dump(user);
  const regex=/test.*@copblock.app$/;
  var count = await new Parse.Query("_User").matches("username",regex).count();
  await populateUser(user,"test"+count+"@copblock.app",process.env.PARSE_PASSWORD || "aa");
  await user.logIn(user.get("username"),"aa");
  const params={ type: "FriendRequest" };
  params.objectId = "dev1";
  const skey = { sessionToken: user.get("sessionToken") };
  console.dump([ {user}, "sendRequest", {params}, {skey} ]);
  const res = await Parse.Cloud.run("sendRequest",params,skey);
  console.dump(res);
}
main(process.argv);
