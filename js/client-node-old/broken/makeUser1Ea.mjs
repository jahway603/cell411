#!node

const parse_login=await import("./parse-login.mjs");

async function populateUser(user,username,password){
  user.set("username",username);
  user.set("email",username);
  user.set("password",password);
  user.set("firstName","rich");
  var parts = user.get("username").split("@");
  if(parts.length==2){
    user.set("lastName","paul ("+parts[1]+")");
  } else if ( parts.length == 1 ) {
    user.set("lastName","paul");
  } else {
    throw new Error("Unexpected parts: "+parts);
  };
  user.set('newPublicCellAlert', true);
  user.set('patrolMode', true);
  if(user.id==null) {
    // gotta get an objectId
    user=await user.save(null,{useMasterKey:true});
  };
  var acl = user.getACL();
  var dirty = false;
  acl=new Parse.ACL();
  dirty=true;
  if(!acl.getPublicReadAccess()) {
    acl.setPublicReadAccess(dirty=true);
  };
  if(!acl.getWriteAccess(user.id)) {
    acl.setWriteAccess(user.id,(dirty=true));
  };
  if(acl.getPublicWriteAccess()) {
    acl.setPublicWriteAccess(!(dirty=true));
  }
  if(dirty) { 
    user.setACL(acl);
    user=await user.save(null,{useMasterKey: true})
  };
};
async function main(argv) {
  console.dump({argv});
  parseLogin();
  const user = new Parse.User();
  if(argv.length<4){
    log("usage: node "+argv[1]+" <username> <password>");
    process.exit(1);
  }
  //populateUser(user,"test@copblock.app",process.env.PARSE_PASSWORD);
}
const args = JSON.parse(JSON.stringify(process.argv));
main(args);
