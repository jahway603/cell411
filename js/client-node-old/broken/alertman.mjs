#!/usr/bin/env node

await import('parse-global');
await import('./parse-login.mjs');
await initializeParse();
const user = await parseLogin();
const username=user.get("username");
const sessionToken=await user.getSessionToken();

async function alertQuery(func, key, val){
  const query = new Parse.Query("Alert");
  (query[func])(key,val);
  return query;
};
async function show(query){
  console.dump(await findFully(query));
};
async function allRelated() {
  var query = await alertQuery("notEqualTo","owner",user.id)
  await show(query);
//     update.containedBy("audience",[user.id]);
//     update.limit(10);
//     console.dump(await update.find());
//     update.containedIn("audience",[user.id]);
//     console.dump(await update.find());
//     update.containedIn("audience",[user]);
};
await allRelated(user);
