#!node

global.res=global.res;
const cell411 = await import("cell411");
console.log(res);
await import("parse-global/pg-connect.js");
await import("pg-promise");
const mkey={useMasterKey: true};
async function removeDevUsers() {
  const userQuery = new Parse.Query(Parse.User);
  const suffix=/@copblock.app$/;
  userQuery.matches("username",suffix);
  userQuery.ascending("username");
  const users = await findFully(userQuery,mkey);
  for(var i=0;i<users.length;i++){
    try {
      await users[i].destroy(mkey);
    } catch ( err ) {
      console.error(err);
    };
  };
};
var username;
async function populateUser(user,i){
  user.set("email",username);
  user.set("firstName","rich");
  user.set("lastName","paul (dev"+i+")");
  user.set('newPublicCellAlert', true);
  user.set("password",password);
  user.set('patrolMode', true);
  user.set("username",username);
  if(user.id==null) {
    // gotta get an objectId
    user=await user.save(null,{useMasterKey:true});
  };
  var acl = new Parse.ACL();
  var dirty = false;
  acl=new Parse.ACL();
  acl.setPublicReadAccess(true);
  acl.setWriteAccess(user.id,true);
  acl.setPublicWriteAccess(false);
  user.setACL(acl);
  user=await user.save(null,{useMasterKey: true})
  return user;
};
const types={};
async function followUp() {
  const userQuery = new Parse.Query(Parse.User);
  const suffix=/@copblock.app$/;
  userQuery.matches("username",suffix);
  userQuery.ascending("username");
  const users = await findFully(userQuery,{useMasterKey: true});
  const allUsers=users;
  console.log({users});
  for(var i=0;i<users.length;i++) {
    console.log(`user #${i}`);
    const user = users[i];
    const userId=user.id;
    console.dump({userId,i});
    const pubCells = await findFully(new Parse.Query("PublicCell").equalTo("owner",user.id).ascending("owner"));
    console.log(pubCells);
    if(pubCells.length==0){
      let cell = new PublicCell();
      cell.set("owner",user);
      console.log(user);
      console.dump(user);
      var desc=user.getName()+"'s automatic cell";
      cell.set("description",desc);
      cell.set("name",desc);
      cell=await cell.save(null,{useMasterKey:true});
      console.dump(cell);
    }
    const priCells={};
    (await new Parse.Query("PrivateCell")
      .equalTo("owner",user)
      .containedIn("type",Object.keys(types)).find()).forEach((cell)=>{
        priCells[cell.get("type")]=cell;
      });
    console.log({userId,priCells});
    //console.dump(cells);
    for(var j=1;j<=5;j++){
      let cell=priCells[j];
      if(cell)
        continue;
      console.log({id: user.id});
      cell=new PrivateCell(types[j]);
      priCells[j]=cell;
      cell.set("owner",user);
      priCells[j]=cell.save(null,{useMasterKey: true});
      console.dump(priCells);
    };
  }
  var last=allUsers[allUsers.length-1];
  for(var i=0;i<allUsers.length;i++){
    const user=allUsers[i];
    user.relation("friends").add(last);
    user.save(null,{useMasterKey: true});
    last=user;
  };
}
async function initial() {
  const suffix=/@copblock.app$/;
  const userQuery = new Parse.Query(Parse.User);
  userQuery.matches("username",suffix);
  userQuery.descending("username");
  userQuery.limit(2);
  const users = await findFully(userQuery,{useMasterKey: true});
  const xusers={};
  for(var i=0;i<users.length;i++){
    const user=users[i];
    if(user==null)
      continue;
    username=user.get("username");
    xusers[username]=user;
  };
  for(var i=1;i<12;i++) {
    var userId="dev"+i;
    username=userId+"@copblock.app";
    var user=xusers[username];
    if(user==null)
      user = new User();
    user=await populateUser(user,i);
    if(userId!=user.id){
      console.log([userId,user.id]);
      await db.none('update "_Session" set "user"=null where "user"=$1',[user.id]);
      await db.none('update "_User" set "objectId"=$1 where "objectId"=$2',[userId,user.id]);
      await db.none('update "_Session" set "user"=$1 where "user" is null',[userId]);
      await db.none('update "_User" set _wperm[1]="objectId"');
    };
  }
  return users;
}
async function main() {
  global.password=process.env.PARSE_PASSWORD;
  if(isNoE(password))
    password="asdf";
  await initializeParse();
  console.log("main: enter");
  console.log(await Parse.getServerHealth());
  (await db.many("select * from \"PrivateCellType\"")).forEach(
    type=>{types[type.type]=type;}
  );
  console.dump(types);
  await removeDevUsers();
  await initial();
  await followUp();
  await pgp.end();
  console.log("main: exit");
};
await main();
