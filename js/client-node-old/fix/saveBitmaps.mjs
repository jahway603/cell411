#!node

const cell411 = await import("cell411");
await import("parse-global/pg-connect.js");
await import("pg-promise");
const mkey={useMasterKey: true};
var username;
async function populateUser(user,i){
  user.set("email",username);
  user.set("firstName","rich");
  user.set("lastName","paul (dev"+i+")");
  user.set('newPublicCellAlert', true);
  user.set("password",password);
  user.set('patrolMode', true);
  user.set("username",username);
  if(user.id==null) {
    // gotta get an objectId
    user=await user.save(null,{useMasterKey:true});
  };
  var acl = new Parse.ACL();
  var dirty = false;
  acl=new Parse.ACL();
  acl.setPublicReadAccess(true);
  acl.setWriteAccess(user.id,true);
  acl.setPublicWriteAccess(false);
  user.setACL(acl);
  user=await user.save(null,{useMasterKey: true})
  return user;
};
const types={};
async function followUp() {
  const userQuery = new Parse.Query(Parse.User);
  const suffix=/@copblock.app$/;
  userQuery.matches("username",suffix);
  userQuery.ascending("username");
  const users = await findFully(userQuery,{useMasterKey: true});
  const allUsers=users;
  console.log({users});
  for(var i=0;i<users.length;i++) {
    console.log(`user #${i}`);
    const user = users[i];
    const userId=user.id;
    console.dump({userId,i});
    const pubCells = await findFully(new Parse.Query("PublicCell").equalTo("owner",user.id).ascending("owner"));
    console.log(pubCells);
    if(pubCells.length==0){
      let cell = new PublicCell();
      cell.set("owner",user);
      console.log(user);
      console.dump(user);
      var desc=user.getName()+"'s automatic cell";
      cell.set("description",desc);
      cell.set("name",desc);
      cell=await cell.save(null,{useMasterKey:true});
      console.dump(cell);
    }
    const priCells={};
    (await new Parse.Query("PrivateCell")
      .equalTo("owner",user)
      .containedIn("type",Object.keys(types)).find()).forEach((cell)=>{
        priCells[cell.get("type")]=cell;
      });
    console.log({userId,priCells});
    //console.dump(cells);
    for(var j=1;j<=5;j++){
      let cell=priCells[j];
      if(cell)
        continue;
      console.log({id: user.id});
      cell=new PrivateCell(types[j]);
      priCells[j]=cell;
      cell.set("owner",user);
      priCells[j]=cell.save(null,{useMasterKey: true});
      console.dump(priCells);
    };
  }
  var last=allUsers[allUsers.length-1];
  for(var i=0;i<allUsers.length;i++){
    const user=allUsers[i];
    user.relation("friends").add(last);
    user.save(null,{useMasterKey: true});
    last=user;
  };
}
async function main() {
  global.password=process.env.PARSE_PASSWORD;
  await initializeParse();
  console.log(await Parse.getServerHealth());
  const rows = await db.any(
    "select   \"objectId\",\n"+
    "         avatar,\n"+
    "         \"avatarURL\",\n"+
    "         \"thumbNailURL\",\n"+
    "         \"thumbNail\"\n"+
    "  from   \"_User\"\n"+
    " where   \"avatarURL\"  is not null\n"+
    "    or   avatar is not null\n"+
    "    or   \"thumbNail\" is not null\n"+
    "    or   \"thumbNailURL\" is not null;\n"
  );
  fs.writeFileSync("saveProfilePicUrls.json",JSON.stringify(rows,null,2));
  await pgp.end();
  console.log("main: exit");
};
await main();
