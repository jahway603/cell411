#!node

global.res=global.res;
const cell411 = await import("cell411-mjs");
import md5 from 'md5';
import { EventEmitter, errorMonitor } from 'events';

const net = await import("net");
var username;

import ppgp from 'pg-promise';
const pgp_cred=await readJson(configDir+'/pg_admin.json');
const initOptions={};
const pgp=ppgp(initOptions);
const db=pgp(pgp_cred);

async function main(file) {
  console.log("main: enter");
  const user = await parseLogin();
  console.log({user});
  const buf = fs.readFileSync(file);
  console.log({buf});
  const params={};
  params.objectId="dev1";
  params.purpose="avatar";

  let res = await Parse.Cloud.run("sendUpload",params,{sessionToken: user.getSessionToken()});
  res=res.port;
  await new Promise((a)=>{setTimeout(a,4000);});
  const client = net.createConnection(res,()=>{
    console.log('connected');
    client.write(buf);
    client.end();
  });
  client.on('data',(data)=>{
    console.dump({data: JSON.parse(data.toString())});
  });
  client.on('end',()=>{
    console.log('done');
  });
  await pgp.end();
  console.log("main: exit");
};

console.log({argv: process.argv});
const file=(process.argv.length>2 ? process.argv[2] : "avatar.png");
console.log({file});
await main(file);
