require('aws-sdk/lib/node_loader');

const AWS = require('aws-sdk/lib/core');

// Load all service classes
require('aws-sdk/clients/all');

/**
 * @api private
 */
module.exports={};
module.exports.AWS=AWS;
