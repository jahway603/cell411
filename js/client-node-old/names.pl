#!/usr/bin/perl

sub numcmp($$){
  my (@a) = split(/(\d+)/, shift);
  my (@b) = split(/(\d+)/, shift);

  while(@a && @b && $a[0] eq $b[0]){
    shift(@a);
    shift(@b); 
  }
  return 0 unless( @a || @b );
  return -1 unless @a;
  return 1 unless @b;

  my $a=shift(@a);
  my $b=shift(@b);
  my $c=(grep { /\d/ } $a, $b);

  if($c==2){
    my $aa=0+$a;
    my $bb=0+$b;
    #    printf("(%10d)\n",$_) for ( $aa, $bb );
    $c = $aa <=> $bb;
    return $c if $c;
    return length($a)<=>length($b);
  } elsif ($c) {
    return ($a =~ /\d/) ? -1 : 1;
  } else {
    return cmp($a,$b);
  }
};
sub xnumcmp($$) {
  my $res=numcmp($_[0],$_[1]);
  #  print (($res<0) ? "<\n$_[0]\n$_[1]\n\n\n"  : "<\n$_[1]\n$_[0]\n\n\n");
  return $res;
};

my(@sort);
my(@data);
@data=<DATA>;
chomp(@data);
use Data::Dumper;

while(@data){
  my $a=shift(@data);
  my $c;
  for(my $i=0;$i<=@sort;$i++){
    my $b=$sort[$i];
    if(defined($b)){
      $c=xnumcmp($a,$b);
    } else {
      $c=-1;
    };
    if($c<0){
      splice(@sort,$i,0,$a);
      last;
    }
  };
  {
    local ($");
    $"="\n";
    print "@sort\n\n";
  }
};
for(@sort){
  print "$_\n";
};
__DATA__
test0000042 user
test0000025 user
test0000037 user
test0000002 user
test00000019 user
test00000011 user
test000003 user
test0000024 user
test0000021 user
test0000015 user
test0004
test0004 user
test0004user
test0004 usr
test0004 use
test0004 user
test0004 user
test018 user
test0038 user
test028 user
test01 user
test0044 user
test29 user
test042 user
test025 user
test037 user
test002 user
test0019 user
test0011 user
test03 user
test024 user
test021 user
test015 user
test0004 user
test018 user
test0038 user
test028 user
test01 user
test0044 user
test29 user
test09 user
test3 user
test45 user
test43 user
test035 user
test10 user
test032 user
test46 user
test07 user
test0033 user
test0036 user
test002 user
test0 user
test017 user
test022 user
test008 user
test023 user
test48 user
test16 user
test013 user
test0039 user
test041 user
test49 user
test040 user
test031 user
test014 user
test47 user
test012 user
test034 user
test05 user
test027 user
test006 user
test026 user
