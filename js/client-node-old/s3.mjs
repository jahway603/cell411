#!/usr/bin/env node
console.log("make.out:1:x");
const fs = await import("fs");
import AWS from 'aws-sdk';
import fetch from 'node-fetch';

const prefix="https://cell411.s3.amazonaws.com/";
const AWScred = JSON.parse(fs.readFileSync(process.env.HOME+"/.parse/aws.cred.json"));
const S3Options = {
  params: { Bucket: "cell411" },
  accessKeyId:AWScred.aws.accessKey,
  secretAccessKey:AWScred.aws.secretKey
};
const s3 = new AWS.S3(S3Options);
const path = await import("path");
const util = await import("util");
const inspect=util.inspect;
const Console = await import("console");
function fjs(obj){
  return JSON.stringify(obj,null,2);
};
console.dump=function dump(obj){
  this.log(fjs(obj));
};
function dump(file,obj){
  return fs.writeFileSync(file,fjs(obj));
};
function launder(obj){
  return JSON.parse(JSON.stringify(obj));
}
//console.log(inspect(path));
const bucketParams = JSON.parse(JSON.stringify(S3Options.params));
async function listFiles(dirName) {
  bucketParams.Prefix=dirName;
  const res=[];
  const promise=new Promise((accept,reject)=>{
    s3.listObjects(bucketParams, (err,res) => {
      if(err){
        reject(err);
      } else {
        accept(res);
      };
    });
  });
  return await promise;
};
async function main() {
  fs.mkdirSync("cell411",{recursive: true});
  process.chdir("cell411");
  console.log("enter");
  const { IsTruncated, Contents } = await listFiles("DevStuff");
  const promises=[];
  for(var i=0;i<Contents.length;i++){
    const key = Contents[i].Key;
    if(!key.match(/DevStuff[/]$/)){
      const basename=path.basename(key);
      console.log(basename);
      if(fs.existsSync(basename))
        continue;
      promises.push(new Promise(async (accept,reject)=>{ 
        const response = await fetch(path.join(prefix,key),{});
        fs.writeFileSync(basename,await response.buffer());
        accept("ok");
      }));
    };
  };
  console.log(`${promises.length} promises`);
  Promise.all(promises);
  console.log("resolved");
  process.chdir(process.env.PWD);
  console.log("leave");
};
console.log("start");
await main();
console.log("done");
//   //     for(var i=0;i<res.length;i++){
//   //       console.dump(res[i]);
//   //     };
//   //     console.log("did it!");
//   //   async function createFile(fileName) {
//   //     const params = {
//   //       Key: fileName,
//   //       Body: fs.readFileSync(fileName),
//   //     };
//   //     params.ACL = 'public-read';
//   //     params.ContentType = "image/png";
//   //     const res=[];
//   //     s3.upload(params, (err,response) => {
//   //       if(err!=null) {
//   //         console.log({err});
//   //       } else {
//   //         res.push(JSON.stringify(response));
//   //       }
//   //     });
//   //   };
