global.Request = Parse.Object.extend("Request");
Parse.Object.registerSubclass('Request',Request);

global.Request.prototype.toString=function toString(){
  return "Request{"+this.id+"}";
};


const User = Parse.Object.extend(Parse.User, 
  {
    getName: function getName() {
      return this.get("firstName")+" "+this.get("lastName");
    },
    findFriends: function findFriends() {
      console.trace("findFriends");
    }
  }
);
global.User=User;
global.User.prototype.toString=function toString(){
  return "User{"+this.id+","+this.get("firstName")+" "+this.get("lastName")+"}";
};
global.User.prototype.getName=function getName() {
  return this.get("firstName")+" "+this.get("lastName");
};
Parse.Object.registerSubclass("User",User);


global.Response = Parse.Object.extend("Response");
Parse.Object.registerSubclass('Response',Response);

global.Alert = Parse.Object.extend("Alert");
Parse.Object.registerSubclass('Alert',Alert);

global.PublicCell = Parse.Object.extend("PublicCell");
global.PublicCell.prototype.toString=function toString(){
  return "PublicCell{"+this.id+","+this.get("name")+"}";
};
Parse.Object.registerSubclass('PublicCell',PublicCell);

global.PrivateCell = Parse.Object.extend("PrivateCell");
global.PrivateCell.prototype.toString=function toString(){
  return "PrivateCell("+this.id+","+this.get("name")+"}";
};
Parse.Object.registerSubclass('PrivateCell',PrivateCell);

global.Role = Parse.Object.extend("_Role");
Parse.Object.registerSubclass('_Role',Role);

global.ChatMsg = Parse.Object.extend("ChatMsg");
Parse.Object.registerSubclass("ChatMsg",ChatMsg);

global.ChatRoom = Parse.Object.extend("ChatRoom");
Parse.Object.registerSubclass("ChatRoom",ChatRoom);

global.PrivacyPolicy = Parse.Object.extend("PrivacyPolicy");
Parse.Object.registerSubclass("PrivacyPolicy",PrivacyPolicy);

global.Friendship = Parse.Object.extend("Friendship");
Parse.Object.registerSubclass("Friendship",Friendship);

