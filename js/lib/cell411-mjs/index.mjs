
global.Parse =(await import("parse/lib/node/Parse.js")).default;
global.isNoE=function(str) {
  return str==null || str.length==0;
};
global.user=global.user;
global.configFile=global.configFile;
global.parseFlavor=global.parseFlavor=process.env.PARSE_FLAVOR;
if(isNoE(parseFlavor)){
  parseFlavor="parse";
};
global.homeDir=process.env.HOME;
global.configDir=`${homeDir}/.parse`;
global.configFile=`${configDir}/config-${parseFlavor}.json`;

global.keys=Object.keys;
global.fs = await import("fs");
global.currentUser=global.currentUser;
global.parseUsername=global.parseUsername;

global.parseConfig=global.ParseConfig;
global.loadParseConfig=async function loadConfig(){
  if(parseConfig)
    return parseConfig;
  const parseText = fs.readFileSync(global.configFile);
  global.parseConfig =JSON.parse(parseText);
  parseConfig.parseFlavor=global.parseFlavor;
  return parseConfig;
};
var parseInitialized=false;
export async function initializeParse() {
  if(parseInitialized)
    return;
  parseConfig=await loadParseConfig();
  Parse.serverURL=parseConfig.publicServerURL; 
  console.log({serverUrl: Parse.serverURL});
  await Parse.initialize(
    parseConfig.appId,
    parseConfig.javascriptKey,
    parseConfig.masterKey
  );
  return;
};
global.initializeParse=initializeParse;
const sessionTokenFile=process.env.HOME+"/.parse/sessionTokens.json";
export async function loadSessionTokens()
{
  if(!fs.existsSync(sessionTokenFile))
    return {};
  const text = fs.readFileSync(sessionTokenFile).toString();
  return JSON.parse(text);
}
export async function getSessionToken(username){
  const sessionTokens=await loadSessionTokens();
  const section=sessionTokens[parseFlavor];
  if(!sessionTokens[parseFlavor])
    return null;
  return section[username];
}
export async function saveSessionToken(username,token){
  const sessionTokens=await loadSessionTokens();
  if(!sessionTokens[parseFlavor])
    sessionTokens[parseFlavor]={};
  const section=sessionTokens[parseFlavor];
  if(token==null){
    delete section[username];
  } else {
    section[username]=token;
  }
  const text = JSON.stringify(sessionTokens,null,2);
  fs.writeFileSync(sessionTokenFile,text+"\n\n");
}
global.readJson=function readJson(file){
  const text=fs.readFileSync(file);
  return JSON.parse(text);
};
var rli=rli;
async function getReadLine() {
  if(rli==null) {
    const process =await import('process');
    const { stdin: input, stderr: output } = process;
    const readline=await import ('node:readline/promises');
    rli=readline.createInterface({input,output,historysize: 0});
  }
  return rli;
}
global.parseLogin=async function parseLogin() {
  await initializeParse();
  try {
    var create=false;
    if(parseUsername==null)
      parseUsername=process.env.PARSE_USER;
    if(isNoE(parseUsername)){
      const rli = await getReadLine();
      while(parseUsername==null){
        parseUsername=await rli.question('login: ');
        if(parseUsername.charAt(0)=='+'){
          create=true;
          parseUsername=parseUsername.substr(1);
        }
        if(parseUsername=="quit"){
          process.exit(0);
        }
      }
    }
    if(parseUsername==null)
      throw new Error("failed to get username");
    if(!parseUsername.match('@'))
      parseUsername=parseUsername+"@copblock.app";
    var sessionToken=await getSessionToken(parseUsername);
    if(sessionToken==null){
      console.log("no session token");
      const rli=await getReadLine();
      user = new Parse.User();
      const password=await rli.question('password: ');
      user.set("username",parseUsername);
      user.set("password",password);
      try {
        if(create) {
          await user.signUp({useMasterKey: true});
        } else {
          await user.logIn({useMasterKey: true});
        }
        sessionToken=await user.getSessionToken();
      } catch ( err ) {
          console.log({err,where:"catch block x"});
      }
    } else {
      try {
        user = await Parse.User.me(sessionToken);
        //console.log("logged in with token");
      } catch ( err ) {
        console.log(err);
        if(err.code==209){
          saveSessionToken(parseUsername,null);
          user=await parseLogin();
          return user;
        } else {
          throw err;
        }
      }
    }
    if(user!=null && user.get("sessionToken")!=null)
      saveSessionToken(user.get("username"),user.get("sessionToken"));
    return user;
  } finally {
    if(rli!=null) {
      rli.close();
      rli=null;
    }
  }
  return user;
}
async function wait(delay) {
  return new Promise((resolve,reject)=>{
    setTimeout(()=>{resolve("done")},delay);
  });
}
async function done() {
  const queue=await Parse.EventuallyQueue.length();
  if(queue){
    await Parse.EventuallyQueue.poll(1);
    console.log("Eventually Queue Run");
  } else {
    await Parse.EventuallyQueue.stopPoll();
    console.log("Eventually Queue Stopped");
  }
}
global.done=done;
global.parseLogin=parseLogin;
export async function findFully(query){
  const res=[];
  const limit=1000;
  query.limit(limit);
  while(true){
    query.skip(res.length);
    const batch=await query.find();
    for(var i=0;i<batch.length;i++)
      res.push(batch[i]);
    if(batch.length<limit)
      break;
  };
  return res;
};

const cell411={};
export default cell411;
export function pp(obj) {
  return JSON.stringify(obj,null,2);
};
global.pp=pp;
