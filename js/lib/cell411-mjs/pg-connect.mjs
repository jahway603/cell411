const initOptions = {
};
async function setup() {
  global.pgpx=(await import('pg-promise')).default;
  global.pgp=pgpx(initOptions);
//	(initOptions);
  global.pgm=(await import('pg-monitor')).default;
  global.pgm.attach(initOptions);
  global.pgs=(await import('pg-connection-string')).default;
  await loadParseConfig();
  global.cn = pgs.parse(parseConfig.databaseURI);
  global.db = pgp(cn); // database instance;
};
await setup();
