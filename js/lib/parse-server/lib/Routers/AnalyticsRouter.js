"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AnalyticsRouter = void 0;

var _PromiseRouter = _interopRequireDefault(require("../PromiseRouter"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// AnalyticsRouter.js
function appOpened(req) {
  const analyticsController = req.config.analyticsController;
  return analyticsController.appOpened(req);
}

function trackEvent(req) {
  const analyticsController = req.config.analyticsController;
  return analyticsController.trackEvent(req);
}

class AnalyticsRouter extends _PromiseRouter.default {
  mountRoutes() {
    this.route('POST', '/events/AppOpened', appOpened);
    this.route('POST', '/events/:eventName', trackEvent);
  }

}

exports.AnalyticsRouter = AnalyticsRouter;