"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.HooksRouter = void 0;

var _node = require("parse/node");

var _PromiseRouter = _interopRequireDefault(require("../PromiseRouter"));

var middleware = _interopRequireWildcard(require("../middlewares"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class HooksRouter extends _PromiseRouter.default {
  createHook(aHook, config) {
    return config.hooksController.createHook(aHook).then(hook => ({
      response: hook
    }));
  }

  updateHook(aHook, config) {
    return config.hooksController.updateHook(aHook).then(hook => ({
      response: hook
    }));
  }

  handlePost(req) {
    return this.createHook(req.body, req.config);
  }

  handleGetFunctions(req) {
    var hooksController = req.config.hooksController;

    if (req.params.functionName) {
      return hooksController.getFunction(req.params.functionName).then(foundFunction => {
        if (!foundFunction) {
          throw new _node.Parse.Error(143, `no function named: ${req.params.functionName} is defined`);
        }

        return Promise.resolve({
          response: foundFunction
        });
      });
    }

    return hooksController.getFunctions().then(functions => {
      return {
        response: functions || []
      };
    }, err => {
      throw err;
    });
  }

  handleGetTriggers(req) {
    var hooksController = req.config.hooksController;

    if (req.params.className && req.params.triggerName) {
      return hooksController.getTrigger(req.params.className, req.params.triggerName).then(foundTrigger => {
        if (!foundTrigger) {
          throw new _node.Parse.Error(143, `class ${req.params.className} does not exist`);
        }

        return Promise.resolve({
          response: foundTrigger
        });
      });
    }

    return hooksController.getTriggers().then(triggers => ({
      response: triggers || []
    }));
  }

  handleDelete(req) {
    var hooksController = req.config.hooksController;

    if (req.params.functionName) {
      return hooksController.deleteFunction(req.params.functionName).then(() => ({
        response: {}
      }));
    } else if (req.params.className && req.params.triggerName) {
      return hooksController.deleteTrigger(req.params.className, req.params.triggerName).then(() => ({
        response: {}
      }));
    }

    return Promise.resolve({
      response: {}
    });
  }

  handleUpdate(req) {
    var hook;

    if (req.params.functionName && req.body.url) {
      hook = {};
      hook.functionName = req.params.functionName;
      hook.url = req.body.url;
    } else if (req.params.className && req.params.triggerName && req.body.url) {
      hook = {};
      hook.className = req.params.className;
      hook.triggerName = req.params.triggerName;
      hook.url = req.body.url;
    } else {
      throw new _node.Parse.Error(143, 'invalid hook declaration');
    }

    return this.updateHook(hook, req.config);
  }

  handlePut(req) {
    var body = req.body;

    if (body.__op == 'Delete') {
      return this.handleDelete(req);
    } else {
      return this.handleUpdate(req);
    }
  }

  mountRoutes() {
    this.route('GET', '/hooks/functions', middleware.promiseEnforceMasterKeyAccess, this.handleGetFunctions.bind(this));
    this.route('GET', '/hooks/triggers', middleware.promiseEnforceMasterKeyAccess, this.handleGetTriggers.bind(this));
    this.route('GET', '/hooks/functions/:functionName', middleware.promiseEnforceMasterKeyAccess, this.handleGetFunctions.bind(this));
    this.route('GET', '/hooks/triggers/:className/:triggerName', middleware.promiseEnforceMasterKeyAccess, this.handleGetTriggers.bind(this));
    this.route('POST', '/hooks/functions', middleware.promiseEnforceMasterKeyAccess, this.handlePost.bind(this));
    this.route('POST', '/hooks/triggers', middleware.promiseEnforceMasterKeyAccess, this.handlePost.bind(this));
    this.route('PUT', '/hooks/functions/:functionName', middleware.promiseEnforceMasterKeyAccess, this.handlePut.bind(this));
    this.route('PUT', '/hooks/triggers/:className/:triggerName', middleware.promiseEnforceMasterKeyAccess, this.handlePut.bind(this));
  }

}

exports.HooksRouter = HooksRouter;
var _default = HooksRouter;
exports.default = _default;