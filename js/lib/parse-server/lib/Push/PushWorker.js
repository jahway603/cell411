"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.PushWorker = void 0;

var _deepcopy = _interopRequireDefault(require("deepcopy"));

var _AdaptableController = _interopRequireDefault(require("../Controllers/AdaptableController"));

var _Auth = require("../Auth");

var _Config = _interopRequireDefault(require("../Config"));

var _PushAdapter = require("../Adapters/Push/PushAdapter");

var _rest = _interopRequireDefault(require("../rest"));

var _StatusHandler = require("../StatusHandler");

var utils = _interopRequireWildcard(require("./utils"));

var _ParseMessageQueue = require("../ParseMessageQueue");

var _PushQueue = require("./PushQueue");

var _logger = _interopRequireDefault(require("../logger"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// -disable-next
function groupByBadge(installations) {
  return installations.reduce((map, installation) => {
    const badge = installation.badge + '';
    map[badge] = map[badge] || [];
    map[badge].push(installation);
    return map;
  }, {});
}

class PushWorker {
  constructor(pushAdapter, subscriberConfig = {}) {
    _AdaptableController.default.validateAdapter(pushAdapter, this, _PushAdapter.PushAdapter);

    this.adapter = pushAdapter;
    this.channel = subscriberConfig.channel || _PushQueue.PushQueue.defaultPushChannel();
    this.subscriber = _ParseMessageQueue.ParseMessageQueue.createSubscriber(subscriberConfig);

    if (this.subscriber) {
      const subscriber = this.subscriber;
      subscriber.subscribe(this.channel);
      subscriber.on('message', (channel, messageStr) => {
        const workItem = JSON.parse(messageStr);
        this.run(workItem);
      });
    }
  }

  run({
    body,
    query,
    pushStatus,
    applicationId,
    UTCOffset
  }) {
    const config = _Config.default.get(applicationId);

    const auth = (0, _Auth.master)(config);
    const where = utils.applyDeviceTokenExists(query.where);
    delete query.where;
    pushStatus = (0, _StatusHandler.pushStatusHandler)(config, pushStatus.objectId);
    return _rest.default.find(config, auth, '_Installation', where, query).then(({
      results
    }) => {
      if (results.length == 0) {
        return;
      }

      return this.sendToAdapter(body, results, pushStatus, config, UTCOffset);
    });
  }

  sendToAdapter(body, installations, pushStatus, config, UTCOffset) {
    // Check if we have locales in the push body
    const locales = utils.getLocalesFromPush(body);

    if (locales.length > 0) {
      // Get all tranformed bodies for each locale
      const bodiesPerLocales = utils.bodiesPerLocales(body, locales); // Group installations on the specified locales (en, fr, default etc...)

      const grouppedInstallations = utils.groupByLocaleIdentifier(installations, locales);
      const promises = Object.keys(grouppedInstallations).map(locale => {
        const installations = grouppedInstallations[locale];
        const body = bodiesPerLocales[locale];
        return this.sendToAdapter(body, installations, pushStatus, config, UTCOffset);
      });
      return Promise.all(promises);
    }

    if (!utils.isPushIncrementing(body)) {
      _logger.default.verbose(`Sending push to ${installations.length}`);

      return this.adapter.send(body, installations, pushStatus.objectId).then(results => {
        return pushStatus.trackSent(results, UTCOffset).then(() => results);
      });
    } // Collect the badges to reduce the # of calls


    const badgeInstallationsMap = groupByBadge(installations); // Map the on the badges count and return the send result

    const promises = Object.keys(badgeInstallationsMap).map(badge => {
      const payload = (0, _deepcopy.default)(body);
      payload.data.badge = parseInt(badge);
      const installations = badgeInstallationsMap[badge];
      return this.sendToAdapter(payload, installations, pushStatus, config, UTCOffset);
    });
    return Promise.all(promises);
  }

}

exports.PushWorker = PushWorker;
var _default = PushWorker;
exports.default = _default;