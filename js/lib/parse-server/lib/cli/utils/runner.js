"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

var _commander = _interopRequireDefault(require("./commander"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function logStartupOptions(options) {
  for (const key in options) {
    let value = options[key];

    if (key == 'masterKey') {
      value = '***REDACTED***';
    }

    if (key == 'push' && options.verbose != true) {
      value = '***REDACTED***';
    }

    if (typeof value === 'object') {
      try {
        value = JSON.stringify(value);
      } catch (e) {
        if (value && value.constructor && value.constructor.name) {
          value = value.constructor.name;
        }
      }
    }
    /* eslint-disable no-console */


    console.log(`${key}: ${value}`);
    /* eslint-enable no-console */
  }
}

function _default({
  definitions,
  help,
  usage,
  start
}) {
  _commander.default.loadDefinitions(definitions);

  if (usage) {
    _commander.default.usage(usage);
  }

  if (help) {
    _commander.default.on('--help', help);
  }

  _commander.default.parse(process.argv, process.env);

  const options = _commander.default.getOptions();

  start(_commander.default, options, function () {
    logStartupOptions(options);
  });
}