"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CLP = void 0;
exports.makeSchema = makeSchema;

// @Typescript 4.1+ // type CLPPermission = 'requiresAuthentication' | '*' |  `user:${string}` | `role:${string}`
class CLP {
  static allow(perms) {
    const out = {};

    for (const [perm, ops] of Object.entries(perms)) {
      // -disable-next Property `@@iterator` is missing in mixed [1] but exists in `$Iterable` [2].
      for (const op of ops) {
        out[op] = out[op] || {};
        out[op][perm] = true;
      }
    }

    return out;
  }

}

exports.CLP = CLP;

function makeSchema(className, schema) {
  // This function solve two things:
  // 1. It provides auto-completion to the users who are implementing schemas
  // 2. It allows forward-compatible point in order to allow future changes to the internal structure of JSONSchema without affecting all the users
  schema.className = className;
  return schema;
}