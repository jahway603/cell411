"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.DefaultMongoURI = void 0;

var _parsers = require("./Options/parsers");

const {
  ParseServerOptions
} = require('./Options/Definitions');

const logsFolder = (() => {
  let folder = './logs/';

  if (typeof process !== 'undefined' && process.env.TESTING === '1') {
    folder = './test_logs/';
  }

  if (process.env.PARSE_SERVER_LOGS_FOLDER) {
    folder = (0, _parsers.nullParser)(process.env.PARSE_SERVER_LOGS_FOLDER);
  }

  return folder;
})();

const {
  verbose,
  level
} = (() => {
  const verbose = process.env.VERBOSE ? true : false;
  return {
    verbose,
    level: verbose ? 'verbose' : undefined
  };
})();

const DefinitionDefaults = Object.keys(ParseServerOptions).reduce((memo, key) => {
  const def = ParseServerOptions[key];

  if (Object.prototype.hasOwnProperty.call(def, 'default')) {
    memo[key] = def.default;
  }

  return memo;
}, {});
const computedDefaults = {
  jsonLogs: process.env.JSON_LOGS || false,
  logsFolder,
  verbose,
  level
};

var _default = Object.assign({}, DefinitionDefaults, computedDefaults);

exports.default = _default;
const DefaultMongoURI = DefinitionDefaults.databaseURI;
exports.DefaultMongoURI = DefaultMongoURI;