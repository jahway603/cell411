"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.LiveQueryController = void 0;

var _ParseCloudCodePublisher = require("../LiveQuery/ParseCloudCodePublisher");

var _Options = require("../Options");

var _triggers = require("./../triggers");

class LiveQueryController {
  constructor(config) {
    // If config is empty, we just assume no classs needs to be registered as LiveQuery
    if (!config || !config.classNames) {
      this.classNames = new Set();
      console.warn("No classes registered for live query");
    } else if (config.classNames instanceof Array) {
      const classNames = config.classNames.map(name => {
        const _name = (0, _triggers.getClassName)(name);

        return new RegExp(`^${_name}$`);
      });
      this.classNames = new Set(classNames);
    } else {
      throw 'liveQuery.classes should be an array of string';
    }

    this.liveQueryPublisher = new _ParseCloudCodePublisher.ParseCloudCodePublisher(config);
  }

  onAfterSave(className, currentObject, originalObject, classLevelPermissions) {
    if (!this.hasLiveQuery(className)) {
      return;
    }

    const req = this._makePublisherRequest(currentObject, originalObject, classLevelPermissions);

    this.liveQueryPublisher.onCloudCodeAfterSave(req);
  }

  onAfterDelete(className, currentObject, originalObject, classLevelPermissions) {
    if (!this.hasLiveQuery(className)) {
      return;
    }

    const req = this._makePublisherRequest(currentObject, originalObject, classLevelPermissions);

    this.liveQueryPublisher.onCloudCodeAfterDelete(req);
  }

  hasLiveQuery(className) {
    for (const name of this.classNames) {
      if (name.test(className)) {
        return true;
      }
    }

    return false;
  }

  _makePublisherRequest(currentObject, originalObject, classLevelPermissions) {
    const req = {
      object: currentObject
    };

    if (currentObject) {
      req.original = originalObject;
    }

    if (classLevelPermissions) {
      req.classLevelPermissions = classLevelPermissions;
    }

    return req;
  }

}

exports.LiveQueryController = LiveQueryController;
var _default = LiveQueryController;
exports.default = _default;