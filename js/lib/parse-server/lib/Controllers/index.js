"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAnalyticsController = getAnalyticsController;
exports.getAuthDataManager = getAuthDataManager;
exports.getCacheController = getCacheController;
exports.getControllers = getControllers;
exports.getDatabaseAdapter = getDatabaseAdapter;
exports.getDatabaseController = getDatabaseController;
exports.getFilesController = getFilesController;
exports.getHooksController = getHooksController;
exports.getLiveQueryController = getLiveQueryController;
exports.getLoggerController = getLoggerController;
exports.getUserController = getUserController;

var _Auth = _interopRequireDefault(require("../Adapters/Auth"));

var _Options = require("../Options");

var _AdapterLoader = require("../Adapters/AdapterLoader");

var _defaults = _interopRequireDefault(require("../defaults"));

var _LoggerController = require("./LoggerController");

var _FilesController = require("./FilesController");

var _HooksController = require("./HooksController");

var _UserController = require("./UserController");

var _CacheController = require("./CacheController");

var _LiveQueryController = require("./LiveQueryController");

var _AnalyticsController = require("./AnalyticsController");

var _DatabaseController = _interopRequireDefault(require("./DatabaseController"));

var _GridFSBucketAdapter = require("../Adapters/Files/GridFSBucketAdapter");

var _WinstonLoggerAdapter = require("../Adapters/Logger/WinstonLoggerAdapter");

var _InMemoryCacheAdapter = require("../Adapters/Cache/InMemoryCacheAdapter");

var _AnalyticsAdapter = require("../Adapters/Analytics/AnalyticsAdapter");

var _MongoStorageAdapter = _interopRequireDefault(require("../Adapters/Storage/Mongo/MongoStorageAdapter"));

var _PostgresStorageAdapter = _interopRequireDefault(require("../Adapters/Storage/Postgres/PostgresStorageAdapter"));

var _SchemaCache = _interopRequireDefault(require("../Adapters/Cache/SchemaCache"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Controllers
//   import { PushController } from './PushController';
//   import { PushQueue } from '../Push/PushQueue';
//   import { PushWorker } from '../Push/PushWorker';
// Adapters
//   import ParsePushAdapter from '@parse/push-adapter';
//import ParseGraphQLController from './ParseGraphQLController';
function getControllers(options) {
  const loggerController = getLoggerController(options);
  const filesController = getFilesController(options);
  const userController = getUserController(options); //     const {
  //       pushController,
  //       hasPushScheduledSupport,
  //       hasPushSupport,
  //       pushControllerQueue,
  //       pushWorker,
  //     } = getPushController(options);

  const cacheController = getCacheController(options);
  const analyticsController = getAnalyticsController(options);
  const liveQueryController = getLiveQueryController(options);
  const databaseController = getDatabaseController(options);
  const hooksController = getHooksController(options, databaseController);
  const authDataManager = getAuthDataManager(options); //  const parseGraphQLController = getParseGraphQLController(options, {
  //    databaseController,
  //    cacheController,
  //  });

  return {
    loggerController,
    filesController,
    userController,
    //       pushController,
    //       hasPushScheduledSupport,
    //       hasPushSupport,
    //       pushWorker,
    //       pushControllerQueue,
    analyticsController,
    cacheController,
    //    parseGraphQLController,
    liveQueryController,
    databaseController,
    hooksController,
    authDataManager,
    schemaCache: _SchemaCache.default
  };
}

function getLoggerController(options) {
  const {
    appId,
    jsonLogs,
    logsFolder,
    verbose,
    logLevel,
    maxLogFiles,
    silent,
    loggerAdapter
  } = options;
  const loggerOptions = {
    jsonLogs,
    logsFolder,
    verbose,
    logLevel,
    silent,
    maxLogFiles
  };
  const loggerControllerAdapter = (0, _AdapterLoader.loadAdapter)(loggerAdapter, _WinstonLoggerAdapter.WinstonLoggerAdapter, loggerOptions);
  return new _LoggerController.LoggerController(loggerControllerAdapter, appId, loggerOptions);
}

function getFilesController(options) {
  const {
    appId,
    databaseURI,
    filesAdapter,
    databaseAdapter,
    preserveFileName,
    fileKey
  } = options;

  if (!filesAdapter && databaseAdapter) {
    throw 'When using an explicit database adapter, you must also use an explicit filesAdapter.';
  }

  const filesControllerAdapter = (0, _AdapterLoader.loadAdapter)(filesAdapter, () => {
    return new _GridFSBucketAdapter.GridFSBucketAdapter(databaseURI, {}, fileKey);
  });
  return new _FilesController.FilesController(filesControllerAdapter, appId, {
    preserveFileName
  });
}

function getUserController(options) {
  const {
    appId,
    emailAdapter,
    verifyUserEmails
  } = options;
  const emailControllerAdapter = (0, _AdapterLoader.loadAdapter)(emailAdapter);
  return new _UserController.UserController(emailControllerAdapter, appId, {
    verifyUserEmails
  });
}

function getCacheController(options) {
  const {
    appId,
    cacheAdapter,
    cacheTTL,
    cacheMaxSize
  } = options;
  const cacheControllerAdapter = (0, _AdapterLoader.loadAdapter)(cacheAdapter, _InMemoryCacheAdapter.InMemoryCacheAdapter, {
    appId: appId,
    ttl: cacheTTL,
    maxSize: cacheMaxSize
  });
  return new _CacheController.CacheController(cacheControllerAdapter, appId);
} //   export function getParseGraphQLController(
//     options: ParseServerOptions,
//     controllerDeps
//   ): ParseGraphQLController {
//     return new ParseGraphQLController({
//       mountGraphQL: options.mountGraphQL,
//       ...controllerDeps,
//     });
//   }
//   


function getAnalyticsController(options) {
  const {
    analyticsAdapter
  } = options;
  const analyticsControllerAdapter = (0, _AdapterLoader.loadAdapter)(analyticsAdapter, _AnalyticsAdapter.AnalyticsAdapter);
  return new _AnalyticsController.AnalyticsController(analyticsControllerAdapter);
}

function getLiveQueryController(options) {
  return new _LiveQueryController.LiveQueryController(options.liveQuery);
}

function getDatabaseController(options) {
  const {
    databaseURI,
    collectionPrefix,
    databaseOptions,
    idempotencyOptions
  } = options;
  let {
    databaseAdapter
  } = options;

  if ((databaseOptions || databaseURI && databaseURI !== _defaults.default.databaseURI || collectionPrefix !== _defaults.default.collectionPrefix) && databaseAdapter) {
    throw 'You cannot specify both a databaseAdapter and a databaseURI/databaseOptions/collectionPrefix.';
  } else if (!databaseAdapter) {
    databaseAdapter = getDatabaseAdapter(databaseURI, collectionPrefix, databaseOptions);
  } else {
    databaseAdapter = (0, _AdapterLoader.loadAdapter)(databaseAdapter);
  }

  return new _DatabaseController.default(databaseAdapter, idempotencyOptions);
}

function getHooksController(options, databaseController) {
  const {
    appId,
    webhookKey
  } = options;
  return new _HooksController.HooksController(appId, databaseController, webhookKey);
} //   interface PushControlling {
//     pushController: PushController;
//     hasPushScheduledSupport: boolean;
//     pushControllerQueue: PushQueue;
//     pushWorker: PushWorker;
//   }
//   export function getPushController(options: ParseServerOptions): PushControlling {
//     const { scheduledPush, push } = options;
//   
//     const pushOptions = Object.assign({}, push);
//     const pushQueueOptions = pushOptions.queueOptions || {};
//     if (pushOptions.queueOptions) {
//       delete pushOptions.queueOptions;
//     }
//   
// Pass the push options too as it works with the default
//     const pushAdapter = loadAdapter(
//       pushOptions && pushOptions.adapter,
//       ParsePushAdapter,
//       pushOptions
//     );
//     // We pass the options and the base class for the adatper,
//     // Note that passing an instance would work too
//     const pushController = new PushController();
//     const hasPushSupport = !!(pushAdapter && push);
//     const hasPushScheduledSupport = hasPushSupport && scheduledPush === true;
//   
//     const { disablePushWorker } = pushQueueOptions;
//   
//     const pushControllerQueue = new PushQueue(pushQueueOptions);
//     let pushWorker;
//     if (!disablePushWorker) {
//       pushWorker = new PushWorker(pushAdapter, pushQueueOptions);
//     }
//     return {
//       pushController,
//       hasPushSupport,
//       hasPushScheduledSupport,
//       pushControllerQueue,
//       pushWorker,
//     };
//   }


function getAuthDataManager(options) {
  const {
    auth,
    enableAnonymousUsers
  } = options;
  return (0, _Auth.default)(auth, enableAnonymousUsers);
}

function getDatabaseAdapter(databaseURI, collectionPrefix, databaseOptions) {
  let protocol;

  try {
    const parsedURI = new URL(databaseURI);
    protocol = parsedURI.protocol ? parsedURI.protocol.toLowerCase() : null;
  } catch (e) {
    /* */
  }

  switch (protocol) {
    case 'postgres:':
    case 'postgresql:':
      return new _PostgresStorageAdapter.default({
        uri: databaseURI,
        collectionPrefix,
        databaseOptions
      });

    default:
      return new _MongoStorageAdapter.default({
        uri: databaseURI,
        collectionPrefix,
        mongoOptions: databaseOptions
      });
  }
}