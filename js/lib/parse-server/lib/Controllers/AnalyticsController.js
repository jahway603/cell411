"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.AnalyticsController = void 0;

var _AdaptableController = _interopRequireDefault(require("./AdaptableController"));

var _AnalyticsAdapter = require("../Adapters/Analytics/AnalyticsAdapter");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class AnalyticsController extends _AdaptableController.default {
  appOpened(req) {
    return Promise.resolve().then(() => {
      return this.adapter.appOpened(req.body, req);
    }).then(response => {
      return {
        response: response || {}
      };
    }).catch(() => {
      return {
        response: {}
      };
    });
  }

  trackEvent(req) {
    return Promise.resolve().then(() => {
      return this.adapter.trackEvent(req.params.eventName, req.body, req);
    }).then(response => {
      return {
        response: response || {}
      };
    }).catch(() => {
      return {
        response: {}
      };
    });
  }

  expectedAdapterType() {
    return _AnalyticsAdapter.AnalyticsAdapter;
  }

}

exports.AnalyticsController = AnalyticsController;
var _default = AnalyticsController;
exports.default = _default;