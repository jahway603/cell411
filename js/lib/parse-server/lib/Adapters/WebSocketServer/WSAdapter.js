"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.WSAdapter = void 0;

var _WSSAdapter = require("./WSSAdapter");

/*eslint no-unused-vars: "off"*/
const WebSocketServer = require('ws').Server;
/**
 * Wrapper for ws node module
 */


class WSAdapter extends _WSSAdapter.WSSAdapter {
  constructor(options) {
    super(options);
    this.options = options;
  }

  onListen() {}

  onConnection(ws) {}

  onError(error) {}

  start() {
    const wss = new WebSocketServer({
      server: this.options.server
    });
    wss.on('listening', this.onListen);
    wss.on('connection', this.onConnection);
    wss.on('error', this.onError);
  }

  close() {}

}

exports.WSAdapter = WSAdapter;
var _default = WSAdapter;
exports.default = _default;