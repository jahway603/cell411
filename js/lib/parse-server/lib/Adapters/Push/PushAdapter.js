"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.PushAdapter = void 0;

/*eslint no-unused-vars: "off"*/
// Push Adapter
//
// Allows you to change the push notification mechanism.
//
// Adapter classes must implement the following functions:
// * getValidPushTypes()
// * send(devices, installations, pushStatus)
//
// Default is ParsePushAdapter, which uses GCM for
// android push and APNS for ios push.

/**
 * @module Adapters
 */

/**
 * @interface PushAdapter
 */
class PushAdapter {
  /**
   * @param {any} body
   * @param {Parse.Installation[]} installations
   * @param {any} pushStatus
   * @returns {Promise}
   */
  send(body, installations, pushStatus) {}
  /**
   * Get an array of valid push types.
   * @returns {Array} An array of valid push types
   */


  getValidPushTypes() {
    return [];
  }

}

exports.PushAdapter = PushAdapter;
var _default = PushAdapter;
exports.default = _default;