"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.NullCacheAdapter = void 0;

class NullCacheAdapter {
  constructor() {}

  get() {
    return new Promise(resolve => {
      return resolve(null);
    });
  }

  put() {
    return Promise.resolve();
  }

  del() {
    return Promise.resolve();
  }

  clear() {
    return Promise.resolve();
  }

}

exports.NullCacheAdapter = NullCacheAdapter;
var _default = NullCacheAdapter;
exports.default = _default;