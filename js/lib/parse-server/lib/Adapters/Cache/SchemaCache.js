"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const SchemaCache = {};
var _default = {
  all() {
    return [...(SchemaCache.allClasses || [])];
  },

  get(className) {
    return this.all().find(cached => cached.className === className);
  },

  put(allSchema) {
    SchemaCache.allClasses = allSchema;
  },

  del(className) {
    this.put(this.all().filter(cached => cached.className !== className));
  },

  clear() {
    delete SchemaCache.allClasses;
  }

};
exports.default = _default;