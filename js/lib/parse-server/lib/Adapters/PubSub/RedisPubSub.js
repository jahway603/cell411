"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RedisPubSub = void 0;

var _redis = _interopRequireDefault(require("redis"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createPublisher({
  redisURL,
  redisOptions = {}
}) {
  redisOptions.no_ready_check = true;
  return _redis.default.createClient(redisURL, redisOptions);
}

function createSubscriber({
  redisURL,
  redisOptions = {}
}) {
  redisOptions.no_ready_check = true;
  return _redis.default.createClient(redisURL, redisOptions);
}

const RedisPubSub = {
  createPublisher,
  createSubscriber
};
exports.RedisPubSub = RedisPubSub;