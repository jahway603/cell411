"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.PubSubAdapter = void 0;

/*eslint no-unused-vars: "off"*/

/**
 * @module Adapters
 */

/**
 * @interface PubSubAdapter
 */
class PubSubAdapter {
  /**
   * @returns {PubSubAdapter.Publisher}
   */
  static createPublisher() {}
  /**
   * @returns {PubSubAdapter.Subscriber}
   */


  static createSubscriber() {}

}
/**
 * @interface Publisher
 * @memberof PubSubAdapter
 */


exports.PubSubAdapter = PubSubAdapter;
var _default = PubSubAdapter;
exports.default = _default;