"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SessionTokenCache = void 0;

var _node = _interopRequireDefault(require("parse/node"));

var _lruCache = _interopRequireDefault(require("lru-cache"));

var _logger = _interopRequireDefault(require("../logger"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function userForSessionToken(sessionToken) {
  var q = new _node.default.Query('_Session');
  q.equalTo('sessionToken', sessionToken);
  return q.first({
    useMasterKey: true
  }).then(function (session) {
    if (!session) {
      return Promise.reject('No session found for session token');
    }

    return session.get('user');
  });
}

class SessionTokenCache {
  constructor(timeout = 30 * 24 * 60 * 60 * 1000, maxSize = 10000) {
    this.cache = new _lruCache.default({
      max: maxSize,
      maxAge: timeout
    });
  }

  getUserId(sessionToken) {
    if (!sessionToken) {
      return Promise.reject('Empty sessionToken');
    }

    const userId = this.cache.get(sessionToken);

    if (userId) {
      _logger.default.verbose('Fetch userId %s of sessionToken %s from Cache', userId, sessionToken);

      return Promise.resolve(userId);
    }

    return userForSessionToken(sessionToken).then(user => {
      _logger.default.verbose('Fetch userId %s of sessionToken %s from Parse', user.id, sessionToken);

      const userId = user.id;
      this.cache.set(sessionToken, userId);
      return Promise.resolve(userId);
    }, error => {
      _logger.default.error('Can not fetch userId for sessionToken %j, error %j', sessionToken, error);

      return Promise.reject(error);
    });
  }

}

exports.SessionTokenCache = SessionTokenCache;