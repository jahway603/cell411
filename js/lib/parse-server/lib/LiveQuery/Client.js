"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Client = void 0;

var _logger = _interopRequireDefault(require("../logger"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const dafaultFields = ['className', 'objectId', 'updatedAt', 'createdAt', 'ACL'];

class Client {
  constructor(id, parseWebSocket, hasMasterKey = false, sessionToken, installationId) {
    this.id = id;
    this.parseWebSocket = parseWebSocket;
    this.hasMasterKey = hasMasterKey;
    this.sessionToken = sessionToken;
    this.installationId = installationId;
    this.roles = [];
    this.subscriptionInfos = new Map();
    this.pushConnect = this._pushEvent('connected');
    this.pushSubscribe = this._pushEvent('subscribed');
    this.pushUnsubscribe = this._pushEvent('unsubscribed');
    this.pushCreate = this._pushEvent('create');
    this.pushEnter = this._pushEvent('enter');
    this.pushUpdate = this._pushEvent('update');
    this.pushDelete = this._pushEvent('delete');
    this.pushLeave = this._pushEvent('leave');
  }

  static pushResponse(parseWebSocket, message) {
    _logger.default.verbose('Push Response : %j', message);

    parseWebSocket.send(message);
  }

  static pushError(parseWebSocket, code, error, reconnect = true, requestId = null) {
    Client.pushResponse(parseWebSocket, JSON.stringify({
      op: 'error',
      error,
      code,
      reconnect,
      requestId
    }));
  }

  addSubscriptionInfo(requestId, subscriptionInfo) {
    this.subscriptionInfos.set(requestId, subscriptionInfo);
  }

  getSubscriptionInfo(requestId) {
    return this.subscriptionInfos.get(requestId);
  }

  deleteSubscriptionInfo(requestId) {
    return this.subscriptionInfos.delete(requestId);
  }

  _pushEvent(type) {
    return function (subscriptionId, parseObjectJSON, parseOriginalObjectJSON) {
      const response = {
        op: type,
        clientId: this.id,
        installationId: this.installationId
      };

      if (typeof subscriptionId !== 'undefined') {
        response['requestId'] = subscriptionId;
      }

      if (typeof parseObjectJSON !== 'undefined') {
        let fields;

        if (this.subscriptionInfos.has(subscriptionId)) {
          fields = this.subscriptionInfos.get(subscriptionId).fields;
        }

        response['object'] = this._toJSONWithFields(parseObjectJSON, fields);

        if (parseOriginalObjectJSON) {
          response['original'] = this._toJSONWithFields(parseOriginalObjectJSON, fields);
        }
      }

      Client.pushResponse(this.parseWebSocket, JSON.stringify(response));
    };
  }

  _toJSONWithFields(parseObjectJSON, fields) {
    if (!fields) {
      return parseObjectJSON;
    }

    const limitedParseObject = {};

    for (const field of dafaultFields) {
      limitedParseObject[field] = parseObjectJSON[field];
    }

    for (const field of fields) {
      if (field in parseObjectJSON) {
        limitedParseObject[field] = parseObjectJSON[field];
      }
    }

    return limitedParseObject;
  }

}

exports.Client = Client;