'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getLogger = getLogger;
exports.setLogger = setLogger;

var _defaults = _interopRequireDefault(require("./defaults"));

var _WinstonLoggerAdapter = require("./Adapters/Logger/WinstonLoggerAdapter");

var _LoggerController = require("./Controllers/LoggerController");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Used for Separate Live Query Server
function defaultLogger() {
  const options = {
    logsFolder: _defaults.default.logsFolder,
    jsonLogs: _defaults.default.jsonLogs,
    verbose: _defaults.default.verbose,
    silent: _defaults.default.silent
  };
  const adapter = new _WinstonLoggerAdapter.WinstonLoggerAdapter(options);
  return new _LoggerController.LoggerController(adapter, null, options);
}

let logger = defaultLogger();

function setLogger(aLogger) {
  logger = aLogger;
}

function getLogger() {
  return logger;
} // for: `import logger from './logger'`


Object.defineProperty(module.exports, 'default', {
  get: getLogger
}); // for: `import { logger } from './logger'`

Object.defineProperty(module.exports, 'logger', {
  get: getLogger
});