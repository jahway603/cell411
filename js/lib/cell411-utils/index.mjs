export function console_dump(obj){
  this.log("dump(",obj,")=",pp(obj));
}
console.dump=console_dump;
export async function findFully(query,xparm,limit){
  const res=[];
  limit||=1000;
  query.limit(limit);
  do {
    const batch=await query.find(xparm);
    for(var i=0;i<batch.length;i++){
      res.push(batch[i]);
    }
    if(batch.length!=limit)
      break;
  }while(1);
  return res;
}

function keyval(key,val){
  if(key !== "classLevelPermissions"){
    return val;
  } else {
    return key;
  }
};

export function pp(... obj) {
  if(obj.length==1){
    return JSON.stringify(obj[0],null,2);
  } else {
    return JSON.stringify(obj,null,2);
  }
}

