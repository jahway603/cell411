
import json5 from 'json5';


export function pp(obj){return JSON.stringify(obj,null,2);};
export function show(obj){ return util.inspect(obj); };
export function keys(obj) { return Object.keys(obj); };

const c4util = { pp, show, keys, json5 };

export default c4util;


