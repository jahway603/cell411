#!node
global.md5=require('md5');
global.fs=require('fs');
global.util=require('util');
global.Console=require("console").Console;
global.process=require("process");
global.childProcess=require("child_process");
global.stdout=process.stdout;
global.stderr=process.stderr;
global.stdin=process.stdin;
global.pp=function(obj){return JSON.stringify(obj,null,2);};
global.show=function show(obj) { console.log(util.inspect(obj));};
global.keys=function keys(obj) { Object.keys(obj); }
global.readJson=function readJson(file){
  const text=fs.readFileSync(file);
  return JSON.parse(text);
};
global.writeJson=function writeJson(file, data) {
  fs.writeFileSync(file, JSON.stringify(data,null,2));
};
global.dumpJson=global.writeJson;
if(!console.dump){
  global.dump=console.dump=function dump(obj){this.log(JSON.stringify(obj,null,0));};
};
global.loadFileMeta=async function loadFileMeta(fileName)
{
  const buf = fs.readFileSync(fileName);
  return {
    buf,
    params: {
      mime:getMimeType(fileName),
      ext:getExtension(fileName),
      md5:md5(buf),
      size:buf.length,
    }
  };
}
global.dumpKeys=function dumpKeys(obj,prefix){
  if(prefix==null)
    prefix="";
  const keys=Object.keys(obj);
  for(var i=0;i<keys.length;i++){
    console.log(prefix,keys[i]);
  };
};
global.getMD5Sum=function getMD5Sum(file) {
  var res=childProcess.execSync("md5sum "+file);
  res=res.toString();
  res=res.split(/\s+/);
  return res[0];
}
global.getMimeType=function getMimeType(file){
  var res=childProcess.execSync("file --mime-type "+file);
  res=res.toString();
  while(res.endsWith("\n"))
    res=res.substring(0,res.length-1);
  res=res.split(' ');
  res=res[1];
  return res;
};
global.getExtension=function getExtension(file){
  var res=childProcess.execSync("file --extension "+file);
  res=res.toString();
  while(res.endsWith("\n"))
    res=res.substring(0,res.length-1);
  res=res.split(' ');
  res=res[1];
  res=res.split('/');
  res=res[0];
  return res;
};


