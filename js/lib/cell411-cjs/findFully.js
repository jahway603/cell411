async function findFully(query) {
  const limit=1000;
  query.limit(limit);
  var users=[];
  var batch;
  var start = new Date();
  do {
    batch=await query.find();
    query.skip(users.length);
    batch.forEach((user)=>{users.push(user)});
  } while(batch.length == limit);
  var finish = new Date();
  console.log((finish.getTime()-start.getTime())/1000);
  return users;
};
global.findFully=findFully;
