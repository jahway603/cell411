const startTime = new Date().getTime();
setInterval(checkFiles,1000);
const list1 = fileList(-1);
const list2 = fileList(1);
function dirPaths(dir,list,val){
  const cloud = fs.readdirSync(dir);
  for(var i=0;i<cloud.length;i++){
    const path=dir+"/"+cloud[i];
    if(path.endsWith(".js")) {
      list.push(path);
    }
  }
};
function fileList(val) {
  const list=[];
  dirPaths(".",list,val);
  dirPaths("cloud",list,val);
  dirPaths("cloud/lib",list,val);
  return list;
}
function checkFiles() {
  const list2 = fileList(1);
  const diff =compare(list1,list2);
  for(var i=0;i<list2.length;i++){
    const file=list2[i];
    const stat=fs.statSync(file);
    if(stat.mtime.getTime()>startTime){
      diff[file]||="changed";
      continue;
    };
  };
  if(Object.keys(diff).length!=0) {
    console.dump(diff);
    process.exit(0);
  }
};
function compare(list1,list2){
  const diff={};
  for(var i=0;i<list1.length;i++){
    diff[list1[i]]=(diff[list1[i]]||0)+1;
  };
  for(var i=0;i<list2.length;i++){
    diff[list2[i]]=(diff[list2[i]]||0)-1;
  };
  for(var key of Object.keys(diff)){
    if(diff[key]==0)
      delete(diff[key]);
    else if(diff[key]<0)
      diff[key]="added";
    else
      diff[key]="removed";
  };
  return diff;
}
