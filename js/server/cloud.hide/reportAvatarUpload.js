const childProcess = require("child_process");
const Buffer = require("node:buffer");
// ----- This is the aws shit.
const AWS = require('aws-sdk');
const AWScred = JSON.parse(fs.readFileSync(process.env.HOME+"/.parse/aws.cred.json"));
const S3Options = {
  params: { Bucket: "cell411" },
  accessKeyId:AWScred.aws.accessKey,
  secretAccessKey:AWScred.aws.secretKey
};
const S3Client = new AWS.S3(S3Options);
function createFile(shortName,fileName,mimeType) {
  return new Promise((resolve,reject)=>{
    console.log({shortName,fileName});
    const params = {
      Key: shortName,
      ContentType: mimeType,
      ACL: "public-read",
    };
    console.dump(params);
    params.Body= fs.readFileSync(fileName);
    S3Client.upload(params, (err,res) => {
      if(err!=null) {
        reject(err);
      } else {
        resolve(res);
      }
    });
  });
};
async function reportAvatarUpload(req) {
  var user = req.user;
  const params = req.params;
  var fileName = req.params.fileName;
  while(fileName.endsWith("\n"))
    fileName=fileName.substring(0,fileName.length()-1);
  var newAvatar="/var/cell411/uploads/"+fileName;
  if(!fs.existsSync(newAvatar)) {
    throw new Error("upload not found");
  }
  var mimeType=getMimeType(newAvatar);
  var ext=getExtension(newAvatar);
  var md5=getMD5Sum(newAvatar);

  var res = await createFile("avatar."+user.id+"."+md5+"."+ext, newAvatar, mimeType);
  if(typeof(res.Location)=="undefined"){
    console.dump(res);
    throw new Error("Failed to save new avatar");
  };
  user.set("avatar",res.Location);
  const thumbFileName=newAvatar+".thumb."+ext;
  try {
    childProcess.execSync(`convert -scale 300x300 ${newAvatar} ${thumbFileName} 2>&1`);
    res = await createFile("thumbnail."+user.id+"."+md5+"."+ext, thumbFileName, mimeType);
  } catch ( err ) {
    console.log(err);
    console.dump(err);
  };
  if(typeof(res.Location)=="undefined"){
    console.dump(res);
    throw new Error("Failed to save new thumbnail");
  };
  user.set("thumbnail",res.Location);
  console.dump({user});
  user.save(null,{useMasterKey: true});
  console.dump({user});
  return { success: true, user };
}
Parse.Cloud.define("reportAvatarUpload",reportAvatarUpload);

