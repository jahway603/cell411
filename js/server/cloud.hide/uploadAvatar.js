const child_process = require("child_process");
const fs = require('fs');
const Buffer = require("node:buffer");
// ----- This is the aws shit.
const AWS = require('aws-sdk');
const AWScred = JSON.parse(fs.readFileSync(process.env.HOME+"/.parse/aws.cred.json"));
const S3Options = {
  params: { Bucket: "cell411" },
  accessKeyId:AWScred.aws.accessKey,
  secretAccessKey:AWScred.aws.secretKey
};
const S3Client = new AWS.S3(S3Options);
function createFile(shortName,fileName,mimeType) {
  console.log({shortName,fileName,mimeType});
  return new Promise((accept,reject)=>{
    const params = {
      Key: shortName,
      ContentType: mimeType,
      ACL: "public-read",
    };
    console.dump(params);
    params.Body= fs.readFileSync(fileName);
    S3Client.upload(params, (err,res) => {
      if(err!=null) {
        reject(err);
      } else {
        accept(res);
      }
    });
  });
};
async function uploadAvatar(req) {
  var user = req.user;
  const params = req.params;
  const { mime, md5 } = params;
  var port=Math.round(Math.random()*10000+20000);
  child_process.exec(`./upload-server ${user.id} ${port}`, async (msg,out,err)=>{
    if(err){
      console.log(`exeerr: ${err}`);
    };
    console.log(`stderr: ${err}`);
    console.log(`stdout: ${out}`);
    const avatar = JSON.parse(out);
    console.dump({avatar});
    const thumbTemp=avatar.filename.replace('avatar','thumbnail');
    child_process.execSync(`convert -scale 300x300 upload/${avatar.filename} upload/${thumbTemp} 2>&1`);
    const {buf,params}=await loadFileMeta("upload/"+thumbTemp);
    const thumbNail=params;
    console.dump({thumbNail});
    const newThumb="thumbnail."+user.id+"."+thumbNail.md5+"."+thumbNail.ext;
    child_process.execSync(`mv upload/${thumbTemp} upload/${newThumb}`);
    console.dump({avatar,thumbNail});
    const createRes1 = await createFile(avatar.filename,'upload/'+avatar.filename,avatar.mime);
    const createRes2 = await createFile(newThumb, "upload/"+newThumb, thumbNail.mime);

    console.dump([createRes1,createRes2]);
//       console.dump({beforeUser: user});
    user.set('avatar',createRes1.Location);
//       console.dump({beforeUser: user});
    user.set('thumbNail',createRes2.Location);
//       console.dump({afterUser: user});
    await user.save(null,{useMasterKey: true});
//       console.dump({user});
  });
  return { port };
}
Parse.Cloud.define("uploadAvatar",uploadAvatar);
