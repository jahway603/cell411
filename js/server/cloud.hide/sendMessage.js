const child_process = require("child_process");
async function finishImage(port) {
  try {
    const stdout=child_process.execSync("./upload-server "+port,{stderr: 'inherit'});
    var string=stdout.toString();
    console.log("\n\n\n");
    console.log({string});
    console.log("\n\n\n");
  } catch ( err ) {
    console.log(err);
  }
};
async function sendMessage(req) {
  const user = req.user;
  const params = req.params;
  const objectId = req.objectId;
  const query = new Parse.Query("ChatMsg");
  const chatMsg = await query.get(objectId);
  const md5 = params.get("md5");
  var count=0;
  if(!md5 || !objectId) {
    var msg;
    if(md5) {
      msg="objectId";
    } else if ( objectId ) {
      msg="md5";
    } else {
      msg="objectId && md5";
    };
    return {
      success: false,
      message: msg
    };
  }
};
Parse.Cloud.define("sendMessage",sendMessage);
