#!/usr/bin/env node

const util=require("util");
const inspect=util.inspect;
const ParseServer = require("parse-server").ParseServer;
const express = require("express");
const fs = require("fs");
const path = require("path");
require("cell411-cjs");
const config=loadConfig();
const app = express();
const pwd=process.env.PWD;

const server=new ParseServer(config);
app.use(config.mountPath, server);
app.get('/resetPassword.html', function(req,res){
  //console.log(req);
  res.status(200).send(
    `url: ${req.url} query: ${JSON.stringify(req.query,null,2)}\n\n`
  );
});
app.get('/index.html', function (req, res) {
  console.log("connection to root");
  res.status(200).send(
    "<html><body>\n\tWho ya gonna call?  The cops might well shoot YOU!\n</body></html>"
  );
});
app.get('/', function (req, res) {
  console.log("connection to root");
  res.status(200).send(
    "Who ya gonna call?  The cops might well shoot YOU!\n"
  );
});

const httpServer = require('http').createServer(app);
httpServer.listen(config.port, "127.0.0.1", function () {
  console.log('cell411 server running on port ' + config.port); 
});
// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);

module.exports = {
  app,
  config,
}
function loadConfig() {
  const flavor = process.env.PARSE_FLAVOR;
  if(flavor==null || flavor.length==0)
    die("missing flavor");
  const home=process.env.HOME;
  const env=process.env;
  const configFile = home+"/.parse/config-"+flavor+".json";
  const configText = fs.readFileSync(configFile).toString();
  const config = JSON.parse(configText);
  config.fileUpload={};
  config.revokeSessionOnPasswordReset=false;
  config.masterKeyIps=[];
  if(Object.hasOwn(config,"filesAdapter")) {
    const filesAdapter=config.filesAdapter;
    const options=filesAdapter.options;
    if(filesAdapter.module=="@parse/s3-files-adapter"){
      const s3ConfigFile = home+"/.parse/aws.cred.json";
      const s3ConfigText = fs.readFileSync(s3ConfigFile).toString();
      const s3Config = JSON.parse(s3ConfigText);
      const overrides={
      };
      overrides.accessKey=s3Config.aws.accessKey;
      overrides.secretKey=s3Config.aws.secretKey;
      config.filesAdapter.options.s3overrides=overrides;
    };
  };

  config || die("failed to read: "+configFile);

  if(env['PARSE_SERVER_LOGS_FOLDER']){
    config.logsFolder=env['PROCESS_SERVER_LOGS_FOLDER'];
  } else if(Object.prototype.hasOwnProperty(config,"logFolder")) {
    env['PARSE_SERVER_LOGS_FOLDER']=config.logsFolder;
  };
  return config;
};
function die(msg) {
  console.error("FATAL ERROR: "+msg);
  process.exit(1);
};
require("./checkFiles.js");
