const cp = require("child_process");

async function waitForUpload(objectId,purpose,port)
{
  let cmd=`./upload-server ${purpose} ${objectId} ${port}`;
  console.log(`about to run upload server\n  cmd=${cmd}`);
  let object;
  if(purpose==="avatar"){
    object=await new Parse.Query("User").get(objectId);
  } else if ( purpose==="chatImage" ) {
    object=await new Parse.Query("ChatMsg").get(objectId);
  } 
  if(object==null){
    console.log("Unable to get object for ",
      purpose," and ", objectId);
  };
  console.log(object);
  let res = cp.exec(cmd ,{},function(error,stdout,stderr){
    console.log({error,stdout,stderr});
    console.log(1);
    if(!error){
      console.log(2);
      const info = JSON.parse(stdout);
      let data=fs.readFileSync("upload/"+info.file);
      data=Array.from(Uint8Array.from(data));
      console.log(3);
      let file=new Parse.File(info.file,data, info.mime);
      object.set(purpose,file);
      console.log(4);
      object.save(null,{useMasterKey: true});
      console.log({object});
    }
  });
}
async function sendUpload(req) {
  const user = req.user;
  if(user==null && !req.master)
    throw new Error("caller is not logged in");
  const objectId = req.params.objectId;
  if(objectId==null)
    throw new Error("Expected objectId, got null");
  const purpose=req.params.purpose;
  if(purpose==null)
    throw new Error("Expected prupose, got null");

  let port = 10000+Math.floor(10000*Math.random())
  waitForUpload(objectId,purpose,port);
  console.log({port});  
  return { port };
};
Parse.Cloud.define("sendUpload", sendUpload);
