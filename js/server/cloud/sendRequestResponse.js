const parse = require('./lib/parse.js');
const { blockCheck, markFriendRequests } = require("./lib/modelUtils.js");
const mk = {useMasterKey: true};

async function sendRequestResponse(req){
  const user = req.user;

  if(user==null)
    throw new Error("User not logged in");

  const params=req.params;

  const objectId = params['objectId'];
  if(objectId==null)
    throw new Error("Expected objectId");

  const type = params['type'];
  const query = new Parse.Query("Request");
  const request = await query.get(objectId);
  if(!request){
    return {
      success: false,
      message: "No request found"
    }
  }

  try {
    var relation;
    if(type == "FriendApprove") {
      console.log("approving friend request");
      if(request.get("status")!=="PENDING") {
        return {
          success: false,
          message: "You can only approve or reject pending requests",
        }
      };
      if(request.get("sentTo").id!=user.id) {
        return {
          success: false,
          message: "request not sent to user attempting approval"
        };
      }
      const friend = request.get("owner");
      friend.fetch();

      if(request.get("cell")!=null) {
        return {
          success: false,
          message: "Cannot approve cell request as friend request" 
        }
      }
      createFriendship(request);
      await markFriendRequests(user,friend,"APPROVED"); 
      return {
        requestType: type, friend: friend.id, success: true
      };
    } else if(type == "FriendReject") {
      console.log("rejecting friend request");
      if(request.get("sentTo").id!=user.id) {
        return {
          success: false,
          message: "friend request not sent to user attempting rejection"
        }
      }
      const friend = request.get("owner");
      friend.fetch();

      if(request.get("cell")!=null) {
        return {
          success: false,
          message: "Attempt to reject cell request as friend request"
        }
      }

      await markFriendRequests(user,friend,"REJECTED"); 
      return { requestType: type, friend: friend.id, success: true };
    } else if (type == "CellJoinApprove") {
      console.log("approving cell join request");

      if(request.get("status")!=="PENDING") {
        return {
          success: false,
          message: "You can only approve or reject pending requests",
        }
      };
      if(request.get("cell")==null) {
        return {
          success: false,
          message: "Attempt to approve friend request as cell request"
        }
      }

      // It looks weird but the owner of the request it talking to the owner of
      // the cell ...
      const member = request.get("owner");
      if(member==null)
        throw new Error("CellJoinApprove missing owner");
      await member.fetch();
      const cell = request.get("cell");
      await cell.fetch();
      const owner = cell.get("owner");
      await owner.fetch();
      if(owner.id!=user.id) {
        return {
          success: false,
          message: "You can only approve cell requests to your cells"
        };
      }

      cell.relation("members").add(member);
      await cell.save(null,mk);
      request.set("status", "APPROVED");

      return { requestType: type, cell: cell.id, member: member.id, success: true };
    } else if (type == "CellJoinReject") {
      console.log("rejecting cell join request");
      if(request.get("status")!=="PENDING") {
        return {
          success: false,
          message: "You can only approve or reject pending requests",
        }
      };
      if(request.get("cell")==null) {
        return {
          success: false,
          message: "Attempt to reject friend request as cell request"
        }
      }

      const cell = request.get("cell");
      await cell.fetch();
      const cellOwner = cell.get("owner");
      if(cellOwner.id !== user.id){
        return {
          success: false,
          message: "Attempt to reject cell request not by cell owner"
        };
      }

      const member = request.get("owner");
      cell.relation("members").remove(member);
      await cell.save(null,mk);
      request.set("status", "REJECTED");

      return { requestType: type, cell: cell.id, member: member.id, success: true };
    } else {
      throw new Error("unexpected request type: "+type);
    };
  } finally {
      await request.save(null, mk);
  }
};
Parse.Cloud.define("sendRequestResponse",sendRequestResponse);
