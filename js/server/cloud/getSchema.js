#!node

function getName(schema){
  return schema.className;
}
Parse.Schema.getName=function() { getName(this); };
async function getSchema(req) {
  const schemas = await Parse.Schema.all();
  const res = {};
  schemas.forEach((s)=>{
    console.log(s);
  });
}
Parse.Cloud.define("getSchema", getSchema);
