const model=require('./lib/parse.js');
async function beforeSave_User(req) {
  console.log("beforeSave_User");
  const user = req.object;
  //console.log("calling storeRU");
  await storeRelationshipUpdates(req,user.id,"_User","friends");
  //console.log("calling storeRU");
  await storeRelationshipUpdates(req,user.id,"_User","spamUsers");
};
Parse.Cloud.beforeSave("_User",beforeSave_User);
