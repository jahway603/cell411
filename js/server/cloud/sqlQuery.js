const util=require("util");
const inspect=util.inspect;

async function sqlQuery(req) {
  const start = new Date().getTime();
  try {
    return {
      sql: req.params.sql,
      res: await Parse.adapter._client.any(req.params.sql),
    };
  } finally {
    const now = new Date().getTime();
    console.log(`Done after: ${(now-start)/1000.0} seconds`);
  };
};
Parse.Cloud.define("sqlQuery",sqlQuery);
