async function gatherRelations(req)
{
  var user = req.user;
  if(user==null)
    throw new Error("You must be logged in to call relations");
  
  var chatRel = [];

//     const req2 = JSON.parse(JSON.stringify(req));
//     const keys = Object.keys(req);
//     for(var i=0;i<keys.length;i++) {
//       const key=keys[i];
//       const disp={};
//       disp.key=key;
//       disp[key]=req[key];
//       console.log(disp);
//     };
  
  const dates = {};
  function updateDate(obj){
    var date=obj.get("updatedAt");
    if(date==null)
      return;
    date=date.getTime();
    if(date==null)
      return;
    dates[obj.id]=date;
  }
  const rels = { dates };
  user.fetch();
  updateDate(user);
  await loadFriends();
  console.log("Private Cells");
  await loadCells("PrivateCell","owner");
  await loadCells("PrivateCell","members");

  console.log("Public Cells");
  await loadCells("PublicCell","owner");
  await loadCells("PublicCell","members");

  console.log("Requests");
  await loadRequests("owner");
  await loadRequests("sentTo");

  await loadRelatedAlerts();

  {
    console.dump(chatRel);
    const chatObj={};
    chatRel.forEach((id)=>{chatObj[id]=1});
    chatRel=Object.keys(chatObj);
    console.dump(chatRel);
  }
  const chatRelId =  "_User:connectedTo:ChatRoom:objectId:"+user.id ;
  rels[chatRelId]=chatRel;
  async function loadRelatedAlerts() {
    console.log("loadRelatedAlerts");
    var relId = [ "_User", "audienceOf", "Alert", "objectId", user.id];
    relId=relId.join(":");
    const rel=[];
    rels[relId]=rel;
    const time = new Date().getTime();
    const minTime = time-86400*1000*3;
    const query = Parse.Query.or(
      new Parse.Query("Alert").containedBy("audience",[user.id]),
      new Parse.Query("Alert").equalTo("owner",user.id)
    );
    query.greaterThan("createdAt", new Date(minTime));
    const alerts = await findFully(query);
    console.dump({query,alerts});
    for(var i=0;i<alerts.length;i++){
      const alert=alerts[i];
      updateDate(alert);
      rel.push(alert.id);
      const chatRoom = alert.get("chatRoom");
      if(chatRoom!=null) {
        updateDate(chatRoom);
        chatRel.push(chatRoom.id);
      };
    };
  };
  async function loadRequests(relName) {
    console.log("loadRequests");
    const query = new Parse.Query("Request").equalTo(relName,user.id);
    query.containedIn("status",["PENDING","RESENT"]);
    const requests=await findFully(query);
    const rel=[];
    for(var i=0;i<requests.length;i++){
      const request=requests[i];
      updateDate(request);
      rel.push(request.id);
    };
    var relId = [ "_User", null, "Request", "objectId", user.id];
    if(rel=="owner"){
      relId[1]="ownerOf";
    } else {
      relId[1]="sentToOf";
    }
    relId=relId.join(":");
    rels[relId]=rel;
  };
  async function loadFriends() {
    console.log("loadFriends");
    var relId = [ "_User", "friends", "_User","objectId", user.id].join(":");
    var rel=[];
    var friends = await findFully(user.relation("friends").query());
    rels[relId]=rel;
    var i;
    for(i=0;i<friends.length;i++) {
      const friend = friends[i]; 
      updateDate(friend);
      rel.push(friend.id);
    };
  }
  async function loadCells(className, relname) {
    console.log("loadCells");
    var query = new Parse.Query(className);
    query.equalTo(relname,user);
    console.dump(query);
    var relId;
    if(relname=="owner"){
      relId = [ "_User", "ownerOf", className, "objectId", user.id ];
    } else {
      relId = [ "_User", "memberOf",className, "objectId", user.id ];
    };
    relId=relId.join(":");
    const rel=[];
    rels[relId]=rel;
    const cells  = await findFully(query);
    for(var i=0;i<cells.length;i++) {
      const cell=cells[i];
      rel.push(cell.id);
      updateDate(cell);
      const chatRoom = cell.get("chatRoom");
      if(chatRoom!=null) {
        updateDate(chatRoom);
        chatRel.push(chatRoom.id);
      };
      const relation = cell.relation("members");
      const query = relation.query();
      query.include("objectId");
      query.include("updatedAt");
      const memRelId = [ className, "members", "_User","objectId", cell.id ].join(":");
      const memRel=[];
      rels[memRelId]=memRel;
      const members = await findFully(cell.relation("members").query());
      console.log(`got ${members.length} members`);
      for(var j=0;j<members.length;j++){
        const member=members[j];
        updateDate(member);
        memRel.push(member.id);
      };
      rels[relId]=rel;
    };

  }
  return rels;
}
async function relations(req) {
  const start = new Date().getTime();
  try {
    
    return await gatherRelations(req);
  } finally {
    const now = new Date().getTime();
    console.log(`Done after: ${(now-start)/1000.0} seconds`);
  };
};
Parse.Cloud.define("relations",relations);
