const model=require('./lib/parse.js');

async function beforeSavePublicCell(req) {
  const cell = req.object;
  console.log({cell});
  const user = req.user;
  console.log({user});
  await storeRelationshipUpdates(req,cell.id,"PublicCell","members");
  if(cell.get("category")==null){
    cell.set("category","Activism");
  }
  var acl = cell.getACL();
  if(acl==null)
    acl=new Parse.ACL();
  acl.setPublicWriteAccess(false);
  acl.setWriteAccess(cell.get("owner").id,true);
  acl.setPublicReadAccess(true);
  cell.setACL(acl);
};
Parse.Cloud.beforeSave("PublicCell",beforeSavePublicCell);
