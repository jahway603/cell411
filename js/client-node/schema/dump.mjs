#!node
import 'cell411-mjs';
import { pp } from 'cell411-utils';


await loadParseConfig();
import fs from 'fs';

async function getSchemas() {
  await initializeParse();
  var health = await Parse.getServerHealth();
  if(!fs.existsSync("schema/")) {
    fs.mkdirSync("schema/");
  };
  return await Parse.Schema.all();
};
Parse.Schema.getName=function() { getName(this); };
function getName(schema){
  return schema.className;
}
const schemas = await getSchemas();
for(var i=0;i<schemas.length;i++){
  console.log(await getName(schemas[i]));
}
console.dump(Parse.Schema);
