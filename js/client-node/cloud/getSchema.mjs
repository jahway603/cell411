import cell411 from 'cell411-mjs';
import ppgp from 'pg-promise';
import pgpm from 'pg-monitor';
console.log({pgpm});
const pgp_cred=await readJson(configDir+'/pg_admin.json');
const initOptions={};
const pgp=ppgp(initOptions);
pgpm.attach(initOptions);
const db=pgp(pgp_cred);

// await db.none('create table foo (first_name, last_name, age) as (select ${name.first}, $<name.last>, $/age/)', {
//     name: {first: 'John', last: 'Dow'},
//     age: 30
// });

function arr2obj(arr,name){
  const res={};
  for(var i=0;i<arr.length;i++){
    const obj=arr[i];
    const id=obj[name];
    delete obj[name];
    res[id]=obj;
  }
  return res;
}
async function main() {
  await initializeParse();
  const user = await parseLogin();
  const sessionToken=user.getSessionToken();
  const schemas = arr2obj(await Parse.Schema.all(),"className");
  const os=[];
  for(var s in schemas){
    var object=schemas[s];
    const schema={};
    for(var f in object.fields){
      const field=object.fields[f];
      var t=field['type'];
      delete field['type'];
      var c=field['targetClass'];
      delete field['targetClass'];
      var r=field['required'];
      delete field['required'];
      var d=field['defaultValue'];
      delete field['defaultValue'];
      const o={};
      o.classname=s;
      o.field=f;
      o.type=t;
      o.target=c;
      o.required=r;
      o.def=d;
      os.push(o);
    }
  }
  for(var i=0;i<os.length;i++){
    var arr=[ os[i] ];
    console.log(arr);
    db.none('insert into fields($1:name) VALUES ($1:list)',arr); 
  };
//   db.none([ 
//   " create table fields ( classname,     field,    type,    target,    required,    def ) as "+
//   "(select              ${classname}, ${field}, ${type}, ${target}, ${required}, ${def});",
//   os[0]]);
};
main()
