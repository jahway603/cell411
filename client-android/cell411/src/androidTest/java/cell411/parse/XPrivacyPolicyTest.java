package cell411.parse;

import androidx.annotation.WorkerThread;
import cell411.utils.DroidUtil;
import cell411.utils.MethodRunnable;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import junit.framework.TestCase;
import org.junit.Test;

public class XPrivacyPolicyTest
  extends TestCase
{
  @Test
  public XPrivacyPolicyTest(){

  }

  MethodRunnable smRunGetLatestCall = MethodRunnable.forVirtual(
    this,"getLatestCall"
  );
  private final XTAG TAG = new XTAG();
  @Test
  @WorkerThread
  public void getLatestCall() {
    if(DroidUtil.isMainThread()){
      DroidUtil.onExec(smRunGetLatestCall);
      return;
    }
    XPrivacyPolicy privacyPolicy = XPrivacyPolicy.getLatestCall();
    XLog.i(TAG,"%s",privacyPolicy.toJSON().toString(2));
  }
}