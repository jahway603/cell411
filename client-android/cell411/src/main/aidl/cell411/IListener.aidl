// IListener.aidl
package cell411;

// Declare any non-default types here with import statements

interface IListener {
    String[] events();
    void event(String event);
}