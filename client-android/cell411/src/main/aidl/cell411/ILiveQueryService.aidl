// ILiveQueryService.aidl
package cell411;

import cell411.IListener;
// Declare any non-default types here with import statements

interface ILiveQueryService {
    boolean isReady();
    void addListener(IListener listener);
    void query(String query, IListener listener);
}