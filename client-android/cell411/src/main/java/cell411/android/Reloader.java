package cell411.android;

import cell411.ui.base.BaseApp;
import cell411.utils.Reflect;
import cell411.utils.WeakList;

public class Reloader implements Runnable {
  private static final Object lock = new Object();
  private static final WeakList<Reloader> smInstances =
      new WeakList<>(Reloader.class);
  private final BaseApp mBaseApp;
  private final Reloadable mReloadable;
  int step;

  public Reloader(BaseApp baseApp, Reloadable reloadable) {
    mBaseApp = baseApp;
    mReloadable = reloadable;
    step = 0;
    smInstances.add(this);
  }

  @Override
  public void run() {
    synchronized(lock){
      Reflect.announce("step: " + step + " " + Thread.currentThread());
      switch (step++) {
        case 0:
          mBaseApp.onUI().post(this);
          break;
        case 1:
          mReloadable.loadData1();
          mBaseApp.getExec().execute(this);
          break;
        case 2:
          mReloadable.loadData2();
          mBaseApp.onUI(this);
          break;
        case 3:
          mReloadable.loadData3();
          mBaseApp.showToast("Reload Complete");
          break;
      };
    }
  }
}
