package cell411.android;

import android.view.View;

import cell411.ui.base.BaseApp;

public interface Reloadable {
  long getLastReloadTime(int which);

  void setLastReloadTime(int which);

  void loadData2();

  void loadData1();

  void loadData3();

  default void refresh() {
    BaseApp app = BaseApp.req();
    assert app!=null;
    app.refresh(this);
  }

  @SuppressWarnings("unused")
  default void refresh(View ignoredView) {
    refresh();
  }
}
