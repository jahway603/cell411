package cell411.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import cell411.ui.base.BaseApp;
import cell411.utils.*;

import javax.annotation.Nonnull;
import java.util.*;

public class RingtoneData {
  private final static TreeMap<String, RingtoneData> smData = new TreeMap<>();
  private final static TreeSet<String> smAllKeys =
    Collect.asTreeSet("ALERT", "REQUEST", "CHAT");
  private final static SharedPreferences smPrefs = BaseApp.req().getAppPrefs();
  private static boolean smLoaded = false;
  private static List<RingtonePair> smTones;
  private final String mKey;
  private final Uri mUri;
  private final String mTitle;

  private RingtoneData(String key, Uri uri, String title) {
    mKey = key;
    mUri = uri;
    mTitle = Util.isNoE(uri) ? "" : title;
  }

  public static List<RingtonePair> fetchAvailableRingtones(Context context) {
    List<RingtonePair> ringtones = new ArrayList<>();
    if (Util.theGovernmentIsLying())
      return ringtones;
    RingtoneManager mgr = new RingtoneManager(context);
    mgr.setType(RingtoneManager.TYPE_RINGTONE);

    int n = mgr.getCursor().getCount();
    for (int i = 0; i < n; i++) {
      Ringtone ringtone = mgr.getRingtone(i);
      Uri uri = mgr.getRingtoneUri(i);
      if (ringtone == null || uri == null) {
        continue;
      }
      RingtonePair pair = new RingtonePair();
      pair.mUri = uri;
      pair.mRingtone = ringtone;
      ringtones.add(pair);
    }
    return ringtones;
  }

  public static void addRingtoneData(final String key, final Uri uri) {
    String title = uri.getQueryParameter("title");
    if (Util.isNoE(title)) {
      throw new IllegalArgumentException("No title for ringtone!");
    }
    RingtoneData value = new RingtoneData(key, uri, title);
    value.store();
    getRingtoneData().put(key, value);
  }

  public static TreeMap<String, RingtoneData> getRingtoneData() {
    if (!smLoaded) {
      smLoaded = true;
      loadRingtoneData();
    }
    return smData;
  }

  public static SharedPreferences getPrefs() {
    return Objects.requireNonNull(BaseApp.req().getAppPrefs());
  }

  private static void loadRingtoneData(String key) {
    SharedPreferences prefs = getPrefs();
    String title = prefs.getString(getTitleKey(key), "");
    String uri = prefs.getString(getUriKey(key), "");
    boolean unsaved = false;
    if (Util.isNoE(title) || Util.isNoE(uri)) {
      unsaved = true;
      if (smTones == null) {
        smTones = fetchAvailableRingtones(BaseApp.req());
      }
      while (Util.isNoE(title) || Util.isNoE(uri)) {
        try {
          if (smTones.isEmpty())
            break;
          int position = (int) (Math.random() * smTones.size());
          RingtonePair pair = smTones.get(position);
          Ringtone tone = pair.mRingtone;
          if (pair.mUri != null) {
            uri = pair.mUri.toString();
          }
          title = tone.getTitle(BaseApp.req());
        } catch (Throwable throwable) {
          throwable.printStackTrace();
        }
      }
    }
    if (Util.isNoE(title) || Util.isNoE(uri)) {
      getRingtoneData().put(key, null);
    } else {
      RingtoneData value = new RingtoneData(key, Uri.parse(uri), title);
      if (unsaved) {
        value.store();
      }
      getRingtoneData().put(key, value);

    }
  }

  public static void loadRingtoneData() {
    for (String key : smAllKeys) {
      loadRingtoneData(key);
    }
  }

  @Nonnull
  private static String getUriKey(final String key) {
    return key + ".uri";
  }

  @Nonnull
  private static String getTitleKey(final String key) {
    return key + ".title";
  }

  private void store() {
    SharedPreferences.Editor edit = getPrefs().edit();
    if (!Util.isNoE(mUri) && !Util.isNoE(mTitle)) {
      edit.putString(getUriKey(mKey), mUri.toString());
      edit.putString(getTitleKey(mKey), mTitle);
    } else {
      edit.remove(getUriKey(mKey));
      edit.remove(getTitleKey(mKey));
    }
    edit.apply();
  }

  public boolean isEmpty() {
    return Util.isNoE(mUri) || Util.isNoE(mTitle);
  }

  public boolean isComplete() {
    return !isEmpty();
  }

  public String getTitle() {
    return mTitle;
  }

  public Uri getUri() {
    return mUri;
  }

  static class RingtonePair {
    Uri mUri;
    Ringtone mRingtone;
  }
}
