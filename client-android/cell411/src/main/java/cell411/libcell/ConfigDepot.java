package cell411.libcell;

import android.graphics.Bitmap;
import cell411.utils.IOUtil;
import cell411.utils.concurrant.STPExecutorPlus;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;

import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

import static android.os.Environment.DIRECTORY_DOCUMENTS;
import static android.os.Environment.DIRECTORY_PICTURES;

public class ConfigDepot
{
  static final AtomicReference<Config> smConfig =
    new AtomicReference<>();

  private ConfigDepot()
  {
    assert false;
  }
  public static void init(Config config){
    if(!smConfig.compareAndSet(null,config))
      throw new IllegalStateException("ConfigDepot.init() called twice");
  }
  static public Config get() {
    return smConfig.get();
  }

  public static boolean isConnected()
  {
    return smConfig.get().isConnected();
  }

  public static File getPictureDir()
  {
    return get().getExtDir(DIRECTORY_PICTURES);
  }


  public static int getVersionCode()
  {
    return 0;
  }
  static public File getCurrentStoreDir()
  {
    return new File(getParseDir(), "config");
  }


  public static File getFlavorDocDir()
  {
    return mkdirs(new File(getDocDir(), getFlavor()));
  }

  public static String getFlavor()
  {
    return get().getFlavor();
  }

  public static File mkdirs(File file, String... subs)
  {
    if (file == null)
      return null;
    if (subs != null && subs.length > 0)
      return mkdirs(mkname(file, subs));
    else if (file.exists())
      return file;
    else if (file.mkdirs())
      return file;
    else
      return null;
  }

  public static File mkname(File file, String... subs)
  {
    return new File(file, String.join("/", subs));
  }

  public static File getCacheDir()
  {
    return get().getApp().getCacheDir();
  }

  public static File getProfileDir()
  {
    return IOUtil.mkdirs(new File(getCacheDir(),"images"));
  }

  //  public static File getParseFileDir(String subDir)
//  {
//
//      File dir = ConfigDepot.mkdirs(getFilesDir(), subDir);
//      if (!dir.exists()) {
//        //noinspection ResultOfMethodCallIgnored
//        dir.mkdirs();
//      }
//      return dir;
//  }
//
//  public static File getParseCacheDir(String subDir)
//  {
//    if (parseDir == null)
//    {
//      parseDir = getApplicationContext().getDir("Parse", Context.MODE_PRIVATE);
//    }
//    String flavor = getFlavor();
//    if (flavor == null)
//    {
//      return null;
//    }
//    return createFileDir(new File(parseDir, flavor));
//  }
//
//    public static File getCacheDir()
//    {
//      if (cacheDir == null)
//      {
//        SubBaseApp subBaseApp = SubBaseApp.get();
//        assert subBaseApp != null;
//        cacheDir = subBaseApp.getFlavoredCacheDir();
//      }
//      return createFileDir(cacheDir);
//    }
//
//    public static File getParseCacheDir(String subDir)
//    {
//      File dir = new File(getCacheDir(), subDir);
//      if (!dir.exists())
//      {
//        //noinspection ResultOfMethodCallIgnored
//        dir.mkdirs();
//      }
//      return dir;
//    }

  public STPExecutorPlus getExec()
  {
    return getExecutor();
  }

  public static STPExecutorPlus getExecutor()
  {
    return get().getExecutor();
  }

  public static File getDocDir()
  {
    return getExtDir(DIRECTORY_DOCUMENTS);
  }
  public static File getPicDir()
  {
    return getExtDir(DIRECTORY_PICTURES);
  }

  public static File getExtDir(String sub)
  {
    return get().getExtDir(sub);
  }

  public static String getVersionName()
  {
    return get().getVersionName();
  }
  public static String getString ( int resId)
  {
    return get().getString(resId);
  }

  public static String getClientKey () {
    return get().getClientKey();
  }

  public static String getAppId () {
    return get().getAppId();
  }
  public static String getParseUrl () {
    return get().getParseUrl();
  }
  public static OkHttpClient getHttpClient () {
    return getHttpClientBuilder().build();
  }


  public static Builder getHttpClientBuilder () {
    return get().getHttpClientBuilder();
  }

  public static String getPackageName () {
    return get().getPackageName();
  }

  public static String getAppName () {
    return get().getAppName();
  }

  public static String getAppVersion () {
    return get().getAppVersion();
  }
  public static File getJsonCacheFile (String name){
    if (!name.endsWith(".json"))
      name = name + ".json";
    File file = getFlavorDocDir();
    file = new File(file, "data");
    return new File(file, name);
  }

  public static File getAvatarDir() {
    return get().getExtDir("avatars");
  }
  public static Bitmap getPlaceHolder() {
    return get().getPlaceHolder();
  }
  public static File getParseDir()
  {
    return mkdirs(get().getParseDir());
  }
}
//
//
//
//
//
//
//
//
//    public static File getMediaOutputDir (String type){
//    return getConfig().getMediaOutputDir(type);
//  }
//
//    public static File getPictureDir () {
//    return mkdirs(getExtDir(DIRECTORY_PICTURES));
//  }
//
//   public static File getKeyValDir () {
//      return mkdirs(getFilesDir(), "keyValue");
//
//}
//}
//
///  File getMediaOutputDir(String type)
//  {
//    return new File(getExtDir(type), "cell411/" + getFlavor());
//  }
//
//  public abstract String getFlavor();
//
//
//  File getFilesFlavoredDir()
//  {
//    return new File(getFilesDir(), getFlavor());
//  }
//
//
//  public static String getFlavor()
//  {
//    return getConfig().getFlavor();
//  }
//
//  public static File mkname(File file, String... subs)
//  {
//    return new File(file, String.join("/", subs));
//  }
//
//
//  public static Bitmap createPlaceholder()
//  {
//    Bitmap placeHolder = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888);
//    Canvas canvas = new Canvas(placeHolder);
//
//    int id = R.drawable.ic_placeholder_user;
//    Paint whitePaint = new Paint();
//    whitePaint.setColor(0xff000000);
//    whitePaint.setStyle(Paint.Style.FILL);
//    canvas.drawRect(0, 0, 300, 300, whitePaint);
//
//    OvalShape ovalShape = new OvalShape();
//    final Drawable background = new ShapeDrawable(ovalShape);
//    final int primaryColor = BaseApp.get().getPrimaryColor();
//
//    background.setTint(0xff000000);
//    background.setBounds(0, 0, 300, 300);
//    background.draw(canvas);
//
//    background.setTint(primaryColor);
//    background.setBounds(20, 20, 280, 280);
//    background.draw(canvas);
//
//    final Drawable drawable =
//      AppCompatResources.getDrawable(ConfigDepot.getContext(), id);
//    int extra = 300 - 262;
//    if (drawable != null) {
//      drawable.setBounds(extra / 2, extra / 2, 300 - extra / 2, 300 - extra / 2);
//      drawable.draw(canvas);
//    }
//    return placeHolder;
//
//
//    public STPExecutorPlus getExec () {
//    return getExecutor();
//  }
//    public static STPExecutorPlus getExecutor () {
//    return getConfig().getExecutor();
//  }
//    public static int getVersionCode () {
//    return getConfig().getVersionCode();
//  }
