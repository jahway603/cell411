package cell411.libcell;

import android.app.Application;
import android.graphics.Bitmap;
import cell411.utils.concurrant.STPExecutorPlus;

import java.io.File;

public interface Config
{
  int                           getVersionCode();
  okhttp3.OkHttpClient.Builder  getHttpClientBuilder();
  STPExecutorPlus               getExecutor();
  String                        getAppId();
  String                        getAppName();
  String                        getAppVersion();
  String                        getClientKey();
  String                        getFlavor();
  String                        getPackageName();
  String                        getParseUrl();
  String                        getString(int            resId);
  String                        getVersionName();

  File getExtDir(String sub);
  Bitmap getPlaceHolder();

  Application getApp();

  File getParseDir();

  boolean isConnected();
}
