package cell411.imgstore;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Looper;
import android.util.Size;
import android.widget.ImageView;
import cell411.json.JSONObject;
import cell411.parse.XUser;
import cell411.ui.base.BaseApp;
import cell411.utils.DroidUtil;
import cell411.utils.IOUtil;
import cell411.utils.PrintString;
import cell411.utils.UrlUtils;
import cell411.utils.Util;
import com.parse.decoder.ParseDecoder;
import com.parse.model.ParseObject;
import com.parse.model.ParseUser;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;

public class ImageData implements CachedResource,
                                  ImageListener,
                                  Runnable {
  static final ArrayList<ImageData> smAllImageData = new ArrayList<>();
  private static final OkImageDownloader smOkImageDownloader = new OkImageDownloader(
    BaseApp.req().getHttpClient()
  );
  private final String mKey;
  private final ParseObject mObject;
  private final ArrayList<Notifier> mNotifiers = new ArrayList<>();
  private final SharedCropper mSharedCropper = SharedCropper.get();
  private final URL mURL;
  private Instant mSince = Instant.now();
  private Bitmap mBitmap;
  private Instant mFailed;
  private CachedStatus mStatus = CachedStatus.NEW_REQUEST;
  private File mFile;
  private long mSize = 0;
  private String mSource = null;
  private Size mOrigDim;
  private int mUses = 0;

  {
    synchronized (smAllImageData) {
      smAllImageData.add(this);
    }
  }

  public ImageData(@Nonnull String key,
                   ParseObject object,
                   URL url,
                   ImageView imageView, ImageListener listener
                   )
  {
    mKey = key;
    mObject = object;
    if (Util.isNoE(url)) {
      if (object instanceof ParseUser) {
        XUser user = (XUser) object;
        mURL = user.getAvatarUrl();
      } else {
        throw new RuntimeException("Only users implemented");
      }
    } else {
      mURL = url;
    }
    merge(imageView, listener);
  }

  public static ImageData fromJSON(JSONObject jsonData) {
    try {
      ImageStore imageStore = ImageStore.get();
      String objectId = jsonData.optString("object");
      if (!Util.isNoE(objectId))
        return null;
      String key = jsonData.getString("key");
      String file = jsonData.optString("file");
      String urlStr = jsonData.optString("url");
      String objStr = jsonData.optString("object");
      ParseObject object = null;
      if (objStr != null) {
        ParseDecoder decoder = ParseDecoder.get();
        object = (ParseObject) decoder.decode(objStr);
      }
      long failed = jsonData.optLong("failed");
      long since = jsonData.optLong("since");
      ImageData data = new ImageData(key, object, UrlUtils.toURL(urlStr),
        null, null);
      if (since != 0)
        data.mSince = Instant.ofEpochMilli(since);
      if (failed != 0)
        data.mFailed = Instant.ofEpochMilli(failed);
      if (!Util.isNoE(file))
        data.mFile = new File(file);
      return data;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public synchronized void dump(PrintString ps) {
    ps.pl(toJSON().toString(2));
  }

  JSONObject toJSON() {
    JSONObject res = new JSONObject();
    res.put("key", mKey);
    res.put("object", getObjectId());
    res.put("file", mFile == null ? null : mFile.getAbsolutePath());
    res.put("url", String.valueOf(mURL));
    if (mFailed != null)
      res.put("failed", mFailed.toEpochMilli());
    res.put("origDim", mOrigDim);
    res.put("size", mSize);
    res.put("status", mStatus);
    if (mSince != null)
      res.put("since", mSince.toEpochMilli());
    return res;
  }

  private String getObjectId() {
    if (mObject == null)
      return null;
    else
      return mObject.getObjectId();
  }

  public URL getURL() {
    return mURL;
  }

  public synchronized void run() {
    if (DroidUtil.isMainThread()) {
      DroidUtil.onExec(this, 0);
      return;
    }
    try {
      assert !DroidUtil.isMainThread();
      if (mBitmap == null && mFile != null) {
        loadFromDisk();
      }
      if (mBitmap == null && mURL != null) {
        loadFromNet();
      }
      assert ((mBitmap != null) == (mSource != null));
      if (mBitmap != null) {
        mStatus = CachedStatus.SUCCESS;
        onLoadingComplete(mURL, mBitmap);
      } else if (mStatus == null) {
        mStatus = new CachedStatus(FailType.UNKNOWN);
      }
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }

  @Override
  public void onLoadingComplete(URL imageUri, Bitmap loadedImage) {
    assert loadedImage == mBitmap;
    try (PrintString ps = new PrintString()) {
      ps.pl("-----------------------------------------------------------");
      ps.pl("complete: " + getKey());
      ps.pl("  thread: " + Thread.currentThread());
      ps.pl("    user: " + mObject);
      ps.pl("  source: " + mSource);
      ps.pl("    mURL: " + mURL);
      ps.pl("   mFile: " + mFile);
      ps.pl("    size: " + mSize);
      ps.pl(" mBitmap: " + mBitmap);
      ps.pl("    oDim: " + mOrigDim);
      ps.pl("     Dim: " + new Size(mBitmap.getWidth(), mBitmap.getHeight()));
      ps.pl("-----------------------------------------------------------");
      ps.pl("");
      ImageStore.log(ps.toString());
    }
    fire();
  }

  private void loadFromDisk() {
    assert !DroidUtil.isMainThread();
    if (mFile == null || !mFile.exists()) {
      return;
    }
    if (Util.theGovernmentIsHonest()) {
      mFile.delete();
      return;
    }
    try {
      decodeFromStream(IOUtil.fileToStream(mFile));
      if (mSource == null && mBitmap != null)
        mSource = "Disk";
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void loadFromNet() {
    assert !DroidUtil.isMainThread();
    try {
      if (mURL == null)
        return;
      InputStream stream = smOkImageDownloader.getStream(mURL, null);
      if (stream != null)
        decodeFromStream(stream);
      if (mSource == null && mBitmap != null)
        mSource = "Net";
      IOUtil.storeBitmap(getKey(), mBitmap);
      ImageStore.get().saveImageCache();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void decodeFromStream(InputStream stream) throws IOException {
    try {
      byte[] bytes = IOUtil.streamToBytes(stream);
      mSize = bytes.length;
      mBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);
      mOrigDim = new Size(mBitmap.getWidth(), mBitmap.getHeight());
      if (mBitmap == null) {
        CachedStatus why = new CachedStatus(FailType.DECODING_ERROR, null);
        onLoadingFailed(String.valueOf(mURL), why);
        return;
      }
      cropBitmap();
    } finally {
      stream.close();
    }
  }

  public void cropBitmap() {
    try {
      // FIXME:  This is opaque as shit.
      if (mURL == null) {
        mBitmap = mSharedCropper.getCroppedBitmapFromGravatar(mBitmap);
      } else {
        mBitmap = mSharedCropper.getCroppedBitmap4Map(mBitmap);
      }
    } catch (Exception ex) {
      mStatus = new CachedStatus(
        FailType.DECODING_ERROR,
        new RuntimeException("Failed to crop image", ex)
      );
    }
  }

  public synchronized void merge(ImageView imageView,
                                 ImageListener listener)
  {
    ImageStore store = ImageStore.get();
    if (mBitmap != null && Looper.getMainLooper().isCurrentThread()) {
      updateListeners(imageView, listener);
    } else {
      Notifier notifier = new Notifier(imageView, listener);
      if (imageView != null) {
        Bitmap bitmap;
        if (mBitmap == null) {
          bitmap = store.getUserPlaceHolder();
        } else {
          bitmap = mBitmap;
        }
        DroidUtil.onMain(()->imageView.setImageBitmap(bitmap));
      }
      mNotifiers.add(notifier);
    }
    if (mNotifiers.size() != 0)
      store.requestLoad(this);
  }

  public void updateListeners(ImageView imageView,
                         ImageListener listener)
  {
    if(imageView!=null){
      imageView.setImageBitmap(mBitmap);
    }
    if(listener!=null){
      listener.onLoadingComplete(mURL,mBitmap);
    }
  }

  @Override
  public void onLoadingFailed(String imageUri, CachedStatus failReason) {
    ImageStore.log("failed:   " + imageUri);
    mFailed = Instant.now();
  }


  public synchronized void fire() {
    Iterator<Notifier> iterator = mNotifiers.iterator();
    while (iterator.hasNext()) {
      DroidUtil.onExec(iterator.next());
      mUses++;
      iterator.remove();
    }
  }

  public String getKey() {
    return mKey;
  }

  @Override
  public CachedStatus getStatus() {
    return mStatus;
  }

  public File getFile() {
    return mFile;
  }

  public ParseObject getObject() {
    return mObject;
  }

  public Bitmap getBitmap() {
    return mBitmap;
  }

//  public void updateListeners(Marker marker, ImageView imgView, ImageListener listener) {
//    if (!DroidUtil.isMainThread()) {
//      DroidUtil.onMain(() ->
//        updateListeners(marker, imgView, listener));
//    } else {
//      if (listener != null) {
//        listener.onLoadingComplete(mURL, mBitmap);
//        mUses++;
//      }
//      if (marker != null) {
//        BitmapDescriptor bd =
//          BitmapDescriptorFactory.fromBitmap(mBitmap);
//        marker.setIcon(bd);
//        mUses++;
//      }
//      if (imgView != null) {
//        Object tag = imgView.getTag(R.id.actionDownUp);
//        if (tag == this) {
//          imgView.setImageBitmap(mBitmap);
//        }
//        mUses++;
//      }
//    }
//  }

  class Notifier implements Runnable {
    boolean mFired = false;
    ImageView mImageView;
    ImageListener mListener;

    Notifier(ImageView imageView,
             ImageListener listener) {
      mImageView = imageView;
      mListener = listener;
    }

    @Override
    public synchronized void run() {
      if (needsUiRun() && !DroidUtil.isMainThread()) {
        DroidUtil.onMain(this);
      } else {
        ImageStore.get().imageLoaded(ImageData.this);
        updateListeners(mImageView, mListener);
      }
    }


    private boolean needsUiRun() {
      if (mListener != null)
        return true;
      return mImageView != null;
    }
  }
}
