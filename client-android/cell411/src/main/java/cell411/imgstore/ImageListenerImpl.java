package cell411.imgstore;

import android.graphics.Bitmap;

import androidx.annotation.CallSuper;

import java.net.URL;

import cell411.utils.UrlUtils;

public class ImageListenerImpl implements ImageListener {
  @CallSuper
  public void onLoadingStarted(String s) {
    ImageStore.log("%s", s);
  }

  @CallSuper
  public void onLoadingFailed(String s, CachedStatus cachedStatus) {
    ImageStore.log("%s", cachedStatus);
  }

  @CallSuper
  public void onLoadingComplete(URL s, Bitmap bitmap) {
    ImageStore.log(UrlUtils.getFileName(s));
  }

  @CallSuper
  public void onLoadingCancelled(String s) {
    ImageStore.log(UrlUtils.getFileName(s));
  }

}
