package cell411.imgstore;

import android.graphics.BitmapFactory;

/**
 * Presents type of fail while image loading
 */
public enum FailType {
  /**
   * Input/output error. Can be caused by network communication fail or error while caching image on file system.
   */
  IO_ERROR,
  /**
   * Error while
   * {@linkplain BitmapFactory#decodeStream(java.io.InputStream, android.graphics.Rect, BitmapFactory.Options)
   * decode image to Bitmap}
   */
  DECODING_ERROR,
  /**
   * (boolean) Network * downloads are denied and requested
   * image wasn't cached in disk cache before.
   */
  NETWORK_DENIED,
  /**
   * Not enough memory to create needed Bitmap for image
   */
  OUT_OF_MEMORY,
  NEW_REQUEST,
  SUCCESS,
  CANCELED,
  /**
   * Unknown error was occurred while loading image
   */
  UNKNOWN
}
