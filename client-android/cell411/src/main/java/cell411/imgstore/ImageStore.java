package cell411.imgstore;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import cell411.json.JSONObject;
import cell411.parse.XUser;
import cell411.ui.base.BaseApp;
import cell411.utils.DroidUtil;
import cell411.utils.IOUtil;
import cell411.utils.ImageUtils;
import cell411.utils.ThreadUtil;
import cell411.utils.UrlUtils;
import cell411.utils.Util;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

public class ImageStore extends ContextWrapper {
  final static AtomicReference<ImageStore> smRef = new AtomicReference<>();
  final File mImageDir = getImageDir();

  private File getImageDir()
  {
    return IOUtil.mkdirs(new File(getCacheDir(),"images"));
  }

  final File mSaveFile = new File(mImageDir, "index.json");
  final List<ImageData> mPending;
  final private Drawable mFailedPlaceHolder;
  final private Drawable mLogo;
  final private Drawable mUserPlaceHolder;
  final private SharedCropper mSharedCropper;
  final private Bitmap mLogoBitmap;
  private final ArrayList<CreateOrMerge> smInstances = new ArrayList<>();
  private final XUser mFakeCurrentXUser = new XUser();
  private ImageCache mImageCache;
  private Future<ImageCache> mFutureImageCache;

  public ImageStore() {
    super(BaseApp.req());
    mPending = new ArrayList<>();
    mSharedCropper = SharedCropper.get();
    mUserPlaceHolder = this.mSharedCropper
      .getForDensity(R.drawable.ic_placeholder_user);
    mFailedPlaceHolder = this.mSharedCropper
      .getForDensity(R.drawable.ic_placeholder_user);
    mLogo = mSharedCropper.getForDensity(app().getApplicationInfo().icon);
    Bitmap temp = Bitmap.createBitmap(141, 141, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(temp);
    if (mLogo == null) {
      Paint green = new Paint();
      green.setColor(0xff00ff00);
      canvas.drawCircle(70, 70, 65, green);
    } else {
      mLogo.draw(canvas);
    }
    mLogoBitmap = temp;
    mFutureImageCache = DroidUtil.getExec().submit(this::loadImageCache);
    DroidUtil.onExec(() -> {
      try {
        mImageCache = mFutureImageCache.get();
      } catch (Exception e) {
        mImageCache = new ImageCache(new JSONObject());
        e.printStackTrace();
      }
      assert mImageCache != null;
    });
  }

  public static ImageStore get() {
    synchronized (smRef) {
      ImageStore store = Util.getRef(smRef);
      if (store == null) {
        if (!smRef.compareAndSet(null, new ImageStore()))
          throw new Error("What the fuck?");
        store = smRef.get();
      }
      return store;
    }
  }

  static XTAG TAG = new XTAG();
  public static void log(String fmt, Object... args) {
    XLog.i(TAG,fmt,args);
  }

  public final BaseApp app() {
    return (BaseApp) getApplicationContext();
  }

  private ImageCache loadImageCache() {
    if (mSaveFile.exists()) {
      try {
        Instant start = Instant.now();
        String text = IOUtil.fileToString(mSaveFile);
        Instant load = Instant.now();
        JSONObject jsonObject = new JSONObject(text);
        ImageCache res = new ImageCache(jsonObject);
        Instant parse = Instant.now();
        ImageStore.log("Load:  %10d",
          load.toEpochMilli() - start.toEpochMilli());
        ImageStore.log("parse: %10d",
          parse.toEpochMilli() - load.toEpochMilli());
        return res;
      } catch (Throwable t) {
        t.printStackTrace();
        log("Failed to load index.json");
      }
    } else {
      log("%s does not exist", mSaveFile);
      IOUtil.stringToFile(mSaveFile, "{}");
    }
    return new ImageCache(new JSONObject());
  }

  public void saveImageCache() {
    synchronized (this) {
      if (mImageCache == null)
        return;
      Instant start = Instant.now();
      JSONObject object = mImageCache.toJSON();
      File temp = new File(mImageDir, "index.new.json");
      Instant format = Instant.now();
      IOUtil.jsonToFile(temp, object);
      temp.renameTo(mSaveFile);
      Instant save = Instant.now();
      ImageStore.log("Format Time: %10d", format.toEpochMilli() - start.toEpochMilli());
      ImageStore.log("Save Time: %10d", save.toEpochMilli() - format.toEpochMilli());
    }
  }

  public Bitmap getUserPlaceHolder() {
    return ImageUtils.getPlaceHolder();
  }

  public Drawable getFailedPlaceHolder() {
    return mFailedPlaceHolder;
  }

  public Drawable getLogo() {
    return mLogo;
  }

  public SharedCropper getSharedCropper() {
    return mSharedCropper;
  }

  public int getGravatarSize() {
    return getSharedCropper().GRAVATAR_SIZE;
  }

  public URL uploadImage(File imageFile) {
    Callable<URL> uploader = new Uploader(imageFile);
    return null;
  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
  }

  public void loadImage(File path, ImageView imgUser) {
    createOrMerge(null, UrlUtils.toURL(path), imgUser, null);
  }

  private void createOrMerge(XUser user, URL url, ImageView imgUser, ImageListener listener) {
    if (user != null && !user.hasProfileImage())
      return;
    String key;
    if (url != null) {
      assert user == null;
    } else if (user == XUser.getCurrentUser()) {
      url = user.getAvatarUrl();
    } else if (user != null) {
      assert (url == null);
      url = user.getAvatarUrl();
    }
    key = UrlUtils.getFileName(url);
    if (key == null) {
      log("WARNING:  user and url are both null.  Fucking off");
      return;
    }
    CreateOrMerge op = new CreateOrMerge(key, user, url, imgUser, listener);
    if (mImageCache == null) {
      DroidUtil.onExec(op);
    } else {
      op.run();
    }
  }

  public void loadImage(XUser XUser, ImageListener listener) {
    createOrMerge(XUser, null, null, listener);
  }

  public void loadImage(XUser XUser, ImageView imgUser) {
    createOrMerge(XUser, null, imgUser, null);
  }

  public void imageLoaded(ImageData data) {
    synchronized (mPending) {
      mPending.remove(data);
    }
  }

  public void loadImage(XUser XUser) {
    loadImage(XUser, (ImageView) null);
  }

  public void requestLoad(ImageData data) {
    synchronized (mPending) {
      if (!mPending.contains(data)) {
        mPending.add(data);
        DroidUtil.onEither(false, data, 500);
      }
    }
  }

  private void loadCache() {
    assert !DroidUtil.isMainThread();
    try {
      mImageCache = mFutureImageCache.get();
    } catch (ExecutionException | InterruptedException e) {
      e.printStackTrace();
      throw Util.rethrow(e);
    }
    log("Loaded cache");
  }

  public Bitmap getLogoBitmap() {
    if (mLogoBitmap == null) {
      Drawable drawable = getLogo();
    }
    return mLogoBitmap;
  }

  static class Uploader implements Callable<URL> {
    private final XUser mCurrentXUser;
    private final File mImageFile;

    public Uploader(File imageFile) {
      mCurrentXUser = XUser.getCurrentUser();
      mImageFile = imageFile;
    }

    @Override
    public URL call() throws Exception {
      throw new RuntimeException("Not Implemented");
    }
  }

  class CreateOrMerge implements Runnable {
    @Nullable
    private final XUser mXUser;
    @Nullable
    private final URL mURL;
    @Nullable
    private final ImageView mImageView;
    @Nullable
    private final ImageListener mListener;
    @Nonnull
    private final String mKey;

    public CreateOrMerge(@Nonnull String key, @Nullable XUser XUser, @Nullable URL url,
                         @Nullable ImageView imageView,
                         @Nullable ImageListener listener
    ) {
      mKey = key;
      assert hasImageSuffix(mKey);
      mXUser = XUser;
      mURL = url;
      mImageView = imageView;
      mListener = listener;
      smInstances.add(this);
      log("%d instances", smInstances);
    }

    private boolean hasImageSuffix(String key)
    {
      return key.endsWith(".png") || key.endsWith(".jpg") ||
        key.endsWith(".jpeg");
    }

    @Override
    public void run() {
      ThreadUtil.waitUntil(this, ()->mImageCache!=null);
      ImageData imageData = mImageCache.get(mKey);
      if (imageData == null) {
        imageData = new ImageData(mKey, mXUser,
          null, mImageView, mListener);
        synchronized (this) {
          ImageData oldVal = mImageCache.get(imageData.getKey());
          assert oldVal == null || oldVal == imageData;
          if (oldVal == null)
            mImageCache.put(imageData);
        }
      }
      if (imageData.getBitmap() != null) {
        imageData.updateListeners(mImageView, mListener);
      } else {
        imageData.merge(mImageView, mListener);
      }
      smInstances.remove(this);
    }
  }
}
