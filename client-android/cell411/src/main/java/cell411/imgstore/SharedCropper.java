package cell411.imgstore;

import android.content.res.Resources;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import cell411.ui.base.BaseApp;
import cell411.utils.Util;

import javax.annotation.Nullable;
import java.lang.ref.WeakReference;


public class SharedCropper {
  static private WeakReference<SharedCropper> smRef;
  public final int GRAVATAR_SIZE;
  public final float mDensity;
  public final int GRAVATAR_SIZE_FULL_SCREEN;
  public final int screenWidth;
  public final int screenHeight;
  private final BaseApp mBaseApp;
  private final Resources mResources;

  public SharedCropper() {
    mBaseApp = BaseApp.req();
    assert mBaseApp != null;
    mResources = mBaseApp.getResources();
    assert mResources != null;
    screenWidth = getResources().getDisplayMetrics().widthPixels;
    screenHeight = getResources().getDisplayMetrics().heightPixels;
    mDensity = getResources().getDisplayMetrics().density;
    GRAVATAR_SIZE = Math.round(70 * mDensity + 0.5f);
    GRAVATAR_SIZE_FULL_SCREEN = Math.round(400 * mDensity + 0.5f);
  }

  public static synchronized SharedCropper get() {
    SharedCropper res = Util.getRef(smRef);
    if (res == null) {
      res = new SharedCropper();
      smRef = new WeakReference<>(res);
    }
    return res;
  }

  private Resources getResources() {
    return mResources;
  }

  public Bitmap getCroppedBitmapFromGravatar(Bitmap bitmap) {
    Bitmap output =
      Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    canvas.drawCircle(0.5f * bitmap.getWidth(), 0.5f * bitmap.getHeight(),
      0.5f * bitmap.getWidth(), paint);
    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    canvas.drawBitmap(bitmap, rect, rect, paint);
    return output;
  }

  public int getCy(Bitmap bitmap) {
    return bitmap.getHeight() / 2;
  }

  public int getCx(Bitmap bitmap) {
    return bitmap.getWidth() / 2;
  }

  public Bitmap getCroppedCircleBitmap4Map(Bitmap bitmap) {
    Bitmap output =
      Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
    canvas.drawCircle(getCx(bitmap), getCy(bitmap), getCx(bitmap), paint);
    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    canvas.drawBitmap(bitmap, rect, rect, paint);
    return Bitmap.createScaledBitmap(output, GRAVATAR_SIZE, GRAVATAR_SIZE, false);
  }

  public Bitmap getCroppedRoundBitmap(@NonNull Bitmap bitmap) {
    Bitmap output;
    if (bitmap.getWidth() < bitmap.getHeight()) {
      output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
    } else {
      output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    }
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    Rect rectDest;
    Rect rectSrc;
    if (bitmap.getWidth() < bitmap.getHeight()) {
      canvas.drawCircle(getCx(bitmap), getCy(bitmap), getCx(bitmap),
        paint);
      double diff = bitmap.getHeight() - bitmap.getWidth();
      rectSrc =
        new Rect(0, (int) diff / 2, bitmap.getWidth(), bitmap.getHeight() - ((int) diff / 2));
      rectDest = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
    } else {
      canvas.drawCircle(getCx(bitmap), getCy(bitmap), getCy(bitmap),
        paint);
      double diff = bitmap.getWidth() - bitmap.getHeight();
      rectSrc =
        new Rect((int) diff / 2, 0, bitmap.getWidth() - ((int) diff / 2), bitmap.getHeight());
      rectDest = new Rect(0, 0, bitmap.getHeight(), bitmap.getHeight());
    }
    canvas.drawBitmap(bitmap, rectSrc, rectDest, null);
    return output;
  }

  public Bitmap getCroppedBitmap4Map(Bitmap bitmap) {
    Bitmap output;
    if (bitmap.getWidth() < bitmap.getHeight()) {
      output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
    } else {
      output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    }
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    Rect rectDest;
    Rect rectSrc;
    if (bitmap.getWidth() < bitmap.getHeight()) {
      canvas.drawCircle(getCx(bitmap), getCy(bitmap), getCx(bitmap),
        paint);
      double diff = bitmap.getHeight() - bitmap.getWidth();
      rectSrc =
        new Rect(0, (int) diff / 2, bitmap.getWidth(), bitmap.getHeight() - ((int) diff / 2));
      rectDest = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
    } else {
      canvas.drawCircle(getCx(bitmap), getCy(bitmap), getCy(bitmap),
        paint);
      double diff = bitmap.getWidth() - bitmap.getHeight();
      rectSrc =
        new Rect((int) diff / 2, 0, bitmap.getWidth() - ((int) diff / 2), bitmap.getHeight());
      rectDest = new Rect(0, 0, bitmap.getHeight(), bitmap.getHeight());
    }
    canvas.drawBitmap(bitmap, rectSrc, rectDest, null);
    return getCroppedCircleBitmap4Map(output);
  }

  @SuppressWarnings("unused")
  public Bitmap getCroppedBitmap4Map2(Bitmap bitmap) {
    Bitmap output;
    if (bitmap.getWidth() < bitmap.getHeight()) {
      output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
    } else {
      output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    }
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    Rect rectDest;
    Rect rectSrc;
    if (bitmap.getWidth() < bitmap.getHeight()) {
      canvas.drawCircle(getCx(bitmap), getCy(bitmap), getCx(bitmap),
        paint);
      double diff = bitmap.getHeight() - bitmap.getWidth();
      rectSrc =
        new Rect(0, (int) diff / 2, bitmap.getWidth(), bitmap.getHeight() - ((int) diff / 2));
      rectDest = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
    } else {
      canvas.drawCircle(getCx(bitmap), getCy(bitmap), getCy(bitmap),
        paint);
      double diff = bitmap.getWidth() - bitmap.getHeight();
      rectSrc =
        new Rect((int) diff / 2, 0, bitmap.getWidth() - ((int) diff / 2), bitmap.getHeight());
      rectDest = new Rect(0, 0, GRAVATAR_SIZE, GRAVATAR_SIZE);
    }
    canvas.drawBitmap(bitmap, rectSrc, rectDest, paint);
    BitmapShader shader;
    shader = new BitmapShader(output, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
    Paint paint2 = new Paint();
    paint2.setAntiAlias(true);
    paint2.setShader(shader);
    RectF rect = new RectF(0.0f, 0.0f, GRAVATAR_SIZE, GRAVATAR_SIZE);
    // rect contains the bounds of the shape
    // radius is the radius in pixels of the rounded corners
    // paint contains the shader that will texture the shape
    canvas.drawRoundRect(rect, 0.5f * GRAVATAR_SIZE, 0.5f * GRAVATAR_SIZE, paint2);
    // FIXME:  I'm not sure if these next two lines are useful.
    //output = Bitmap.createScaledBitmap(output, GRAVATAR_SIZE,
    //GRAVATAR_SIZE, false);
    return output;
  }

  @Nullable
  public Drawable getForDensity(@DrawableRes int res) {
    Resources.Theme theme = mBaseApp.getTheme();
    Resources store = mResources;
    if (theme == null || store == null || res == 0)
      return null;
    return ResourcesCompat.getDrawableForDensity(
      store, res, (int) mDensity, theme);
  }

  public Drawable getForDensity(int resId, int tint) {
    Resources.Theme theme = mBaseApp.getTheme();
    Resources store = mResources;
    if (theme == null || store == null || resId == 0)
      return null;
    Drawable res = ResourcesCompat.getDrawableForDensity(
      store, resId, (int) mDensity, theme);
    if(res==null)
      return null;
    PorterDuff.Mode mode = PorterDuff.Mode.XOR;
    res.setColorFilter( new PorterDuffColorFilter(tint, mode));
    return res;
  }

  public float getDensity() {
    return mDensity;
  }
}
