package cell411.imgstore;

import cell411.utils.IOUtil;
import cell411.utils.UrlType;
import cell411.utils.Util;
import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class OkImageDownloader {
  final OkHttpClient mClient;

  public OkImageDownloader(OkHttpClient client) {
    mClient = client;
  }

  public InputStream getStream(String strUrl, Object extra) throws IOException {
    return getStream(new URL(strUrl), extra);
  }

  public InputStream getStream(URL url, Object extra) throws IOException {
    if (UrlType.forUrl(url) == UrlType.file) {
      String strUrl = url.getPath();
      if (Util.isNoE(strUrl))
        return null;
      File file = new File(strUrl);
      if (!file.exists())
        return null;
      return IOUtil.fileToStream(file);
    } else {
      Request req = createRequest(url);
      Call call = mClient.newCall(req);
      try (Response res = call.execute()) {
        int code = res.code();
        if (res.isSuccessful()) {
          ResponseBody body = res.body();
          if(body==null){
            return new ByteArrayInputStream(new byte[0]);
          }
          byte[] content = body.bytes();
          return new ByteArrayInputStream(content);
        } else {
          return null;
        }
      }
    }
  }

  private Request createRequest(URL imageUri) {
    Builder builder = new Builder();
    builder.get();
    builder.url(imageUri);
    builder.cacheControl(new CacheControl.Builder().noCache().build());
    return builder.build();
  }

}
