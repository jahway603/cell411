package cell411.imgstore;

public interface CachedResource {
  CachedStatus getStatus();
}
