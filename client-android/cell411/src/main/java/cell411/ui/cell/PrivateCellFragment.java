package cell411.ui.cell;

import static cell411.utils.ViewType.vtNull;
import static cell411.utils.ViewType.vtPrivateCell;
import static cell411.utils.ViewType.vtString;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Comparator;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.RelationWatcher;
import cell411.logic.rel.Rel;
import cell411.parse.XPrivateCell;
import cell411.parse.util.XItem;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.FragmentFactory;
import cell411.utils.Reflect;
import cell411.utils.ViewType;

/**
 * Created by Sachin on 18-04-2016.
 */
public class PrivateCellFragment extends BaseFragment {
  public static final String BROADCAST_ACTION_NEW_PRIVATE_CELL =
    "com.safearx.cell411.NEW_PRIVATE_CELL_RECEIVER";
  private static final Comparator<? super XItem> smComparator =
    Comparator.comparing(XItem::getText).thenComparing(XItem::getObjectId);
  private final CellListAdapter mCellListAdapter =
    new CellListAdapter();
  private final LiveQueryService mService;
  private final FragmentFactory mMemberFragmentFactory =
    FragmentFactory.fromClass(PrivateCellMemberFragment.class);
  private RelationWatcher mRelations;
  private Rel mOwnedCells;
  private Rel mJoinedCells;
  private RecyclerView mRecyclerView;

  public PrivateCellFragment() {
    super(R.layout.fragment_private_cells);
    mService = LiveQueryService.opt();
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState) {
    Reflect.announce(true);
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    FloatingActionButton fab = view.findViewById(R.id.fab);
    mRecyclerView = view.findViewById(R.id.fragment_private_cells);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    mRecyclerView.setAdapter(mCellListAdapter);
    refresh();
  }

  public void loadData1() {
    super.loadData1();
  }

  public void loadData3() {
    super.loadData3();
  }

  public void loadData2() {
    super.loadData2();
    mRelations = mService.getRelationWatcher();
    mOwnedCells = mRelations.getOwnedCells("PrivateCell");

    mOwnedCells.deleteObserver(mCellListAdapter);
    mOwnedCells.addObserver(mCellListAdapter);

    mCellListAdapter.update(mOwnedCells, null);
    Reflect.announce(false);
  }

  // Provide a reference to the views for each data item
  // Complex data items may need more than one view per item, and
  // you provide access to all the views for a data item in a view holder
  public static class ViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
    private TextView txtCellName;
    private TextView txtInfo;

    public ViewHolder(View view, int type) {
      super(view);
      if (type == vtPrivateCell.ordinal()) {
        txtCellName = view.findViewById(R.id.txt_cell_name);
      } else {
        txtInfo = view.findViewById(R.id.txt_info);
      }
    }
  }

  public class CellListAdapter extends RecyclerView.Adapter<ViewHolder> implements MyObserver {
    public final ArrayList<XItem> mItems = new ArrayList<>();
    private final View.OnClickListener mOnClickListener = this::onClick;
    private final View.OnLongClickListener mOnLongClickListener = this::onLongClick;

    public CellListAdapter() {
      mItems.add(new XItem("", "Loading Data..."));
    }

    public void onClick(View v) {
      int position = mRecyclerView.getChildAdapterPosition(v);
      XItem item = mItems.get(position);
      if (item.getViewType() != vtPrivateCell) {
        return;
      }
      XPrivateCell cell = item.getPrivateCell();
      mMemberFragmentFactory.setObjectId(cell.getObjectId());
      push(mMemberFragmentFactory);
    }

    public boolean onLongClick(View v) {
      int position = mRecyclerView.getChildAdapterPosition(v);
      if (position >= mItems.size()) {
        return false;
      }
      XItem item = mItems.get(position);
      if (item.getViewType() != vtPrivateCell) {
        return false;
      }
      final XPrivateCell privateCell = item.getPrivateCell();
      if (privateCell.getCellType() == 5) {
        CellDialogs.showDeleteCellDialog(getActivity(), privateCell, success ->
        {
          if (success) {
            showToast("Cell " + privateCell.getName() + " deleted");
          } else {
            showToast("Cell " + privateCell.getName() + " not deleted");
          }
          refresh();
        });
      } else {
        showToast("Default Cells cannot be deleted");
      }
      return false;
    }

    // Create new views (invoked by the layout manager)
    @Nonnull
    @Override
    public PrivateCellFragment.ViewHolder onCreateViewHolder(@Nonnull ViewGroup parent,
                                                             int viewType) {
      // create a new view
      View v = null;
      if (viewType == vtPrivateCell.ordinal()) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_private_cell, parent, false);
      } else if (viewType == vtString.ordinal()) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      } else if (viewType == vtNull.ordinal()) {
        v = new View(getContext());
      }
      // set the view's size, margins, padding's and layout parameters
      PrivateCellFragment.ViewHolder vh = new ViewHolder(v, viewType);
      if (v != null) {
        if (viewType == vtPrivateCell.ordinal()) {
          v.setOnClickListener(mOnClickListener);
          v.setOnLongClickListener(mOnLongClickListener);
        }
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final @Nonnull PrivateCellFragment.ViewHolder viewHolder,
                                 final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      XItem item = mItems.get(position);
      ViewType viewType = item.getViewType();
      if (viewType == vtPrivateCell) {
        viewHolder.txtCellName.setText(item.getPrivateCell().getName());
      } else if (viewType == vtString) {
        viewHolder.txtInfo.setText(item.getText());
      } else if (viewType == vtNull) {
        Reflect.announce("Say nothing, act natural");
      } else {
        throw new IllegalArgumentException("viewType==" + viewType);
      }
    }

    public ViewType getViewType(int position) {
      return getItem(position).getViewType();
    }

    @Override
    public int getItemViewType(int position) {
      return getViewType(position).ordinal();
    }

    private XItem getItem(int position) {
      return mItems.get(position);
    }

    @Override
    public int getItemCount() {
      return mItems.size();
    }

    //    public void change() {
    //      ArrayList<XPrivateCell> objects = new ArrayList<>(mPrivatCellWatcher.getValues());
    //      XUser user = XUser.getCurrentUser();
    //      objects.removeIf((cell)-> !user.hasSameId(cell.getOwner()));
    //      int oldSize = mItems.size();
    //      int newSize = objects.size();
    //      ArrayList<XItem> newItems = Util.transform(objects, XItem::new);
    //      mItems.clear();
    //      mItems.addAll(newItems);
    //      mItems.sort(smComparator);
    //      if(oldSize < newSize) {
    //        if(oldSize>0)
    //          notifyItemRangeChanged(0,oldSize);
    //        notifyItemRangeInserted(oldSize,newSize);
    //      } else if ( newSize < oldSize) {
    //        if(newSize>0)
    //          notifyItemRangeChanged(0,newSize);
    //        notifyItemRangeRemoved(newSize,oldSize);
    //      } else {
    //        notifyItemRangeChanged(0,newSize);
    //      }
    //    }

    @Override
    public void update(final MyObservable o, final Object arg) {
      System.out.println("update(" + o + ", " + arg + ")");
      if (o == mOwnedCells) {
        mItems.clear();
        for (String id : mOwnedCells.getRelatedIds()) {
          XPrivateCell cell = LiveQueryService.getObject(id);
          XItem item = new XItem(cell);
          mItems.add(item);
        }
        onUI(this::notifyDataSetChanged);
      } else {
        throw new RuntimeException("Unexpected Observable");
      }
    }
  }
}

