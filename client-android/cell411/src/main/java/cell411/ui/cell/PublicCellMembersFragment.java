package cell411.ui.cell;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cell411.enums.CellCategory;
import cell411.enums.CellStatus;
import cell411.enums.RequestType;
import cell411.logic.LiveQueryService;
import cell411.logic.RelationWatcher;
import cell411.logic.rel.Rel;
import cell411.parse.XPublicCell;
import cell411.parse.XUser;
import cell411.parse.util.XItem;
import cell411.services.DataService;
import cell411.ui.base.BaseActivity;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.FragmentFactory.TypedFactory;
import cell411.ui.friend.AddFriendModules;
import cell411.ui.profile.UserFragment;
import cell411.ui.widget.CircularImageView;
import cell411.utils.Collect;
import cell411.utils.Reflect;
import cell411.utils.Util;
import cell411.utils.ViewType;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.parse.ParseQuery;
import com.parse.model.ParseUser;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static cell411.utils.ViewType.vtString;
import static cell411.utils.ViewType.vtUser;

/**
 * Created by Sachin on 7/13/2015.
 */
@SuppressLint("NotifyDataSetChanged")
public class PublicCellMembersFragment extends BaseFragment {
  private static final XTAG TAG = new XTAG();
  private final static FragmentFactory mUserFragmentFactory =
    FragmentFactory.fromClass(UserFragment.class);
  private final MemberListAdapter mAdapter = new MemberListAdapter();
  private final List<XItem> mMemberList = new ArrayList<>();
  private final TypedFactory<PublicCellCreateOrEditFragment> mCreateOrEditFactory =
    FragmentFactory.fromClass(PublicCellCreateOrEditFragment.class);
  private final LiveQueryService mLiveQueryService;
  private TextView mTxtStatus;
  private TextView mTxtTotalMembers;
  private FloatingActionButton mFabChat;
  private TextView mTxtCity;
  private TextView mTxtCellName;
  private TextView mTxtCellCategory;
  private TextView mTxtDescription;
  private boolean mShowJoin = false;
  private RecyclerView mRecyclerView;
  private String mObjectId;
  private XPublicCell mPublicCell;
  private Rel mFriendRel;
  private Button mEditButton;
  private Button mDoneButton;
  private RelationWatcher mRelationWatcher;

  public PublicCellMembersFragment() {
    super(R.layout.fragment_public_cell_members);
    mLiveQueryService = LiveQueryService.opt();
    if (mLiveQueryService == null) {
      return;
    }
    mRelationWatcher = mLiveQueryService.getRelationWatcher();
  }

  private static void showRequestVerificationDialog(BaseActivity ignored1, XPublicCell ignored2) {
    //    AlertDialog.Builder alert = new AlertDialog.Builder(activity);
    //    alert.setTitle(R.string.request_verification);
    //    LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Service
    //    .LAYOUT_INFLATER_SERVICE);
    //    View view = inflater.inflate(R.layout.layout_request_verification, null);
    //    final TextView txtBtnRequestVerification = view.findViewById(R.id
    //    .txt_btn_request_verification);
    //    final int verificationStatus = publicCell.getVerificationStatus();
    //    XLog.i(TAG, "verificationStatus: " + verificationStatus);
    //    if (verificationStatus == 1) { // APPROVED
    //      txtBtnRequestVerification.setText(R.string.officially_verified);
    //      txtBtnRequestVerification.setBackgroundColor(Color.parseColor("#008000"));
    //      txtBtnRequestVerification.setEnabled(false);
    //    } else if (verificationStatus == -1) { // PENDING
    //      txtBtnRequestVerification.setText(R.string.verification_pending);
    //      txtBtnRequestVerification.setBackgroundColor(Color.GRAY);
    //      txtBtnRequestVerification.setEnabled(false);
    //    } else if (verificationStatus == -2) { // REJECTED
    //      txtBtnRequestVerification.setText(R.string.not_verified);
    //      txtBtnRequestVerification.setBackgroundColor(Color.RED);
    //      txtBtnRequestVerification.setEnabled(false);
    //    }
    //    alert.setView(view);
    //    alert.setPositiveButton(R.string.dialog_btn_done, (dialog, which) -> dialog.dismiss());
    //    final AlertDialog dialog = alert.create();
    //    txtBtnRequestVerification.setOnClickListener(v -> {
    //      if (publicCell.getVerificationStatus() != -1) {
    //        requestVerificationOfPublicCell(activity, publicCell, txtBtnRequestVerification);
    //        publicCell.setVerificationStatus(-1);
    //      }
    //    });
    //    dialog.show();
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState) {
    Reflect.announce(true);
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  //  private static void requestVerificationOfPublicCell(final BaseActivity activity,
  //                                                      final XPublicCell mPublicCell,
  //                                                      final TextView txtBtnRequestVerification)
  //  {
  //    mPublicCell.setVerificationStatus(-1);
  //    mPublicCell.saveInBackground(e ->
  //    {
  //      if (e != null) {
  //        activity.handleException("updating verification status", e, null, true);
  //        return;
  //      }
  //      // Verification request sent successfully
  //      txtBtnRequestVerification.setText(R.string.verification_pending);
  //      txtBtnRequestVerification.setBackgroundColor(Color.GRAY);
  //      txtBtnRequestVerification.setEnabled(false);
  //    });
  //  }
  @Override
  public void onViewCreated(@Nonnull final View view, @Nullable final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    Bundle arguments = getArguments();
    String objectId;
    if (arguments != null) {
      objectId = arguments.getString("objectId");
    } else {
      showAlertDialog("No objectId Provided", success -> pop());
      return;
    }
    mObjectId = objectId;
    mTxtCellName = findViewById(R.id.txt_cell_name);
    mTxtCellCategory = findViewById(R.id.txt_cell_category);
    mTxtDescription = findViewById(R.id.txt_description);
    mTxtCity = findViewById(R.id.txt_city);
    mTxtStatus = findViewById(R.id.txt_status);
    mTxtTotalMembers = findViewById(R.id.txt_total_members);
    mFabChat = findViewById(R.id.fab_request_ride);
    mShowJoin = false;
    mDoneButton = findViewById(R.id.done);
    mDoneButton.setOnClickListener(this::onButtonClicked);
    mEditButton = findViewById(R.id.edit);
    mEditButton.setOnClickListener(this::onButtonClicked);
    mFabChat.setOnClickListener(this::onButtonClicked);
    mRecyclerView = findViewById(R.id.list_members);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
    mRecyclerView.setHasFixedSize(true);
    mRecyclerView.setAdapter(mAdapter);
    mTxtTotalMembers.setOnClickListener(System.out::println);
  }

  private void onButtonClicked(final View view) {
    if (view == mEditButton) {
      mCreateOrEditFactory.setObjectId(mPublicCell.getObjectId());
      push(mCreateOrEditFactory);
    } else if (view == mDoneButton) {
      finish();
    } else if (view == mFabChat) {
      Cell411.req().openChat(mPublicCell);
    }
  }

  public void loadData2() {
    super.loadData2();
    if (mObjectId != null) {
      mPublicCell = LiveQueryService.getObject(mObjectId);
      if (mPublicCell == null) {
        mPublicCell = XPublicCell.q().get(mObjectId);
      }
    }
    if (mPublicCell == null) {
      showAlertDialog("Unable to load cell " + mObjectId, success -> pop());
    }

    mFriendRel = mRelationWatcher.getFriends();
    Rel memberIds = mRelationWatcher.getMemberRel(mPublicCell);
    if (memberIds == null) {
//      ParseRelation<XUser> memberRel = mPublicCell.getRelation("memberRel");
//      memberIds = mRelationWatcher.getRel(memberRel);
//      mLiveQueryService.getUserWatcher().addRel(memberIds);
//      ParseQuery<XUser> query = memberRel.getQuery();
//      List<XUser> list = query.find();
//      memberIds.addAll(Util.transform(list, ParseObject::getObjectId));
    }
    ArrayList<XUser> memberList = new ArrayList<>();
    ArrayList<String> temp = new ArrayList<>();
    for (String id : memberIds.getRelatedIds()) {
      XUser user = ds().getUser(id);
      if (user == null) {
        temp.add(id);
      } else {
        memberList.add(user);
      }
    }
    if (!temp.isEmpty()) {
      ParseQuery<XUser> users = XUser.q();
      users.whereContainedIn("objectId", temp);
      memberList.addAll(DataService.findFully(users));
    }
    Collect.replaceAll(mMemberList, Util.transform(memberList, XItem::new));
  }

  public void loadData3() {
    super.loadData3();
    if (mPublicCell == null) {
      showAlertDialog("Unable to load public cell " + mObjectId);
      return;
    }
    CellStatus status = mPublicCell.getStatus();
    if (mEditButton != null) {
      mEditButton.setEnabled(false);
    }
    if (status == CellStatus.OWNER) {
      mFabChat.show();
      mTxtStatus.setText(R.string.you_are_the_owner_of_this_cell);
      mEditButton.setEnabled(true);
    } else if (status == CellStatus.JOINED) {
      mFabChat.show();
      mTxtStatus.setText(R.string.you_are_a_member_of_this_cell);
    } else if (status == CellStatus.PENDING) {
      mFabChat.hide();
      mTxtStatus.setText(R.string.request_sent_for_approval);
    } else if (status == CellStatus.NOT_JOINED) {
      mFabChat.hide();
      mTxtStatus.setText(R.string.you_are_not_a_member_of_this_cell);
      mShowJoin = true;
    } else {
      String text = "Unexpected status: " + status.toString();
      mTxtStatus.setText(text);
      onRF(this::refresh, 1000);
    }
    mTxtCellName.setText(mPublicCell.getName());
    CellCategory category = mPublicCell.getCategory();
    mTxtCellCategory.setText(category.toString());
    if (mPublicCell.getDescription() != null) {
      mTxtDescription.setText(mPublicCell.getDescription());
    }
    if (mPublicCell.getLocation() != null) {
      String text = "Waiting for city";
      mTxtCity.setText(text);
      ds().requestCity(mPublicCell.getLocation(), address -> mTxtCity.setText(address.cityPlus()));
    } else {
      mTxtCity.setText(Util.format("No location on file"));
    }
    if (mMemberList.isEmpty()) {
      mTxtTotalMembers.setText(R.string.no_members);
    } else {
      mMemberList.sort(this::nameCompare);
      String member_count = Util.format(R.string.members, mMemberList.size() - 1);
      mTxtTotalMembers.setText(member_count);
    }
    mAdapter.run();
  }

  private int nameCompare(final XItem lhs, final XItem rhs) {
    if (lhs.getViewType() != rhs.getViewType()) {
      return Integer.compare(lhs.getViewType().ordinal(), rhs.getViewType().ordinal());
    } else if (lhs.getViewType() == vtUser) {
      return lhs.getUser().nameCompare(rhs.getUser());
    } else {
      return lhs.getObjectId().compareTo(rhs.getObjectId());
    }
  }

  //  @Override
  //  public boolean onCreateOptionsMenu(Menu menu) {
  //    if (mShowJoin) {
  //      MenuInflater inflater = getMenuInflater();
  //      inflater.inflate(R.menu.menu_public_cell_members, menu);
  //      mJoinMenuItem = menu.findItem(R.id.action_join);
  //      return true;
  //    } else {
  //      if (mPublicCell.getStatus() == CellStatus.OWNER) {
  //        MenuInflater inflater = getMenuInflater();
  //        inflater.inflate(R.menu.menu_request_verification, menu);
  //        return true;
  //      } else {
  //        return super.onCreateOptionsMenu(menu);
  //      }
  //    }
  //  }
  //
  //  @Override
  //  public boolean onPrepareOptionsMenu(Menu menu) {
  //    if (!mShowJoin && mJoinMenuItem != null) {
  //      mJoinMenuItem.setVisible(false);
  //    } else if (mJoinMenuItem != null) {
  //      if (mPublicCell.getStatus() == CellStatus.JOINED) {
  //        mJoinMenuItem.setTitle(R.string.leave_this_cell);
  //      } else {
  //        mJoinMenuItem.setTitle(R.string.join_this_cell);
  //      }
  //    }
  //    return super.onPrepareOptionsMenu(menu);
  //  }

  private void showRemoveMemberDialog(XUser user) {
    AlertDialog.Builder alert = new AlertDialog.Builder(activity());
    alert.setMessage(getString(R.string.dialog_msg_remove_member, user.getName()));
    alert.setNegativeButton(R.string.dialog_btn_no, (dialogInterface, i) ->
    {
    });
    alert.setPositiveButton(R.string.dialog_btn_yes, (dialogInterface, i) ->
    {
      RelationWatcher watcher = LiveQueryService.opt().getRelationWatcher();
      watcher.removeCellMember(mPublicCell, user);
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == android.R.id.home) {
      finish();
      return true;
    } else if (itemId == R.id.action_join) {
      if (mShowJoin) {
        if (mPublicCell != null) {
          if (mPublicCell.getStatus() == CellStatus.JOINED) {
            mFabChat.hide();
            CellDialogs.showLeaveCellDialog(activity(), mPublicCell, null);
          } else {
            CellDialogs.joinCell(mPublicCell, null);
          }
        } else {
          showToast(R.string.please_wait);
        }
      }
      return true;
    } else if (itemId == R.id.action_request_verification) {
      showRequestVerificationDialog(activity(), mPublicCell);
      return true;
    } else if (itemId == R.id.action_edit_cell) {
      mCreateOrEditFactory.setObjectId(mPublicCell.getObjectId());
      push(mCreateOrEditFactory);
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  public void setPublicCell(final XPublicCell publicCell) {
    mPublicCell = publicCell;
  }

  private static class ViewHolder extends RecyclerView.ViewHolder {
    private TextView txtUserName;
    private CircularImageView imgUser;
    private TextView txtRemove;
    private TextView txtBtnAddFriend;

    public ViewHolder(@Nonnull View itemView) {
      super(itemView);
    }
  }

  private class MemberListAdapter extends RecyclerView.Adapter<ViewHolder> implements Runnable {
    final ArrayList<XItem> mItems = new ArrayList<>();

    public MemberListAdapter() {
      super();
    }

    //    public void replaceItems(List<XItem> items)
    //    {
    //      mItems.clear();
    //      mItems.addAll(items);
    //      notifyDataSetChanged();
    //    }

    @Nonnull
    @Override
    public ViewHolder onCreateViewHolder(@Nonnull ViewGroup parent, int i) {
      ViewType viewType = ViewType.valueOf(i);
      View v;
      if (viewType == vtString) {
        v = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cell_public_cell_title, parent, false);
      } else if (viewType == ViewType.vtUser) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_user, parent, false);
      } else {
        throw new RuntimeException("Bad ViewType: " + viewType);
      }
      return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@Nonnull ViewHolder viewHolder, int position) {
      final XItem item = mItems.get(position);
      ViewType viewType = item.getViewType();
      XUser currentUser = (XUser) ParseUser.getCurrentUser();

      if (viewType == ViewType.vtUser) {
        viewHolder.txtUserName = viewHolder.itemView.findViewById(R.id.name);
        viewHolder.imgUser = viewHolder.itemView.findViewById(R.id.avatar);
        viewHolder.txtBtnAddFriend = viewHolder.itemView.findViewById(R.id.txt_btn_action);
        viewHolder.txtRemove = viewHolder.itemView.findViewById(R.id.txt_btn_neg);
        final XUser user = item.getUser();
        viewHolder.itemView.setOnClickListener(this::onUserClick);
        viewHolder.txtUserName.setText(user.getName());
        viewHolder.txtUserName.setOnClickListener(this::onUserClick);
        viewHolder.imgUser.setImageBitmap(user.getAvatarPic());
        final String userId = user.getObjectId();
        String currentUserId = currentUser.getObjectId();
        final String creatorUserId = mPublicCell.getOwner().getObjectId();
        if (creatorUserId.equals(currentUserId) && !userId.equals(currentUserId)) {
          viewHolder.txtRemove.setVisibility(View.VISIBLE);
          viewHolder.txtRemove.setOnClickListener(v ->
          {
            XLog.i(TAG, "remove clicked");
            showRemoveMemberDialog(user);
          });
        } else {
          viewHolder.txtRemove.setVisibility(View.GONE);
          viewHolder.txtRemove.setOnClickListener(null);
        }
        viewHolder.txtBtnAddFriend.setVisibility(View.VISIBLE);
        viewHolder.txtBtnAddFriend.setOnClickListener(null);
        if (currentUser.getObjectId().equals(userId)) {
          viewHolder.txtBtnAddFriend.setVisibility(View.GONE);
        } else if (mFriendRel.contains(user.getObjectId())) {
          viewHolder.txtBtnAddFriend.setText(R.string.un_friend);
          viewHolder.txtBtnAddFriend.setEnabled(true);
          viewHolder.txtBtnAddFriend.setOnClickListener(view ->
          {
            XLog.i(TAG, "delete friend clicked");
            AddFriendModules.showDeleteFriendDialog(activity(), user, null);
          });
        } else {
          viewHolder.txtBtnAddFriend.setText(R.string.add_friend);
          viewHolder.txtBtnAddFriend.setEnabled(true);
          viewHolder.txtBtnAddFriend.setOnClickListener(v ->
          {
            ds().handleRequest(RequestType.FriendRequest, user, null, null);
            viewHolder.txtBtnAddFriend.setText(R.string.resend);
            viewHolder.txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
          });
        }
      } else if (viewType == vtString) {
        String text = item.getText();
        XLog.i(TAG, "text: " + text);
      } else {
        throw new IllegalArgumentException("Unexpected ViewType: " + viewType);
      }
    }

    private void onUserClick(View view) {
      while (view != null && view.getParent() != mRecyclerView) {
        view = (View) view.getParent();
      }
      if (view == null) {
        return;
      }
      int pos = mRecyclerView.getChildAdapterPosition(view);
      XItem item = mItems.get(pos);
      if (item.getViewType() != ViewType.vtUser) {
        return;
      }
      XUser user = item.getUser();
      FragmentFactory factory = mUserFragmentFactory;
      factory.setObjectId(user.getObjectId());
      push(factory);
    }

    @Override
    public int getItemViewType(int position) {
      return mItems.get(position).getViewType().ordinal();
    }

    @Override
    public int getItemCount() {
      return mItems.size();
    }

    @Override
    public void onDetachedFromRecyclerView(@Nonnull RecyclerView recyclerView) {
      super.onDetachedFromRecyclerView(recyclerView);
    }

    public void run() {
      HashSet<String> oldIds = new HashSet<>();
      for (XItem item : mItems) {
        oldIds.add(item.getObjectId());
      }
      HashSet<String> newIds = new HashSet<>();
      for (XItem xItem : mMemberList) {
        newIds.add(xItem.getObjectId());
      }
      if (oldIds.equals(newIds)) {
        return;
      }
      HashSet<String> removedIds = new HashSet<>(oldIds);
      for (String id : newIds) {
        removedIds.remove(id);
      }
      if (!removedIds.isEmpty()) {
        for (int i = 0; i < mItems.size(); i++) {
          if (removedIds.contains(mItems.get(i).getObjectId())) {
            mItems.remove(i);
            notifyItemRemoved(i);
            --i;
          }
        }
      }
      HashSet<String> addedIds = new HashSet<>(newIds);
      for (String id : oldIds) {
        addedIds.remove(id);
      }
      for (XItem item : mMemberList) {
        if (addedIds.contains(item.getObjectId())) {
          int oldSize = mItems.size();
          mItems.add(item);
          notifyItemInserted(oldSize);
        }
      }
    }
  }
}
