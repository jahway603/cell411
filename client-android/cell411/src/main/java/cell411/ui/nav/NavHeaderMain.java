package cell411.ui.nav;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cell411.libcell.ConfigDepot;
import cell411.parse.XUser;
import cell411.ui.base.BaseContext;
import cell411.utils.Reflect;
import cell411.utils.Util;
import cell411.utils.ValueObserver;
import com.parse.Parse;
import com.parse.model.ParseUser;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nullable;

public class NavHeaderMain
  extends RelativeLayout
  implements BaseContext,
             Runnable,
             ValueObserver<Bitmap>
{
  private ImageView imgUser;
  private TextView txtName;
  private TextView txtEmail;
  private TextView txtBloodGroup;
  private TextView txtVersion;

  public NavHeaderMain(Context context)
  {
    this(context, null);
  }

  public NavHeaderMain(Context context, AttributeSet attrs)
  {
    this(context, attrs, 0);
  }

  public NavHeaderMain(Context context, AttributeSet attrs, int defStyleAttr)
  {
    this(context, attrs, defStyleAttr, 0);
  }

  public NavHeaderMain(Context context, AttributeSet attrs, int defStyleAttr,
                       int defStyleRes)
  {
    super(context, attrs, defStyleAttr, defStyleRes);
  }

  public String getVersion()
  {
    return Cell411.req().getVersion();
  }

  @Override
  protected void onAttachedToWindow()
  {
    Reflect.announce(this);
    super.onAttachedToWindow();
  }

  @Override
  protected void onDetachedFromWindow()
  {
    Reflect.announce(this);
    super.onDetachedFromWindow();
  }

  public void run()
  {
    if (!isOnUI()) {
      onUI(this);
      return;
    }
    if (!Parse.isInitialized()) {
      onUI(this, 500);
      return;
    }
    XUser currentUser = (XUser) ParseUser.getCurrentUser();
    if (currentUser == null) {
      onUI(this, 1000);
      return;
    }
    if (imgUser == null) {
      imgUser = findViewById(R.id.avatar);
      txtName = findViewById(R.id.name);
      txtEmail = findViewById(R.id.txt_email);
      txtBloodGroup = findViewById(R.id.txt_blood_group);
      txtVersion = findViewById(R.id.txt_version);
    }
    XUser user = (XUser) ParseUser.getCurrentUser();
    if (user != null) {
      setVisibility(VISIBLE);
      user.addAvatarObserver(new ValueObserver<Bitmap>()
      {
        @Override
        public void onChange(@Nullable Bitmap newValue,
                             @Nullable Bitmap oldValue)
        {
          if(newValue!=null){
            imgUser.setImageBitmap(newValue);
          }
        }
      });
      Bitmap avatar = user.getAvatarPic();
      if (avatar != null) {
        imgUser.setImageBitmap(avatar);
      }
      if (txtName != null) {
        txtName.setText(Util.nvl(user.getName(), "no name"));
      }
      if (txtEmail != null) {
        txtEmail.setText(Util.nvl(user.getEmail(), "no email"));
      }
      if (txtBloodGroup != null) {
        txtBloodGroup.setText(Util.nvl(user.getBloodType(), "N/A"));
      }
      if (txtVersion != null) {
        txtVersion.setText(getVersion());
      }
    } else {
      setVisibility(GONE);
    }
  }

  public void onChange(@Nullable Bitmap newValue, @Nullable Bitmap oldValue)
  {
    ParseUser user = ParseUser.getCurrentUser();
    Bitmap bitmap = null;
    if (user != null) {
      XUser xUser = (XUser) user;
      if(Util.theGovernmentIsLying())
        throw new RuntimeException();
      else
        bitmap = xUser.getAvatarPic();
    }
    if (bitmap == null)
      bitmap = ConfigDepot.getPlaceHolder();
    Bitmap finalBitmap = bitmap;
    Runnable runnable = () -> imgUser.setImageBitmap(finalBitmap);
    if (isOnUI())
      runnable.run();
    else
      onUI(runnable);
  }
}
