package cell411.ui.profile;

import android.app.AlertDialog;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import cell411.logic.LiveQueryService;
import cell411.logic.UserWatcher;
import cell411.parse.XAddress;
import cell411.parse.XUser;
import cell411.services.LocationService;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.friend.AddFriendModules;
import cell411.utils.Reflect;
import cell411.utils.Util;
import com.parse.ParseCloud;
import com.parse.model.ParseGeoPoint;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;

import static cell411.enums.RequestType.FriendRequest;

public class UserFragment extends ModelBaseFragment {
  FragmentFactory mImageFragmentFactory = FragmentFactory.fromClass(ProfileImageFragment.class);
  private TextView mTxtAlertsSent;
  private TextView mTxtAlertsResponded;
  private LinearLayout mLinearLayout;
  private RelativeLayout mAddFriendButton;
  private ImageView mAddFriendImage;
  private TextView mAddFriendText;
  private RelativeLayout mSpamButton;
  private XUser mUser;
  private int COLOR_PRIMARY;
  private int COLOR_WHITE;
  private TextView mTxtCity;
  private ImageView mImgUser;
  private TextView mTxtName;
  private TextView mTxtEmail;
  private ImageView mCloseImage;
  private boolean mIsFriend;

  public UserFragment() {
    super(R.layout.fragment_user);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState) {
    Reflect.announce(true);
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  public void onAddressSet(XAddress address) {
    mTxtCity.setText(address.cityPlus());
  }

  @Override
  public void onViewCreated(@Nonnull final View view, @Nullable final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    COLOR_PRIMARY = getColor(R.color.highlight_color);
    COLOR_WHITE = getColor(R.color.white);
    mImgUser = findViewById(R.id.avatar);
    mTxtName = findViewById(R.id.name);
    mTxtEmail = findViewById(R.id.txt_email);
    mTxtCity = findViewById(R.id.txt_city_name);
    mTxtAlertsSent = findViewById(R.id.txt_alerts_sent);
    mTxtAlertsResponded = findViewById(R.id.txt_alerts_responded);
    mLinearLayout = findViewById(R.id.ll_actions);
    mAddFriendButton = findViewById(R.id.rl_btn_add_friend);
    mAddFriendImage = findViewById(R.id.img_btn_add_friend);
    mAddFriendText = findViewById(R.id.txt_btn_action);
    mSpamButton = findViewById(R.id.rl_btn_spam);
    mLinearLayout.setVisibility(View.GONE);
    mCloseImage = findViewById(R.id.img_close);
    mCloseImage.setOnClickListener(v -> finish());
    mImgUser.setOnClickListener(this::showProfileImage);
    mAddFriendButton.setOnClickListener(this::onAddFriendClicked);
    mSpamButton.setOnClickListener(this::onSpamButtonClicked);
    mSpamButton.setEnabled(false);
    mLinearLayout.setVisibility(View.VISIBLE);
    String text = "Waiting for data";
    mTxtName.setText(text);
    mTxtCity.setText(text);
    mUser=findUser();
    refresh();
  }

  private XUser findUser()
  {
    Bundle arguments = getArguments();
    XUser user=null;
    if (arguments != null) {
      String objectId = arguments.getString("objectId");
      if (objectId == null) {
        throw new RuntimeException("Missing objectId");
      }
      user = LiveQueryService.getObject(objectId);
    }
    if (user == null) {
      throw new RuntimeException("Missing user");
    }
    return user;
  }

  @Override
  public void loadData1() {
    super.loadData1();
  }

  public void loadData2() {
    super.loadData2();
    try {
      if (mUser == null) {
        showAlertDialog("User not found");
        finish();
        return;
      }
      mUser.fetchIfNeeded();
      ParseGeoPoint location = mUser.getLocation();
      if (location == null && mUser.isCurrentUser()) {
        location = LocationService.get().getParseGeoPoint();
      }
      if (location != null) {
        ds().requestCity(location, this::onAddressSet);
      }
      HashMap<String, Object> params = new HashMap<>();
      params.put("user", mUser.getObjectId());
      HashMap<String, Object> result = ParseCloud.run("countAlerts", params);
      String sent = String.valueOf(result.get("sent"));
      String resp = String.valueOf(result.get("responded"));
      onUI(getRunnable(sent, resp));
      LiveQueryService lqs = LiveQueryService.opt();
      if (lqs != null) {
        UserWatcher watcher = lqs.getUserWatcher();
        mIsFriend = watcher.isFriend(mUser);
      }
    } catch (Throwable e) {
      handleException("loading user", e, success -> finish(), true);
    }
  }

  @Nonnull
  private Runnable getRunnable(String sent, String resp) {
    return new CountUpdater(sent, resp);
  }

  @Override
  public void loadData3() {
    super.loadData3();
    mImgUser.setImageBitmap(mUser.getAvatarPic());
    mTxtName.setText(mUser.getName());
    if (isFriend()) {
      mAddFriendImage.setImageTintList(ColorStateList.valueOf(COLOR_PRIMARY));
      mAddFriendButton.setBackgroundResource(R.drawable.bg_un_friend);
      mAddFriendText.setText(R.string.un_friend);
      mAddFriendText.setTextColor(getColor(R.color.highlight_color));
      String email = mUser.getEmail();
      if (Util.isNoE(email)) {
        mTxtEmail.setVisibility(View.GONE);
      } else {
        mTxtEmail.setText(email);
      }
    } else {
      mAddFriendImage.setImageTintList(ColorStateList.valueOf(COLOR_WHITE));
      mAddFriendButton.setBackgroundResource(R.drawable.bg_cell_join);
      mAddFriendText.setText(R.string.add_friend);
      mAddFriendText.setTextColor(Color.WHITE);
      mTxtEmail.setVisibility(View.GONE);
    }
  }

  private void showProfileImage(View ignored) {
    mImageFragmentFactory.setObjectId(mUser.getObjectId());
    push(mImageFragmentFactory);
  }

  private void onSpamButtonClicked(View view) {
    //    if (!mAnySpam) {
    //      AddFriendModules.showFlagAlertDialog(
    //        this, mUser, ok -> loadData3());
    //    } else {
    showToast(mUser.getName() + " is already blocked");
    //    }
  }

  private void onAddFriendClicked(View view) {
    mAddFriendButton.setBackgroundResource(R.drawable.bg_cell_join_processing);
    if (isFriend()) {
      // Delete this friend
      AddFriendModules.showDeleteFriendDialog(getContext(), mUser, success -> loadData3());
    } else { // Add Friend (send friend request)
      ds().handleRequest(FriendRequest, mUser, t -> showAlertDialog("Failed to send friend request: " + t), null);
    }
  }

  private void showFriendRequestFailedDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
    alert.setMessage(R.string.cannot_send_friend_request);
    alert.setPositiveButton(R.string.dialog_btn_ok, (dialogInterface, i) ->
    {
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public boolean isFriend() {
    return mIsFriend;
  }

  public void setUser(final XUser user) {
    mUser = user;
  }

  class CountUpdater implements Runnable {
    private final String mSent;
    private final String mResponded;

    CountUpdater(String sent, String responded) {
      mSent = sent;
      mResponded = responded;
    }

    @Override
    public void run() {
      mTxtAlertsSent.setText(mSent);
      mTxtAlertsResponded.setText(mResponded);
      if (mUser.getLocation() == null) {
        mTxtCity.setText(R.string.no_location_on_file);
      }
    }
  }
}
