package cell411.ui.profile;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cell411.parse.CountryInfo;
import cell411.ui.CountryListAdapter;
import cell411.ui.base.BaseContext;
import cell411.ui.widget.CCSpinner;
import cell411.ui.widget.UtilityMethods;
import cell411.utils.MethodRunnable;
import cell411.utils.ObservableValue;
import cell411.utils.ValueObserver;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;

public class PhoneNumberEdit
  extends RelativeLayout
  implements AdapterView.OnItemSelectedListener,
  TextView.OnEditorActionListener,
  ValueObserver<String>,
  BaseContext, View.OnFocusChangeListener {
  ArrayList<CountryInfo> mList = new ArrayList<>();
  CountryListAdapter mAdapter;
  EditText etNumber;
  CCSpinner spCode;
  ObservableValue<String> mValue = new ObservableValue<>(String.class, "");
  MethodRunnable mUpdateValue = MethodRunnable.forVirtual(this,
    "updateValue");

  public PhoneNumberEdit(@Nonnull final Context context) {
    this(context, null);
  }

  public PhoneNumberEdit(@Nonnull final Context context, @Nullable final AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public PhoneNumberEdit(@Nonnull final Context context, @Nullable final AttributeSet attrs,
                         final int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    mAdapter = new CountryListAdapter(context, mList);
    mValue.addObserver(this);
    onUI(mUpdateValue);
  }

  @Override
  public void onViewAdded(final View child) {
    super.onViewAdded(child);
    if (child instanceof CCSpinner) {
      spCode = (CCSpinner) child;
      spCode.setAdapter(mAdapter);
      spCode.setOnItemSelectedListener(this);
    } else if (child instanceof EditText) {
      etNumber = (EditText) child;
      etNumber.setOnFocusChangeListener(this);
    }
  }

  @Override
  public void onItemSelected(final AdapterView<?> parent, final View view, final int position,
                             final long id) {
    updateValue();
  }

  @Override
  public void onNothingSelected(final AdapterView<?> parent) {
    updateValue();
  }

  @Override
  public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
    updateValue();
    return false;
  }

  public void updateValue() {
    CountryInfo info = (CountryInfo) spCode.getSelectedItem();
    String number = etNumber.getText().toString().trim();
    mValue.set(info.dialingCode + number);
  }

  @Override
  protected void onLayout(final boolean changed, final int l, final int t, final int r,
                          final int b) {
    super.onLayout(changed, l, t, r, b);
  }

  public String getValue() {
    updateValue();
    return mValue.get();
  }

  public void setValue(final String value) {
    mValue.set(value);
  }

  @Override
  public void onChange(@Nullable final String newValue, @Nullable final String oldValue) {
    String value = mValue.get();
    UtilityMethods.setPhoneAndCountryCode(value, etNumber, spCode, mList);
  }

  @Override
  public void onFocusChange(final View v, final boolean hasFocus) {
    updateValue();
  }
}
