package cell411.ui.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.parse.XUser;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.XLog;
import cell411.utils.XTAG;

/**
 * Created by Sachin on 19-04-2016.
 */
public class ProfileEditFragment extends ModelBaseFragment {
  final static XTAG TAG = new XTAG();
  TreeMap<String, View> mDataControls = new TreeMap<>();
  XUser mUser = XUser.getCurrentUser();
  Button mDoneButton;
  Button mSaveButton;
  Button mResetButton;
  HashSet<String> mIgnored;

  public ProfileEditFragment() {
    super(R.layout.fragment_profile_edit);
  }

  public String get(EditText et) {
    return et.getText().toString();
  }

  private BloodTypeControl wrap(BloodTypeControl bloodTypeControl) {
    return bloodTypeControl;
  }

  private PhoneNumberEdit wrap(PhoneNumberEdit mobileNumber) {
    return mobileNumber;
  }

  private EditText wrap(EditText et) {
    return et;
  }

  public HashSet<String> ignored() {
    if (mIgnored == null) {
      HashSet<String> trash = new HashSet<>();
      trash.add("newPublicCellAlert");
      trash.add("authData");
      trash.add("avatar");
      trash.add("friends");
      trash.add("username");
      trash.add("thumbNail");
      trash.add("sessionToken");
      trash.add("location");
      trash.add("consented");
      trash.add("spamUsers");
      mIgnored = new HashSet<>(trash);
    }
    return mIgnored;
  }

  Map<String, Object> getData(XUser xuser) {
    TreeMap<String, Object> res = new TreeMap<>();
    HashSet<String> keys = new HashSet<>(xuser.getState().availableKeys());
    keys.removeAll(ignored());

    for (String key : keys) {
      Object val = null;
      if (xuser.has(key)) {
        val = xuser.get(key);
      }
      res.put(key, val);
    }
    return res;
  }

  Map<String, Object> getData(TreeMap<String, View> dataControls) {
    TreeMap<String, Object> res = new TreeMap<>();
    for (String key : dataControls.keySet()) {
      View view = dataControls.get(key);
      Object value;
      if (view instanceof EditText) {
        EditText et = (EditText) view;
        value = et.getText().toString().trim();
      } else if (view instanceof BloodTypeControl) {
        BloodTypeControl btc = (BloodTypeControl) view;
        value = btc.getValue();
      } else if (view instanceof PhoneNumberEdit) {
        PhoneNumberEdit pne = (PhoneNumberEdit) view;
        value = pne.getValue();
      } else if (view == null) {
        throw new RuntimeException("Null view");
      } else {
        throw new RuntimeException("Unexpected type: " + view.getClass());
      }
      res.put(key, value);
    }
    return res;
  }

  @Override
  public void onViewCreated(@Nonnull final View view, @Nullable final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    EditText etFirstName = findViewById(R.id.et_first_name);
    mDataControls.put("firstName", wrap(etFirstName));
    EditText etLastName = findViewById(R.id.et_last_name);
    mDataControls.put("lastName", wrap(etLastName));
    EditText etEmail = findViewById(R.id.et_email);
    mDataControls.put("email", wrap(etEmail));
    PhoneNumberEdit mobileNumber = findViewById(R.id.mobile_number);
    mDataControls.put("mobileNumber", wrap(mobileNumber));

    EditText etEmergencyContactName = findViewById(R.id.et_emergency_name);
    mDataControls.put("emergencyContactName", wrap(etEmergencyContactName));

    PhoneNumberEdit emergencyNumber = findViewById(R.id.emergency_number);
    mDataControls.put("emergencyContactNumber", wrap(emergencyNumber));

    BloodTypeControl bloodTypeControl = findViewById(R.id.blood_group);
    mDataControls.put("bloodType", wrap(bloodTypeControl));

    EditText etAllergies = findViewById(R.id.et_allergies);
    mDataControls.put("allergies", wrap(etAllergies));

    EditText etOtherMedicalConditions = findViewById(R.id.et_other_medical_conditions);
    mDataControls.put("otherMedicalConditions", wrap(etOtherMedicalConditions));

    mDoneButton = findViewById(R.id.done);
    mSaveButton = findViewById(R.id.save);
    mResetButton = findViewById(R.id.reset);
    mDoneButton.setOnClickListener(new ClickListener());
    mSaveButton.setOnClickListener(new ClickListener());
    mResetButton.setOnClickListener(new ClickListener());

    objectToControl();
  }

  boolean equal(Object nVal, Object oVal) {
    if (nVal == null) {
      nVal = "";
    }
    if (oVal == null) {
      oVal = "";
    }
    return nVal.equals(oVal);
  }

  void controlToObject() {
    for (String key : mDataControls.keySet()) {
      Object nVal = getData(key);
      Object oVal = mUser.get(key);
      if (equal(nVal, oVal)) {
        continue;
      }
      mUser.put(key, nVal);
    }
    if (mUser.isDirty()) {
      mUser.saveInBackground();
    }
  }

  void objectToControl() {
    for (String key : mDataControls.keySet()) {
      String value = null;
      if (mUser.has(key))
        value = mUser.getString(key);
      if (value == null)
        value = "";
      setData(key, value);
      assert (value.equals(getData(key)));
      XLog.i(TAG, "key: %s val: %s", key, value);
    }
  }

  private Object getData(String key) {
    return getData(mDataControls.get(key));
  }

  private void setData(final String key, final String value) {
    View control = mDataControls.get(key);
    Objects.requireNonNull(control);
    setData(control, value);
  }

  private void setData(View control, Object value) {
    if (control instanceof EditText) {
      ((EditText) control).setText(String.valueOf(value));
    } else if (control instanceof PhoneNumberEdit) {
      ((PhoneNumberEdit) control).setValue(String.valueOf(value));
    } else if (control instanceof BloodTypeControl) {
      ((BloodTypeControl) control).setValue(String.valueOf(value));
    } else {
      throw new RuntimeException("No way to set a " + control.getClass());
    }
  }

  private Object getData(final View control) {
    if (control instanceof EditText) {
      return ((EditText) control).getText().toString().trim();
    } else if (control instanceof PhoneNumberEdit) {
      return ((PhoneNumberEdit) control).getValue();
    } else if (control instanceof BloodTypeControl) {
      return ((BloodTypeControl) control).getValue();
    } else {
      throw new RuntimeException("Unexpected control type: " + control.getClass());
    }
  }

  //    private void checkEmailAndSave(final XUser parseUser, final String email) {
  //        final XUser currentUser = (XUser) ParseUser.getCurrentUser();
  //        final String objectId = currentUser.getObjectId();
  //        if (currentUser.getUsername()
  //                .contains("@")) {
  //            ParseQuery<XUser> userParseQuery = ParseQuery.getQuery("_User");
  //            userParseQuery.whereEqualTo("email", email);
  //            userParseQuery.findInBackground((list, e) ->
  //            {
  //                System.out.println("entering done");
  //                boolean fail = false;
  //                if (e != null) {
  //                    handleException("FIXME:  doing what?", e, null);
  //                    return;
  //                }
  //                if (list != null && list.size() > 0) {
  //                    System.out.println(currentUser.getObjectId());
  //                    System.out.println(currentUser.getEmail());
  //                    for (XUser user : list) {
  //                        if (user.getObjectId()
  //                                .equals(objectId)) {
  //                            continue;
  //                        }
  //                        fail = true;
  //                        break;
  //                    }
  //                }
  //                if (fail) {
  //                    showEmailAlreadyRegisteredAlert(email);
  //                } else {
  //                    parseUser.setUsername(email.toLowerCase(Locale.US)
  //                            .trim());
  //                    saveUser(parseUser);
  //                }
  //            });
  //        } else if (!email.isEmpty()) {
  //            XLog.i("ProfileEditActivity", "!email.isEmpty()");
  //            ParseQuery<XUser> userParseQuery = ParseQuery.getQuery("_User");
  //            userParseQuery.whereEqualTo("username", email);
  //            userParseQuery.findInBackground((list, e) ->
  //            {
  //                if (e == null) {
  //                    if (list == null || list.size() == 0) {
  //                        parseUser.setEmail(email.toLowerCase(Locale.US)
  //                                .trim());
  //                        saveUser(parseUser);
  //                    } else {
  //                        // show email already registered
  //                        showEmailAlreadyRegisteredAlert(email);
  //                    }
  //                } else {
  //                    handleException("FIXME:  doing what?", e, null);
  //                }
  //            });
  //        } else {
  //        String firstName = etFirstName.getText()
  //                .toString()
  //                .trim();
  //        String lastName = etLastName.getText()
  //                .toString()
  //                .trim();
  //        String countryCode = "";
  //        if (spCountryCode.getSelectedItemPosition() > 0) {
  //            countryCode = ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode;
  //        }
  //        final String mobileNumber = etMobileNumber.getText()
  //                .toString()
  //                .trim();
  //        String emergencyCountryCode = "";
  //        if (spEmergencyCountryCode.getSelectedItemPosition() > 0) {
  //            emergencyCountryCode = ((CountryInfo) spEmergencyCountryCode.getSelectedItem()).dialingCode;
  //        }
  //        String emergencyContactName = etEmergencyContactName.getText()
  //                .toString()
  //                .trim();
  //        String emergencyContactPhone = etEmergencyContactPhone.getText()
  //                .toString()
  //                .trim();
  //        String allergies = etAllergies.getText()
  //                .toString()
  //                .trim();
  //        String otherMedicalConditions = etOtherMedicalConditions.getText()
  //                .toString()
  //                .trim();
  //        if (!mobileNumber.isEmpty()) {
  //            parseUser.put("mobileNumber", countryCode + mobileNumber.trim());
  //        } else {
  //            parseUser.put("mobileNumber", mobileNumber.trim());
  //        }
  //        parseUser.put("firstName", firstName.trim());
  //        parseUser.put("lastName", lastName.trim());
  //        parseUser.put("emergencyContactName", emergencyContactName.trim());
  //        if (!emergencyContactPhone.isEmpty()) {
  //            parseUser.put("emergencyContactNumber", emergencyCountryCode + emergencyContactPhone.trim());
  //        } else {
  //            parseUser.put("emergencyContactNumber", emergencyContactPhone.trim());
  //        }
  //        if(mBloodTypeControl!=null)
  //            parseUser.put("bloodType", mBloodTypeControl.getValue());
  //        parseUser.put("allergies", allergies.trim());
  //        parseUser.put("otherMedicalConditions", otherMedicalConditions.trim());
  //        parseUser.saveInBackground(e ->
  //        {
  //            {
  //                if (e == null) {
  //                    // Make an API call to LMA server for the updated user information
  //                    Cell411.get().showToast(R.string.account_updated_successfully);
  //                    finish();
  //                } else {
  //                    handleException("Saving edited profile", e, null);
  //                }
  //            }
  //        });
  //    }

  private void showEmailAlreadyRegisteredAlert(String email) {
    showAlertDialog(email + " " + getString(R.string.email_already_registered));
  }

  static class HashListMap extends TreeMap<String, List<View>> {
    public List<View> put(String key, List<View> value) {
      List<View> list = computeIfAbsent(key, (k) -> new ArrayList<>());
      assert list != null;
      list.addAll(value);
      return list;
    }

    public List<View> put(String key, View value) {
      return put(key, Collections.singletonList(value));
    }
  }

  private class ClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
      if (v == mSaveButton) {
        controlToObject();
      } else if (v == mDoneButton) {
        pop();
      } else if (v == mResetButton) {
        objectToControl();
      } else {
        showToast("Unexpected click: " + v);
      }
    }
  }

  //    @Override
  //        etFirstName.setText(user.getFirstName());
  //        etLastName.setText(user.getLastName());
  //        if (user.getUsername()
  //                .contains("@")) {
  //            etEmail.setText(user.getUsername());
  //        } else {
  //            etEmail.setText(user.getEmail());
  //        }
  //
  //        bloodTypeControl.setValue(user.getBloodType());
  //        String emergencyContactName = user.getEmergencyContactName();
  //        if (!Util.isNoE(emergencyContactName)) {
  //            etEmergencyContactName.setText(emergencyContactName);
  //        }
  //        String allergies = user.getAllergies();
  //        if (!Util.isNoE(allergies)) {
  //            etAllergies.setText(allergies);
  //        }
  //        String otherMedicalConditions = user.getOtherMedicalConditions();
  //        if (!Util.isNoE(otherMedicalConditions)) {
  //            etOtherMedicalConditions.setText(otherMedicalConditions);
  //        }
  //    }
  //
  //    private DataView makeDataView(final PhoneNumberEdit mobileNumber) {
  //        return mobileNumber;
  //    }

  //    list = new ArrayList<>();
  //        UtilityMethods.initializeCountryCodeList(list);
  //        list.add(0, new CountryInfo(getString(R.string.code), getString(R.string.code), null));
  ////    final CountryListAdapter countryListAdapter = new CountryListAdapter(this, this, R.layout.cell_country, list);
  ////        spCountryCode.setAdapter(countryListAdapter);
  //        spCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
  //        @Override
  //        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
  //            CountryInfo countryInfo = (CountryInfo) spCountryCode.getSelectedItem();
  //            for (CountryInfo info : list) {
  //                info.selected = false;
  //            }
  //            countryInfo.selected = true;
  //            countryListAdapter.notifyDataSetChanged();
  //        }
  //
  //        @Override
  //        public void onNothingSelected(AdapterView<?> adapterView) {
  //        }
  //    });
  //    listEmergencyPhoneCountryCode = new ArrayList<>();
  //        UtilityMethods.initializeCountryCodeList(listEmergencyPhoneCountryCode);
  //        listEmergencyPhoneCountryCode.add(0, new CountryInfo(getString(R.string.code), getString(R.string.code), null));
  //    final CountryListAdapter countryListAdapterEmergencyNumber = new CountryListAdapter(this, this, R.layout.cell_country,
  //      listEmergencyPhoneCountryCode);
  //        countryListAdapterEmergencyNumber.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
  //
  //        spEmergencyCountryCode.setAdapter(countryListAdapterEmergencyNumber);
  //        spEmergencyCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
  //        @Override
  //        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
  //            CountryInfo countryInfo = (CountryInfo) spEmergencyCountryCode.getSelectedItem();
  //            for (CountryInfo info : listEmergencyPhoneCountryCode) {
  //                info.selected = false;
  //            }
  //            countryInfo.selected = true;
  //            countryListAdapterEmergencyNumber.notifyDataSetChanged();
  //            XLog.i(TAG,
  //              "countryInfo: " + countryInfo.name + " (" + countryInfo.shortCode + ") + " + countryInfo.dialingCode);
  //            XLog.i(TAG, "position: " + position);
  //        }
  //
  //        @Override
  //        public void onNothingSelected(AdapterView<?> adapterView) {
  //        }
  //    });
  //
  //    String mobileNumber = user.getMobileNumber();
  //    String emergencyContactNumber = user.getEmergencyContactNumber();
  //        UtilityMethods.setPhoneAndCountryCode(mobileNumber, etMobileNumber, spCountryCode, list);
  //        UtilityMethods.setPhoneAndCountryCode(emergencyContactNumber, etEmergencyContactPhone, spEmergencyCountryCode,
  //    list);
  //

  //    @Override
  //    protected void onResume() {
  //        super.onResume();
  //        setData("mobileNumber");

  //        UtilityMethods.setPhoneAndCountryCode((ParseUser.getCurrentUser())
  //                .getString("mobileNumber"), etMobileNumber, spCountryCode, list);
  //    }

}

