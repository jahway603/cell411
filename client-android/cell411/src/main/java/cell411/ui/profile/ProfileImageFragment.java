package cell411.ui.profile;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import cell411.imgstore.ImageStore;
import cell411.libcell.ConfigDepot;
import cell411.logic.LiveQueryService;
import cell411.parse.XUser;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.MainActivity;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.ValueObserver;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
//import cell411.utils.ImageFactory;

public class ProfileImageFragment
  extends ModelBaseFragment
{
  XUser mUser;
  private ImageView mImageView;
  private TextView mTxtNoProfilePic;

  public ProfileImageFragment()
  {
    super(R.layout.fragment_image);
  }

  static FragmentFactory smFragmentFactory =
    FragmentFactory.fromClass(ProfileImageFragment.class);

  static class Opener implements OnClickListener
  {
    final XUser mUser;

    Opener(XUser user)
    {
      mUser = user;
    }

    @Override
    public void onClick(View v)
    {
      smFragmentFactory.setObjectId(mUser.getObjectId());
      MainActivity activity = Cell411.req().getMainActivity();
      activity.push(smFragmentFactory);
    }
  }
  public static View.OnClickListener getOpener(XUser user)
  {
    return new Opener(user);
  }

  @Override
  public void onViewCreated(@Nonnull View view,
                            @Nullable Bundle savedInstanceState)
  {
    Bundle arguments = require(getArguments());
    String objectId = arguments.getString("objectId");
    mUser = require(LiveQueryService.getObject(objectId));
    if (!mUser.hasProfileImage()) {
      finish();
      return;
    }
    mImageView = findViewById(R.id.image);
    mTxtNoProfilePic = findViewById(R.id.txt_no_profile_picture);
    mTxtNoProfilePic.setVisibility(View.VISIBLE);
    mImageView.setVisibility(View.VISIBLE);
    mTxtNoProfilePic.setVisibility(View.GONE);
    ImageStore.get().loadImage(mUser, mImageView);
  }

  private class BitmapValueObserver
    implements ValueObserver<Bitmap>,
               Runnable
  {
    private Bitmap mNewValue;

    @Override
    public void onChange(@Nullable Bitmap newValue, @Nullable Bitmap oldValue)
    {
      mNewValue = newValue;
      onUI(this);
    }

    public void run()
    {
      if (mNewValue == null)
        mNewValue = ConfigDepot.getPlaceHolder();
      mImageView.setImageBitmap(mNewValue);
    }
  }
}

