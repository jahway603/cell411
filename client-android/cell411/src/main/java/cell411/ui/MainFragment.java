package cell411.ui;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.annotation.MainThread;

import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.parse.XEntity;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.XSelectFragment;
import cell411.ui.chat.ChatFragment;
import cell411.ui.chat.TabChatFragment;
import cell411.utils.Reflect;

public class MainFragment extends XSelectFragment implements View.OnClickListener {
  FragmentFactory mChatFactory = FragmentFactory.fromClass(ChatFragment.class);

  public MainFragment() {
    super(R.layout.fragment_main);
  }

  @Override
  public void onViewCreated(@Nonnull final View view, @Nullable final Bundle savedInstanceState) {
    FragmentTab.setupTabBar(this);
    if (getFactories().isEmpty()) {
      setFactories(FragmentTab.mFragmentTabs);
    } else {
      selectFragment();
    }
    super.onViewCreated(view, savedInstanceState);
  }


  @Override
  public void selectFragment() {
    super.selectFragment();
    for (FragmentFactory factory : getFactories()) {
      if (factory instanceof FragmentTab) {
        FragmentTab tab = (FragmentTab) factory;
        tab.setSelected(tab.mIndex == getIndex());
      }
    }
  }

  @MainThread
  @Override
  public void onSaveInstanceState(@Nonnull Bundle outState) {
    Reflect.announce("onSaveInstanceState");
    super.onSaveInstanceState(outState);
    outState.putInt("index", getIndex());
  }

  @MainThread
  @CallSuper
  @Override
  public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
    super.onViewStateRestored(savedInstanceState);
    if (savedInstanceState != null) {
      int index = savedInstanceState.getInt("index");
      setIndex(index);
    }
  }

  @Override
  public void onClick(final View view) {
    if (view == null) {
      return;
    }
    int index = (int) view.getTag();
    selectFragment(index);
  }

  public void openChat(XEntity cell) {
    selectFragment(TabChatFragment.class);
    mChatFactory.setObjectId(cell.getObjectId());
    push(mChatFactory);
  }
}
