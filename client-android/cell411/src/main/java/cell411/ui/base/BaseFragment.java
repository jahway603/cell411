package cell411.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.view.*;
import androidx.annotation.*;
import androidx.fragment.app.Fragment;
import cell411.android.Reloadable;
import cell411.utils.Reflect;
import cell411.utils.Util;
import com.safearx.cell411.Cell411Activity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class BaseFragment extends Fragment implements BaseContext, Reloadable {
  private final long[] lastReloadTimes = new long[]{
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  };

  public BaseFragment(@LayoutRes int layout) {
    super(layout);
  }

  @MainThread
  @CallSuper
  @Override
  public void onDetach() {
    super.onDetach();
  }

  @MainThread
  @CallSuper
  @Override
  public void onAttach(@Nonnull Context context) {
    super.onAttach(context);
  }

  @Override
  public void setLastReloadTime(int which) {
    lastReloadTimes[which] = System.currentTimeMillis();
  }

  @Override
  public long getLastReloadTime(int which) {
    return lastReloadTimes[which];
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    Reflect.announce(getClass());
    Reflect.announce(this);
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }

  @MainThread
  @Override
  public void onSaveInstanceState(@Nonnull Bundle outState) {
    Reflect.announce(this.getClass());
    super.onSaveInstanceState(outState);
  }

  @CallSuper
  public void onResume() {
    super.onResume();
    hideSoftKeyboard();
    refresh();
  }

  public int getColor(final int colorAccent) {
    return requireActivity().getColor(colorAccent);
  }

  protected <V extends View> V findViewById(final int id, boolean req) {
    if (req) {
      return require(findViewById(id));
    } else {
      return findViewById(id);
    }
  }

  public <V extends View> V findViewById(final int id) {
    return requireView().findViewById(id);
  }

  @CallSuper
  public void onPause() {
    super.onPause();
  }

  public String getTitle() {
    return Util.makeWords(getClass().getSimpleName());
  }

  @Override
  public BaseActivity activity() {
    return (BaseActivity) getActivity();
  }

  @CallSuper
  public void loadData1() {
    setLastReloadTime(1);
  }

  @CallSuper
  public void loadData2() {
    long newTime = System.currentTimeMillis();
    long oldTime = lastReloadTimes[2];
    System.out.println("Time elapsed: " + (newTime - oldTime));
    setLastReloadTime(2);
  }

  @CallSuper
  public void loadData3() {
    setLastReloadTime(3);
  }

  @SuppressWarnings("unused")
  public void finish(final View ignoredView) {
    finish();
  }

  public void finish() {
    pop();
  }

  public boolean pop() {
    Cell411Activity activity = (Cell411Activity) activity();
    if (activity == null) {
      return false;
    }
    return activity.pop();
  }

  public boolean onBackPressed() {
    return true;
  }

  public final void push(FragmentFactory factory) {
    push(factory, false, false);
  }

  public final void push(FragmentFactory factory, boolean b, boolean b1) {
    Cell411Activity activity = (Cell411Activity) activity();
    if (activity != null) {
      activity.push(factory, b, b1);
    }
  }

}
