package cell411.ui.base;

import cell411.utils.OnCompletionListener;

public interface RealMessageForwarder {
  void realShowAlertDialog(final String message,
                           final OnCompletionListener listener);

  void realShowYesNoDialog(String title, String text,
                           OnCompletionListener listener);

  void realShowToast(final String message);

  void realHandleException(final String message, final Throwable throwable,
                           final OnCompletionListener listener,
                           final boolean dialog);

}
