package cell411.ui.base;

import android.os.Bundle;
import cell411.parse.XEntity;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;

abstract public class MainActivity extends BaseActivity {
  private FragmentFrame mFragmentFrame;

  public MainActivity(int layout) {
    super(layout);
  }

  public abstract void closeDrawer();

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mFragmentFrame = findViewById(R.id.id_fragment_frame);
    getFragmentFrame().setFragMan(getSupportFragmentManager());
  }

  abstract public void openChat(XEntity cell);

  public void push(FragmentFactory factory, boolean pop, boolean all) {
    synchronized (this) {
      getFragmentFrame().push(factory, pop, all);
    }
  }

  public void popAll() {
    getFragmentFrame().popAll();
  }

  public boolean pop() {
    synchronized (this) {
      return getFragmentFrame().pop();
    }
  }

  public void onFragmentPopped() {
  }

  @Nonnull
  public FragmentFrame getFragmentFrame() {
    return Objects.requireNonNull(mFragmentFrame);
  }

  public void push(FragmentFactory factory) {
    push(factory, false, false);
  }
}