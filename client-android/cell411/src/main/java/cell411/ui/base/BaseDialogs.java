package cell411.ui.base;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;

import android.content.DialogInterface;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.parse.ParseException;
import com.parse.model.ParseUser;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.utils.OnCompletionListener;
import cell411.utils.PrintString;
import cell411.utils.Util;
import cell411.utils.XLog;
import cell411.utils.XTAG;

public class BaseDialogs {
  private static final XTAG TAG = new XTAG();
  static BaseDialogs smInstance = new BaseDialogs();
  static List<AlertDialog> mShowing = new ArrayList<>();

//  static {
//    XLog.i(TAG, "loading class");
//  }

  private boolean mStopWithTheFuckingDialogs = false;

  static public void onUIThread(Runnable runnable) {
    BaseApp.req().onUI(runnable, 0);
    if (Util.theGovernmentIsHonest() && smInstance != null) {
      smInstance.mStopWithTheFuckingDialogs = true;
    }
  }

  static public void onUIThread(Runnable runnable, long delay) {
    BaseApp.req().onUI(runnable, delay);
  }

  public static void showSessionExpiredAlertDialog(OnCompletionListener listener) {
    onUIThread(createSessionExpiredAlertDialog(listener));
  }

  public static DialogShower createSessionExpiredAlertDialog(OnCompletionListener listener) {
    XLog.i(TAG, "showSessionExpiredAlertDialog() invoked");
    XLog.i(TAG, "currentUser: %s", String.valueOf(ParseUser.getCurrentUser()));
    return createAlertDialog("Session Expired",
      "We are sorry, you have been logged out, please log in again.", null,
      listener);
  }

  public static boolean isDialogShowing() {
    return !getShowing().isEmpty();
  }

  public static DialogShower createYesNoDialog(String message,
                                               OnCompletionListener listener) {
    return createYesNoDialog(null, message, listener);
  }

  public static DialogShower createYesNoDialog(String title, String message,
                                               OnCompletionListener listener) {
    return new YesNoShower(listener, title, message);

  }

  public static void showExceptionDialog(Throwable e, String what, OnCompletionListener listener) {
    onUIThread(createExceptionDialog(e, what, listener));
  }

  public static DialogShower createExceptionDialog(
    Throwable e, String what, OnCompletionListener listener) {
    e.printStackTrace();
    if (get() != null && get().mStopWithTheFuckingDialogs) {
      return null;
    }
    PrintString m = new PrintString();
    String title = "Error";
    m.p("We have encountered ");
    if (e instanceof ParseException) {
      title = "Database Error";
      m.p("a database");
    } else {
      m.p("an");
    }
    m.p(" error, while ");
    m.p(what);
    m.p(".  This may be a transient problem.  " + "If it continues, please contact us." + "\n");
    Integer code = Util.castAndCall(e, ParseException.class, ParseException::getCode);
    if (code != null) {
      m.pl("ParseException code: " + code);
    }
    m.pl("\n");
    e.printStackTrace(m);
    return createAlertDialog(title, m.toString(), null, listener);
  }

  private static BaseDialogs get() {
    return smInstance;
  }

  public static DialogShower createAlertDialog(String message) {
    return createAlertDialog(null, message, null, null);
  }

  public static DialogShower createAlertDialog(@Nonnull String message,
                                               @Nullable DialogInterface.OnDismissListener listener) {
    return createAlertDialog(null, message, listener, null);
  }

  public static DialogShower createAlertDialog(String title, String message,
                                               DialogInterface.OnDismissListener listener,
                                               OnCompletionListener listener2) {
    return new AlertDialogShower(title, message, listener, listener2);
  }

  public static DialogShower createAlertDialog(@Nonnull String message,
                                               @Nullable OnCompletionListener listener) {
    return createAlertDialog(null, message, null, listener);
  }

  public synchronized static List<AlertDialog> getShowing() {
    return mShowing;
  }

  public static void showEnterTextDialog(String title, String hint, String initVal,
                                         OnCompletionListener listener) {
    onUIThread(createEnterTextDialog(title, hint, initVal, listener));
  }

  public static DialogShower createEnterTextDialog(@StringRes int title, @StringRes int hint,
                                                   String initVal, OnCompletionListener listener) {
    return createEnterTextDialog(Util.getString(title), Util.getString(hint), initVal, listener);
  }

  public static DialogShower createEnterTextDialog(String title, String hint, String initVal,
                                                   OnCompletionListener listener) {
    return new DialogShower() {
      @Override
      protected AlertDialog createDialog() {
        EnterTextDialog dialog = new EnterTextDialog();
        dialog.setTitle(title);
        dialog.setHint(hint);
        dialog.setInitVal(initVal);
        dialog.setOnDismissListener(d ->
        {
          mText = dialog.getAnswer();
          if (listener != null) {
            listener.done(true);
          }
        });
        return dialog;
      }
    };
  }

  public static void showYesNoDialog(final String title,
                                     final OnCompletionListener listener) {
    showYesNoDialog(title, null, listener);
  }

  public static void showYesNoDialog(final String title, String message,
                                     final OnCompletionListener listener) {
    onUIThread(createYesNoDialog(title, message, listener));
  }

  public static void realShowAlertDialog(final String message, final OnCompletionListener listener) {
    onUIThread(createAlertDialog(message, listener));
  }

  public static DialogShower showNoButtonDialog(String title, String message,
                                                OnCompletionListener listener) {
    DialogShower noButtonDialog = createNoButtonDialog(title, message, null, listener);
    onUIThread(noButtonDialog);
    return noButtonDialog;
  }

  public static DialogShower createNoButtonDialog(String title, final String message,
                                                  OnCompletionListener onCompletionListener) {
    return createNoButtonDialog(title, message, null, null);
  }

  public static DialogShower createNoButtonDialog(String title, String message,
                                                  DialogInterface.OnDismissListener listener,
                                                  OnCompletionListener listener2) {
    DialogShower result = new DialogShower() {
      @Override
      protected AlertDialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, getThemeRes());
        if (!Util.isNoE(title)) {
          builder.setTitle(title);
        }
        if (!Util.isNoE(message)) {
          builder.setMessage(message);
        }

        //        builder.setPositiveButton(R.string.dialog_btn_ok, this);
        final AlertDialog dialog = builder.create();
        dialog.setCancelable(getCancelable());
        dialog.setOnDismissListener(dialog1 ->
        {
          if (listener != null) {
            listener.onDismiss(dialog1);
          }
          if (listener2 != null) {
            listener2.done(true);
          }
          getShowing().removeIf((d) -> d == dialog1);
        });
        return dialog;
      }
    };
    return result;
  }

  //    public static class ExtraDismissListener implements DialogInterface.OnDismissListener,
  //            DialogInterface.OnShowListener {
  //        DialogInterface.OnDismissListener mListener;
  //        OnCompletionListener mCompletionListener;
  //
  //
  //        public ExtraDismissListener() {
  //            this(null, null);
  //        }
  //
  //        public ExtraDismissListener(DialogInterface.OnDismissListener listener,
  //                                    OnCompletionListener completionListener) {
  //            mListener = listener;
  //            mCompletionListener = completionListener;
  //        }
  //
  //        public ExtraDismissListener(Dialog dialog) {
  //            dialog.setOnShowListener(this);
  //            dialog.setOnDismissListener(this);
  //        }
  //
  //        boolean success() {
  //            return true;
  //        }
  //
  //        final public void onDismiss(DialogInterface dialog) {
  //            getDismissListeners().remove(this);
  //            if (mCompletionListener != null) {
  //                mCompletionListener.done(success());
  //            }
  //            if (mListener != null) {
  //                mListener.onDismiss(dialog);
  //            }
  //        }
  //
  //        protected void finalize() {
  //            XLog.i(TAG, "Warning, you leaked an ExtraDismissListener, that'll bite you!");
  //            getDismissListeners().remove(this);
  //        }
  //
  //        @Override
  //        public void onShow(DialogInterface dialog) {
  //            getDismissListeners().add(this);
  //        }
  //
  //        public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
  //            if (mListener == null) {
  //                mListener = listener;
  //            } else {
  //                DialogInterface.OnDismissListener oldListener = mListener;
  //                mListener = dialog ->
  //                {
  //                    try {
  //                        oldListener.onDismiss(dialog);
  //                    } catch (Exception ignored) {
  //
  //                    }
  //                    try {
  //                        listener.onDismiss(dialog);
  //                    } catch (Exception ignored) {
  //
  //                    }
  //                };
  //            }
  //        }
  //
  //        public void setOnCompletionListener(OnCompletionListener listener) {
  //            if (mCompletionListener == null) {
  //                mCompletionListener = listener;
  //            } else {
  //                final OnCompletionListener oldListener = mCompletionListener;
  //                mCompletionListener = new OnCompletionListener() {
  //                    @Override
  //                    public void done(boolean success) {
  //                        try {
  //                            oldListener.done(success);
  //                        } catch (Throwable ignored) {
  //
  //                        }
  //                        try {
  //                            listener.done(success);
  //                        } catch (Throwable ignored) {
  //
  //                        }
  //                    }
  //                };
  //            }
  //        }
  //    }

  public static class YesNoDialog extends AlertDialog {
    Boolean mAnswer = null;

    protected YesNoDialog(OnCompletionListener listener) {
      super(BaseApp.req().getCurrentActivity(), android.R.style.Theme_Material_Dialog_Alert);
      AlertDialog mDialog = this;
      setOnDismissListener(new OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
          listener.done(mAnswer);
          getShowing().removeIf((d) -> d == mDialog);
        }
      });
    }

    public void onButtonClick(DialogInterface dialog, int which) {
      assert dialog == this;
      switch (which) {
        case BUTTON_POSITIVE:
        case BUTTON_NEGATIVE:
          mAnswer = which == BUTTON_POSITIVE;
          break;
        default:
          mAnswer = null;
          break;
      }
    }
  }

  private static class YesNoShower extends DialogShower {
    private final OnCompletionListener mListener;
    private final String mTitle;
    private final String mMessage;

    public YesNoShower(OnCompletionListener listener, String title, String message) {
      mListener = listener;
      mTitle = Util.isNoE(title) ? "Query" : title;
      mMessage = message;
    }

    @Override
    protected AlertDialog createDialog() {
      YesNoDialog yesNoDialog = new YesNoDialog(mListener);
      yesNoDialog.setCancelable(false);
      yesNoDialog.setButton(BUTTON_POSITIVE, "Yes", yesNoDialog::onButtonClick);
      yesNoDialog.setButton(BUTTON_NEGATIVE, "No", yesNoDialog::onButtonClick);
      yesNoDialog.setTitle(mTitle);
      yesNoDialog.setMessage(mMessage);
      yesNoDialog.setOnDismissListener(dialog ->
      {
        BaseDialogs.getShowing().removeIf((dlg) -> dlg == yesNoDialog);
        mListener.done(yesNoDialog.mAnswer);
      });
      return yesNoDialog;
    }
  }

  private static class AlertDialogShower extends DialogShower {
    private final String mTitle;
    private final String mMessage;
    private final DialogInterface.OnDismissListener mListener;
    private final OnCompletionListener mListener2;

    public AlertDialogShower(String title, String message,
                             DialogInterface.OnDismissListener listener,
                             OnCompletionListener listener2)
    {
      mTitle = title;
      mMessage = message;
      mListener = listener;
      mListener2 = listener2;
    }

    @Override
    protected AlertDialog createDialog() {
      AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, getThemeRes());
      builder.setTitle(mTitle);
      builder.setMessage(mMessage);

      builder.setPositiveButton(R.string.dialog_btn_ok, this);
      final AlertDialog dialog = builder.create();
      dialog.setCancelable(getCancelable());
      dialog.setOnDismissListener(dialog1 ->
      {
        if (mListener != null) {
          mListener.onDismiss(dialog1);
        }
        if (mListener2 != null) {
          mListener2.done(true);
        }
        getShowing().removeIf((d) -> d == dialog1);
      });
      return dialog;
    }
  }
}
