package cell411.ui.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import androidx.core.content.ContextCompat;
import cell411.utils.concurrant.STPExecutorPlus;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public abstract class BaseAnim extends View {
  static int smSerial = 0;
  static ArrayList<BaseAnim> smInstances = new ArrayList<>();

  private final Drawable[] mFrames;
  private final int mDuration;
  int mSerial = ++smSerial;
  int mFrame = 0;
  private ScheduledFuture<?> mTimer;
  private Rect mRect;

  public BaseAnim(Context context) {
    this(context, null, 0);
  }

  public BaseAnim(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public BaseAnim(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    mFrames = getFrames();
    mDuration = getDuration();
    mRect = makeDrawRect();

    smInstances.add(this);
    addOnLayoutChangeListener(this::onLayoutChange);
    addOnAttachStateChangeListener(new AttachStateChangeListener());
  }

  public Drawable getEditModeFrame() {
    Drawable[] frames = getFrames();
    return frames[4];
  }

  @Override
  public void setVisibility(int visibility) {
    super.setVisibility(visibility);
    if (isInEditMode()) {
      return;
    }
    if (isAttachedToWindow() && visibility == View.VISIBLE) {
      if (mTimer == null) {
        long rate = mDuration / mFrames.length;
        final STPExecutorPlus ex = BaseApp.getExecutor();
        mTimer = ex.scheduleAtFixedRate(new InvalidateRunnable(), 0, rate, TimeUnit.MILLISECONDS);
      }
    } else {
      if (mTimer != null) {
        mTimer.cancel(false);
        mTimer = null;
      }
    }
  }

  @Nonnull
  public String toString() {
    return getClass().getSimpleName() + " #" + mSerial;
  }

  protected Rect makeDrawRect() {
    if (getWidth() < getPaddingLeft() + getPaddingRight()) {
      if (mRect == null) {
        return new Rect(0, 0, 0, 0);
      } else {
        return mRect;
      }
    }
    int left = getPaddingLeft();
    int top = getPaddingTop();
    int right = getWidth() - left - getPaddingRight();
    int bottom = getHeight() - top - getPaddingBottom();
    return new Rect(left, top, right, bottom);
  }

  public abstract int[] getFrameIds();

  public Drawable[] getFrames() {
    int[] frameIds = getFrameIds();
    Context context = getContext();
    Drawable[] frames = new Drawable[frameIds.length];
    for (int i = 0; i < frames.length; i++) {
      frames[i] = ContextCompat.getDrawable(context, frameIds[i]);
    }
    return frames;
  }

  public abstract int getDuration();

  @SuppressLint("DrawAllocation")
  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    if (mFrames == null) {
      return;
    }
    if (mRect == null) {
      return;
    }
    mFrame = mFrame % mFrames.length;
    if (mFrames[mFrame] == null) {
      return;
    }
    Drawable drawable = mFrames[mFrame++];
    drawable.setBounds(mRect);
    drawable.draw(canvas);
  }

  private void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft,
                              int oldTop, int oldRight, int oldBottom) {
    mRect = makeDrawRect();
  }

  private class AttachStateChangeListener implements OnAttachStateChangeListener {
    @Override
    public void onViewAttachedToWindow(View v) {
      if (v == BaseAnim.this) {
        setVisibility(getVisibility());
      }
    }

    @Override
    public void onViewDetachedFromWindow(View v) {
      if (v == BaseAnim.this) {
        setVisibility(View.GONE);
      }
    }
  }

  private class InvalidateRunnable implements Runnable {

    @Override
    public void run() {
      invalidate();
    }
  }
}
