package cell411.ui.base;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.android.Reloadable;
import cell411.parse.XEntity;
import cell411.utils.Reflect;
import cell411.utils.XLog;
import cell411.utils.XTAG;

public abstract class BaseActivity extends AppCompatActivity implements BaseContext, Reloadable {
  public static final XTAG TAG = new XTAG();
  static public final int PERMISSION_GRANTED = PackageManager.PERMISSION_GRANTED;
  private final long[] mLastReloadTimes = new long[10];

  public BaseActivity() {
    super();
  }

  public BaseActivity(@LayoutRes int layout) {
    super(layout);
  }

  @CallSuper
  @Override
  protected void onSaveInstanceState(@Nonnull Bundle outState) {
    outState.putString("text", "this is a test.  this is only a test.");
    super.onSaveInstanceState(outState);
  }

  @Override
  public long getLastReloadTime(int which) {
    return mLastReloadTimes[which];
  }

  public void setLastReloadTime(int which) {
    mLastReloadTimes[which + 3] =
      mLastReloadTimes[which];
    mLastReloadTimes[which] =
      System.currentTimeMillis();
    mLastReloadTimes[which + 6] =
      mLastReloadTimes[which]
        - mLastReloadTimes[which + 3];
  }

  @Override
  public boolean onOptionsItemSelected(@Nonnull MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      onBackPressed();
      return false;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
  }

  @Override
  protected void onRestoreInstanceState(@Nonnull Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  public void showSoftKeyboard() {
    if (getCurrentFocus() != null) {
      InputMethodManager inputMethodManager =
        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.showSoftInput(getCurrentFocus(), 0);
    }
  }

  @Override
  public void startActivity(Intent intent, @Nullable Bundle options) {
    try {
      Reflect.announce(true);
      if (BaseDialogs.isDialogShowing()) {
        for (Dialog dialog : BaseDialogs.getShowing()) {
          dialog.dismiss();
        }
        onUI(() -> startActivity(intent, options), 500);
      } else {
        super.startActivity(intent, options);
      }
    } catch (Throwable t) {
      t.printStackTrace();
    } finally {
      Reflect.announce(false);
    }
  }

  @SuppressWarnings("deprecation")
  @Override
  public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
    try {
      Reflect.announce(true);
      if (BaseDialogs.isDialogShowing()) {
        XLog.i(TAG, "delaying startActivity For result");
        onUI(() -> startActivityForResult(intent, requestCode, options), 500);
      } else {
        XLog.i(TAG, "calling startActivity For result");
        super.startActivityForResult(intent, requestCode, options);
      }
    } finally {
      Reflect.announce(false);
    }
  }

  @CallSuper
  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
  }

  public void refresh() {
    BaseApp.req().refresh(this);
  }

  public void finish() {
    if (BaseDialogs.isDialogShowing()) {
      onUI(this::finish, 500);
    } else {
      super.finish();
    }
  }

  @Override
  public BaseActivity activity() {
    return this;
  }

  @Override
  public void openChat(final XEntity publicCell) {
  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  protected void onResume() {
    super.onResume();
    //    BaseApp.get().setInstanceStateSaved(false);
    hideSoftKeyboard();
  }

  public void setDisplayUpAsHome() {
    final ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
      actionBar.setDisplayShowHomeEnabled(true);
    }
  }

  public void hideSoftKeyboard() {
    if (getCurrentFocus() != null) {
      InputMethodManager inputMethodManager =
        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
  }

  @CallSuper
  public void loadData2() {
    Reflect.announce(true);
    setLastReloadTime(2);
  }

  @CallSuper
  public void loadData1() {
    Reflect.announce(true);
    setLastReloadTime(1);
  }

  @CallSuper
  public void loadData3() {
    Reflect.announce(true);
    setLastReloadTime(3);
  }

  public boolean hasPerm(String thePerm) {
    return checkSelfPermission(thePerm) == PERMISSION_GRANTED;
  }

}