package cell411.ui.base;

import androidx.annotation.LayoutRes;

import com.safearx.cell411.R;

public class EmptyFragment extends ModelBaseFragment {
  public EmptyFragment() {
    super(R.layout.empty_layout);
  }

  public EmptyFragment(@LayoutRes int layout) {
    super(layout);
  }
}
