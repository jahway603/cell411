package cell411.ui.base;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Looper;
import android.widget.Toast;
import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import cell411.android.ConnectivityNotifier;
import cell411.android.ManifestInfo;
import cell411.android.Reloadable;
import cell411.android.Reloader;
import cell411.libcell.Config;
import cell411.libcell.ConfigDepot;
import cell411.methods.Dialogs;
import cell411.parse.XAlert;
import cell411.parse.XChatMsg;
import cell411.parse.XChatRoom;
import cell411.parse.XPrivacyPolicy;
import cell411.parse.XPrivateCell;
import cell411.parse.XPublicCell;
import cell411.parse.XRelationshipUpdate;
import cell411.parse.XRequest;
import cell411.parse.XResponse;
import cell411.parse.XUser;
import cell411.services.DataService;
import cell411.services.LocationService;
import cell411.utils.CarefulHandler;
import cell411.utils.ExceptionHandler;
import cell411.utils.HttpLogInterceptor;
import cell411.utils.ImageUtils;
import cell411.utils.LifeCycleIH;
import cell411.utils.LocationUtil;
import cell411.utils.ObservableValue;
import cell411.utils.OnCompletionListener;
import cell411.utils.Reflect;
import cell411.utils.TheWolf;
import cell411.utils.UncaughtHandler;
import cell411.utils.Util;
import cell411.utils.ValueObserver;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import cell411.utils.concurrant.STPExecutorPlus;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.http.ParseSyncUtils;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseUser;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;

import javax.annotation.Nonnull;
import java.io.File;
import java.lang.ref.WeakReference;
import java.net.SocketException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicReference;

public abstract class BaseApp extends Application implements BaseContext,
    ExceptionHandler,
    RealMessageForwarder,
    RealExceptionHandler {

  public final static CarefulHandler smUIHandler;
  private final static XTAG TAG = new XTAG();
  private static final boolean smUseInterceptor = false;
  private static final ThreadStuff smThreadStuff = new ThreadStuff();
//  private static final ByteArrayOutputStream smBAOS;
//  private static final SimplePrintStream smSPS;
//  static {
//    smBAOS=new ByteArrayOutputStream();
//    smSPS = new SimplePrintStream(smBAOS);
//    System.setOut(smSPS);
//    System.setErr(smSPS);
//  }
//  private static final Redirect smRedir;
  static WeakReference<BaseApp> smInstance;

  static {
    smUIHandler = new CarefulHandler(Looper.getMainLooper());
    UncaughtHandler.startCatcher();
//    smRedir = new Redirect();
  }

  final AtomicReference<Config> smConfig;
  final DataService mDataService = DataService.init(this);
  final LocationService mLocationService = new LocationService(this);
  final ObservableValue<Boolean> smConnected = new ObservableValue<>(Boolean.class, false);
  final ConnectListener mConnectListener = new ConnectListener();
  private final Stack<BaseActivity> mActivityStack = new Stack<>();
  private final ScreenOffReceiver mScreenOffReceiver = new ScreenOffReceiver();
  private final ObservableValue<MainActivity> mMainActivity = new ObservableValue<>(MainActivity.class, null);
  private OkHttpClient smClient;
  private OkHttpClient.Builder mBuilder;

  {
    smConfig = new AtomicReference<>();
  }

  public BaseApp() {
    TheWolf.setRealExceptionHandler(this);
  }

  public static LocationService getLocationService() {
    return req().mLocationService;
  }

  public static BaseApp req() {
    return Objects.requireNonNull(opt());
  }

  @Nullable
  public static BaseApp opt() {
    BaseApp result = null;
    if (smInstance != null) {
      result = smInstance.get();
    }
    return result;
  }

  public static CarefulHandler getUIHandler() {
    return smUIHandler;
  }

  public static DataService getDataServer() {
    return req().mDataService;
  }

  public static void callRefresh(Reloadable reloadable) {
    req().refresh(reloadable);
  }

  //  Object o = Executors.newSingleThreadExecutor(new ThreadFactory() {
//    @Override
//    public Thread newThread(Runnable r) {
//      return Looper.getMainLooper().getThread();
//    }
//  });
  public final void refresh(Reloadable reloadable) {
    if (reloadable != null) {
      getExec().execute(new Reloader(this, reloadable));
    }
  }

  public static STPExecutorPlus getExecutor() {
    return smThreadStuff.getExecutor();
  }

  public void initParse() {
    if (Parse.isInitialized()) {
      return;
    }
    Reflect.announce("Starting Parse");
    Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
    ParseSyncUtils.registerSubclass(XUser.class);
    ParseSyncUtils.registerSubclass(XPublicCell.class);
    ParseSyncUtils.registerSubclass(XAlert.class);
    ParseSyncUtils.registerSubclass(XPrivateCell.class);
    ParseSyncUtils.registerSubclass(XPrivacyPolicy.class);
    ParseSyncUtils.registerSubclass(XResponse.class);
    ParseSyncUtils.registerSubclass(XRequest.class);
    ParseSyncUtils.registerSubclass(XChatRoom.class);
    ParseSyncUtils.registerSubclass(XChatMsg.class);
    ParseSyncUtils.registerSubclass(XRelationshipUpdate.class);

    ParseSyncUtils.registerParseSubclasses();
    Parse.Configuration.Builder builder = new Parse.Configuration.Builder();

    String app = ConfigDepot.getAppId();
    String url = ConfigDepot.getParseUrl();
    String key = ConfigDepot.getClientKey();
    String flavor = ConfigDepot.getFlavor();
    //      assert flavor.equals("empty");

    while (url.endsWith("/")) {
      url = url.substring(0, url.length() - 1);
    }
    url = url + "/" + flavor + "/";

    builder.applicationId(app);
    builder.clientKey(key);
    builder.server(url);
    builder.clientBuilder(getClientBuilder());
    Parse.initialize(builder.build());
    Reflect.announce("Got Parse");
  }

  public synchronized OkHttpClient.Builder getClientBuilder() {
    if (mBuilder == null) {
      mBuilder = new OkHttpClient.Builder();
      if (smUseInterceptor) {
        mBuilder.addInterceptor(new HttpLogInterceptor());
      }
    }
    return mBuilder;
  }

  public synchronized OkHttpClient getHttpClient() {
    if (smClient == null) {
      smClient = getClientBuilder().build();
    }
    return smClient;
  }

  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
  }

  public Executor getMainExecutor() {
    return this::onUI;
  }

  public ApplicationInfo getApplicationInfo() {
    if (getBaseContext() == null) return null;
    return super.getApplicationInfo();
  }

  @CallSuper
  @Override
  public void onCreate() {
    ConfigDepot.init(new ConfigImpl());
    registerActivityLifecycleCallbacks(LifeCycleIH.create());
    Reflect.announce("BaseApp");
    super.onCreate();
    assert smInstance == null;
    smInstance = new WeakReference<>(this);
    TheWolf.showToast("Starting");
    Reflect.announce("BaseApp");
    setupConnectivityWatcher();
    addConnectionListener((newValue, oldValue) -> System.out.println(mConnectListener));

    LocationService.addObserver(new LocationObserver());
  }

  private void setupConnectivityWatcher() {
    ConnectivityNotifier notifier = ConnectivityNotifier.getNotifier(this);
    notifier.addListener(mConnectListener);
    smConnected.set(null);
    mConnectListener.networkConnectivityStatusChanged(this, null);
  }

  //    smConfig.set(new ConfigImpl(this));

  public void addConnectionListener(ValueObserver<Boolean> listener) {
    smConnected.addObserver(listener);
  }

  @Override
  public void realShowAlertDialog(final String message, final OnCompletionListener listener) {
    if (req().isOnUI()) {
      BaseDialogs.realShowAlertDialog(message, listener);
    } else {
      req().onUI(() -> realShowAlertDialog(message, listener));
    }
  }

  @Override
  public void realShowYesNoDialog(String title, String text, OnCompletionListener listener) {
    Dialogs.showYesNoDialog(title, text, listener);
  }

  @Override
  public void realShowToast(String msg) {
    BaseActivity curr = getCurrentActivity();
    if (curr != null && req().isOnUI()) {
      Toast.makeText(curr, msg, Toast.LENGTH_LONG).show();
    } else {
      req().onUI(() -> realShowToast(msg), 250);
    }
  }

  @Override
  public void realHandleException(@Nonnull String whileDoing, @Nonnull Throwable pe, @Nullable OnCompletionListener listener, final boolean dialog) {
    if (pe instanceof ThreadDeath) {
      System.out.println("" + pe);
      throw (ThreadDeath) pe;
    }
    pe.printStackTrace();
    if (isBadSessionException(pe)) {
      handleBadSessionException();
    } else if (isNetworkException(pe) && !smConnected.get()) {
      announce("Ignoring network exception while disconnected");
    } else if (isBadGatewayException(pe)) {
      XLog.i(TAG, "Ignoring bad gateway exception");
    } else if (dialog) {
      BaseDialogs.showExceptionDialog(pe, whileDoing, listener);
    }
  }

  public static boolean isBadSessionException(Throwable e) {
    if (e instanceof ParseException) {
      ParseException pe = (ParseException) e;
      switch (pe.getCode()) {
        case ParseException.SESSION_MISSING:
        case ParseException.INVALID_LINKED_SESSION:
        case ParseException.INVALID_SESSION_TOKEN:
          return true;
      }
    }
    return false;
  }

  private void handleBadSessionException() {
    showSessionExpiredDialog();
  }

  private boolean isNetworkException(final Throwable t) {
    if (t instanceof ParseException) {
      ParseException pe = (ParseException) t;
      Throwable cause = pe.getCause();
      if (pe.getCode() == ParseException.CONNECTION_FAILED) {
        if (cause instanceof SocketException) {
          SocketException se = (SocketException) cause;
          String message = se.getMessage();
          if (message == null) {
            return false;
          } else {
            return message.equals("Software caused connection abort");
          }
        }
      }
    }
    return false;
  }

  private boolean isBadGatewayException(Throwable e) {
    if (!(e instanceof ParseException)) {
      return false;
    }
    ParseException pe = (ParseException) e;
    String message = pe.getMessage();
    return "502 Bad Gateway".equals(message);
  }

  private void showSessionExpiredDialog() {
    XUser.logOut();
    BaseDialogs.showSessionExpiredAlertDialog(success -> {
      try {
        XUser.logOut();
      } catch (Exception e) {
        e.printStackTrace();
      }
      try {
        XUser.logOut();
      } catch (Exception e) {
        e.printStackTrace();
      }
      Intent intent = new Intent(BaseApp.this, getMainActivityClass());
      intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
      intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
      BaseActivity activity = getCurrentActivity();
      if (activity == null) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
      } else {
        activity.startActivity(intent);
      }
    });
  }

  protected abstract Class<?> getMainActivityClass();

  public BaseActivity getCurrentActivity() {
    if (hasCurrentActivity()) {
      return mActivityStack.peek();
    } else {
      return null;
    }
  }

  public boolean hasCurrentActivity() {
    return !mActivityStack.isEmpty();
  }

  public boolean isConnected() {
    return smConnected.get() == Boolean.TRUE;
  }

  @Override
  protected void finalize() throws Throwable {
    removeConnectionListener(null);
    System.out.println(getScreenOffReceiver());
    super.finalize();
  }

  public void removeConnectionListener(ValueObserver<Boolean> changed) {
    smConnected.removeObserver(changed);
  }

  public ScreenOffReceiver getScreenOffReceiver() {
    return mScreenOffReceiver;
  }

  public void onBootComplete() {
    XLog.i(TAG, "onBootComplete called");
  }

  public Set<String> getMissingPermissions() {
    BaseActivity activity = activity();
    if (activity == null) {
      return getAllPermissions();
    }
    return getMissingPermissions(activity);
  }

  @Override
  public BaseActivity activity() {
    return getCurrentActivity();
  }

  public abstract Set<String> getAllPermissions();

  public Set<String> getMissingPermissions(BaseActivity activity) {
    HashSet<String> res = new HashSet<>();
    for (String perm : getAllPermissions()) {
      if (!activity.hasPerm(perm)) {
        res.add(perm);
      }
    }
    return res;
  }

  public STPExecutorPlus getExec() {
    return getExecutor();
  }

  public CarefulHandler onUI() {
    return getUIHandler();
  }

  public void onUI(Runnable runnable, long delay) {
    onUI().postDelayed(runnable, delay);
  }

  public void onUI(Runnable runnable) {
    smUIHandler.postDelayed(runnable, 0);
  }

  public SharedPreferences getAppPrefs() {
    return getSharedPreferences("AppPrefs", MODE_PRIVATE);
  }

  public File getJsonCacheFile(String name) {
    return new File(ConfigDepot.getFlavorDocDir(), name + ".json");
  }

  protected abstract void sendLocationNotification(ParseGeoPoint geoPoint);

  public void pushActivity(BaseActivity baseActivity) {
    mActivityStack.push(baseActivity);
    if (baseActivity instanceof MainActivity) {
      mMainActivity.set((MainActivity) baseActivity);
    }
  }

  public void popActivity(BaseActivity baseActivity) {
    assert mActivityStack.peek() == baseActivity;
    mActivityStack.pop();
  }

  public MainActivity getMainActivity() {
    return mMainActivity.get();
  }

  public Boolean hasPermissions() {
    Set<String> missing = getAllPermissions();
    BaseActivity activity = getCurrentActivity();
    if (activity == null) {
      return false;
    }
    for (String perm : missing) {
      if (!activity.hasPerm(perm)) {
        return false;
      }
    }
    return true;
  }

  abstract public int getPrimaryColor();

  public abstract boolean isDone();

  public abstract int getNotificationWidth();

  public abstract int getNotificationHeight();

  public void sendToast(Throwable e) {
    showToast(String.valueOf(e));
  }

  public void sendToast(String format, Object[] args) {
    showToast(format, args);
  }

  public void sendToast(int format, Object[] args) {
    showToast(format, args);
  }

  static class ScreenOffReceiver extends BroadcastReceiver {
    boolean mScreenOn = true;
    String mText = "[ ScreenOffReceiver: Not yet set ]";

    @Override
    public void onReceive(Context context, Intent intent) {
      mText = Util.format("ScreenOffReceiver received:  %s", mScreenOn);
      mScreenOn = intent.getAction().equals(Intent.ACTION_SCREEN_ON);
    }

    @Nonnull
    public String toString() {
      return mText;
    }
  }

//  static class Redirect {
//    final PrintStream mOut = System.out;
//    final PrintStream mErr = System.err;
//    final PrintString mMe = new PrintString();
//
//    Redirect() {
//    }
//
//    @Nonnull
//    public String toString() {
//      return mMe.toString();
//    }
//
//    void attach() {
//      synchronized (System.out) {
//        Reflect.announce("Replacing System Out And Err");
//        System.setOut(mMe);
//        System.setErr(mMe);
//      }
//    }
//  }

  static class ThreadStuff {
    final STPExecutorPlus mExecutor;

    final ThreadFactory mFactory;
    final ThreadGroup mGroup;

    {
      mFactory = new GroupedThreadFactory();
      mExecutor = new STPExecutorPlus(5, mFactory);
      Thread thread = Thread.currentThread();
      ThreadFactory tf = getFactory();
      ThreadGroup parent = thread.getThreadGroup();
      while (parent != null && parent.getParent() != null) {
        parent = parent.getParent();
      }
      mGroup = new ThreadGroup(parent, "[[[ Cell411 Thread Group ]]]");
    }

    ThreadStuff() {
    }

    ThreadGroup getGroup() {
      return mGroup;
    }

    public STPExecutorPlus getExecutor() {
      return mExecutor;
    }

    ThreadFactory getFactory() {
      return mFactory;
    }

    class GroupedThreadFactory implements ThreadFactory {
      @Override
      public Thread newThread(Runnable r) {
        return new Thread(getGroup(), r);
      }
    }

  }

  class LocationObserver implements ValueObserver<Location>,
      Runnable {
    Location mNewValue;
    Location mOldValue;
    long mLastUpdate = 0;
    long mLastSave = 0;

    @Override
    public void onChange(@Nullable final Location newValue, @Nullable final Location oldValue) {
      mNewValue = newValue;
      mOldValue = oldValue;
      mLastUpdate = System.currentTimeMillis();
      run();
    }

    @Override
    public void run() {
      if (mLastSave >= mLastUpdate) {
        return;
      }
      if (!Parse.isInitialized() || ParseUser.getCurrentUser()==null) {
        return;
      }
      ParseGeoPoint geoPoint = LocationUtil.getGeoPoint(mNewValue);
      if (geoPoint == null) {
        return;
      }
      XUser user = XUser.getCurrentUser();
      ParseGeoPoint lastLoc = user.getLocation();
      if (lastLoc == null || geoPoint.distanceInKilometersTo(lastLoc) > 0.1) {
        sendLocationNotification(geoPoint);
        user.setLocation(geoPoint);
        user.saveInBackground();
      }
    }

  }

  class ConnectListener implements ConnectivityNotifier.ConnectivityListener {
    @Override
    public void networkConnectivityStatusChanged(Context context, Intent intent) {
      smConnected.set(ConnectivityNotifier.isConnected(context));
    }

    @Nonnull
    public String toString() {
      return Util.format("[ConnectListener; Net is %s ]", smConnected);
    }
  }

  class ConfigImpl implements Config,
      Runnable {
    private final ApplicationInfo mAppInfo;
    private final Builder mHttpClientBuilder;
    private final String mPackageName;
    private final PackageInfo mPkgInfo;
    private final String mAppVersion;
    private final String mAppName;
    private final File mParseDir;
    private final ObservableValue<Boolean> mConnected = new ObservableValue<>(false);

    @SuppressWarnings("deprecation")
    public ConfigImpl() {
      try {
        PackageManager pm = getPackageManager();
        mPackageName = BaseApp.this.getPackageName();
        mParseDir = BaseApp.this.getDir("Parse", MODE_PRIVATE);
        mPkgInfo = pm.getPackageInfo(mPackageName, 0);
        mAppVersion = String.valueOf(mPkgInfo.versionCode);
        mAppInfo = pm.getApplicationInfo(mPackageName, 0);
        mAppName = pm.getApplicationLabel(mAppInfo).toString();
        mHttpClientBuilder = new Builder();
        addConnectionListener(mConnected);
      } catch (Exception ex) {
        throw Util.rethrow(ex);
      }
    }

    @Override
    public int getVersionCode() {
      return ManifestInfo.req().getVersionCode();
    }

    @Override
    public Builder getHttpClientBuilder() {
      return mHttpClientBuilder;
    }

    @Override
    public STPExecutorPlus getExecutor() {
      return Cell411.getExecutor();
    }

    @Override
    public String getAppId() {
      return getString(R.string.parse_application_id);
    }

    @Override
    public String getAppName() {
      return mAppName;
    }

    @Override
    public String getAppVersion() {
      return mAppVersion;
    }

    @Override
    public String getClientKey() {
      return getString(R.string.parse_client_key);
    }

    @Override
    public String getFlavor() {
      return getString(R.string.parse_api_flavor);
    }

    @Override
    public String getPackageName() {
      return mPackageName;
    }

    @Override
    public String getParseUrl() {
      return getString(R.string.parse_api_url);
    }

    @Override
    public String getString(@StringRes int resId) {
      return req().getString(resId);
    }

    @Override
    public String getVersionName() {
      return ManifestInfo.req().getVersionName();
    }

    @Override
    public File getExtDir(String sub) {
      return getDir(sub, Context.MODE_PRIVATE);
    }

    public Bitmap getPlaceHolder() {
      return ImageUtils.getPlaceHolder();
    }

    @Override
    public Application getApp() {
      return BaseApp.this;
    }

    public File getParseDir() {
      return mParseDir;
    }

    @Override
    public boolean isConnected() {
      return mConnected.get() != null && mConnected.get();
    }

    @Override
    public void run() {
    }

  }
}
