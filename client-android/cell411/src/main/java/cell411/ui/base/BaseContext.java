package cell411.ui.base;

import android.content.SharedPreferences;
import androidx.annotation.StringRes;
import cell411.parse.XEntity;
import cell411.services.DataService;
import cell411.services.LocationService;
import cell411.utils.*;
import cell411.utils.concurrant.STPExecutorPlus;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.TimeUnit;

public interface BaseContext extends ExceptionHandler {
  default void announce(String fmt, Object... args) {
    Reflect.announce(fmt, args);
  }

  @Nullable
  default BaseActivity activity() {
    return app().getCurrentActivity();
  }

  //  default Handler onXP() {
  //    return XParse.getXPHandler();
  //  }
  //  default void onXP(Runnable runnable) {
  //    onXP().post(runnable);
  //  }
  //
  //  default void onXP(Runnable runnable, long delay) {
  //    onXP().postDelayed(runnable, delay);
  //  }

  @Nonnull
  default BaseApp app() {
    return BaseApp.req();
  }

  default String getString(@StringRes int resId) {
    return require(activity()).getString(resId);
  }

  default String getString(@StringRes int resId, Object... objs) {
    return require(activity()).getString(resId, objs);
  }

  default <X> X require(X object) {
    if (object == null) {
      throw new NullPointerException("Required object was null");
    }
    return object;
  }

  default void hideSoftKeyboard() {
    BaseActivity activity = activity();
    if (activity != null) {
      activity.hideSoftKeyboard();
    }
  }

  default void showSoftKeyboard() {
    BaseActivity activity = activity();
    if (activity != null) {
      activity.showSoftKeyboard();
    }
  }

  default DataService ds() {
    return BaseApp.getDataServer();
  }

  default LocationService ls() {
    return BaseApp.getLocationService();
  }

  default STPExecutorPlus onRF() {
    return BaseApp.getExecutor();
  }

  default STPExecutorPlus getExec() {
    return BaseApp.getExecutor();
  }

  default void onRF(Runnable runnable) {
    onRF(runnable, 500);
  }

  default void onRF(Runnable runnable, long delay) {
    onRF().schedule(runnable, delay, TimeUnit.MILLISECONDS);
  }

  default CarefulHandler onUI() {
    return BaseApp.getUIHandler();
  }

  default void onUI(Runnable runnable, long delay) {
    onUI().postDelayed(runnable, delay);
  }

  default void onUI(Runnable runnable) {
    onUI().post(runnable);
  }

  //  default boolean isOnLQ()
  //  {
  //    return onLQ().getLooper().isCurrentThread();
  //  }
  default boolean isOnUI() {
    return onUI().getLooper().isCurrentThread();
  }

  default boolean isOnRF() {
    return !isOnUI();
  }

  default SharedPreferences getAppPrefs() {
    return app().getAppPrefs();
  }

  //  default void onLQ(Runnable runnable){
  //    onLQ(runnable,0);
  //  }

  //  default void onLQ(Runnable runnable, long delay) {
  //    Handler handler = onLQ();
  //    if (handler == null) {
  //      onUI(() -> {
  //        onLQ(runnable, 200);
  //      }, 200);
  //    } else {
  //      handler.postDelayed(runnable, delay);
  //    }
  //  }

  default void openChat(XEntity publicCell) {
    app().openChat(publicCell);
  }
}
