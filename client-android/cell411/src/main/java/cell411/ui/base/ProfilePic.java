// package cell411.ui.base;
//
// import android.content.Context;
// import android.graphics.Bitmap;
// import android.graphics.drawable.ColorDrawable;
// import android.util.AttributeSet;
// import android.view.View;
// import android.view.View.OnClickListener;
// import androidx.annotation.Nullable;
// import androidx.appcompat.widget.AppCompatImageView;
// import cell411.imgstore.CachedStatus;
// import cell411.imgstore.ImageListener;
// import cell411.imgstore.ImageStore;
// import cell411.parse.XUser;
// import cell411.ui.profile.ProfileImageFragment;
// import com.safearx.cell411.Cell411;
//
// import java.net.URL;
//
// public class ProfilePic
//   extends AppCompatImageView
//   implements OnClickListener
// {
//   static FragmentFactory smFragmentFactory = FragmentFactory.fromClass(
//     ProfileImageFragment.class);
//   XUser mUser;
//
//   public ProfilePic(Context context)
//   {
//     this(context,null,0);
//   }
//
//   public ProfilePic(Context context, @Nullable AttributeSet attrs)
//   {
//     this(context, attrs,0);
//   }
//
//   public ProfilePic(Context context, @Nullable AttributeSet attrs,
//                     int defStyleAttr)
//   {
//     super(context, attrs, defStyleAttr);
//     setOnClickListener(this);
//   }
//
//   @Override
//   public void setOnClickListener(OnClickListener listener){
//     if(hasOnClickListeners())
//       throw new RuntimeException("Cannot reset OnClickListener");
//     else
//       super.setOnClickListener(listener);
//   }
//   @Override
//   public boolean hasOnClickListeners()
//   {
//     return super.hasOnClickListeners();
//   }
//
//   public void setUser(XUser user)
//   {
//     mUser=user;
//     ImageStore.get().loadImage(mUser, new ImageListener()
//     {
//       @Override
//       public void onLoadingFailed(String imageUri, CachedStatus cachedStatus)
//       {
//         setImageDrawable(new ColorDrawable(0xff000000));
//       }
//
//       @Override
//       public void onLoadingComplete(URL imageUri, Bitmap loadedImage)
//       {
//         setImage()
//       }
//     });
//   }
//
//   @Override
//   public void onClick(View v)
//   {
//     smFragmentFactory.setObjectId(mUser.getObjectId());
//     Cell411.req().getMainActivity().push(smFragmentFactory);
//   }
// }
//