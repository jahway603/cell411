package cell411.ui.base;

import cell411.utils.OnCompletionListener;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface RealExceptionHandler
{
  void realShowAlertDialog(String message, OnCompletionListener listener);

  void realShowToast(String msg);

  void realHandleException(@Nonnull String whileDoing, @Nonnull Throwable pe,
                           @Nullable OnCompletionListener listener,
                           boolean dialog);

  boolean isNotReady();

  void realShowYesNoDialog(String title, String text, OnCompletionListener listener);
}
