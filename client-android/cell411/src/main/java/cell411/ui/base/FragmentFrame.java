package cell411.ui.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.safearx.cell411.Cell411;

import java.util.Stack;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class FragmentFrame extends FrameLayout
  implements BaseContext {
  final Stack<FragmentFactory> mFragmentStack = new Stack<>();
  FragmentManager mFragMan;

  public FragmentFrame(@Nonnull Context context) {
    super(context);
  }

  public FragmentFrame(@Nonnull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public FragmentFrame(@Nonnull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  void setFragMan(FragmentManager fragMan) {
    assert mFragMan == null;
    mFragMan = fragMan;
  }

  @Override
  protected void attachViewToParent(View child, int index, ViewGroup.LayoutParams params) {
    if (mFragMan == null) {
      throw new IllegalStateException("Should have FragMan First");
    }
    super.attachViewToParent(child, index, params);
  }

  public int getDepth() {
    return mFragmentStack.size();
  }

  public void onBackPressed() {
    pop();
  }

  public void push(final FragmentFactory newFactory) {
    push(newFactory, false, false);
  }

  public boolean pop() {
    push(null, true, false);
    return true;
  }

  public void popAll() {
    push(null, true, true);
  }

  public boolean isEmpty() {
    return mFragmentStack.isEmpty();
  }

  public void push(FragmentFactory factory, boolean pop, boolean all) {
    Runnable runnable = new FragPusher(factory, pop, all);
    if (isOnUI()) {
      runnable.run();
    } else {
      Cell411.req().onUI(runnable);
    }

  }

  public FragmentFactory getTopFactory() {
    if (isEmpty()) {
      return null;
    } else {
      return mFragmentStack.peek();
    }
  }

  private class FragPusher implements Runnable {
    final private boolean mAll;
    final private boolean mPop;
    private FragmentFactory mNewFactory;

    public FragPusher(FragmentFactory factory, boolean pop, boolean all) {
      mNewFactory = factory;
      mPop = pop;
      mAll = all;
    }

    @Override
    public void run() {
      FragmentTransaction ft = mFragMan.beginTransaction();
      if (mAll) {
        for (int i = 0; i < mFragmentStack.size(); i++) {
          FragmentFactory factory = mFragmentStack.remove(i--);
          BaseFragment fragment = factory.get(false);
          if (fragment != null && fragment.isAdded()) {
            ft.remove(fragment);
          }
        }
      } else if (mPop && !mFragmentStack.isEmpty()) {
        FragmentFactory factory = mFragmentStack.pop();
        BaseFragment fragment = factory.get(false);
        if (fragment != null && fragment.isAdded()) {
          ft.remove(fragment);
        }
      } else if (!mFragmentStack.isEmpty()) {
        FragmentFactory factory = mFragmentStack.peek();
        BaseFragment fragment = factory.get(false);
        if (fragment != null && fragment.isAdded()) {
          ft.hide(fragment);
        }
      }
      if (mNewFactory != null) {
        mFragmentStack.push(mNewFactory);
      }
      if (!mFragmentStack.isEmpty()) {
        mNewFactory = mFragmentStack.peek();
        Fragment newFragment = mNewFactory.get(true);
        if (!newFragment.isAdded()) {
          ft.add(getId(), newFragment);
        }
        ft.show(newFragment);
      }
      ft.commitNowAllowingStateLoss();
    }
  }
}

