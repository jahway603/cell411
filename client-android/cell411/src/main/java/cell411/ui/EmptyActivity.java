package cell411.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.safearx.cell411.R;

public class EmptyActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_empty);
  }
}