package cell411.ui.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.IdRes;
import cell411.android.RingtoneData;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.Reflect;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.TreeMap;

import static android.media.RingtoneManager.ACTION_RINGTONE_PICKER;
import static android.media.RingtoneManager.EXTRA_RINGTONE_EXISTING_URI;
import static android.media.RingtoneManager.EXTRA_RINGTONE_PICKED_URI;

public class SelectRingtoneFragment extends ModelBaseFragment {
  private final String[] mKeys = Cell411.req().getTones().keySet().toArray(new String[0]);
  Button mDone;
  int mIdx = -1;
  Button[] mPickButtons = new Button[]{
    null,
    null,
    null
  };
  TextView[] mTextViews = new TextView[]{
    null,
    null,
    null
  };
  Button[] mFauxButtons = new Button[]{
    null,
    null,
    null
  };
  PickRingtone mContract = new PickRingtone();
  //  FakeStuff mFakeStuff = new FakeStuff();
  private View mView;

  public SelectRingtoneFragment() {
    super(R.layout.select_ringtone_fragment);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState) {
    Reflect.announce(true);
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  public void setup(int idx, @IdRes int txt, @IdRes int btn, @IdRes int faux) {
    String text = "";
    String key = mKeys[idx];
    RingtoneData tone = getData().get(key);
    if (tone != null) {
      text = tone.getTitle();
    }

    mPickButtons[idx] = mView.findViewById(btn);
    mTextViews[idx] = mView.findViewById(txt);
    mFauxButtons[idx] = mView.findViewById(faux);
    Button pickButton, fauxButton;
    TextView textView;

    Arrays.asList(pickButton = mPickButtons[idx], textView = mTextViews[idx],
      mFauxButtons[idx]).forEach((view) -> view.setTag(idx));

    pickButton.setTag(idx);
    textView.setTag(idx);
    textView.setText(text);
    pickButton.setOnClickListener(this::onButtonClicked);
  }

//  private void onSimulateClicked(final View view)
//  {
//    if (isOnUI())
//    {
//      BaseApp.getExecutor().post(() -> onSimulateClicked(view));
//      return;
//    }
//    NotificationAgent agent = Cell411.get().getNotificationAgent();
//    agent.restore();
//    int tag = (int) view.getTag();
//    switch (tag)
//    {
//      case 0:
//        agent.sendAlertNotification(mFakeStuff.alert());
//        break;
//      case 1:
//        agent.sendRequestNotification(mFakeStuff.request());
//        break;
//      case 2:
//        agent.sendChatNotification(mFakeStuff.chatMsg());
//        break;
//    }}

  public void restore() {
    setup(0, R.id.txt_alert_ringtone, R.id.btn_alert_ringtone, R.id.btn_faux_alert);
    setup(1, R.id.txt_request_ringtone, R.id.btn_request_ringtone, R.id.btn_faux_request);
    setup(2, R.id.txt_chat_ringtone, R.id.btn_chat_ringtone, R.id.btn_faux_chat);
  }  ActivityResultCallback<Uri> mCallback = result ->
  {
    if (result == null) {
      return;
    }

    RingtoneData.addRingtoneData(mKeys[mIdx], result);
    restore();
  };

  @Override
  public void onViewCreated(@Nonnull final View view, @Nullable final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mView = view;
    mDone = mView.findViewById(R.id.btn_done);
    mDone.setOnClickListener(this::onDoneButtonClicked);
    restore();
  }

  private void onDoneButtonClicked(final View view) {
    finish();
  }

  private void onButtonClicked(final View view) {
    mIdx = (int) (Integer) view.getTag();
    String key = mKeys[mIdx];
    mLauncher.launch(getData().get(key));
  }

  public TreeMap<String, RingtoneData> getData() {
    return Cell411.req().getTones();
  }

  public static class PickRingtone extends ActivityResultContract<RingtoneData, Uri> {
    @Nonnull
    @Override
    public Intent createIntent(@Nonnull Context context, @Nullable RingtoneData ringtoneData) {
      Intent intent = new Intent(ACTION_RINGTONE_PICKER);
      if (ringtoneData != null) {
        intent.putExtra(EXTRA_RINGTONE_EXISTING_URI, ringtoneData.getUri());
      }
      return intent;
    }

    @Override
    public Uri parseResult(int resultCode, @Nullable Intent result) {
      if (resultCode != Activity.RESULT_OK || result == null) {
        return null;
      }

      if (VERSION.SDK_INT >= VERSION_CODES.TIRAMISU) {
        return result.getParcelableExtra(EXTRA_RINGTONE_PICKED_URI, Uri.class);
      } else {
        //noinspection deprecation
        return result.getParcelableExtra(EXTRA_RINGTONE_PICKED_URI);
      }
    }
  }

  ActivityResultLauncher<RingtoneData> mLauncher = registerForActivityResult(mContract, mCallback);



}
