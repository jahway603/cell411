package cell411.ui.welcome;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import cell411.ui.base.*;
import com.parse.ParseException;
import com.parse.model.ParseUser;
import com.safearx.cell411.R;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.parse.CountryInfo;
import cell411.parse.XUser;
import cell411.ui.Cell411GuiUtils;
import cell411.utils.Util;
import cell411.utils.XLog;
import cell411.utils.XTAG;

/**
 * Created by Sachin on 14-04-2016.
 */
public class RegisterFragment extends BaseFragment {
  private static final XTAG TAG = new XTAG();

  static {
    XLog.i(TAG, "loading class");
  }

  private EditText etEmail;
  private EditText etPassword;
  private EditText etFirstName;
  private EditText etLastName;
  private EditText etMobile;
  private Spinner spCountryCode;
  private AccountFragment mAccountFragment;

  public RegisterFragment() {
    super(R.layout.fragment_register);
  }

  @Nonnull
  private ClickableSpan getClickableSpan(URLSpan span) {
    return new ClickableSpan() {
      public void onClick(View view) {
        if (span.getURL().equals("terms")) {
          Intent intentWeb = new Intent(Intent.ACTION_VIEW);
          intentWeb.setData(Uri.parse(getString(R.string.terms_and_conditions_url)));
          startActivity(intentWeb);
        }
      }
    };
  }

  public void onClick(View v) {
    int id = v.getId();
    if (id == R.id.txt_btn_sign_up) {
      signUp();
    } else if (id == R.id.lbl_terms_and_conditions_part_1) {
      Intent intentWeb = new Intent(Intent.ACTION_VIEW);
      intentWeb.setData(Uri.parse(getString(R.string.terms_and_conditions_url)));
      startActivity(intentWeb);
    }
  }

  private void signUp() {
    if (isOnUI()) {
      BaseApp.getExecutor().execute(this::signUp);
      return;
    }
    try {
      Map<String, String> vals = new TreeMap<>();
      vals.put("email", getText(etEmail, R.string.validation_email));
      vals.put("password", getText(etPassword, R.string.validation_password));
      vals.put("firstName", getText(etFirstName, R.string.validation_firstname));
      vals.put("lastName", getText(etLastName, R.string.validation_lastname));
      vals.put("mobileNumber", getText(etMobile, R.string.validation_mobile_number));
      XUser user = new XUser();

      String temp = vals.get("email");
      if (temp != null && temp.contains("@")) {
        vals.put("email", Util.lc(Util.trim(temp)));
      } else {
        throw new ParseException(ParseException.OTHER_CAUSE,
          getString(R.string.validation_email_invalid));
      }
      user.setUsername(temp);
      user.setEmail(temp);
      temp = Objects.requireNonNull(vals.get("mobileNumber"));
      if (!temp.startsWith("+")) {
        String cc = ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode;
        vals.put("mobileNumber", cc + temp);
      }
      user.setMobileNumber(temp);
      user.setPassword(vals.get("password"));
      user.setFirstName(Objects.requireNonNull(vals.get("firstName")));
      user.setLastName(Objects.requireNonNull(vals.get("lastName")));
      user.setPatrolMode(false);
      user.setEmailVerified(false);
      user.put("roleId", 0);
      user.put("phoneVerified", false);
      user.put("newPublicCellAlert", false);

      ParseUser result = user.signUp();
      assert (result == XUser.getCurrentUser());
    } catch (ParseException e) {
      showAlertDialog(e.getMessage());
    }
  }

  private String getText(EditText editText, int msgId) {
    String res = editText.getText().toString().trim();
    if (res.isEmpty()) {
      showToast(msgId);
      return null;
    } else {
      return res;
    }
  }

  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    assert isAdded();
    mAccountFragment = (AccountFragment) getParentFragment();
    etEmail = view.findViewById(R.id.et_email);
    etPassword = view.findViewById(R.id.et_password);
    etFirstName = view.findViewById(R.id.et_first_name);
    etLastName = view.findViewById(R.id.et_last_name);
    etMobile = view.findViewById(R.id.et_mobile);
    TextView txtBtnSignUp = view.findViewById(R.id.txt_btn_sign_up);
    TextView txtBtnTermsAndConditions = view.findViewById(R.id.lbl_terms_and_conditions_part_1);
    txtBtnTermsAndConditions.setOnClickListener(this::onClick);
    String text = getString(R.string.t_n_c, getString(R.string.app_name),
      getString(R.string.btn_terms_and_conditions));
    Cell411GuiUtils.setTextViewHTML(txtBtnTermsAndConditions, text, this::getClickableSpan);
    TextView txtBtnSignIn = view.findViewById(R.id.txt_btn_sign_in);
    txtBtnSignUp.setOnClickListener(this::onClick);
    txtBtnSignIn.setOnClickListener(this::toLoginFragment);

    view.findViewById(R.id.rl_separator).setVisibility(View.GONE);
    BaseActivity activity = activity();
    assert activity != null;
    spCountryCode = Cell411GuiUtils.createCCSpinner(activity, null, null);
  }

  public void toLoginFragment(final View view) {
    mAccountFragment.selectFragment(LoginFragment.class);
  }
}

