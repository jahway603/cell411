package cell411.ui.welcome;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.model.ParseUser;
import com.safearx.cell411.R;

import java.util.Locale;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.methods.Dialogs;
import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseFragment;
import cell411.utils.OnCompletionListener;

public class LoginFragment extends BaseFragment {
  private static final String TAG = "LoginActivity";

//  static {
//    XLog.i(TAG, "loading class");
//  }

  private EditText etEmail;
  private EditText etPassword;
  private TextView txtBtnLogin;
  private AccountFragment mAccountFragment;

  public LoginFragment() {
    super(R.layout.fragment_login);
  }

  @Nonnull
  private String getPassword() {
    return etPassword.getText().toString().trim();
  }

  @Nonnull
  private String getEmail() {
    return etEmail.getText().toString().trim().toLowerCase(Locale.US);
  }

  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    assert isAdded();
    mAccountFragment = (AccountFragment) getParentFragment();

    etEmail = view.findViewById(R.id.et_email);
    etPassword = view.findViewById(R.id.et_password);
    txtBtnLogin = view.findViewById(R.id.txt_btn_login);
    TextView txtBtnForgotPassword = view.findViewById(R.id.txt_btn_forgot_password);
    TextView txtBtnSignUp = view.findViewById(R.id.txt_btn_sign_up);
    txtBtnLogin.setOnClickListener(this::loginClicked);
    txtBtnForgotPassword.setOnClickListener(this::forgotPasswordClicked);
    txtBtnSignUp.setOnClickListener(this::toRegisterFragment);
    view.findViewById(R.id.rl_separator).setVisibility(View.GONE);
    view.findViewById(R.id.txt_copblock_plug).setVisibility(View.VISIBLE);
    ViewGroup group = view.findViewById(R.id.button_grid);

    for (int i = 0; i < 8; i++) {
      Button button = new Button(activity());
      String text = "Dev" + (i + 1);
      button.setText(text);
      button.setOnClickListener(this::quickLogin);
      group.addView(button);
    }
  }

  private void forgotPasswordClicked(View view) {
    Dialogs.showForgotPasswordDialog(activity(), new OnCompletionListener() {
      @Override
      public void done(final boolean success) {
        if (success) {
          showAlertDialog("Request sent");
        } else {
          showAlertDialog("Request not sent");
        }
      }
    });
  }

  public void toRegisterFragment(final View view) {
    mAccountFragment.selectFragment(RegisterFragment.class);
  }

  private void loginClicked(View view) {
    String email = getEmail();
    String password = getPassword();
    if (email.isEmpty()) {
      BaseApp.req().showToast(R.string.validation_email);
    } else if (password.isEmpty()) {
      BaseApp.req().showToast(R.string.validation_password);
    } else {
      hideSoftKeyboard();
      if (!email.contains("@")) {
        email = email + "@copblock.app";
      }
      System.out.println("logIn");
      ParseUser.logIn(email, password, (user, pe) ->
      {
        if (pe != null) {
          handleException("logging in", pe);
        } else {
          pop();
        }
      });
    }
  }

  private void quickLogin(Button view) {
    String email = view.getText().toString().toLowerCase(Locale.US) + "@copblock.app";
    etEmail.setText(email);
    etPassword.setText(R.string.bullshit_password);
    loginClicked(view);
  }

  private void quickLogin(View view) {
    if (view instanceof Button) {
      quickLogin((Button) view);
    }
  }
}

