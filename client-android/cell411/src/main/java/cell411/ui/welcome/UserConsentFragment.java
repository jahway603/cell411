package cell411.ui.welcome;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import cell411.parse.XUser;
import cell411.ui.Cell411GuiUtils;
import cell411.ui.base.ModelBaseFragment;
import cell411.utils.Reflect;
import com.parse.model.ParseUser;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.function.Function;

public class UserConsentFragment
  extends ModelBaseFragment
{
  private ArrayList<CheckBox> mCBList;
  private TextView mTxtBtnOk;
  private TextView mTxtTitle;
  private String mTextTitle;
  private String mTextDescription;
  private TextView mTxtDescription;

  public UserConsentFragment()
  {
    super(R.layout.fragment_user_consent);
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState)
  {
    Reflect.announce(true);
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@NonNull View view,
                            @Nullable Bundle savedInstanceState)
  {
    super.onViewCreated(view, savedInstanceState);
    mTxtTitle = view.findViewById(R.id.page_title);
    mTextTitle = getString(R.string.title_privacy_policy);
    mTxtDescription = view.findViewById(R.id.description);
    mTextDescription = getString(R.string.description_privacy_policy);
    mCBList = new ArrayList<>();
    mCBList.add(view.findViewById(R.id.cb_we_dont_sell_or_give_data));
    mCBList.add(view.findViewById(R.id.cb_can_delete_account));
    mCBList.add(view.findViewById(R.id.cb_you_agree_to_process_data));
    mCBList.add(view.findViewById(R.id.cb_read_privacy_policy));
    mTxtBtnOk = view.findViewById(R.id.txt_btn_ok);
    Drawable drawable = mCBList.get(0).getButtonDrawable();
    Function<URLSpan, ClickableSpan> pSpan = PolicySpan::new;
    Cell411GuiUtils.setTextViewHTML(mTxtDescription, mTextDescription, pSpan);
    Cell411GuiUtils.setTextViewHTML(mTxtTitle, mTextTitle, pSpan);

    CompoundButton.OnCheckedChangeListener onCheckedChangeListener =
      (compoundButton, isChecked) ->
      {
        mTxtBtnOk.setEnabled(allChecked(mCBList));
      };
    for (CheckBox box : mCBList) {
      box.setOnCheckedChangeListener(onCheckedChangeListener);
    }
    mTxtBtnOk.setOnClickListener(this::onConsentClicked);
  }

  private boolean allChecked(ArrayList<CheckBox> cbList)
  {
    for (CheckBox box : cbList) {
      if (!box.isChecked()) {
        return false;
      }
    }
    return true;
  }

  private void onConsentClicked(View view)
  {
    if (allChecked(mCBList)) {

      ds().getExec().execute((() ->
      {
        XUser user = (XUser) ParseUser.getCurrentUser();
        user.setConsented(true);
        user.save();

        pop();
      }));
    } else {
      Cell411.req().showAlertDialog(getString(R.string.consent_instruction));
    }
  }

  class PolicySpan
    extends ClickableSpan
  {
    private final URLSpan span;

    PolicySpan(URLSpan span)
    {
      this.span = span;
    }

    public void onClick(View view)
    {
      String url;
      String spanUrl = span.getURL();
      if (spanUrl.equalsIgnoreCase("terms")) {
        url = getString(R.string.terms_and_conditions_url);
      } else if (spanUrl.equalsIgnoreCase("privacy_policy")) {
        url = getString(R.string.privacy_policy_url);
      } else {
        Cell411.req().showAlertDialog("Unexpected link url: " + spanUrl);
        return;
      }
      Intent intentWeb = new Intent(Intent.ACTION_VIEW);
      if (!url.isEmpty()) {
        intentWeb.setData(Uri.parse(url));
      }
      startActivity(intentWeb);
    }
  }
}

