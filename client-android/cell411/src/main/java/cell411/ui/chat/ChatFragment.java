package cell411.ui.chat;

import static cell411.ui.chat.ChatViewType.VIEW_TYPE_RECEIVED;
import static cell411.ui.chat.ChatViewType.VIEW_TYPE_RECEIVED_IMAGE;
import static cell411.ui.chat.ChatViewType.VIEW_TYPE_RECEIVED_LOCATION;
import static cell411.ui.chat.ChatViewType.VIEW_TYPE_SENT;
import static cell411.ui.chat.ChatViewType.VIEW_TYPE_SENT_IMAGE;
import static cell411.ui.chat.ChatViewType.VIEW_TYPE_SENT_LOCATION;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.CallSuper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseQuery;
import com.parse.model.ObjectEvent;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseFile;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseUser;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.logic.LiveQueryService;
import cell411.parse.XChatMsg;
import cell411.parse.XChatRoom;
import cell411.parse.XEntity;
import cell411.parse.XUser;
import cell411.services.DataService;
import cell411.services.LocationService;
import cell411.ui.base.BaseApp;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.ip.ImagePickerContract;
import cell411.ui.utils.ip.PicPrefs;
import cell411.utils.ImageUtils;
import cell411.utils.LocationUtil;
import cell411.utils.Reflect;
import cell411.utils.Util;
import cell411.utils.ValueObserver;
import cell411.utils.XLog;

public class ChatFragment extends ModelBaseFragment {
  public static final String TAG = "ChatActivity";
  private final ChatListAdapter mMessages = new ChatListAdapter();
  private final List<XChatMsg> mList = new ArrayList<>();
  private final LiveQueryService mLiveQueryService;
  private final ImagePickerContract mImagePickerContract;
  private final ActivityResultLauncher<PicPrefs> mImagePickerLauncher;
  private final Callback mCallback;
  ObjectEventsHandler mHandler = new ObjectEventsHandler();
  LocationChanged mLocationChanged = new LocationChanged();
  private XChatRoom mChatRoom;
  private RelativeLayout mEmptyMarker;
  private RecyclerView mMsgView;
  private EditText mEditText;
  private Button mSendLocation;
  private Button mSendImage;
  private Button mSendPlain;
  private ParseQuery<XChatMsg> mQuery;
  private Location mLocation;

  {
    mCallback = new Callback();
    mImagePickerContract = new ImagePickerContract();
    mImagePickerLauncher = registerForActivityResult(mImagePickerContract,
      mCallback);
  }

  public ChatFragment() {
    super(R.layout.fragment_chat);
    mLiveQueryService = LiveQueryService.req();
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState) {
    Reflect.announce(true);
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  public void loadData2() {
    super.loadData2();
    if (mQuery != null) {
      mLiveQueryService.unsubscribe(mQuery);
    }
    mQuery = XChatMsg.q();
    mQuery.whereEqualTo("chatRoom", mChatRoom);
    mLiveQueryService.subscribe(mQuery, mHandler);
    mList.clear();
    mList.addAll(DataService.findFully(mQuery));
  }

  @CallSuper
  @Override
  public void loadData3() {
    super.loadData3();
    mMessages.run();
  }

  @Override
  public void onResume() {
    super.onResume();
    LocationService.addObserver(mLocationChanged);
    mLocation = LocationService.get().getLocation();
  }

  @Override
  public void onPause() {
    super.onPause();
    LocationService.removeObserver(mLocationChanged);
  }

  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mEmptyMarker = view.findViewById(R.id.disp_empty);
    mMsgView = view.findViewById(R.id.msg_view);
    mEditText = view.findViewById(R.id.edit_text);
    mSendImage = view.findViewById(R.id.rad_image);
    mSendLocation = view.findViewById(R.id.rad_location);
    mSendPlain = view.findViewById(R.id.rad_plain);
    mSendImage.setOnClickListener(this::onSendClicked);
    mSendLocation.setOnClickListener(this::onSendClicked);
    mSendPlain.setOnClickListener(this::onSendClicked);
    if (mEmptyMarker != null)
      mEmptyMarker.setVisibility(View.VISIBLE);
    mMsgView.setVisibility(View.INVISIBLE);
    mMsgView.setAdapter(mMessages);
    LinearLayoutManager layout = new LinearLayoutManager(Cell411.req());
    mMsgView.setLayoutManager(layout);
    Bundle arguments = getArguments();
    assert arguments != null;
    String objectId = arguments.getString("objectId");
    XEntity entity = LiveQueryService.getObject(objectId);
    assert entity != null;
    String type = entity.getClassName();
    String name = entity.getEntityName();
    System.out.println("type: " + type + " name: " + name);
    mChatRoom = entity.getChatRoom();
    if (mChatRoom == null) {
      mChatRoom = new XChatRoom();
      entity.setChatRoom(mChatRoom);
      entity.saveInBackground();
    }
  }

  public void onSendClicked(View view) {
    if (mMessages.getItemCount() == 0) {
      hideSoftKeyboard();
    }
    if (view == mSendPlain) {
      String text = mEditText.getText().toString();
      mEditText.setText("");
      if (Util.isNoE(text)) {
        Cell411.req().showToast("Enter text, select image, or select loc");
        return;
      }
      getExec().execute(new MessageSender(text));
    } else if (view == mSendLocation) {
      ParseGeoPoint location = LocationUtil.getGeoPoint(mLocation);
      if (location == null) {
        showToast("Location not available.  Please try again");
        return;
      }
      getExec().execute(new MessageSender(location));
    } else if (view == mSendImage) {
      String baseName = "chat_" + mChatRoom.getObjectId() + "_" + Util.sortableDate();
      PicPrefs prefs = new PicPrefs(baseName, "image/jpeg");
      try {
        mImagePickerLauncher.launch(prefs);
      } catch (Exception e) {
        e.printStackTrace();
        showToast("Failed to start picture picker");
      }
    }
  }

  private void handleBitmapMessage(ChatItem item) {
    XLog.i(TAG, "handleBitmapMessage");
    XChatMsg msg = item.mMsg;
    msg.fetchIfNeeded();
    ParseFile image = msg.getParseFile("image");
    if (image == null) {
      return;
    }
    item.mBitmap = getBitmap(image);
    BaseApp.req().onUI(mMessages::notifyDataSetChanged, 0);
  }

  public Bitmap getBitmap(byte[] image)
  {
    return BitmapFactory.decodeByteArray(image,0,image.length);
  }
  public Bitmap getBitmap(ParseFile image) {
    byte[] bytes = image.getData();
    return ImageUtils.getBitmap(bytes);
  }


  static class ChatItem {
    final ChatViewType mViewType;
    final Date mDate;
    final XChatMsg mMsg;
    public Bitmap mBitmap;

    ChatItem(XChatMsg msg) {
      mDate = msg.getCreatedAt();
      mMsg = msg;
      boolean hasImage = msg.has("image") && msg.getImage() != null;
      boolean hasLocation = msg.has("location") && msg.getLocation() != null;
      boolean iSent = XUser.getCurrentUser().hasSameId(msg.getOwner());
      switch ((iSent ? 0x100 : 0x000) + (hasImage ? 0x001 : 0x000) +
        (hasLocation ? 0x010 : 0x000)) {

        case 0x000:
          mViewType = VIEW_TYPE_RECEIVED;
          break;
        case 0x001:
          mViewType = VIEW_TYPE_RECEIVED_IMAGE;
          break;
        case 0x010:
        case 0x011:
          mViewType = VIEW_TYPE_RECEIVED_LOCATION;
          break;

        case 0x100:
          mViewType = VIEW_TYPE_SENT;
          break;
        case 0x101:
          mViewType = VIEW_TYPE_SENT_IMAGE;
          break;

        case 0x110:
        case 0x111:
          mViewType = VIEW_TYPE_SENT_LOCATION;
          break;

        default:
          throw new RuntimeException("Unexpected combo");
      }
    }
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
    final TextView txtName;
    final TextView txtMsg;
    final TextView txtDate;
    final ImageView img;
    private final TextView txtTime;

    public ViewHolder(View view) {
      super(view);
      img = view.findViewById(R.id.image);
      txtDate = view.findViewById(R.id.txt_date);
      txtMsg = view.findViewById(R.id.text);
      txtName = view.findViewById(R.id.name);
      txtTime = view.findViewById(R.id.txt_msg_time);
    }
  }

  class LocationChanged implements ValueObserver<Location> {
    @Override
    public void onChange(@Nullable Location newValue, @Nullable Location oldValue) {
      System.out.println("New location: " + newValue);
      mLocation = newValue;
    }
  }

  class Callback implements ActivityResultCallback<Integer> {
    private Integer mResult;

    @Override
    public void onActivityResult(Integer result) {
      mResult = result;
      PicPrefs prefs = ImagePickerContract.claimPicPref(mResult);
      getExec().execute(new MessageSender(prefs.mBitmap));
    }

  }

  class ObjectEventsHandler implements ObjectEventsCallback<XChatMsg> {
    @Override
    public void onEvents(final ObjectEvent<XChatMsg> event) {
      XChatMsg msg = event.getObject();
      switch (event.getEventId()) {
        case ENTER:
        case CREATE:
          mList.add(msg);
          break;

        case LEAVE:
        case DELETE:
          mList.remove(msg);
          break;

        case UPDATE: {
          boolean placed = false;
          for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).hasSameId(msg)) {
              mList.set(i, msg);
              placed = true;
              break;
            }
          }
          if (!placed) {
            mList.add(msg);
          }
          break;
        }
      }
      BaseApp.req().onUI(mMessages);
    }
  }

  public class ChatListAdapter extends RecyclerView.Adapter<ViewHolder> implements Runnable {
    public final ArrayList<ChatItem> mItems = new ArrayList<>();

    public ChatListAdapter() {
    }

    // Create new views (invoked by the layout manager)
    @Nonnull
    @Override
    public ViewHolder onCreateViewHolder(@Nonnull ViewGroup parent, int iViewType) {
      ChatViewType viewType = ChatViewType.valueOf(iViewType);
      switch (viewType) {
        case VIEW_TYPE_DATE:
        case VIEW_TYPE_RECEIVED:
        case VIEW_TYPE_SENT:
        case VIEW_TYPE_RECEIVED_IMAGE:
        case VIEW_TYPE_SENT_IMAGE:
        case VIEW_TYPE_RECEIVED_LOCATION:
        case VIEW_TYPE_SENT_LOCATION:
          break;
        default:
          throw new RuntimeException("Unexpected view type: " + viewType);
      }
      LayoutInflater inflater = LayoutInflater.from(parent.getContext());
      View view = inflater.inflate(viewType.getLayout(), parent, false);

      return new ViewHolder(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void run() {
      mItems.clear();
      mItems.addAll(Util.transform(mList, ChatItem::new));
      notifyDataSetChanged();
      if (mItems.size() == 0) {
        mMsgView.setVisibility(View.INVISIBLE);
        if (mEmptyMarker != null)
          mEmptyMarker.setVisibility(View.VISIBLE);
      } else {
        mMsgView.setVisibility(View.VISIBLE);
        if (mEmptyMarker != null)
          mEmptyMarker.setVisibility(View.GONE);
        mMsgView.scrollToPosition(mMessages.getItemCount() - 1);
      }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@Nonnull final ViewHolder vh, final int position) {
      final ChatItem item = mItems.get(position);
      if (item == null) {
        return;
      }
      final XChatMsg msg = item.mMsg;
      Bitmap bitmap = item.mBitmap;
      final XUser owner = msg.getOwner();
      final String name = (owner == null ? null : owner.getName());
      final Date date = (msg.getCreatedAt());
      final String text = (msg.getText());
      final ParseFile imageFile = (msg.getImage());
      final boolean iSent = (ParseUser.getCurrentUser().hasSameId(owner));
      if (vh.txtName != null) {
        if (!iSent && name != null) {
          vh.txtName.setText(name);
          vh.txtName.setVisibility(View.VISIBLE);
        } else {
          vh.txtName.setVisibility(View.GONE);
        }
      }
      if (vh.txtDate != null) {
        if (date != null) {
          vh.txtDate.setText(Util.formatDate(date, true));
          vh.txtDate.setVisibility(View.VISIBLE);
        } else {
          vh.txtDate.setVisibility(View.GONE);
        }
      }
      if (vh.txtTime != null) {
        if (date != null) {
          vh.txtTime.setText(Util.formatTime(date));
          vh.txtTime.setVisibility(View.VISIBLE);
        } else {
          vh.txtTime.setVisibility(View.GONE);
        }
      }
      if (vh.txtMsg != null) {
        if (text != null) {
          vh.txtMsg.setText(text);
          vh.txtMsg.setVisibility(View.VISIBLE);
        } else {
          vh.txtMsg.setVisibility(View.GONE);
        }
      }
      if (vh.img != null) {
        if (bitmap != null) {
          vh.img.setImageBitmap(bitmap);
        } else if (imageFile != null) {
          if (imageFile.isDataAvailable()) {
            byte[] bytes = imageFile.getData();
            item.mBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            vh.img.setImageBitmap(item.mBitmap);
          } else {
            ds().getExec().execute(() -> ChatFragment.this.handleBitmapMessage(item));
          }
        } else if (vh.txtMsg != null) {
          vh.img.setVisibility(View.GONE);
          String oldText = "";
          if (vh.txtMsg.getVisibility() != View.GONE) {
            oldText = vh.txtMsg.getText().toString() + "\n\n";
          }
          oldText = oldText + "There should be an image attached to this message";
          vh.txtMsg.setVisibility(View.VISIBLE);
          vh.txtMsg.setText(oldText);
        }
      }

    }

    @Override
    public int getItemViewType(int position) {
      return mItems.get(position).mViewType.ordinal();
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return mItems.size();
    }

    public void add(XChatMsg xChatMsg) {
      mItems.add(new ChatItem(xChatMsg));
      BaseApp.req().onUI(this::notifyDataSetChanged, 0);
    }

    public void update(XChatMsg msg) {
      for (int i = 0; i < mItems.size(); i++) {
        if (!mItems.get(i).mMsg.hasSameId(msg)) {
          continue;
        }
        mItems.remove(i);
        i--;
        BaseApp.req().onUI(this::notifyDataSetChanged, 0);
      }

    }
  }

  class MessageSender implements Runnable {
    final String mMessage;
    final Bitmap mImage;
    final ParseGeoPoint mLocation;

    MessageSender(String message, Bitmap image, ParseGeoPoint location) {
      mLocation = location;
      mImage = image;
      mMessage = message;
    }

    MessageSender(String message) {
      this(message, null, null);
    }

    MessageSender(Bitmap image) {
      this(null, image, null);
    }

    MessageSender(ParseGeoPoint location) {
      this(null, null, location);
    }

    @Override
    public void run() {
      XChatMsg msg = new XChatMsg();
      XUser currentUser = XUser.getCurrentUser();
      msg.setOwner(currentUser);
      msg.setChatRoom(mChatRoom);
      if (mLocation != null) {
        msg.setLocation(mLocation);
      } else if (mImage != null) {
        try {
          String fileName = "img_" + mChatRoom.getObjectId() +
            "_" + mMessages.getItemCount();
          ParseFile parseFile = ImageUtils.createParseFile(fileName, mImage);
          msg.setImage(parseFile);
          msg.save();
          return;
        } catch (Throwable throwable) {
          System.out.println("Failed to save and upload picture");
        }
      } else if (mMessage != null) {
        msg.setText(mMessage);
      } else {
        throw new RuntimeException("WHere is my payload?");
      }
      msg.save();
    }
  }
}
