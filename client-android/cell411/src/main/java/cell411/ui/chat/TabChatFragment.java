package cell411.ui.chat;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.*;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cell411.enums.EntityType;
import cell411.logic.*;
import cell411.parse.XChatRoom;
import cell411.parse.XEntity;
import cell411.ui.base.*;
import cell411.utils.Util;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

public class TabChatFragment extends ModelBaseFragment {
  private final ChatRoomListAdapter mAdapter = new ChatRoomListAdapter();
  FragmentFactory mNewChatFactory = FragmentFactory.fromClass(NewChatFragment.class);

  HashSet<String> mRemoved = new HashSet<>();
  private View mDispEmpty;
  private RecyclerView mRecycler;
  private Button mButton;

  public TabChatFragment() {
    super(R.layout.fragment_chat_rooms);
  }

  ChatRoomWatcher getChatRoomWatcher() {
    return getLiveQueryService().getChatRoomWatcher();
  }

  LiveQueryService getLiveQueryService() {
    return LiveQueryService.req();
  }

  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    mDispEmpty = findViewById(R.id.disp_empty, true);

    mRecycler = findViewById(R.id.recycler, true);
    mRecycler.setHasFixedSize(true);
    mRecycler.setLayoutManager(new LinearLayoutManager(requireActivity()));
    mRecycler.setAdapter(mAdapter);

    mButton = findViewById(R.id.fab_new_chat, true);
    mButton.setOnClickListener(this::onNewChatClick);

    mRecycler.setVisibility(View.GONE);
    mAdapter.change();
  }

  public void onNewChatClick(View view) {
    if (view == mButton) {
      push(mNewChatFactory);
    }
  }

  @Override
  @CallSuper
  public void loadData3() {
    super.loadData3();
    LiveQueryService lqs = LiveQueryService.opt();
    if (lqs == null) {
      return;
    }

    mAdapter.change();
  }

  public static class ChatRoomViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
    private final TextView txtChatRoomName;
    private final TextView txtTime;

    public ChatRoomViewHolder(View view) {
      super(view);
      txtChatRoomName = view.findViewById(R.id.txt_chat_room_name);
      txtTime = view.findViewById(R.id.txt_time);
    }
  }

  public class ChatRoomListAdapter extends RecyclerView.Adapter<ChatRoomViewHolder>
    implements LQListener<XChatRoom> {

    private static final int VIEW_TYPE_PUBLIC_CELL = 0;
    private static final int VIEW_TYPE_ALERT = 1;
    private static final int VIEW_TYPE_PRIVATE_CELL = 2;
    ArrayList<XChatRoom> mItems = new ArrayList<>();
    FragmentFactory mFragmentFactory = FragmentFactory.fromClass(ChatFragment.class);

    public ChatRoomListAdapter() {
    }

    public void onChatRoomClick(View v) {
      int position = mRecycler.getChildAdapterPosition(v);
      XChatRoom room = mItems.get(position);
      XEntity entity = room.getEntity();
      openChat(entity);
    }

    public boolean onChatRoomLongClick(View v) {
      int position = mRecycler.getChildAdapterPosition(v);
      // Delete this friend
      showDeleteChatRoomDialog(position);
      return false;
    }

    @Override
    @Nonnull
    public TabChatFragment.ChatRoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      LayoutInflater inflater = LayoutInflater.from(parent.getContext());
      View v =
        inflater.inflate(R.layout.cell_chat_room, parent, false);
      TabChatFragment.ChatRoomViewHolder vh = new ChatRoomViewHolder(v);
      v.setOnClickListener(this::onChatRoomClick);
      v.setOnLongClickListener(this::onChatRoomLongClick);
      return vh;
    }

    @Override
    public void onBindViewHolder(@Nonnull TabChatFragment.ChatRoomViewHolder chatRoomViewHolder,
                                 int position)
    {
      final XChatRoom chatRoom = mItems.get(position);
      final Date lastDate = chatRoom.getUpdatedAt();
      chatRoomViewHolder.txtTime.setText(Util.formatTime(lastDate));
      XEntity entity = chatRoom.getEntity();
      String entityName = entity.getEntityName();
      chatRoomViewHolder.txtChatRoomName.setText(entityName);
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      XChatRoom xChatRoom = mItems.get(position);
      XEntity entity = xChatRoom.getEntity();
      EntityType type = entity.getType();
      if (type == EntityType.PUBLIC_CELL) {
        return VIEW_TYPE_PUBLIC_CELL;
      } else if (type == EntityType.PRIVATE_CELL) {
        return VIEW_TYPE_PRIVATE_CELL;
      } else if (type == EntityType.ALERT) {
        return VIEW_TYPE_ALERT;
      } else {
        throw new RuntimeException("Unexpected ItemViewType");
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return mItems.size();
    }

    private void showDeleteChatRoomDialog(final int position) {
      assert BaseApp.req().isOnUI();

      final FragmentActivity activity = getActivity();
      assert activity != null;
      AlertDialog.Builder alert = new AlertDialog.Builder(activity);
      alert.setMessage(R.string.dialog_msg_delete_chat);
      alert.setNegativeButton(R.string.dialog_btn_cancel, Util.nullClickListener());
      alert.setPositiveButton(R.string.dialog_btn_yes, (dialogInterface, i) ->
      {
        XChatRoom item = mAdapter.mItems.remove(position);
        mRemoved.add(item.getObjectId());
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void change() {
      mItems.clear();
      Class<XChatRoom> type = XChatRoom.class;
      mItems.addAll(Util.transform(getChatRoomWatcher().getValues(), type::cast));
      onUI(() ->
      {
        notifyDataSetChanged();
        mRecycler.setVisibility(View.VISIBLE);
        if (mItems.isEmpty()) {
          mDispEmpty.setVisibility(View.VISIBLE);
        } else {
          mDispEmpty.setVisibility(View.GONE);
        }
      });
    }
  }
}

