package cell411.ui.alert;

import android.os.Bundle;
import android.view.*;
import android.widget.*;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cell411.enums.ProblemType;
import cell411.logic.*;
import cell411.parse.XAlert;
import cell411.parse.util.XItem;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.friend.AddFriendModules;
import cell411.ui.widget.CircularImageView;
import cell411.ui.widget.ProblemTypeInfo;
import cell411.utils.Reflect;
import cell411.utils.Util;
import com.safearx.cell411.Cell411;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

import static cell411.utils.ViewType.vtAlert;
import static cell411.utils.ViewType.vtString;

public class AlertFragment extends ModelBaseFragment {
  private final AlertsListAdapter mAdapter = new AlertsListAdapter();
  private AlertWatcher mWatcher;
  private RecyclerView mRecycler;

  public AlertFragment() {
    super(R.layout.fragment_alert);
  }

  static int compare(XItem lhs, XItem rhs) {
    if (lhs == null) {
      return rhs == null ? 0 : -1;
    } else if (rhs == null) {
      return 1;
    } else {
      Date d1 = lhs.getParseObject().getUpdatedAt();
      Date d2 = rhs.getParseObject().getUpdatedAt();
      int cmp = 0;
      if (d1 != d2) {
        if (d1 == null) {
          cmp = -1;
        } else if (d2 == null) {
          cmp = 1;
        } else {
          cmp = d1.compareTo(d2);
        }
      }
      if (cmp == 0) {
        cmp = lhs.getParseObject().getObjectId().compareTo(rhs.getObjectId());
      }

      return -cmp;
    }
  }

  @Nullable
  @Override
  public View onCreateView(@Nonnull final LayoutInflater inflater,
                           @Nullable final ViewGroup container,
                           @Nullable final Bundle savedInstanceState) {
    Reflect.announce(true);
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mRecycler = view.findViewById(R.id.rv_alerts);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    mAdapter.mItems.clear();
    mAdapter.mItems.add(new XItem("", "Loading Data"));
    mRecycler.setAdapter(mAdapter);
    mRecycler.setHasFixedSize(true);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
    mRecycler.setLayoutManager(linearLayoutManager);
  }

  @Override
  public void loadData2() {
    //    ParseQuery<ParseObject> query = ParseQuery.getQuery("ProblemType");
    //    List<ParseObject> list = query.find();
    //    for(int i=0;i<list.size();i++){
    //      list.get(i).delete();
    //      list.remove(i--);
    //    }
    ////    ProblemType[] types = ProblemType.values();
    //    for(int i=list.size();i<types.length;i++){
    //      list.add(new ParseObject("ProblemType"));
    ////      list.get(i).setObjectId(types[i].toString());
    //      list.get(i).put("name",types[i].toString());
    //      list.get(i).save();
    //    }
    super.loadData2();
    boolean done = false;
    try {
      if (!LiveQueryService.opt().isReady()) {
        return;
      }

      if (mWatcher != null) {
        LiveQueryService lqs = LiveQueryService.opt();
        assert lqs != null;
        mWatcher = lqs.getAlertWatcher();
      }
      if (mWatcher == null) {
        return;
      }
      done = true;
    } finally {
      if (!done) {
        refresh();
      }
    }
  }

  public void loadData3() {
    super.loadData3();
  }

  // Provide a reference to the views for each data item
  // Complex data items may need more than one view per item, and
  // you provide access to all the views for a data item in a view holder
  public static class ViewHolder extends RecyclerView.ViewHolder {
    // each data item is just a string in this case
    private TextView txtAlert;
    private TextView txtAlertTime;
    private LinearLayout llBtnFlag;
    private CircularImageView imgUser;
    private ImageView imgChat;
    private ImageView imgAlertType;
    private TextView txtInfo;

    public ViewHolder(View view, int type) {
      super(view);
      if (type == vtAlert.ordinal()) {
        txtAlert = view.findViewById(R.id.txt_alert);
        txtAlert.setTextSize(20);
        txtAlertTime = view.findViewById(R.id.txt_alert_time);
        txtAlertTime.setTextSize(20);
        llBtnFlag = view.findViewById(R.id.rl_btn_flag);
        imgUser = view.findViewById(R.id.avatar);
        imgChat = view.findViewById(R.id.img_chat);
        imgAlertType = view.findViewById(R.id.img_alert_type);
      } else if (type == vtString.ordinal()) {
        txtInfo = view.findViewById(R.id.text);
      }
    }
  }

  public class AlertsListAdapter extends RecyclerView.Adapter<ViewHolder>
    implements LQListener<XAlert> {
    private final ArrayList<XItem> mItems = new ArrayList<>();

    // Provide a suitable constructor (depends on the kind of dataset)
    FragmentFactory mFragmentFactory = FragmentFactory.fromClass(AlertDetailFragment.class);

    public AlertsListAdapter() {
    }

    public void onAlertClicked(View v) {
      int pos = mRecycler.getChildAdapterPosition(v);
      XItem item = mAdapter.mItems.get(pos);
      if (item.getViewType() == vtAlert) {
        XAlert alert = item.getAlert();
        mFragmentFactory.setObjectId(alert.getObjectId());
        push(mFragmentFactory);
      }
    }

    public void setData(List<XItem> items) {
      Iterator<XItem> iterator = mItems.iterator();
      while (iterator.hasNext()) {
        XItem item = iterator.next();
        int index = mItems.indexOf(item);
        if (index == -1) {
          continue;
        }
        iterator.remove();
      }
      iterator = items.iterator();
      while (iterator.hasNext()) {
        mItems.add(iterator.next());
      }
      mItems.sort(AlertFragment::compare);
      onUI(this::notifyDataSetChanged);
    }

    // Create new views (invoked by the layout manager)
    @Nonnull
    @Override
    public ViewHolder onCreateViewHolder(@Nonnull ViewGroup parent, int viewType) {

      // create a new view
      View v;
      System.out.println(parent);
      if (viewType == vtAlert.ordinal()) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_alert, parent, false);
      } else if (viewType == vtString.ordinal()) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      } else {
        throw new IllegalArgumentException("viewType should be ALERT or FOOTER");
      }
      ViewHolder vh = new ViewHolder(v, viewType);
      v.setOnClickListener(this::onAlertClicked);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@Nonnull final ViewHolder viewHolder, final int position) {
      //      System.out.println(viewHolder);
      XItem item = mItems.get(position);
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if (getItemViewType(position) != vtAlert.ordinal()) {
        viewHolder.txtInfo.setText(item.getText());
      } else {
        final XAlert alert = item.getAlert();
        if (alert.getOwner() == null) {
          return;
        }
        String description;
        String issuer;
        if (alert.isSelfAlert()) {
          issuer = "You";
        } else {
          issuer = alert.getOwner().getName();
        }
        ProblemType problemType = alert.getProblemType();
        if (problemType == null) {
          alert.setProblemType(ProblemType.UN_RECOGNIZED);
          problemType = alert.getProblemType();
          alert.saveInBackground();
        }
        description = issuer + " issued a " + problemType + " alert";

        viewHolder.txtAlert.setText(description);
        viewHolder.txtAlertTime.setText(Util.formatDateTime(alert.getCreatedAt()));
        viewHolder.imgUser.setImageBitmap(alert.getOwner().getAvatarPic());

        ProblemTypeInfo problemTypeInfo = ProblemTypeInfo.valueOf(problemType.ordinal());
        viewHolder.imgAlertType.setImageResource(problemTypeInfo.getImageRes());
        viewHolder.imgAlertType.setBackgroundResource(problemTypeInfo.getBGDrawable());
        if (alert.isSelfAlert()) {
          viewHolder.llBtnFlag.setVisibility(View.GONE);
        } else {
          viewHolder.llBtnFlag.setVisibility(View.VISIBLE);
          viewHolder.llBtnFlag.setBackgroundResource(R.drawable.bg_user_flag);
        }
        viewHolder.llBtnFlag.setOnClickListener(view -> showFlagAlertDialog(alert));
        if (System.currentTimeMillis() >=
          alert.getCreatedAt().getTime() + Cell411.TIME_TO_LIVE_FOR_CHAT_ON_ALERTS) {
          viewHolder.imgChat.setVisibility(View.GONE);
        } else {
          viewHolder.imgChat.setVisibility(View.VISIBLE);
          viewHolder.imgChat.setOnClickListener(view -> Cell411.req().openChat(alert));
        }
      }
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      return mItems.get(position).getViewType().ordinal();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return mItems.size();
    }

    void showFlagAlertDialog(final XAlert cell411Alert) {
      AddFriendModules.showFlagAlertDialog(getActivity(), cell411Alert.getOwner(), null);
    }

    @Override
    public void change() {
      Collection<XAlert> data = mWatcher.getValues();
      if (data.isEmpty()) {
        List<XItem> list = new ArrayList<>();
        list.add(new XItem("", "No Alerts Active"));
        setData(list);
      } else {
        setData(XItem.asList(data));
      }
    }

  }
}
