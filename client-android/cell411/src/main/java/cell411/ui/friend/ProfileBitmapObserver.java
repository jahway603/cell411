// package cell411.ui.friend;
//
// import android.graphics.Bitmap;
// import android.view.View;
// import android.widget.ImageView;
// import cell411.parse.XUser;
// import cell411.ui.base.BaseApp;
// import cell411.ui.base.FragmentFactory;
// import cell411.ui.base.MainActivity;
// import cell411.ui.profile.ProfileImageFragment;
// import cell411.utils.ValueObserver;
// import cell411.utils.XLog;
// import cell411.utils.XTAG;
// import com.safearx.cell411.R;
//
// import javax.annotation.Nullable;
//
// class ProfileBitmapObserver
//     implements ValueObserver<Bitmap>,
//     View.OnClickListener,
//     Runnable
// {
//   static final XTAG TAG = new XTAG();
//   static FragmentFactory smProfileImageFactory =
//       FragmentFactory.fromClass(ProfileImageFragment.class);
//   private static final int smImgUserTag = R.id.img_user;
//   private final XUser mUser;
//   private final ImageView mImageView;
//
//   public ProfileBitmapObserver(ImageView imageView, XUser user) {
//     mImageView = imageView;
//     mUser = user;
//     mImageView.setTag(R.id.img_user, this);
//     mImageView.setOnClickListener(this);
//     mUser.addAvatarObserver(this);
//   }
//
//   public static void setImage(XUser user, ImageView imgUser) {
//     imgUser.setTag(smImgUserTag, null);
//     if (user.hasProfileImage()) {
//       ImageStore.get
//     } else {
//       imgUser.setImageBitmap(null);
//       imgUser.setOnClickListener(null);
//     }
//   }
//
//   @Override
//   public void onChange(@Nullable Bitmap newValue, @Nullable Bitmap oldValue) {
//     if(checkFresh())
//       BaseApp.get().onUI(this);
//   }
//
//   private boolean checkFresh() {
//     if(mImageView!=null && mImageView.getTag(smImgUserTag)==this){
//       return true;
//     } else {
//       mUser.removeAvatarObserver(this);
//       return false;
//     }
//   }
//
//   @Override
//   public void run() {
//     if (mImageView.getTag(R.id.img_user) != this) {
//       mUser.removeAvatarObserver(this);
//       return;
//     }
//     if(mUser.hasProfileImage()){
//
//     }
//   }
//
//   @Override
//   public void onClick(View v) {
//     XLog.i(TAG, "Starting profile image activity");
//     MainActivity mainActivity = BaseApp.get().getMainActivity();
//     mainActivity.push(smProfileImageFactory);
//   }
// }
//