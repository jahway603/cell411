package cell411.ui.friend;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.RelationWatcher;
import cell411.logic.RequestWatcher;
import cell411.logic.rel.AggregateRel;
import cell411.logic.rel.Rel;
import cell411.methods.Dialogs;
import cell411.parse.XUser;
import cell411.parse.util.XItem;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.DialogShower;
import cell411.ui.base.FragmentFactory;
import cell411.ui.profile.UserFragment;
import cell411.ui.widget.CircularImageView;
import cell411.ui.widget.QueryRunner;
import cell411.utils.Collect;
import cell411.utils.DroidUtil;
import cell411.utils.Util;
import cell411.utils.ViewType;
import cell411.utils.XTAG;
import cell411.utils.func.Func0;
import com.parse.ParseQuery;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static cell411.enums.RequestType.FriendRequest;
import static cell411.utils.ViewType.vtNull;
import static cell411.utils.ViewType.vtString;
import static cell411.utils.ViewType.vtUser;

/**
 * Created by Sachin on 18-04-2016.
 */
public class FriendSearchFragment extends BaseFragment {
  public static final XTAG TAG = new XTAG();
  private final static String smWaitingForQuery = "Waiting for Query";
  private final UserListAdapter mAdapter = new UserListAdapter();
  private final AggregateRel mRemoved = new AggregateRel();
  private final Rel mSelfRel;
  private final FragmentFactory mUserFragmentFactory =
    FragmentFactory.fromClass(UserFragment.class);
  private QueryRunner<XUser> mRunner;
  private String mQuery = null;
  private TextView mCount;
  private int mHighWater = 0;

  public FriendSearchFragment() {
    super(R.layout.fragment_search_friend);
    mSelfRel = new Rel();
    mSelfRel.addAll(Collect.asList(XUser.getCurrentUser().getObjectId()));
  }

  static int compare(ViewType lhs, ViewType rhs) {
    return lhs.ordinal() - rhs.ordinal();
  }

  static int compare(XUser lhs, XUser rhs) {
    return lhs.nameCompare(rhs);
  }

  static int compare(String lhs, String rhs) {
    return String.CASE_INSENSITIVE_ORDER.compare(lhs, rhs);
  }

  static int compare(XItem lhs, XItem rhs) {
    int cmp = compare(lhs.getViewType(), rhs.getViewType());
    if (cmp == 0 && lhs.getViewType() == vtUser) {
      cmp = compare(lhs.getUser(), rhs.getUser());
    }
    if (cmp == 0) {
      cmp = compare(lhs.getObjectId(), rhs.getObjectId());
    }
    if (cmp < 0) {
      cmp = -1;
    } else if (cmp > 0) {
      cmp = 1;
    }
    return cmp;
  }

  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    SearchView mSearchView = view.findViewById(R.id.searchview_user);
    mCount = view.findViewById(R.id.count);
    mCount.setText(smWaitingForQuery);
    RecyclerView mRecycler = view.findViewById(R.id.rv_users);

    mSearchView.setIconifiedByDefault(false);
    mSearchView.setOnQueryTextListener(new TextListener());
    mSearchView.setOnCloseListener(() -> false);
    mRecycler.setHasFixedSize(true);
    mRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
    mSearchView.setQuery(mQuery, false);
    mRecycler.setAdapter(mAdapter);
  }

  private String baseRegex(String str) {
    return "^" + Util.lc(Util.trim(str));
  }

  ParseQuery<XUser> buildNameQuery() {
    ParseQuery<XUser> query1 = ParseQuery.getQuery("_User");
    ParseQuery<XUser> query2 = ParseQuery.getQuery("_User");
    if (mQuery.length() == 0) {
      return null;
    }
    String[] toks = mQuery.split("\\s+");
    if (toks.length > 0) {
      toks[0] = baseRegex(toks[0]);
    }
    if (toks.length > 1) {
      toks[1] = baseRegex(toks[1]);
    }
    if (toks.length > 2) {
      String msg = Util.format("Cannot build query from %d tokens", toks.length);
      showAlertDialog(msg);
      return null;
    }
    if (toks.length == 2) {
      // With two tokens, one must match the first name, while
      // the other matches the last name.
      query1.whereMatches("lastName", toks[1], "i");
      query2.whereMatches("firstName", toks[1], "i");
    }
    // With one token, it can match either name.
    query1.whereMatches("firstName", toks[0], "i");
    query2.whereMatches("lastName", toks[0], "i");
    return ParseQuery.or(Arrays.asList(query1, query2));
  }

  public ParseQuery<XUser> buildQuery() {
    ParseQuery<XUser> query = buildNameQuery();
    if (query != null) {
      query.whereNotContainedIn("objectId", mRemoved.getRelatedIds());
      query.orderByAscending("firstName").addAscendingOrder("lastName")
        .addAscendingOrder("objectId");
    }
    return query;
  }

  public void onResume() {
    super.onResume();

  }

  public void onPause() {
    super.onPause();
    mRunner = null;
  }

  public void onUserClick(View v) {
    UserViewHolder vh = (UserViewHolder) v.getTag();
    mUserFragmentFactory.setObjectId(vh.mUser.getObjectId());
    push(mUserFragmentFactory);
  }

  public void onFriendClick(View v) {
    String msg = "Sending Friend Request";
    DialogShower shower = Dialogs.createNoButtonDialog("Sending ...", msg, null);
    onUI(shower);

    UserViewHolder vh = (UserViewHolder) v.getTag();
    ds().handleRequest(FriendRequest, vh.mUser, (ex) -> shower.dismiss(), null);
  }

  void startQuery() {
    if (Util.isNoE(mQuery)) {
      return;
    }

    LiveQueryService liveQueryService = LiveQueryService.opt();
    if (liveQueryService == null) {
      return;
    }
    RequestWatcher requestWatcher = liveQueryService.getRequestWatcher();
    RelationWatcher relationWatcher = liveQueryService.getRelationWatcher();
    mRemoved.addRels(relationWatcher.getFriends(),
      relationWatcher.getCounterParties(),
      relationWatcher.getBlocksMe(), relationWatcher.getMyBlocks(), mSelfRel);
    mRemoved.addObserver(mAdapter);
    // Since we are specifically interested in users to whom we are
    // not connected, we have to run the query ourselves.

    ParseQuery<XUser> query = buildQuery();
    if (query != null) {
      Func0<QueryRunner<XUser>> getCur = this::getRunner;
      mRunner = new QueryRunner<>(query, getCur, mAdapter);
      onUI(mRunner);
    }
  }

  QueryRunner<XUser> getRunner() {
    return mRunner;
  }

  public static class UserViewHolder extends RecyclerView.ViewHolder {
    View mView;
    XUser mUser;
    CircularImageView mImageView;
    TextView mTextView;
    TextView mButton1;
    TextView mButton2;

    UserViewHolder(View view) {
      super(view);
      mView = view;
      mImageView = view.findViewById(R.id.avatar);
      mTextView = view.findViewById(R.id.name);
      mButton1 = view.findViewById(R.id.txt_btn_action);
      mButton2 = view.findViewById(R.id.txt_btn_neg);
      mView.setTag(this);
      mImageView.setTag(this);
      mTextView.setTag(this);
      mButton1.setTag(this);
      mButton2.setTag(this);
      reset();
    }

    public void reset() {
      mUser = null;
      mView.setVisibility(View.VISIBLE);
      mView.setOnClickListener(null);
      mImageView.setVisibility(View.VISIBLE);
      mImageView.setOnClickListener(null);
      mTextView.setVisibility(View.VISIBLE);
      mTextView.setOnClickListener(null);
      mButton1.setVisibility(View.VISIBLE);
      mButton1.setOnClickListener(null);
      mButton2.setVisibility(View.GONE);
      mButton2.setOnClickListener(null);
    }
  }

  public class UserListAdapter extends Adapter<UserViewHolder>
    implements Runnable, MyObserver, PropertyChangeListener {
    final private ArrayList<XItem> mItems = new ArrayList<>();
    final private ArrayList<XUser> mAllUsers = new ArrayList<>();

    UserListAdapter() {
      mItems.add(new XItem("nq", "Enter a Query"));
    }

    @Nonnull
    @Override
    public UserViewHolder onCreateViewHolder(@Nonnull ViewGroup parent, int viewTypeId) {
      LayoutInflater li = LayoutInflater.from(parent.getContext());
      View v = li.inflate(R.layout.cell_user, parent, false);
      return new UserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@Nonnull UserViewHolder vh, int position) {
      mHighWater = Math.max(mHighWater, position);
      XItem item = get(position);
      vh.reset();
      if (item.getViewType() == vtUser) {
        final XUser user = item.getUser();
        vh.mUser = user;
        vh.mView.setOnClickListener(FriendSearchFragment.this::onUserClick);
        vh.mTextView.setText(user.getName());
        vh.mTextView.setOnClickListener(FriendSearchFragment.this::onUserClick);
        DroidUtil.setupImage(user,vh.mImageView);
        vh.mButton1.setOnClickListener(FriendSearchFragment.this::onFriendClick);
      } else if (item.getViewType() == vtNull) {
        vh.mTextView.setVisibility(View.INVISIBLE);
        vh.mImageView.setVisibility(View.GONE);
        vh.mButton1.setVisibility(View.GONE);
      } else if (item.getViewType() == vtString) {
        vh.mTextView.setVisibility(View.VISIBLE);
        vh.mTextView.setText(item.getText());
        vh.mImageView.setVisibility(View.GONE);
        vh.mButton1.setVisibility(View.GONE);
      }
    }

    @Override
    public int getItemCount() {
      return mItems.size();
    }

    public XItem get(int position) {
      return mItems.get(position);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public synchronized void run() {
      if (!isOnUI()) {
        onUI(this);
        return;
      }
      if (mRunner != null) {
        mAllUsers.clear();
        mAllUsers.addAll(mRunner.getList());
      }
      mItems.clear();
      mItems.addAll(Util.transform(mAllUsers, XItem::new));
      mItems.removeIf((item) -> mRemoved.contains(item.getObjectId()));
      mItems.sort(FriendSearchFragment::compare);
      mCount.setText(Util.format(R.string.num_items, mItems.size()));
      notifyDataSetChanged();
    }

    @Override
    public void update(MyObservable o, Object arg) {
      onUI(this);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      mAllUsers.clear();
      mAllUsers.addAll((Collection<? extends XUser>) evt.getNewValue());
      onUI(this);
    }
  }

  private class TextListener implements SearchView.OnQueryTextListener {
    @Override
    public boolean onQueryTextSubmit(String query) {
      if (Util.isNoE(query)) {
        mQuery = null;
        return true;
      } else {
        mQuery = query.trim();
      }
      hideSoftKeyboard();
      startQuery();
      return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
      return false;
    }
  }

}
