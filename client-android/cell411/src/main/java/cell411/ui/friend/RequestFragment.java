package cell411.ui.friend;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cell411.enums.RequestType;
import cell411.json.JSONObject;
import cell411.logic.LQListener;
import cell411.logic.LiveQueryService;
import cell411.logic.MyObservable;
import cell411.logic.MyObserver;
import cell411.logic.RelationWatcher;
import cell411.logic.RequestWatcher;
import cell411.logic.rel.Rel;
import cell411.methods.Dialogs;
import cell411.parse.JSONCallback;
import cell411.parse.XPublicCell;
import cell411.parse.XRequest;
import cell411.parse.XUser;
import cell411.parse.util.XItem;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.ModelBaseFragment;
import cell411.ui.profile.UserFragment;
import cell411.ui.widget.RVAdapter;
import cell411.utils.Collect;
import cell411.utils.DroidUtil;
import cell411.utils.Util;
import cell411.utils.ViewType;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

import static cell411.enums.RequestType.CellJoinApprove;
import static cell411.enums.RequestType.CellJoinCancel;
import static cell411.enums.RequestType.CellJoinReject;
import static cell411.enums.RequestType.CellJoinResend;
import static cell411.enums.RequestType.FriendApprove;
import static cell411.enums.RequestType.FriendCancel;
import static cell411.enums.RequestType.FriendReject;
import static cell411.enums.RequestType.FriendResend;

/**
 * Created by Sachin on 18-04-2016.
 */
public class RequestFragment extends ModelBaseFragment {
  private static final XTAG TAG = new XTAG();

  static {
    XLog.i(TAG, "Loading Class");
  }

  private final RequestListAdapter mRequestListAdapter = new RequestListAdapter();
  private final FragmentFactory mUserFactory =
    FragmentFactory.fromClass(UserFragment.class);

  RelationWatcher mRelationWatcher;
  private AlertDialog mDialog;
  private AlertDialog mResultDialog;
  private final RVAdapter mAdapterObserver = new RVAdapter() {
    @Override
    public void onChanged() {
      if (mRequestListAdapter.mItems.size() > 1) {
        mTxtNoRequests.setVisibility(View.GONE);
      } else {
        mTxtNoRequests.setVisibility(View.VISIBLE);
      }
    }
  };
  private final Rel mOwnerOf;
  private final Rel mSentToOf;
  private boolean mRequestInFlight = false;
  private final JSONCallback mCallback = new JSONCallback() {
    @Override
    public void done(boolean success, JSONObject result) {
      if (!isOnUI()) {
        onUI(() -> done(success, result));
        return;
      }
      System.out.println(mOwnerOf);
      if (mDialog != null) {
        mDialog.dismiss();
      }
      if (mResultDialog == null) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.dialog_btn_ok,
          (dialog, which) -> dialog.dismiss());
        mResultDialog = builder.create();
        mResultDialog.setOnDismissListener(dialog -> mRequestInFlight = false);
      }
      mResultDialog.setMessage(result.toString(2));
      onUI(() -> {
        if (mResultDialog != null)
          mResultDialog.show();
      });
    }
  };
  private TextView mTxtNoRequests;

  public RequestFragment() {
    super(R.layout.fragment_friend_requests);
    RequestWatcher requestWatcher = LiveQueryService.req().getRequestWatcher();
    mOwnerOf= requestWatcher.getOwned();
    mSentToOf= requestWatcher.getReceived();
    mRequestListAdapter.registerAdapterDataObserver(mAdapterObserver);
  }

  @Override
  public void onViewCreated(@Nonnull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    RecyclerView recyclerView = view.findViewById(R.id.rv_requests);
    mTxtNoRequests = view.findViewById(R.id.txt_no_requests);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(linearLayoutManager);
    mRequestListAdapter.mItems.clear();
    mRequestListAdapter.mItems.add(new XItem("", "Loading Data ..."));
    recyclerView.setAdapter(mRequestListAdapter);
  }

  public void loadData1() {
    super.loadData1();
    mRequestListAdapter.change();
  }
  public void loadData2() {
    super.loadData2();
  }
  @Override
  public void loadData3() {
    super.loadData3();
  }

  void friendRequestAction(XRequest req, boolean ownedByMe) {
    String message;
    if(ownedByMe) {
      message="Would you like to resend(yes) or cancel(no) your request?";
    } else {
      message="Would you like to accept(yes) or reject(no) the request?";
    }
    Dialogs.showYesNoDialog("Request", message,
      success -> friendRequestAction(req,ownedByMe,success));
  }
  void friendRequestAction(XRequest req, boolean ownedByMe, boolean positive) {
    if (mRequestInFlight)
      return;
    mRequestInFlight = true;
    final RequestType requestType;
    if (ownedByMe) {
      if (positive) {
        if (req.getCell() == null) {
          requestType = FriendResend;
        } else {
          requestType = CellJoinResend;
        }
      } else {
        if (req.getCell() == null) {
          requestType = FriendCancel;
        } else {
          requestType = CellJoinCancel;
        }
      }
    } else {
      if (positive) {
        if (req.getCell() == null) {
          requestType = FriendApprove;
        } else {
          requestType = CellJoinApprove;
        }
      } else {
        if (req.getCell() == null) {
          requestType = FriendReject;
        } else {
          requestType = CellJoinReject;
        }
      }
    }
    String msg = Util.format("Sending %s request", requestType);
    if (mDialog == null) {
      AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
      builder.setCancelable(false);
      mDialog = builder.create();
    }
    mDialog.setMessage(msg);
    mDialog.show();
    ds().handleRequest(requestType, req, null, mCallback);
  }

  private RelationWatcher getRelationWatcher() {
    if (mRelationWatcher != null) {
      return mRelationWatcher;
    }
    LiveQueryService service = LiveQueryService.opt();
    if (service != null) {
      mRelationWatcher = service.getRelationWatcher();
    }
    return mRelationWatcher;
  }

  public static class RequestViewHolder extends RecyclerView.ViewHolder {
    public XUser otherUser;
    public ImageView mAvatar;
    public boolean ownedByMe;
    public TextView mUserName;
    public TextView mAction;
    public TextView txtInfo;
    public View view;
    public XPublicCell cell;

    public RequestViewHolder(View v, ViewType viewType) {
      super(v);
      view = v;
      switch (viewType) {
        case vtRequest:
          mAvatar = v.findViewById(R.id.avatar);
          mUserName = v.findViewById(R.id.name);
          mAction = v.findViewById(R.id.b_respond);
          break;
        case vtString:
        case vtNull:
          txtInfo = v.findViewById(R.id.name);
          break;
      }
    }
  }

  static private final ItemCompare reqComp = new ItemCompare();
  public class RequestListAdapter extends RecyclerView.Adapter<RequestViewHolder>
    implements LQListener<XRequest>, MyObserver {
    public final List<XItem> mItems = new ArrayList<>();
    private final XItem mOFRTitle = new XItem("", "Sent");
    private final XItem mIFRTitle = new XItem("", "Received");
    private final XItem mOCRTitle = new XItem("", "Sent Cell Reqs");
    private final XItem mICRTitle = new XItem("", "Recd Cell Reqs");
    private final List<XItem> miFReq = new ArrayList<>();
    private final List<XItem> moFReq = new ArrayList<>();
    private final List<XItem> miCReq = new ArrayList<>();
    private final List<XItem> moCReq = new ArrayList<>();
    private final List<List<XItem>> mLists = Collect.asList(miFReq, moFReq, miCReq, moCReq);

    public RequestListAdapter() {
      mItems.add(new XItem("", "Loading Data"));
    }


    @Nonnull
    public RequestViewHolder onCreateViewHolder(@Nonnull ViewGroup parent, int viewType) {
      View v;
      ViewType vt = ViewType.valueOf(viewType);
      switch (vt) {
        case vtRequest:
          v = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.cell_friend_request, parent, false);
          break;
        case vtNull:
        case vtString:
          v = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.cell_public_cell_title, parent, false);
          break;
        default:
          String msg = "Unexpected viewType: " + viewType;
          throw new RuntimeException(msg);
      }
      return new RequestViewHolder(v, vt);
    }

    @Override
    public int getItemCount() {
      return mItems.size();
    }

    public void onBindViewHolder(@Nonnull RequestViewHolder vh, int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      XItem item = mItems.get(position);
      switch (item.getViewType()) {
        case vtRequest:
          final XRequest req = item.getRequest();
          vh.ownedByMe = req.ownedByCurrent();
          if (vh.ownedByMe) {
            vh.otherUser = req.getSentTo();
          } else {
            vh.otherUser = req.getOwner();
          }
          vh.cell = req.getCell();
          if (vh.otherUser == null) {
            return;
          }

          DroidUtil.setupImage(vh.otherUser,vh.mAvatar);

          if (vh.cell == null) {
            vh.mUserName.setText(vh.otherUser.getName());
          } else {
            vh.mUserName.setText(vh.cell.getName());
          }
          vh.mAction.setOnClickListener(view -> friendRequestAction(req, vh.ownedByMe));

          vh.itemView.setOnClickListener(view ->
          {
            mUserFactory.setObjectId(vh.otherUser.getObjectId());
            push(mUserFactory);
          });

          break;
        case vtString:
          final String text = item.getText();
          vh.txtInfo.setText(text);
          break;
        case vtNull:
          vh.view.setVisibility(View.INVISIBLE);
          break;
      }
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      return mItems.get(position).getViewType().ordinal();
    }

    public synchronized void change() {
      mItems.clear();
      for (List<XItem> list : mLists) {
        list.clear();
      }
      splitList(mOwnerOf, miFReq, miCReq, mIFRTitle, mICRTitle);
      splitList(mSentToOf, moFReq, moCReq, mOFRTitle, mOCRTitle);

      mItems.clear();
      for(List<XItem> list : mLists) {
        mItems.addAll(list);
      }

      onUI(this::notifyDataSetChanged);
    }

    private void splitList(Rel ids, List<XItem> fList, List<XItem> cList,
                           XItem fTitle, XItem cTitle)
    {
      for (String id : ids.getRelatedIds()) {
        XRequest req = LiveQueryService.getObject(id);
        if (req == null)
          continue;
        if (req.getCell() == null) {
          fList.add(new XItem(req));
        } else {
          cList.add(new XItem(req));
        }
      }
      polish(fList,fTitle);
      polish(cList,cTitle);
    }

    private void polish(List<XItem> list, XItem title)
    {
      list.sort(reqComp);
      list.add(0,title);
    }

    @Override
    public synchronized void update(MyObservable o, Object arg) {
      change();
    }
  }

}

