package cell411.ui.widget;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.shapes.OvalShape;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import javax.annotation.Nullable;

public class GalleryImageFragmentVB extends LinearLayout {
  private final Paint mPaint;
  RectF mRect = new RectF();
  OvalShape mShape = new OvalShape();

  public GalleryImageFragmentVB(Context context) {
    this(context, null);
  }

  public GalleryImageFragmentVB(Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public GalleryImageFragmentVB(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    this(context, attrs, defStyleAttr, 0);
  }

  public GalleryImageFragmentVB(Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                                int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
    mPaint = new Paint();
    mPaint.setColor(0xff00ff00);
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    canvas.drawOval(mRect, mPaint);
  }

  @Override
  protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
    super.onLayout(changed, left, top, right, bottom);
    mRect.set(left, top, right, bottom);
  }
}
