package cell411.ui.widget;

import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseContext;
import cell411.utils.func.Func0;
import com.parse.ParseQuery;
import com.parse.model.ParseObject;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

public class QueryRunner<X extends ParseObject> implements Runnable, BaseContext {
  final int mLimit = 10;
  private final ArrayList<X> mList = new ArrayList<>();
  private final ParseQuery<X> mQuery;
  Func0<QueryRunner<X>> mGetCur;
  PropertyChangeSupport mPCS = new PropertyChangeSupport(this);
  private List<X> mBatch = null;

  public QueryRunner(ParseQuery<X> query, Func0<QueryRunner<X>> getCur,
                     PropertyChangeListener listener) {
    mGetCur = getCur;
    mQuery = query;
    mQuery.setLimit(mLimit);
    mPCS.addPropertyChangeListener(listener);
    BaseApp.getExecutor().execute(this);
  }

  @Override
  public void run() {
    if (mGetCur.apply() != this) {
      return;
    }

    if (isOnUI()) {
      mPCS.firePropertyChange("list", null, mList);
      if (mBatch == null || mBatch.size() == mLimit) {
        BaseApp.getExecutor().execute(this);
      }
    } else {
      mQuery.setSkip(mList.size());
      mBatch = mQuery.find();
      mList.addAll(mBatch);
      onUI(this);
    }
  }

  public List<X> getList() {
    return Collections.unmodifiableList(mList);
  }
}
