package cell411.ui.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.safearx.cell411.R;

import cell411.ui.base.BaseAnim;
import cell411.utils.Reflect;

public class XBlinker extends BaseAnim {
  public static final String TAG = Reflect.getTag();
  private static final int[] mFrames = {
    R.drawable.red_blink_0,
    R.drawable.red_blink_1
  };

  public XBlinker(Context context) {
    this(context, null);
  }

  public XBlinker(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public XBlinker(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override
  public int[] getFrameIds() {
    return mFrames;
  }

  @Override
  public int getDuration() {
    return 1000;
  }
}

