package cell411.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.gridlayout.widget.GridLayout;

import com.parse.model.ParseUser;

import cell411.ui.base.BaseContext;
import cell411.utils.Util;

public class StatusBar extends GridLayout implements BaseContext {

  private final boolean mHideWhenReady = true;
  public TextView mStatus;
  public XBlinker mBlinker;
  public TextView mLabel;

  public StatusBar(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  public StatusBar(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public StatusBar(Context context) {
    this(context, null, 0);
    setOnClickListener(this::onClicked);
  }

  private void onClicked(View view) {
    showAlertDialog("" + view);
  }

  //  @Override
  //  public void addView(View child, int index, ViewGroup.LayoutParams params)
  //  {
  //    super.addView(child,index,params);
  //    int id = child.getId();
  //    if (id == R.id.lbl_status) {
  //      mLabel = (TextView) child;
  //      mLabel.setTextSize(20);
  //    } else if (id == R.id.status) {
  //      mStatus = (TextView) child;
  //      mStatus.setTextSize(20);
  //    } else if (id == R.id.blinker) {
  //      mBlinker = (XBlinkingRedSymbol) child;
  //      mBlinker.setOnClickListener(this::blinkerClicked);
  //    }
  //  }

  private void blinkerClicked(View view) {
    showAlertDialog("The flashiing red light is telling you that you have no net " +
      "You can use the app, but you will not receive alerts, nor will " +
      "you be able to send them.");
  }

  @Override
  public void onViewAdded(final View child) {
    super.onViewAdded(child);
  }

  public void updateUI() {
    boolean connected = app().isConnected();
    if (connected && ParseUser.getCurrentUser() != null && mHideWhenReady) {
      setVisibility(View.GONE);
    } else {
      if (mStatus != null) {
        mStatus.setText(Util.isoDate());
      }
      if (mBlinker != null) {
        int visibility = connected ? GONE : VISIBLE;
        if (mBlinker.getVisibility() != visibility) {
          mBlinker.setVisibility(visibility);
        }
      }
    }
  }

  //  @Override
  //  protected void onLayout(boolean changed, int l, int t, int r, int b) {
  //    super.onLayout(changed, l, t, r, b);
  //    XLog.i(TAG, "onLayout( %10s, %10d, %10d, %10d, %10d )", changed, l, t, r, b);
  //  }
  //
  //  public void updateUI() {
  //    BaseApp app = app();
  //    XParse.State state = xpr().getState();
  //    boolean connected = app.isConnected();
  //    if (mHideWhenReady) {
  //      boolean hide = connected && state == XParse.State.Ready;
  //      setVisibility(hide ? View.GONE : View.VISIBLE);
  //    }
  //    if (mBlinker != null) {
  //      mBlinker.setOnClickListener(this::blinkerClicked);
  //      mBlinker.setVisibility(connected ? View.GONE : View.VISIBLE);
  //    }
  //    if (mStatus != null)
  //      mStatus.setText(Util.makeWords(String.valueOf(state)));
  //  }
}
