package cell411.ui.widget;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatSpinner;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CCSpinner extends AppCompatSpinner {
  public CCSpinner(@Nonnull Context context) {
    this(context, null, 0, 0);
  }

  public CCSpinner(@Nonnull Context context, int mode) {
    this(context, null, 0, 0);
  }

  public CCSpinner(@Nonnull Context context, @Nullable AttributeSet attrs) {
    this(context, attrs, 0, 0);
  }

  public CCSpinner(@Nonnull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    this(context, attrs, defStyleAttr, 0);
  }

  public CCSpinner(@Nonnull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                   int mode) {
    super(context, attrs, defStyleAttr, mode);
  }

  public CCSpinner(@Nonnull Context context, @Nullable AttributeSet attrs, int defStyleAttr,
                   int mode, Theme popupTheme) {
    super(context, attrs, defStyleAttr, mode, popupTheme);
  }

}
