package cell411.methods;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.parse.model.ParseUser;
import com.safearx.cell411.R;

import java.util.ArrayList;

import cell411.parse.XUser;
import cell411.ui.base.*;
import cell411.utils.OnCompletionListener;

/**
 * Created by Sachin on 27-03-2016.
 */
public class Dialogs extends BaseDialogs {
  static ArrayList<ExtraDismissListener> mDismissListeners = new ArrayList<>();

  public static void showForgotPasswordDialog(BaseActivity activity, OnCompletionListener listener) {
    new ForgotPasswordDialog(activity, listener).run();
  }

  @SuppressWarnings("deprecation")
  public static void showConfirmDeletionAlertDialog(Activity activity) {
    AlertDialog.Builder alert = new AlertDialog.Builder(activity);
    alert.setMessage(activity.getString(R.string.dialog_msg_delete_my_account));
    alert.setNegativeButton(R.string.dialog_btn_no, (dialog, which) ->
    {
    });
    alert.setPositiveButton(R.string.dialog_btn_yes, (dialogInterface, i) ->
    {
      final ProgressDialog dialog = new ProgressDialog(activity);
      dialog.setMessage(activity.getString(R.string.dialog_msg_deleting_account));
      dialog.setCancelable(false);
      dialog.show();
      XUser user = XUser.getCurrentUser();
      user.deleteInBackground();
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

//  public static void showQRCodeDialog(BaseActivity activity)
//  {
  //    AlertDialog.Builder alert = new AlertDialog.Builder(activity);
  //    alert.setTitle(R.string.dialog_title_qr_code);
  //    LayoutInflater inflater =
  //      (LayoutInflater) activity.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
  //    View view = inflater.inflate(R.layout.layout_qrcode, null);
  //    final LinearLayout linearLayout = view.findViewById(R.id.ll_qrcode);
  //    final ImageView imgQRCode = view.findViewById(R.id.img_qrcode);
  //    TextView txtEmail = view.findViewById(R.id.txt_email);
  //    XUser currentUser = XUser.getCurrentUser();
  //    txtEmail.setText(currentUser.getEmail());
  //    //Find screen size
  //    //Encode with a QR Code image
  //    int smallerDimension =
  //      Math.min(ImageUtils.getScreenWidth(), ImageUtils.getScreenHeight()) * 3 / 4;
  //    QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(currentUser.getUsername(),
  //    smallerDimension);
  //    Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
  //    imgQRCode.setImageBitmap(bitmap);
  //    alert.setView(view);
  //    alert.setNegativeButton(R.string.dialog_btn_cancel, (dialog, arg1) -> dialog.dismiss());
  //    alert.setPositiveButton(R.string.dialog_btn_share, (dialog, which) -> {
  //      linearLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
  //        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
  //      linearLayout.layout(0, 0, linearLayout.getMeasuredWidth(), linearLayout
  //      .getMeasuredHeight());
  //      linearLayout.setDrawingCacheEnabled(true);
  //      linearLayout.buildDrawingCache(true);
  //      Bitmap finalBitmap = Bitmap.createBitmap(linearLayout.getDrawingCache());
  //      linearLayout.destroyDrawingCache();
  //      Intent share = new Intent(Intent.ACTION_SEND);
  //      share.setType("image/jpeg");
  //      ByteArrayOutputStream bytes = new ByteArrayOutputStream();
  //      assert finalBitmap != null;
  //      finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
  //      File cacheDir;
  //      // Make sure external shared storage is available
  //      if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
  //        // We can read and write the media
  //        cacheDir = activity.getExternalFilesDir(null);
  //      } else {
  //        // Load another directory, probably local memory
  //        cacheDir = activity.getFilesDir();
  //      }
  //      File photoFile = new File(cacheDir + File.separator + "cell411.jpg");
  //      if (!IOUtil.createNewFile(photoFile))
  //        return;
  //      try (FileOutputStream fo = new FileOutputStream(photoFile)) {
  //        fo.write(bytes.toByteArray());
  //      } catch (IOException e) {
  //        e.printStackTrace();
  //      }
  //      Uri photoURI =
  //        FileProvider.getUriForFile(activity, "app.copblock.dev",
  //          photoFile);
  //
  //      share.putExtra(Intent.EXTRA_STREAM, photoURI);
  //      share.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string
  //      .share_qr_code_subject));
  //      share.putExtra(Intent.EXTRA_TEXT, activity.getString(R.string.share_qr_code_desc));
  //      activity.startActivity(Intent.createChooser(share, activity.getString(R.string
  //      .share_qr_code_title)));
  //    });
  //    androidx.appcompat.app.AlertDialog dialog = alert.create();
  //    dialog.show();
//  }

//  public static void showLogoutAlertDialog()
//  {
//    onUIThread(createLogoutAlertDialog());
//  }

//  public static DialogShower createLogoutAlertDialog()
//  {
//    return createYesNoDialog(getString(R.string.dialog_logout_message), success ->
//    {
//      if (success)
//      {
//        ParseUser.logOut();
//      }
//    });
  //    DialogShower shower = new DialogShower() {
  //      protected AlertDialog createDialog(){
  //        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
  //        alert.setCancelable(false);
  //        LayoutInflater inflater =
  //          (LayoutInflater) activity.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
  //        View view = inflater.inflate(R.layout.dialog_logout, null);
  //        alert.setView(view);
  //        alert.setNegativeButton(R.string.dialog_btn_cancel, Util.nullClickListener());
  //        alert.setPositiveButton(R.string.dialog_btn_logout, new DialogInterface
  //        .OnClickListener() {
  //          @Override
  //          public void onClick(final DialogInterface dialog, final int which) {
  //            ParseUser.logOut();
  //          }
  //        });
  //        return alert.create();
  //      };
  //    };
//  }

  public void showAlertDialog(final String message, OnCompletionListener listener) {
    onUIThread(createAlertDialog(message));
  }

  static class ForgotPasswordDialog {
    final BaseActivity mActivity;
    final DialogShower smShower;
    Listener mListener = new Listener();

    OnCompletionListener mOuterListener;

    ForgotPasswordDialog(final BaseActivity activity, final OnCompletionListener listener) {
      mActivity = activity;
      mOuterListener = listener;
      smShower = createEnterTextDialog(R.string.dialog_title_forgot_password,
        R.string.dialog_message_tap_submit,
        "dev1@copblock.app", mListener);
    }

    public void run() {
      onUIThread(smShower);
    }

    class Listener implements OnCompletionListener {

      @Override
      public void done(final boolean success) {
        if (success) {
          BaseApp.req();
          BaseApp.getExecutor().execute(() ->
                {
                  ParseUser.requestPasswordReset(smShower.getText());
                  BaseApp.req().onUI(() ->
                    mOuterListener.done(true));
                });
        } else {
          mOuterListener.done(false);
        }
      }
    }
  }

  static class ExtraDismissListener implements DialogInterface.OnDismissListener {
    final DialogInterface.OnDismissListener mListener;
    final OnCompletionListener mCompletionListener;

    ExtraDismissListener(DialogInterface.OnDismissListener listener,
                         OnCompletionListener completionListener) {
      mListener = listener;
      mCompletionListener = completionListener;
      mDismissListeners.add(this);
    }

    boolean success() {
      return true;
    }

    public void onDismiss(DialogInterface dialog) {
      if (mCompletionListener != null) {
        mCompletionListener.done(success());
      }
      if (mListener != null) {
        mListener.onDismiss(dialog);
      }
      mDismissListeners.remove(this);
    }
  }

}


