package cell411.parse;

import cell411.json.JSONObject;

public interface JSONCallback {
  void done(boolean success, JSONObject result);
}
