package cell411.parse;

import com.parse.ParseClassName;
import com.parse.ParseQuery;

import cell411.utils.WeakList;

@ParseClassName("ChatRoom")
public class XChatRoom extends XObject {
  static WeakList<XChatRoom> smInstances=new WeakList<>(XChatRoom.class);
  static XChatRoom forEntity(XEntity entity)
  {
//    if(entity.getChatRoom()!=null)
//      return entity.getChatRoom();
//
//    Iterator<XChatRoom> iterator = smInstances.iterator();
//    while(iterator.hasNext()){
//      XChatRoom chatRoom = iterator.next();
//      if(chatRoom==null)
//        continue;
//      if (chatRoom.mEntity==null)
//        continue;
//      if(chatRoom.mEntity.getObjectId().equals(entity.getObjectId()));
//    }
    return null;
  };

  XEntity mEntity;
  public XChatRoom() {
  }

  public static ParseQuery<XChatRoom> q() {
    return ParseQuery.getQuery((XChatRoom.class));
  }

  public String getName() {
    return getString("name");
  }

  private void setName(String name) {
    put("name", name);
  }


  public XEntity getEntity() {
    return mEntity;
  }
}
