package cell411.parse;

import cell411.enums.CellStatus;
import cell411.logic.rel.Rel.Key;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

public abstract class XBaseCell extends XEntity {
  private static MemberLookup smMemberLookup;
  TreeSet<String> mMembers;
  CellStatus mStatus = CellStatus.INITIALIZING;
  private Key mKey;

  protected XBaseCell()
  {

  }

  public static void setMemberLookup(MemberLookup memberLookup) {
    smMemberLookup = memberLookup;
  }

  final public String getName() {
    return getString("name");
  }

  // if the member list is null, we do not know if we have
  // members or not.  When we load the list, it will be here,
  // even if it is empty, so we know it has been loaded.
  final public void setName(String name) {
    put("name", name);
  }

  final public CellStatus getStatus() {
    if (mStatus == CellStatus.INITIALIZING) {
      SortedSet<String> memberIds = getMemberIds();
      if (memberIds == null) {
        return CellStatus.INITIALIZING;
      }
      XUser user = XUser.getCurrentUser();
      if (getOwner().hasSameId(user)) {
        mStatus = CellStatus.OWNER;
      } else if (memberIds.contains(user.getObjectId())) {
        mStatus = CellStatus.JOINED;
      } else {
        mStatus = CellStatus.NOT_JOINED;
      }
    }
    return mStatus;
  }

  final public void setStatus(CellStatus status) {
    mStatus = status;
  }

  public TreeSet<String> getMemberIds() {
    if (smMemberLookup == null) {
      throw new RuntimeException("Trying to getMemberIds, but no lookup!");
    }
    return new TreeSet<>(smMemberLookup.getMembers(this));
  }

  public int nameCompare(XBaseCell other) {
    return getName().compareTo(other.getName());
  }

  public void setMembersKey(Key key)
  {
    mKey=key;
  }
  public Key getMembersKey()
  {
    return mKey;
  }

  public abstract boolean isPublic();

  public interface MemberLookup {
    Collection<String> getMembers(XBaseCell cell);

    boolean setMembers(XBaseCell cell, Collection<String> ids);
  }

}

