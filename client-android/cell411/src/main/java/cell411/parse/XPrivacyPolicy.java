package cell411.parse;

import cell411.utils.DroidUtil;
import cell411.utils.MethodCallable;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.concurrent.Future;

@ParseClassName("PrivacyPolicy")
public class XPrivacyPolicy extends XObject {
  public static final XTAG TAG = new XTAG();

  public XPrivacyPolicy() {
    XLog.i(TAG, "constructor");
  }

  public String getTOSUrl() {
    return getString("tosUrl");
  }

  public String getPPUrl() {
    return getString("ppUrl");
  }

  static XPrivacyPolicy getLatestCall()
  throws ParseException
  {
    assert !DroidUtil.isMainThread();
    ParseQuery<XPrivacyPolicy> query =
      ParseQuery.getQuery(XPrivacyPolicy.class);
    query.orderByDescending("createdAt");
    query.setLimit(1);
    XPrivacyPolicy policy = query.getFirst();
    XLog.i(TAG,"%s",policy.toJSON().toString(2));
    return policy;
  }
  static
  Future<XPrivacyPolicy> getLatestFuture(){
    MethodCallable<XPrivacyPolicy> callable = MethodCallable.forStatic(
      cell411.parse.XPrivacyPolicy.class, cell411.parse.XPrivacyPolicy.class,"getLatestCall");
    return DroidUtil.getExec().submit(callable);
  }
}
