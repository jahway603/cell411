package cell411.logic;

import androidx.annotation.CallSuper;
import cell411.json.JSONObject;
import cell411.logic.rel.AggregateRel;
import cell411.parse.XChatRoom;
import cell411.parse.XEntity;
import cell411.utils.Reflect;
import cell411.utils.TimeLog;
import cell411.utils.Util;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import com.parse.ParseQuery;
import com.parse.encoder.FullObjectCoder;
import com.parse.model.ObjectEvent;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public abstract class Watcher<X extends ParseObject> extends CacheObject
    implements MyObserver, ObjectEventsCallback<X>
{
//  final protected HashSet<LQListener<X>> mListeners = new HashSet<>();
  final protected RelationWatcher mRelationWatcher;
  final protected AggregateRel mIds;
  final protected Class<X> mType;
  protected final String mClassName;
  protected ParseQuery<X> mQuery;
  volatile protected Date mLastBatch;
  private boolean mInitialQuery;

  public Watcher(@Nonnull RelationWatcher relationWatcher,
                 @Nonnull String name,
                 @Nonnull Class<X> type) {
    super(relationWatcher.getService(),name);
    assert relationWatcher != null;
    mRelationWatcher = relationWatcher;
    mType = type;
    ParseQuery<X> query = ParseQuery.getQuery(type);
    mClassName = query.getClassName();
    mIds = (AggregateRel) mRelationWatcher.getRel(mClassName);
    setRunInitialQuery(true);
  }

//    ParseUser user = ParseUser.getCurrentUser();
//    RelationWatcher watcher = mService.getRelationWatcher();
//    if (user == null || watcher == null) {
//      BaseApp.getExecutor().schedule(this::allRels, 1, TimeUnit.SECONDS);
//    } else {
//      doAllRels(mService.getRelationWatcher(), user);
//    }
//    return null;
//  }

//  synchronized public void addRel(Rel rel)
//  {
//    getKeySets().put(rel.getKeyStr(), rel);
//    rel.addObserver(this);
//  }

  abstract public ParseQuery<X> directQuery();

  synchronized public ParseQuery<X> createQuery(Set<String> dirty) {
    ParseQuery<X> query = ParseQuery.getQuery(mType);
    TreeSet<String> ids = new TreeSet<>(dirty);
    query.whereContainedIn("objectId", ids);
    return query;
  }

  @Override
  synchronized public void update(final MyObservable o, final Object arg) {
    if (mDoneLoading && getIds(true).size() > 0) {
      MultiCacheObject.queueNetLoad(this);
    }
  }

  @Override
  synchronized public void createSubscription() {
    // first time
    if(mQuery==null) {
      mQuery=directQuery();
    }
    ParseQuery<X> newQuery = directQuery();
    if (newQuery == null) {
      newQuery = createQuery(getIds(true));
    }

    ParseQuery<X> oldQuery = mQuery;
    if (oldQuery != null) {
      if (newQuery != null) {
        int cmp = oldQuery.compareTo(newQuery);
        if (cmp != 0) {
          getService().unsubscribe(oldQuery);
          mQuery = newQuery;
          getService().subscribe(mQuery, this);
        }
      } else {
        getService().unsubscribe(oldQuery);
      }
    } else {
      getService().subscribe(newQuery, this);
    }
  }

//  public synchronized HashSet<LQListener<X>> copyListeners() {
//    return new HashSet<>(mListeners);
//  }
//
//  public synchronized void addListener(LQListener<X> listener) {
//    removeListener(listener);
//    mListeners.add(listener);
//  }
//
//  public synchronized void removeListener(final LQListener<X> listener) {
//    while (mListeners.contains(listener)) {
//      mListeners.remove(listener);
//    }
//  }

  @Override
  synchronized public void saveToCache(JSONObject json) {
    Reflect.announce("name=%s",getName());
    if (mLastBatch == null) {
      mLastBatch = new Date();
    }
    HashMap<String, X> mData = new HashMap<>();
    for (String id : mIds) {
      mData.put(id, getObject(id));
    }
    FullObjectCoder.get().saveData(getCacheFile(), mData, mLastBatch);
  }


  @Override
  synchronized public void report(TimeLog ps) {
    int inMem = 0;
    for(String id : mIds){
      ParseObject obj = getObject(id);
    }
    ps.printf("Watcher for %s, %d ids, %s\n",mClassName,mIds.getSize(),mIds);
    ps.pl("  Data Items: " + mIds.getSize());
    ps.pl();
  }

  synchronized public void loadFromCache() {
    Reflect.announce("name=%s",getName());
    if (!cacheExists()) {
      return;
    }
    String text = fileToString();
    final Map<String, X> data = new TreeMap<>(getData());
    if (text != null && text.length() > 0) {
      mLastBatch = FullObjectCoder.get().loadData(text, data);
      for (String id : data.keySet()) {
        X obj = data.get(id);
        if (mType.isInstance(obj)) {
          data.put(id, mType.cast(obj));
        }
      }
    } else {
      mLastBatch = new Date(0);
    }
    HashSet<String> keys = new HashSet<>(getIds(false));
    data.keySet().removeIf(key -> !keys.contains(key));
  }

  public synchronized Collection<X> getValues() {
    return getValues(mIds,mType);
  }


  public synchronized Map<String, X> getData() {
    Map<String, X> map = new HashMap<>();
    for (String id : mIds) {
      map.put(id, getObject(id));
    }
    return map;
  }

  synchronized public boolean runInitialQuery() {
    return mInitialQuery;
  }

  @Override
  public synchronized void loadFromNet(Collection<ParseObject> extras) {
    try {
      Reflect.announce("name=%s",getName());
      Date batchDate = new Date();
      Set<String> dirty = getIds(true);
       mQuery = createQuery(dirty);
      if (runInitialQuery() || dirty.size() > 0) {
        setRunInitialQuery(false);
        List<X> items = new ArrayList<>();

        int size;
        List<X> batch;
        do {
          size = items.size();
          mQuery.setSkip(size);
          batch = mQuery.find();
          items.addAll(batch);
        } while (batch.size() == mQuery.getLimit());
        for (X x : items) {
          greetObject(x, extras);
        }
      }
      mLastBatch = batchDate;
    } finally {
      Reflect.announce();
    }
  }

  private void setRunInitialQuery(boolean init)
  {
    mInitialQuery=init;
  }

  XTAG TAG = new XTAG();
  protected synchronized Set<String> getIds(boolean dirty) {
    if (dirty) {
      Set<String> result = getIds(false);
      for(String id : result) {
        ParseObject object = getObject(id);
        Date updated = object==null ? null : object.getUpdatedAt();
        XLog.i(TAG, "%s => %s %s", id, updated, object);
      }
      result.removeIf(id -> !isDirty(id));
      return result;
    } else {
      return new TreeSet<>( mIds.mRelatedIds);
    }
  }

  protected synchronized boolean isDirty(final String key) {
    X object = getObject(key);
    if (object == null)
      return true;
    Date update = object.getUpdatedAt();
    Date latest = mRelationWatcher.getDate(key);
    if (update == null)
      return true;
    if (latest == null) {
      mRelationWatcher.setDate(key, update);
      return false;
    }
    return update.before(latest);
  }

  @CallSuper
  synchronized void greetObject(X po, Collection<ParseObject> list) {
    po.fetchIfNeeded();
    if (po instanceof XEntity) {
      XChatRoom room = (XChatRoom) po.getParseObject("chatRoom");
      if (room != null)
        room.fetchIfNeeded();
    }
//    for (String key : po.getRelations().keySet()) {
//      if (po.has(key)) {
//        ParseRelation<? extends ParseObject> rel = po.getRelation(key);
//        list.addAll(rel.fetch());
//      }
//    }
    putObject(po);
  }

  @Override
  synchronized public void onEvents(final ObjectEvent<X> event)
  {
    HashSet<ParseObject> needs = new HashSet<>();
    switch (event.getEventId()) {
      case CREATE:
      case ENTER:
      case UPDATE:
        greetObject(event.getObject(), needs);
        break;
      case LEAVE:
      case DELETE:
        removeObject(event.getObject());
        break;
    }
  }

  public int size() {
    return mIds.getSize();
  }

  @Nonnull
  synchronized public String toString() {
    return Util.format("Watcher{name=%-10s,items=%d}", getName(), size());
  }

  synchronized protected void removeObject(final X object) {
    mIds.remove(object.getObjectId());
  }
}

