package cell411.logic;

import cell411.json.JSONObject;
import cell411.utils.TimeLog;
import com.parse.model.ParseObject;

import java.io.File;
import java.util.Collection;

public interface ICacheObject {

  String getName();

  void onUI(Runnable runnable);
  void onUI(Runnable runnable, long delay);

  void loadFromNet(Collection<ParseObject> objs);

  void loadFromCache();

  void saveToCache(JSONObject json);

  void clear();

  boolean cacheExists();

  File getCacheFile();

  File getBackupFile();

  void report(TimeLog ps);

  void createSubscription();

  void setInitialLoadComplete();
}
