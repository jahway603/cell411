package cell411.logic.rel;

import java.util.*;

import javax.annotation.Nonnull;

public class RelationRelater {
  @Nonnull
  final RelationHolder mHolder;
  public final List<Rel> mBasicRels = getBasicRels();

  RelationRelater(@Nonnull RelationHolder holder) {
    mHolder = holder;
  }

  public List<Rel> getBasicRels() {
    List<Rel> list = new ArrayList<>();
    assert mHolder != null;
    list.add(mHolder.mCurrentUser);
    list.add(mHolder.mMyBlocks);
    list.add(mHolder.mRequestsOwned);
    list.add(mHolder.mAlertsOwned);
    list.add(mHolder.mAlertsSentTo);
    list.add(mHolder.mBlocksMe);
    list.add(mHolder.mRequestsSentTo);
    list.add(mHolder.mCounterParties);
    list.add(mHolder.mRequestCells);
    list.add(mHolder.mPubCellsOwned);
    list.add(mHolder.mPriCellsOwned);
    list.add(mHolder.mPubCellsJoined);
    list.add(mHolder.mPriCellsJoined);
    list.add(mHolder.mFriends);
    return Collections.unmodifiableList(list);
  }
//  {
//    mAllUsers.addRels(mFriends);
//    mAllUsers.addRels(mCounterParties);
//    mAllUsers.addRels(mPubCellMembers);
//    mAllUsers.addRels(mCurrentUser);
//  }

}
