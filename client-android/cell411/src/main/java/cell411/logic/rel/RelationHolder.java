package cell411.logic.rel;

import androidx.annotation.Nullable;
import cell411.json.JSONObject;
import cell411.libcell.ConfigDepot;
import cell411.logic.rel.Rel.Key;
import cell411.parse.XBaseCell;
import cell411.utils.Reflect;
import cell411.utils.Reflect.GetSet;
import cell411.utils.TheWolf;
import cell411.utils.Util;
import com.parse.model.ParseUser;
import com.parse.utils.ParseFileUtils;

import javax.annotation.Nonnull;
import java.io.File;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class RelationHolder
{
  public final static String[] mPriMemberPrefix =
    "PrivateCell:members:_User:objectId:".split(":");
  public final static String[] mPubMemberPrefix =
    "PublicCell:members:_User:objectId:".split(":");

  Rel.Key getCellMembersKey(XBaseCell cell){
    Rel.Key key = cell.getMembersKey();
    if(key!=null)
      return key;
    return getCellMembersKey(cell.isPublic(),cell.getObjectId());
  }
  public final HashMap<String, GetSet<Rel>> mById;
  public AggregateRel mAllUsers;
  public AggregateRel mAllCells;
  public AggregateRel mCounterParties;
  public AggregateRel mPubCells;
  public AggregateRel mPriCells;
  public AggregateRel mAlerts;
  public AggregateRel mRequests;
  public AggregateRel mEntities;
  public AggregateRel mAllPubMembers;
  public AggregateRel mAllPriMembers;
  public AggregateRel mAllChatRooms;
  public Rel mJoinedChatRooms;
  public Rel mCurrentUser = CurrentUserRel.get();
  public Rel mFriends;
  public String mUserId;
  public Rel mPubCellsOwned;
  public Rel mPriCellsOwned;
  public Rel mPubCellsJoined;
  public Rel mPriCellsJoined;
  public Rel mMyBlocks;
  public Rel mBlocksMe;
  public Rel mAlertsOwned;
  public Rel mAlertsSentTo;
  public Rel mChatRooms;
  public Rel mRequestsOwned;
  public Rel mRequestsSentTo;
  public Rel mRequestCells;
  public final HashMap<Rel.Key,Rel> mPubCellMembers =
    new HashMap<>();
  public final HashMap<Rel.Key,Rel> mPriCellMembers
    = new HashMap<>();
  public Rel mUnknown;
  File mFile = ConfigDepot
    .getJsonCacheFile("Holder");

  public RelationHolder() {
    Reflect.announce(Thread.currentThread());

    ParseUser user = ParseUser.getCurrentUser();
    mUserId = user.getObjectId();
    mById = loadHolder(this);
    mAllUsers.addRels(mFriends, mCounterParties,
        mCurrentUser,
        mAllPubMembers, mAllPriMembers);
    mAllChatRooms.addRels(mChatRooms);
    mPubCells.addRels(mPubCellsJoined, mPubCellsOwned);
    mPriCells.addRels(mPriCellsJoined, mPriCellsOwned);
    mPubCells.addObserver((o, arg) ->
    {
      TreeSet<Key> keys = new TreeSet<>();
      for(String id : mPubCells.getRelatedIds()) {
        keys.add(getCellMembersKey(true,id));
      }
      mPubCellMembers.keySet().retainAll(keys);
      for(Key key : keys) {
        Rel rel = mPubCellMembers.computeIfAbsent(key, Rel::new);
        System.out.println(key+" => "+rel);
      }
    });
    mPriCells.addObserver((o, arg) ->
    {
      TreeSet<Key> keys = new TreeSet<>();
      for(String id : mPriCells.getRelatedIds()) {
        keys.add(getCellMembersKey(false,id));
      }
      mPriCellMembers.keySet().retainAll(keys);
      for(Key key : keys) {
        Rel rel = mPriCellMembers.computeIfAbsent(key, Rel::new);
        System.out.println(key+" => "+rel);
      }
    });
    mRequests.addRels(mRequestsOwned, mRequestsSentTo);
    mAlerts.addRels(mAlertsOwned, mAlertsSentTo);
    mEntities.addRels(mAlerts, mPriCells, mPubCells);
  }

  private Rel.Key getCellMembersKey(boolean pub, String id)
  {
    String[] arr = pub ? mPubMemberPrefix : mPriMemberPrefix;
    assert arr.length==5;
    assert arr[4]==null;
    synchronized(this){
      try {
        arr[4] = id;
        return new Key(arr);
      } finally {
        arr[4]=null;
      }
    }
  }

  @Nonnull
  public static HashMap<String, GetSet<Rel>> loadHolder(RelationHolder rh) {
    File file = rh.mFile;
    if (!file.exists()) {
      ParseFileUtils.writeJSONObjectToFile(file, new JSONObject());
    }
    String text = //ParseFileUtils.readFileToString(file,"UTF-8");
        "{\n" +
            "   \"_User:audienceOf:Alert:objectId:*\" : \"mAlertsSentTo\",\n" +
            "   \"_User:ownerOf:Alert:objectId:*\" : \"mAlertsOwned\",\n" +
            "   \"_User:connectedTo:ChatRoom:objectId:*\" : \"mJoinedChatRooms\",\n" +
            "   \"_User:friends:_User:objectId:*\" : \"mFriends\",\n" +
            "   \"_User:memberOf:PrivateCell:objectId:*\" : \"mPriCellsJoined\",\n" +
            "   \"_User:memberOf:PublicCell:objectId:*\" : \"mPubCellsJoined\",\n" +
            "   \"_User:ownerOf:PrivateCell:objectId:*\" : \"mPriCellsOwned\",\n" +
            "   \"_User:ownerOf:PublicCell:objectId:*\" : \"mPubCellsOwned\",\n" +
            "   \"_User:ownerOf:Request:objectId:*\" : \"mRequestsOwned\",\n" +
            "   \"_User:sentToOf:Request:objectId:*\" : \"mRequestsSentTo\",\n" +
            "   \"_User:ownerOf:Request:objectId:*\" : \"mRequestsOwned\",\n" +
            "   \"_User:sentToOf:Request:objectId:*\" : \"mRequestsSentTo\",\n" +
            "   \"Alert\":\"mAlerts\",\n" +
            "   \"Request\":\"mRequests\",\n" +
            "   \"PublicCell\":\"mPubCells\",\n" +
            "   \"PrivateCell\":\"mPriCells\",\n" +
            "   \"_User\":\"mAllUsers\",\n" +
            "   \"ChatRoom\":\"mAllChatRooms\",\n" +
            "   \"PubCellMembers\":\"mAllPubMembers\",\n" +
            "   \"PriCellMembers\":\"mAllPriMembers\",\n" +
            "   \"CounterParties\":\"mCounterParties\",\n" +
            "   \"Entity\":\"mEntities\"\n" +
           "}\n";
    JSONObject json = new JSONObject(text);
    HashMap<String, GetSet<Rel>> byId = new HashMap<>();
    Map<String, Object> stringObjectMap = json.asMap();
    for (String key : stringObjectMap.keySet()) {
      String name = String.valueOf(stringObjectMap.get(key));
      if (name.equals("null") || name.equals("")) {
        TheWolf.showToast("No fields name for " + key);
        name = "mUnknown";
      }
      try {
        GetSet<Rel> gs = Reflect.getGetSet(rh, name, Rel.class);
        byId.put(key, gs );
        Class<? super Rel> type = gs.getType();
        Constructor<? super Rel> cons = type.getConstructor(Key.class);
        Rel object = (Rel) cons.newInstance(new Key(key));
        gs.set(object);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    return byId;
  }
  void setRel(Rel rel){
    Rel real = getRel(rel.getKey());
    real.setRelatedIds(rel.mRelatedIds);
  }

  public JSONObject toJSON() {
    JSONObject json = new JSONObject();
    synchronized (mById) {
      for (String key : mById.keySet()) {
        GetSet<Rel> gs = mById.get(key);
        assert gs != null;
        String fieldName = gs.getName();
        json.put(key, fieldName);
      }
    }
    return json;
  }

  public Rel getRel(String[] strs) {
    if (strs[4].equals(mUserId)) {
      strs[4] = "*";
    }
    Key key = new Key(strs);
    return getRel(key);
  }
  public Rel getRel(Key key){
    String str = key.toString();
    Rel res;
    if (isPubMembersRel(key.mOwningClass, key.mOwningField)) {
      synchronized(mPubCellMembers) {
        res = mPubCellMembers.computeIfAbsent(key, Rel::new);
      }
    } else if (isPriMembersRel(key.mOwningClass, key.mOwningField)) {
      synchronized(mPriCellMembers) {
        res = mPriCellMembers.computeIfAbsent(key, Rel::new);
      }
    } else {
      GetSet<Rel> accessor = mById.get(str);
      if (accessor == null) {
        synchronized (mById) {
          accessor = Reflect.getGetSet(this, "mUnknown", Rel.class);
          mById.put(str, accessor);
        }
      }
      res = accessor.get();
      if (res == null) {
        res = new Rel(new Key(str));
        accessor.set(res);
        System.out.println("Commented Save Here");
      }
    }
    return res;
  }

  private boolean isPriMembersRel(String type, String field) {
    if(type==null || field==null)
      return false;
    if(type.equals("PrivateCell")){
      return field.equals("members");
    }
    return false;
  }

  private boolean isPubMembersRel(String type, String field) {
    if(type==null || field==null)
      return false;
    if(type.equals("PublicCell")){
      return field.equals("members");
    }
    return false;
  }

  public Iterable<? extends Rel> values() {
    synchronized (mById) {
      return Util.transform(mById.keySet(), s -> {
        GetSet<Rel> gs = mById.get(s);
        if (gs == null)
          return null;
        return gs.get();
      });
    }
  }

  public void clear()
  {
    for(String id : mById.keySet()) {
      GetSet<Rel> accessor;
      accessor=mById.get(id);
      if(accessor!=null)
        accessor.set(null);
    }
  }

  public Rel getCellMembers(boolean pub, Key key)
  {
    return (pub?mPubCellMembers:mPriCellMembers).get(key);
  }

  public Rel getCellMembers(XBaseCell cell)
  {
    return getCellMembers(cell.isPublic(),getCellMembersKey(cell));
  }

  //  private class SaveEventually implements Runnable {
//    private long mReadyTime;
//
//    public SaveEventually() {
//    }
//
//    @Override
//    public void run() {
//      long delta = mReadyTime - System.currentTimeMillis();
//      System.out.println(this + " read in " + delta);
//      if (delta > 0) {
//        ConfigDepot.getExecutor()
//          .schedule(this, delta, TimeUnit.MILLISECONDS);
//      } else {
//        synchronized (mById) {
//          JSONObject json = toJSON();
//          File file = mFile;
//          String text = json.toString(2);
//
//          ParseFileUtils.writeJSONObjectToFile(file, json);
//        }
//      }
//    }
//
//  }

  public static class ShortKeyHashMap
    extends HashMap<String,Rel>
  {
    @Nullable
    @Override
    public Rel put(String key, Rel value)
    {
      assert key.indexOf(':')==-1;
      assert value.getKey()!=null;
      return super.put(key, value);
    }
    public Rel get(String key) {
      assert key.indexOf(':')==-1;
      Rel value = super.get(key);
      assert value==null || value.getKey()!=null;
      return value;
    }
  }
}
