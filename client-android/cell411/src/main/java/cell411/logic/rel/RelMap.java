//package cell411.logic.rel;
//
//import com.parse.model.ParseObject;
//
//import java.util.TreeMap;
//import java.util.TreeSet;
//
//
//public class RelMap //extends TreeMap<String, Rel>
//{
//  public TreeMap<String, Rel> mMap = new TreeMap<>();
//
//  public RelMap()
//  {
//  }
//
//  public Rel put(Rel rel)
//  {
//    String key = rel.getKeyStr();
//    Rel old = mMap.computeIfAbsent(key, (k) -> rel);
//    if (old != rel)
//      old.setRelatedIds(rel.getRelatedIds());
//    return old;
//  }
//
//  public Rel get(String key, Class<? extends ParseObject> type)
//  {
//    Rel res = mMap.get(key);
//    if (res != null)
//    {
//      return res;
//    }
//    put(new Rel(key, null));
//    return get(key, type);
//  }
//
//  public void clear()
//  {
//    for (Rel value : mMap.values())
//    {
//      value.clear();
//    }
//  }
//
//  public boolean containsKey(final String key)
//  {
//    return mMap.containsKey(key);
//  }
//
//  public int size()
//  {
//    return mMap.size();
//  }
//
//  public TreeSet<Rel> values()
//  {
//    return new TreeSet<>(mMap.values());
//  }
//
//  public TreeSet<String> keySet()
//  {
//    return new TreeSet<>(mMap.keySet());
//  }
//
//}
//