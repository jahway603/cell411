package cell411.logic.rel;

import cell411.logic.MyObservable;
import cell411.logic.MyObserver;

import java.util.HashSet;

public class AggregateRel extends Rel implements MyObserver {
  final HashSet<Rel> mRels = new HashSet<>();

  public AggregateRel() {
    super();
  }
  public AggregateRel(Key key){
    super(key);
  }

  public void addRels(Rel... rels) {
    for (Rel rel : rels) {
      if(rel==null) {
        System.out.println("Warning:  missing rel");
        continue;
      }
      mRels.add(rel);
      rel.addObserver(this);
    }
    update(null, null);
  }

  @Override
  public void update(MyObservable o, Object arg) {
    HashSet<String> ids = new HashSet<>();
    for (Rel rel : mRels) {
      ids.addAll(rel.getRelatedIds());
    }
    setRelatedIds(ids);
  }

  public HashSet<Rel> getRels() {
    return mRels;
  }

  public HashSet<Rel> values() {
    return getRels();
  }
}
