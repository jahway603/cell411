package cell411.logic.rel;

import com.parse.model.ParseUser;

import java.lang.ref.WeakReference;
import java.util.TreeSet;

import javax.annotation.Nullable;

import cell411.utils.ValueObserver;

public class CurrentUserRel extends Rel implements ValueObserver<ParseUser> {
  static WeakReference<CurrentUserRel> smInstance =
    new WeakReference<>(new CurrentUserRel());

  private CurrentUserRel() {
    super(new Key("*"));
    assert smInstance == null;
    smInstance = new WeakReference<>(this);
    ParseUser.addCurrentUserObserver(this);
    ParseUser user = ParseUser.getCurrentUser();
    if (user != null) {
      add(user.getObjectId());
    }
  }

  public synchronized static CurrentUserRel get() {
    return smInstance.get();
  }

  @Override
  public void onChange(@Nullable ParseUser newValue, @Nullable ParseUser oldValue) {
    TreeSet<String> keys = new TreeSet<>();
    if (newValue != null) {
      keys.add(newValue.getObjectId());
    }
    setRelatedIds(keys);
  }
}
