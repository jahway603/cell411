//package cell411.logic.rel;
//
//import com.parse.model.ParseObject;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.TreeSet;
//
//public class FullRel extends Rel
//{
//  final Key mKey;
//
//  public FullRel(Key key, TreeSet<String> ids)
//  {
//    super(key.makeString(), ids);
//    mKey = key;
//  }
//
//  public static Class<? extends ParseObject> getClass(String table)
//  {
//    return ParseObject.getClassForTable(table);
//  }
//
//  public static Rel create(List<String> keyVals)
//  {
//    return new FullRel(new Key(keyVals), null);
//  }
//
//  public Key getKey()
//  {
//    return mKey;
//  }
//}
//