package cell411.logic.rel;

import cell411.json.JSONArray;
import cell411.logic.MyObservable;
import cell411.utils.*;
import com.parse.model.ParseObject;
import com.parse.model.ParseRelation;

import javax.annotation.Nonnull;
import java.io.PrintStream;
import java.util.*;

public class Rel
    extends MyObservable
    implements Comparable<Rel>,
    Iterable<String>
{
  public final TreeSet<String> mRelatedIds = new TreeSet<>();
  private final Key mKey;
  boolean mHangFire = false;
  private boolean mDisabled = false;

  public Rel() {
    this(null);
  }

  public Rel(Key key) {
    mKey = key;
  }

  //  public Rel(Key key, Set<String> ids)
//  {
//    if(ids!=null)
//      mRelatedIds.addAll(ids);
//  }

//  private Rel(Key key, Iterable<String> ids) {
//    mKey=key;
//
//  }

//  private Rel(List<String> asList) {
//    mKey=new Key(asList);
//  }

//  public static Rel create(final JSONArray relArray)
//  {
//    TypedIterator<Object> iterator = (relArray.iterator());
//    Key key = new Key(iterator);
//    TreeSet<String> ids = new TreeSet<>();
//    if (iterator.hasNext())
//    {
//      Integer count = iterator.nextInteger();
//      Collect.addAll(ids, iterator.strings());
//      assert ids.size() == count;
//    }
//    assert !iterator.hasNext();
//    return new Rel(key.toString(), ids);
//  }

//  public static Rel create(String keyStr, Class<? extends ParseObject> type)
//  {
//    return new Rel(Key.create(keyStr), null);
//  }

  public void setDisabled(boolean disabled) {
    mDisabled = disabled;
  }

  public void hangFire(boolean hanging) {
    mHangFire = hanging;
    if (!mHangFire) {
      notifyObservers();
    }
  }

  /**
   * Get your very own copy of the set of related objects.
   *
   * @return A copy of the set of related Objects
   */
  public Set<String> getRelatedIds() {
    return Collections.unmodifiableSet(mRelatedIds);
  }

  /**
   * Set the set of related objects to a new set.  This
   * actually updates the database, if it can.  For now,
   * that means if this object exactly mirrors an actual
   * relation in the physical database.  You can change
   * your friends list ... you cannot change the list of
   * people that have listed you as a friend.  It's not
   * a security thing, I just have not had time to write
   * it.
   *
   * @param newSet The new set of objectIds.
   */
  public synchronized boolean setRelatedIds(Collection<String> newSet) {
    Reflect.announce(this);
    Reflect.announce("   newset:  "+newSet);
    boolean changed;
    assert !mDisabled;
    if (newSet == null || newSet.isEmpty()) {
      changed = !mRelatedIds.isEmpty();
      mRelatedIds.clear();
    } else {
      boolean removed = mRelatedIds.retainAll(newSet);
      boolean added = mRelatedIds.addAll(newSet);
      changed = added || removed;
    }
    if (changed) {
      setChanged();
    }
    hangFire(mHangFire);
    return changed;
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  public void dump(PrintStream out) {
    assert !mDisabled;
    int lineWidth = 80;
    out.println(getKeyStr());
    if (mRelatedIds.size() == 0) {
      out.println("  []");
      return;
    }
    // we sort first by length, so the last key is the longest
    String lastId = mRelatedIds.last();
    int maxWidth = lastId.length();
    int fieldWidth = maxWidth + 6;
    int columns = lineWidth / fieldWidth + 1;
    ArrayList<String[]> groups = new ArrayList<>();
    String fmt = "%-" + (1 + maxWidth) + "s";
    {
      String[] group = new String[columns];
      int gridx = 0;
      for (String rel : getRelatedIds()) {
        rel = Util.format(fmt, rel + ",");
        if (gridx == group.length) {
          groups.add(group);
          group = new String[columns];
          gridx = 0;
        }
        group[gridx++] = rel;
      }
      while (gridx < group.length) {
        group[gridx++] = Util.format(fmt, "");
      }
      groups.add(group);
    }
    {
      for (String[] group : groups) {
        String line = String.join(" ", group);

        out.println("  [" + line + "]");
      }
    }
  }

  @Nonnull
  @Override
  public String toString() {
    assert !mDisabled;
    String key = getKeyStr();
    return Util.format("%s => %s", key, mRelatedIds);
  }

  public final String getKeyStr() {
    if (mKey == null) {
      return "No Key";
    } else {
      return mKey.toString();
    }
  }

  public boolean removeAll(Collection<String> ids) {
    assert !mDisabled;
    Set<String> newSet = new HashSet<>(mRelatedIds);
    newSet.removeAll(ids);
    return setRelatedIds(newSet);
  }

  public boolean addAll(Collection<String> ids) {
    assert !mDisabled;
    Set<String> newSet = new HashSet<>(mRelatedIds);
    newSet.addAll(ids);
    return setRelatedIds(newSet);
  }

  public boolean add(String id) {
    return addAll(Collect.asList(id));
  }

  public boolean remove(String id) {
    return removeAll(Collect.asList(id));
  }

  public boolean contains(String id) {
    assert !mDisabled;
    return mRelatedIds.contains(id);
  }

  public void clear() {
    assert !mDisabled;
    setRelatedIds(new HashSet<>());
  }

  @Override
  public int compareTo(Rel o) {
    return getKeyStr().compareTo(o.getKeyStr());
  }

  public int getSize() {
    return mRelatedIds.size();
  }

  public JSONArray toJSON() {
    Key key = getKey();
    if(key==null) {
      System.out.println("Its null");
      return null;
    }
    JSONArray res = key.toJSON();
    TreeSet<String> ids = new TreeSet<>(getRelatedIds());
    res.put(ids.size());
    res.putAll(ids);
    return res;
  }

  public Key getKey() {
    return mKey;
  }

  @Nonnull
  @Override
  public Iterator<String> iterator() {
    return mRelatedIds.iterator();
  }

  public static class Key implements Comparable<Key> {
    public final String mOwningClass;
    public final String mOwningField;
    public final String mOwningId;
    // The className of the related Object
    final String mRelatedClass;
    final String mRelatedField;
    public String mString;

    public Key(String owningClass, String owningField,
               String relatedClass, String relatedField,
               String owningId) {
      mOwningClass = owningClass;
      mOwningField = owningField;
      mRelatedClass = relatedClass;
      mRelatedField = relatedField;
      mOwningId = owningId;
    }

    public Key(String string) {
      mString = string.intern();
      mOwningClass = null;
      mOwningField = null;
      mRelatedClass = null;
      mRelatedField = null;
      mOwningId = null;
    }

    public Key(String[] strs) {
      this(strs[0], strs[1], strs[2], strs[3], strs[4]);
    }

    public Key(List<String> keyParts)
    {
      this(Collect.toArray(String.class,keyParts));
    }

    //    public Key(final Iterable<String> array)
//    {
//      this(array.iterator());
//    }

//    public <X extends ParseObject> Key(final ParseRelation<X> friends)
//    {
//      this(
//      );
//    }

//    public Key(TypedIterator<Object> i)
//    {
//      this(i.strings());
//    }

//    public Key(String keyStr) {
//      this(Arrays.asList(keyStr.split(":")));
//    }

    public static Key create(ParseRelation<? extends ParseObject> i) {
      return new Key(i.getParentClassName(), i.getKey(),
        i.getTargetClass(), "objectId",
        i.getParentId());
    }

    public static Key create(String currentUser) {
      if (currentUser.contains(":")) {
        String[] parts = currentUser.split(":");
        if (parts.length != 5)
          throw new RuntimeException("Key must have one seg or 5");
        return new Key(
          parts[0], parts[1], parts[2], parts[3], parts[4]
        );
      } else {
        return new Key(currentUser, null, null, null, null);
      }
    }

    public static Key create(
      String s1, String s2, String s3, String s4, String s5
    ) {
      return new Key(s1, s2, s3, s4, s5);
    }

    public static Key create(String[] keyStrs)
    {
      assert keyStrs.length==5;
      return new Key(keyStrs);
    }

    @Nonnull
    public String toString() {
      if (mString == null) {
        mString = makeString();
      }
      return mString;
    }

    public String makeString() {
      if (mString == null) {
        if (mOwningClass == null) {
          return "The Unknown Key";
        } else {
          StringJoiner joiner = new StringJoiner(":");
          joiner.add(mOwningClass)
            .add(mOwningField)
            .add(mRelatedClass)
            .add(mRelatedField)
            .add(mOwningId);
          mString = joiner.toString();
        }
      }
      return mString;
    }

    public JSONArray toJSON()
    {
      if(mOwningClass!=null && mRelatedClass !=null ) {
        JSONArray res = new JSONArray();
        res.put(mOwningClass);
        res.put(mOwningField);
        res.put(mRelatedClass);
        res.put(mRelatedField);
        res.put(mOwningId);
        return res;
      } else if ( mString!=null && mString.contains(":")) {
        return create(mString).toJSON();
      } else {
        return new JSONArray("['No String']");
      }
    }

    static int compare(int prev, String lhs, String rhs){
      if(prev!=0)
        return prev;
      return lhs.compareTo(rhs);
    }
    @Override
    public int compareTo(Key o)
    {
      if(o==null)
        return 1;
      int res = 0;
      res = compare(res,mOwningClass, o.mOwningClass);
      res = compare(res,mOwningField, o.mOwningField);
      res = compare(res,mRelatedClass, o.mRelatedClass);
      res = compare(res,mRelatedField, o.mRelatedField);
      res = compare(res,mOwningId, o.mOwningId);
      return res;
    }
  }
}
