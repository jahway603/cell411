package cell411.logic;

import cell411.json.JSONObject;
import cell411.logic.rel.Rel;
import cell411.parse.XRequest;
import cell411.parse.XUser;
import com.parse.ParseQuery;
import com.parse.model.ObjectEvent;
import com.parse.model.ParseObject;

import java.util.*;

public class RequestWatcher extends Watcher<XRequest> {
  private static final Set<String> smStatusSet =
    Collections.unmodifiableSet(new HashSet<>(Arrays.asList("PENDING", "RESENT")));
  private final Rel mOwned;
  private final Rel mReceived;

  public RequestWatcher(RelationWatcher relationWatcher)
  {
    super(relationWatcher, "Requests", XRequest.class);
    mOwned=relationWatcher.getOwnedRequests();
    mReceived=relationWatcher.getReceivedRequests();
  }


  @Override
  public void createSubscription() {
    super.createSubscription();
  }

  @Override
  public void saveToCache(JSONObject json) {
    super.saveToCache(json);
  }

  @Override
  protected Set<String> getIds(boolean dirty) {
    return super.getIds(dirty);
  }

  public ParseQuery<XRequest> createQuery(Set<String> dirty) {
    return super.createQuery(dirty);
  }

  @Override
  public ParseQuery<XRequest> directQuery() {
    ParseQuery<XRequest> q1 = XRequest.q();
    XUser currentUser = XUser.getCurrentUser();
    ParseQuery<XRequest> q2 = XRequest.q();
    q1.whereEqualTo("owner", currentUser);
    q2.whereEqualTo("sentTo", currentUser);
    q1 = ParseQuery.or(q1, q2);
    return q1.whereContainedIn("status", smStatusSet);
  }

  @Override
  public void loadFromCache() {
    super.loadFromCache();
  }

  @Override
  public void loadFromNet(Collection<ParseObject> objs) {
    super.loadFromNet(objs);
  }

  @Override
  public synchronized void onEvents(ObjectEvent<XRequest> event) {
    super.onEvents(event);
  }

  @Override
  public synchronized void update(final MyObservable o, final Object arg) {
    super.update(o, arg);
  }

  public Rel getOwned()
  {
    return mOwned;
  }
  public Rel getReceived()
  {
    return mReceived;
  }
}
