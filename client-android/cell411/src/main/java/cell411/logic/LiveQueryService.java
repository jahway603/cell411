package cell411.logic;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import cell411.IListener;
import cell411.ILiveQueryService;
import cell411.logic.rel.Rel;
import cell411.parse.XBaseCell;
import cell411.parse.XPrivateCell;
import cell411.parse.XPublicCell;
import cell411.ui.base.BaseApp;
import cell411.utils.ExceptionHandler;
import cell411.utils.ObservableValue;
import cell411.utils.OnCompletionListener;
import cell411.utils.Reflect;
import cell411.utils.TimeLog;
import cell411.utils.ValueObserver;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.callback.ParseCallback2;
import com.parse.http.ParseSyncUtils;
import com.parse.livequery.LiveQueryException;
import com.parse.livequery.ParseLiveQueryClient;
import com.parse.livequery.ParseLiveQueryClientCallbacks;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;
import com.safearx.cell411.R;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import static java.lang.Boolean.FALSE;
import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;

public class LiveQueryService
  extends Service
  implements ParseLiveQueryClientCallbacks, ExceptionHandler,
             LiveQueryIface
{
  static LQStatics smStatics = new LQStatics();
  static int smReloadCount = 0;
  public final String FOREGROUND = "ForeGround";
  final ScheduledExecutorService mExecutorService = newSingleThreadScheduledExecutor(new LQThreadFactory());
  private final List<ParseQuery<?>> mParseQueries = new ArrayList<>();
  private final int ID_FOREGROUND;
  ObservableValue<Boolean> mReady = new ObservableValue<>(Boolean.class, FALSE);
  LocalLiveQueryHandler mBinder = new LocalLiveQueryHandler();
  private MultiCacheObject mMultiCacheObject = new MultiCacheObject(this);
//  private TimeLog mTimeLog = new TimeLog(
//    Cell411.req().getLogStream());
  private NotificationChannel mChannel;
  private ParseLiveQueryClient mClient;
  private ByteArrayOutputStream mLogText;

  public LiveQueryService() {
    ID_FOREGROUND = 17751984;
    smStatics.smInstance = new WeakReference<>(this);
    mExecutorService.execute(() ->
      XBaseCell.setMemberLookup(getRelationWatcher()));
  }

  @Nullable
  public static <X extends ParseObject>
  X getObject(String objectId) {
    LiveQueryService service = opt();
    if (service == null)
      return null;
    return MultiCacheObject.getObject(objectId);
  }


  @Nonnull
  public static <X extends ParseObject>
  X getOrStub(String objectId, Class<X> type) {
    return ObjectCache.getObject(objectId,type);
  }
  public static LiveQueryService opt() {
    return smStatics.get();
  }

  public static LiveQueryService req() {
    return Objects.requireNonNull(opt());
  }

  public static void getPermRequests(Set<String> perms) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
      perms.add(Manifest.permission.FOREGROUND_SERVICE);
    }
  }

  public Rel getCounterParties() {
    return getRelationWatcher().getCounterParties();
  }

  public void addReadyObserver(ValueObserver<? super Boolean> observer) {
    mReady.addObserver(observer);
  }

  public boolean isReady() {
    return mReady.get();
  }

  public void setReady(boolean b) {
    mReady.set(b);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    super.onStartCommand(intent, flags, startId);
    showNotification();
    reload(success -> setReady(true));
    return START_STICKY;
  }

  public void reload(OnCompletionListener listener) {
    System.out.printf("Reload #%d\n", smReloadCount++);
    ReloadCache job = new ReloadCache(listener);
    mExecutorService.submit(job);
  }

  public void showNotification() {
    NotificationManagerCompat nm = NotificationManagerCompat.from(this);
    if (mChannel == null) {
      mChannel = new NotificationChannel(FOREGROUND, FOREGROUND, NotificationManager.IMPORTANCE_LOW);
      mChannel.setShowBadge(false);
      nm.createNotificationChannel(mChannel);
    }
    Class<?> type = getClass();

    Intent notificationIntent = new Intent(this, type);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE);
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, FOREGROUND);
    notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
    notificationBuilder.setOngoing(true);
    notificationBuilder.setPriority(NotificationCompat.PRIORITY_MIN);
    notificationBuilder.setShowWhen(false);
    notificationBuilder.setWhen(0);
    notificationBuilder.setContentTitle(getClass().getSimpleName());
    notificationBuilder.setChannelId(FOREGROUND);

    String message = "The Service Is Running";
    notificationBuilder.setContentText(message);

    notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
    if (pendingIntent != null) {
      notificationBuilder.setContentIntent(pendingIntent);
    }

    startForeground(ID_FOREGROUND, notificationBuilder.build());
  }

//  public TimeLog getTimeLog() {
//    if (mTimeLog == null) {
//      mTimeLog = new TimeLog(System.out);
//    }
//    return mTimeLog;
//  }

  @Override
  public <T extends ParseObject>
  ParseObjectSubscription<T> subscribe(
    ParseQuery<T> query,
    ObjectEventsCallback<T> callback)
  {
    mParseQueries.add(query);
    return new ParseObjectSubscription<>(
      this,query,callback
    );
  }

  @Override
  public ParseLiveQueryClient getClient() {
    if (mClient == null) {
      mClient = new ParseLiveQueryClient();
      mClient.registerListener(this);
    }
    return mClient;
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return mBinder;
  }

  @Override
  public <T extends ParseObject> void unsubscribe(ParseQuery<T> query) {
    mClient.unsubscribe(query);
  }

  public void maybeReconnect() {
    mClient.connectIfNeeded();
  }

  public ChatRoomWatcher getChatRoomWatcher() {
    return getCache(true).getChatRoomWatcher();
  }

  public RelationWatcher getRelationWatcher() {
    return getCache(true).getRelationWatcher();
  }

  public void clear() {
    for (ParseQuery<?> query : mParseQueries) {
      unsubscribe(query);
    }
    mParseQueries.clear();
    if (mMultiCacheObject != null) {
      mMultiCacheObject.clear();
      mMultiCacheObject = null;
    }
  }

  public AlertWatcher getAlertWatcher() {
    return getCache(true).getAlertWatcher();
  }

  public RequestWatcher getRequestWatcher() {
    return getCache(true).getRequestWatcher();
  }

  public CellWatcher<XPublicCell> getPublicCellWatcher() {
    return getCache(true).getPublicCellWatcher();
  }

  public UserWatcher getUserWatcher() {
    return getCache(true).getUserWatcher();
  }

  public CellWatcher<XPrivateCell> getPrivateCellWatcher() {
    return getCache(true).getPrivateCellWatcher();
  }

  @Override
  public void onLiveQueryClientConnected(final ParseLiveQueryClient client) {
    Reflect.announce("" + client);
  }

  @Override
  public void onLiveQueryClientDisconnected(final ParseLiveQueryClient client, final boolean userInitiated) {
    Reflect.announce("" + client);
    BaseApp.getExecutor().schedule(this::maybeReconnect, 15000, TimeUnit.MILLISECONDS);
  }

  @Override
  public void onLiveQueryError(final ParseLiveQueryClient client, final LiveQueryException reason) {
    Reflect.announce("" + client);
  }

  @Override
  public void onSocketError(final ParseLiveQueryClient client, final Throwable reason) {
    Reflect.announce(client + " " + reason);
  }

  public void queueNetLoad(CacheObject xWatcher) {
    BaseApp.getExecutor()
        .schedule((Runnable) () -> mExecutorService.submit(xWatcher.createNetLoader()), 500,
            TimeUnit.MILLISECONDS);
  }

  public ExecutorService getExecutor() {
    return mExecutorService;
  }

  public String report() {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    TimeLog log = new TimeLog(stream);
    report(log);
    return stream.toString();
  }

  public void report(TimeLog timeLog) {
    MultiCacheObject multiCacheObject = getCache(true);
    timeLog.pf("multiCacheObject=%s", multiCacheObject);
    if (multiCacheObject != null) {
      multiCacheObject.report(timeLog);
    }
    timeLog.pf("multiCacheObject=%s", multiCacheObject);
    System.out.println("report complete");
  }

  public synchronized MultiCacheObject getCache(boolean create) {
    if (create && mMultiCacheObject == null) {
      mMultiCacheObject = new MultiCacheObject(this);
    }
    return mMultiCacheObject;
  }

  static class LQStatics {
    private WeakReference<LiveQueryService> smInstance;

    public LiveQueryService get() {
      if (smInstance == null) {
        return null;
      }
      return smInstance.get();
    }
  }

  public void onUI(Runnable runnable){
    BaseApp.req().onUI(runnable);
  }
  public void onUI(Runnable runnable, long delay) {
    BaseApp.req().onUI(runnable,delay);
  }
  static class LQThreadFactory implements ThreadFactory {
    @Override
    public Thread newThread(Runnable r) {
      return new Thread(r, "LiveQueryThread");
    }
  }

  class LocalLiveQueryHandler extends ILiveQueryService.Stub
  {
    @Override
    public boolean isReady() {
      return mReady.get();
    }

    @Override
    public void addListener(final IListener listener) throws RemoteException {
      String[] strings = listener.events();
      for (int i = 0; i < strings.length; i++) {
        System.out.println(strings[i] + " => " + strings[i + 1]);
      }
    }

    @Override
    public void query(String query, IListener listener)
      throws RemoteException {
      getExecutor().submit(new RunnableQuery(query, listener));
    }

    public void done(RunnableQuery runnableQuery) {
      try {
        runnableQuery.mListener.event(runnableQuery.mResult);
      } catch (RemoteException e) {
        e.printStackTrace();
      } finally {
        Reflect.announce("x");
      }
    }
  }

  class RunnableQuery implements Runnable, ParseCallback2<String> {
    public String mTextQuery;
    public IListener mListener;
    public String mResult;
    public ParseException mParseException;
    public boolean mSentInitial = false;

    RunnableQuery(String textQuery, IListener listener) {
      mTextQuery = textQuery;
      mListener = listener;
    }

    public void run() {
      if (!mSentInitial) {
        ParseSyncUtils.runQuery(mTextQuery, this);
      } else {
        mBinder.done(this);
      }
    }

    @Override
    public void done(String s, ParseException t2) {
      mResult = s;
      mParseException = t2;
      mExecutorService.submit(this);
    }
  }

  private class ReloadCache implements Callable<Boolean> {
    private final OnCompletionListener mOnCompletionListener;

    public ReloadCache(final OnCompletionListener onCompletionListener) {
      mOnCompletionListener = onCompletionListener;
    }

    public Boolean call() {
      try {
        getCache(true).run();
        if (mOnCompletionListener != null) {
          mOnCompletionListener.done(true);
        }
        return true;
      } catch (Throwable throwable) {
        throwable.printStackTrace();
        if (mOnCompletionListener != null) {
          mOnCompletionListener.done(false);
        } else {
          handleException("reloading cache", throwable);
        }
        return false;
      }
    }
  }

}
