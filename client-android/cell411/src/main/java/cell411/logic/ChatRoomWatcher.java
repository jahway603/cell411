package cell411.logic;

import cell411.logic.rel.AggregateRel;
import cell411.logic.rel.Rel;
import cell411.parse.XChatRoom;
import cell411.parse.XEntity;
import com.parse.ParseQuery;
import com.parse.model.ParseObject;

import javax.annotation.Nonnull;
import java.util.*;

public class ChatRoomWatcher extends Watcher<XChatRoom> {
  private final AggregateRel mEntityRel;

  public ChatRoomWatcher(@Nonnull RelationWatcher relationWatcher) {
    super(relationWatcher, "ChatRooms", XChatRoom.class);
    System.out.println(mRelationWatcher);
    Rel entities = mRelationWatcher.getRel("Entity");
    mEntityRel= (AggregateRel) entities;
    entities.addObserver(new MyObserver() {
      @Override
      public void update(MyObservable o, Object arg) {
        if(o!=mEntityRel)
          return;
        Map<String,XEntity> wholeSet;
        for(String id : mEntityRel) {
          ParseObject object = MultiCacheObject.getObject(id);
          if(!XEntity.class.isInstance(object))
            return;
          XEntity xo=(XEntity) object;
          if(object.has("chatRoom")){
            XChatRoom chatRoom=xo.getChatRoom();
            MultiCacheObject.putObject(chatRoom);
          }
        }
      }
    });
  }

//  public void doAllRels(final RelationWatcher relationWatcher, final ParseUser user) {
//    String userId = user.getObjectId();
//    List<String> keys = Arrays.asList("_User", "ownerOf", "PublicCell", "objectId", userId);
//    if (mInputRels == null) {
//      mInputRels = new ArrayList<>();
//      mInputRels.add(relationWatcher.getRel(keys));
//      keys.set(1, "memberOf");
//      mInputRels.add(relationWatcher.getRel(keys));
//      keys.set(2, "PrivateCell");
//      mInputRels.add(relationWatcher.getRel(keys));
//      keys.set(1, "ownerOf");
//      mInputRels.add(relationWatcher.getRel(keys));
//      keys.set(2, "Alert");
//      mInputRels.add(relationWatcher.getRel(keys));
//      keys.set(1, "audienceOf");
//      mInputRels.add(relationWatcher.getRel(keys));
//      mEntityRel = mRelationWatcher.getAggregateRel("allChatEntities", XEntity.class);
//      mEntityRel.addRels(mInputRels.toArray(new Rel[]{}));
//      mChatRoomRel = mRelationWatcher.getAggregateRel("allChatRooms", XChatRoom.class);
//      addRel(mChatRoomRel);
//      mEntityRel.addObserver((o, arg) ->
//      {
//        List<String> chatRooms = new ArrayList<>();
//        for (String key : mEntityRel.getRelatedIds()) {
//          ParseObject obj = MultiCacheObject.getObject(key);
//          XEntity ent = (XEntity) obj;
//          if (ent != null) {
//            XChatRoom room = ent.getChatRoom();
//            if (room != null) {
//              chatRooms.add(room.getObjectId());
//            }
//          }
//        }
//        mChatRoomRel.setRelatedIds(chatRooms);
//      });
//    }
//    mEntityRel.setChanged();
//    mEntityRel.notifyObservers();
//  }

//  public synchronized void addRel(Rel rel) {
//    super.addRel(rel);
//  }

  @Override
  public synchronized void loadFromNet(Collection<ParseObject> extras) {
    super.loadFromNet(extras);
  }

  @Override
  public synchronized void loadFromCache() {
    super.loadFromCache();
  }

  @Override
  public synchronized ParseQuery<XChatRoom> createQuery(Set<String> dirty) {
    return directQuery();
  }

  @Override
  public ParseQuery<XChatRoom> directQuery() {
    ParseQuery<XChatRoom> query = XChatRoom.q();
    query.whereContainedIn("entity", mEntityRel.mRelatedIds);
    return query;
  }

  @Override
  protected synchronized Set<String> getIds(boolean dirty) {
    Set<String> set = super.getIds(dirty);
    set.add("x");
    return set;
  }

//  @Override
//  void greetObject(final XChatRoom po, final Collection<ParseObject> list) {
//    super.greetObject(po, list);
//    List<String> keys = Collect.asList("_User", "participatesIn", "ChatRoom", "objectId",
//      XUser.getCurrentUser().getObjectId());
//
//    Rel rel = getRelationWatcher().getRel(keys);
//    rel.setRelatedIds(new TreeSet<>(mData.keySet()));
//  }

}
