package cell411.logic;

import cell411.json.JSONObject;
import cell411.libcell.ConfigDepot;
import cell411.utils.IOUtil;
import cell411.utils.Reflect;
import com.parse.model.ParseObject;

import javax.annotation.Nonnull;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.HashSet;

public abstract class CacheObject
  extends ObjectCache
  implements ICacheObject
{
  //  protected static String smDelimiter = " : ";
  private final String mName;
  private final File mCacheFile;
  protected boolean mDoneLoading = false;
  PropertyChangeSupport mPCS = new PropertyChangeSupport(this);
  LoadFromNet mLastLaunched;

  public CacheObject(LiveQueryIface service, String name) {
    super(service);
    mName = name;
    mCacheFile = ConfigDepot.getJsonCacheFile(name);
  }

  static MultiCacheObject smInstance;
  static MultiCacheObject get(){
    return smInstance;
  }
//  @Override
//  public PropertyChangeSupport getPCS() {
//    return mPCS;
//  }

  public void stringToFile(String text) {
    if (getCacheFile().exists()) {
      IOUtil.renameTo(getCacheFile(), getBackupFile());
    }
    IOUtil.stringToFile(getCacheFile(), text);
  }

  public String fileToString() {
    return IOUtil.fileToString(getCacheFile());
  }

  public void jsonToFile(JSONObject object) {
    stringToFile(object.toString(2));
  }

  public void jsonToFile(Object object) {
    JSONObject jsonObject;
    if(JSONObject.class.isInstance(object)){
      jsonObject=JSONObject.class.cast(object);
      assert jsonObject!=null;
    } else {
      JSONObject wrapper = new JSONObject();
      wrapper.put("__wrapped", object);
      jsonObject=wrapper;
    }
    jsonToFile(jsonObject);
  }

  @Override
  @Nonnull
  public File getBackupFile() {
    return new File(getCacheFile().toString() + ".bak");
  }

//  public <X> X fileToJSON(Class<X> type)
//  {
//    if (type == JSONObject.class) {
//      String json = fileToString();
//      JSONObject obj = new JSONObject(json);
//      return type.cast(obj);
//    } else {
//      JSONObject wrapper = fileToJSON(JSONObject.class);
//      if (!wrapper.has("__wrapped")) {
//        return null;
//      }
//      return type.cast(wrapper.get("__wrapped"));
//    }
//  }

  @Override
  public boolean cacheExists() {
    return getCacheFile().exists();
  }

  @Override
  public String getName() {
    return mName;
  }

  @Override
  public File getCacheFile() {
    return mCacheFile;
  }

  public void clear() {
    IOUtil.delete(getCacheFile());
  }

  public void setInitialLoadComplete() {
    mDoneLoading = true;
  }

  public Runnable createNetLoader() {
    return new LoadFromNet();
  }


  class LoadFromNet implements Runnable {
    LoadFromNet() {
      mLastLaunched = this;
    }

    @Override
    public void run() {
      if (mLastLaunched != this) {
        return;
      }
      Reflect.announce("About to run loadFromNet");
      HashSet<ParseObject> objects = new HashSet<>();
      synchronized (CacheObject.this) {
        Reflect.announce("Obtained Lock");
        loadFromNet(objects);
        Reflect.announce("Releasing Lock");
      }
      Reflect.announce("Done running loadFromNet");
    }
  }
}
