package cell411.logic;

import com.parse.ParseQuery;
import com.parse.livequery.*;
import com.parse.model.*;

import cell411.utils.Reflect;

public class ParseObjectSubscription<T extends ParseObject>
  implements HandleUnsubscribeCallback<T>,
             com.parse.model.ObjectEventsCallback<T>,
             ErrorCallback<T>,
             HandleSubscribeCallback<T>
{
  final ParseQuery<T> mQuery;
  final com.parse.model.ObjectEventsCallback<T> mCallback;
  private final MultiCacheObject mCache;

  public ParseObjectSubscription(final LiveQueryIface iface,
                                  final ParseQuery<T> query,
                                 final ObjectEventsCallback<T> callback)
  {
    mCache = iface.getCache(false);
    mQuery = query;
    mCallback = callback;
    ParseLiveQueryClient client=iface.getClient();
    Subscription<T> handler=client.subscribe(query);//,callback);

//    Subscription<T> handler = iface.subscribe(mQuery,null);

    handler.handleEvents(this);
    handler.handleError(this);
    handler.handleSubscribe(this);
    handler.handleUnsubscribe(this);
  }

  @Override
  public void onEvents(final ObjectEvent<T> event) {
    Dispatcher<T> dispatcher = event.dispatcher(mCallback);
    MultiCacheObject.get().getExecutor().submit(dispatcher);
  }

  public void onUnsubscribe(final ParseQuery<T> tParseQuery) {
    Reflect.announce(tParseQuery.getClassName());
  }

  public void onError(final ParseQuery<T> tParseQuery, final LiveQueryException e) {
    Reflect.announce(tParseQuery.getClassName());
  }

  public void onSubscribe(final ParseQuery<T> tParseQuery) {
    Reflect.announce(tParseQuery.getClassName());
  }
}
