package cell411.logic;

import com.parse.ParseQuery;
import com.parse.model.ParseObject;
import com.parse.model.ParseUser;

import cell411.parse.XBaseCell;
import cell411.parse.XUser;

import javax.annotation.Nonnull;
import java.util.Collection;

public class CellWatcher<X extends XBaseCell> extends Watcher<X> {
  public CellWatcher(@Nonnull RelationWatcher relationWatcher, Class<X> type) {
    super(relationWatcher, "CellWatcher(" + type.getSimpleName() + ")", type);
  }

  public void doAllRels(final RelationWatcher relationWatcher, final ParseUser user) {
    String userId = user.getObjectId();
//    addRel(relationWatcher.getRel(mClassName + ".tempCells", mType));
  }

  @Override
  public ParseQuery<X> directQuery() {
    XUser user = XUser.getCurrentUser();
    ParseQuery<X> query1 = ParseQuery.getQuery(mType).whereEqualTo("owner", user.getObjectId());
    ParseQuery<X> query2 = ParseQuery.getQuery(mType).whereEqualTo("owner", user);
    ParseQuery<X> query3 = ParseQuery.getQuery(mType).whereContainedIn("objectId", getIds(false));
    return ParseQuery.or(query1, query2, query3);
  }

//  @CallSuper
//  @Override
//  void greetObject(X po, Collection<ParseObject> list)
//  {
//    super.greetObject(po, list);
//    XUser user = po.getOwner();
//    XUser currentUser = XUser.getCurrentUser();
//    Collection<String> members = po.getMemberIds();
//    if (members == null)
//    {
//      ParseRelation<XUser> relation = po.getRelation("members");
//      ParseQuery<XUser> query = relation.getQuery();
//      List<XUser> newMembers = query.find();
//      List<String> parts =
//          Collect.asList(po.getClassName(), "members", "_User", "objectId", po.getObjectId());
//      Rel rel = mRelationWatcher.getRel();
//      HashSet<String> ids = new HashSet<>(Util.transform(newMembers, ParseObject::getObjectId));
//      rel.setRelatedIds(ids);
//      members = po.getMemberIds();
//      assert members != null;
//    }
//    if (user == currentUser || user.hasSameId(currentUser))
//    {
//      po.setStatus(CellStatus.OWNER);
//    } else if (members.contains(currentUser.getObjectId()))
//    {
//      po.setStatus(CellStatus.JOINED);
//    } else
//    {
//      po.setStatus(CellStatus.NOT_JOINED);
//    }
//    for (String id : members)
//    {
//      list.add(MultiCacheObject.getObject(id));
//    }
//  }

  @Override
  public synchronized void update(MyObservable o, Object arg) {
    super.update(o, arg);
  }

  @Override
  public synchronized void loadFromNet(Collection<ParseObject> extras)
  {
    super.loadFromNet(extras);
  }
}
