package cell411.logic;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import cell411.ILiveQueryService;
import cell411.utils.Reflect;
import cell411.utils.ThreadUtil;
import cell411.utils.Util;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import com.safearx.cell411.Cell411;

import javax.annotation.Nonnull;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class LiveQueryConnection
  implements ServiceConnection,
             Callable<Throwable>,
             Runnable
{
  private final Cell411 mCell411;
  private final Intent mIntent;
  private final Context mContext;
  private ILiveQueryService mService;
  private final XTAG TAG = new XTAG();

  public LiveQueryConnection(@Nonnull Cell411 cell411, @Nonnull Context context)
  {
    XLog.i(TAG,"cell411=%s",cell411);
    assert cell411 != null;
    assert context!=null;
    mCell411 = cell411;
    mContext = context;
    mIntent = new Intent(context,LiveQueryService.class);
  }

  @Override
  public synchronized void onServiceConnected(@Nonnull final ComponentName name,
                                              @Nonnull final IBinder service)
  {
    mService = ILiveQueryService.Stub.asInterface(service);
    ThreadUtil.notify(this, true);
  }

  @Override
  public synchronized void onServiceDisconnected(final ComponentName name)
  {
    ThreadUtil.notify(this, true);
  }

  @Override
  public synchronized void onBindingDied(final ComponentName name)
  {
    ThreadUtil.notify(this, true);
  }

  @Override
  public synchronized void onNullBinding(final ComponentName name)
  {
    ThreadUtil.notify(this, true);
  }

  public synchronized Throwable call()
  {
    assert !mCell411.getMainLooper().isCurrentThread();
    try {
      XLog.i(TAG,"call()");
      ComponentName name = mCell411.startForegroundService(mIntent);
      Reflect.announce(name);
      mCell411.getExec().schedule((Runnable) this, 100, TimeUnit.MILLISECONDS);
      ThreadUtil.waitUntil(this, () -> mService != null, 100);
      assert LiveQueryService.opt() != null;
      ThreadUtil.waitUntil(this, LiveQueryService.req()::isReady);
      return null;
    } catch (Throwable t) {
      Util.throwThreadDeath(t);
      return t;
    }
  }

  public void run()
  {
    mCell411.bindService(mIntent, this, Context.BIND_ABOVE_CLIENT);
  }
}
