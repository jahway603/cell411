package cell411.logic;

import cell411.logic.rel.Rel;
import cell411.parse.XUser;
import com.parse.ParseQuery;
import com.parse.model.ParseObject;

import java.util.*;

public class UserWatcher extends Watcher<XUser> {
  Rel mFriends = mRelationWatcher.getFriends();

  public UserWatcher(final RelationWatcher relationWatcher) {
    super(relationWatcher, "users", XUser.class);
  }

  @Override
  protected Set<String> getIds(final boolean dirty) {
    return super.getIds(dirty);
  }

  @Override
  public ParseQuery<XUser> directQuery() {
    XUser user = XUser.getCurrentUser();
    return user.queryFriends();
  }

  @Override
  public synchronized ParseQuery<XUser> createQuery(Set<String> dirty)
  {
    ParseQuery<XUser> query = super.createQuery(dirty);
    query.include("avatarURL");
    query.include("thumbNailURL");
    return query;
  }

  @Override
  public synchronized Map<String, XUser> getData() {
    return super.getData();
  }

  public boolean isFriend(XUser user) {
    return isFriend(user.getObjectId());
  }

  //  public void updateFromNet() {
  //    TreeSet<String> oldKeys = new TreeSet<>(mData.keySet());
  //    loadFromNet();
  //    TreeSet<String> newKeys = new TreeSet<>(mData.keySet());
  //    newKeys.removeAll(oldKeys);
  //    if (newKeys.isEmpty())
  //      return;
  //    UserWatcher uw = this;
  //    onUI(() -> {
  //      HashSet<LQListener<XUser>> listeners = copyListeners();
  //      for (LQListener<XUser> listener : listeners) {
  //        listener.done(uw, null);
  //      }
  //    });
  //  }

  @Override
  synchronized void greetObject(XUser po, Collection<ParseObject> list) {
    super.greetObject(po, list);

  }

  @Override
  public void loadFromCache() {
    super.loadFromCache();
  }

  @Override
  public void loadFromNet(Collection<ParseObject> objs) {
    //    Reflect.announce();
    super.loadFromNet(objs);

  }

  public boolean isFriend(String id) {
    return mFriends.contains(id);
  }

}
