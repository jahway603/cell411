package cell411.logic;

import cell411.json.JSONArray;
import cell411.json.JSONObject;
import cell411.logic.rel.AggregateRel;
import cell411.logic.rel.Rel;
import cell411.logic.rel.Rel.Key;
import cell411.logic.rel.RelationHolder;
import cell411.parse.XBaseCell;
import cell411.parse.XPublicCell;
import cell411.parse.XRelationshipUpdate;
import cell411.parse.XUser;
import cell411.utils.Collect;
import cell411.utils.TimeLog;
import cell411.utils.Util;
import com.parse.ParseCloud;
import com.parse.ParseQuery;
import com.parse.model.ObjectEvent;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;
import com.parse.model.ParseRelation;
import com.parse.operation.ParseOperationSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static cell411.libcell.ConfigDepot.getExecutor;

// The data is pretty minimal, but if we come up without
// net, we need it.
@SuppressWarnings("unused")
public class RelationWatcher extends CacheObject
  implements ObjectEventsCallback<XRelationshipUpdate>,
             XBaseCell.MemberLookup
{
  static final Set<String> smEmptyIdSet = new TreeSet<>();
  private final Map<String, Date> mDates = new TreeMap<>();
  private final RelationHolder mHolder = new RelationHolder();
  String mUserId;
  private Map<String, Integer> mAllBlocks;
  private JSONArray mJsonArray;
  private final List<Key> mToSave = new ArrayList<>();

  public RelationWatcher(MultiCacheObject multiCacheObject) {
    super(multiCacheObject.mService,"RelationshipUpdate");
  }

  public ParseQuery<XRelationshipUpdate> createQuery(boolean dirty) {
    ParseQuery<XRelationshipUpdate> q1 = XRelationshipUpdate.q();
    if (dirty) {
      return q1.whereContainedIn("objectId", smEmptyIdSet);
    }
    XUser currentUser = XUser.getCurrentUser();
    ParseQuery<XRelationshipUpdate> q2 = XRelationshipUpdate.q();
    q1.whereEqualTo("owningId", currentUser.getObjectId());
    q2.whereEqualTo("relatedId", currentUser.getObjectId());
    return ParseQuery.or(q1, q2);
  }

  String makeLine() {
    char[] dashes = new char[50];
    Arrays.fill(dashes, '-');
    return new String(dashes);
  }

  @Override
  public void report(TimeLog ps) {
    String line = makeLine();

    ps.pl(line);
    for (Rel rel : Collect.asList(mHolder.mFriends)) {
      ps.pl(line);
      rel.dump(ps);
      ps.pl().pl().pl();
    }
    ps.pl(line);
    ps.pl(line);
  }

  @Override
  public void createSubscription() {
    ParseQuery<XRelationshipUpdate> query = createQuery(false);
    mService.subscribe(query, this);
  }

  public Map<String,String> getObjects(Map<?,?> input) {
    Map<String,String> res = new HashMap<>();
    for(Object key : input.keySet()){
      res.put(String.valueOf(key), String.valueOf(input.get(key)));
    }
    return res;
  }
  @SuppressWarnings("unchecked")
  @Override
  public void loadFromNet(Collection<ParseObject> temp)
  {
    Map<String, String> params = new HashMap<>();
    Object result = ParseCloud.run("relations", params);
    assert result instanceof List;
    List<?> list = (List<?>) result;
    Iterator<?> resItr = ((List<?>) result).iterator();
    Map<String,Date> dates = new HashMap<>();
    if(resItr.hasNext()) {
      Map<String,Object> rawDates = (Map<String, Object>) resItr.next();
      for(String key : rawDates.keySet()) {
        Number date = ((Number) rawDates.get(key));
        if(date==null)
          continue;
        dates.put(key,new Date(date.longValue()));
      }
    }
    ArrayList<Rel> rels = new ArrayList<>();
    ArrayList<Key> toSave = new ArrayList<>();
    while(resItr.hasNext()){
      List<Object> item = (List<Object>) resItr.next();
      Iterator<Object> iterator = item.iterator();
      String[] keyStrs = new String[5];
      for(int i=0;i<keyStrs.length;i++){
        keyStrs[i]=String.valueOf(iterator.next());
      }
      if(keyStrs[4].equals(curId()))
        keyStrs[4]="*";
      int elements = Integer.parseInt(String.valueOf(iterator.next()));
      Rel.Key key = Rel.Key.create(keyStrs);
      List<String> oids = Util.transform(iterator, String::valueOf);
      toSave.add(key);
      if(mHolder.getRel(key).getKey()==null) {
        System.out.println(mHolder.getRel(key));
        assert mHolder.getRel(key).getKey() != null;
      }
      mHolder.getRel(key).setRelatedIds(oids);
    }
    mToSave.clear();
    mToSave.addAll(toSave);
    mDates.clear();
    mDates.putAll(dates);
  }

  private Map<Rel.Key,Rel> handleRelations(Map<String, Collection<?>> relations)
  {
    Map<Rel.Key,Rel> res = new HashMap<>();
    Iterator<String> keys = relations.keySet().iterator();
    String key;
    while(keys.hasNext()){
      key=keys.next();
      Collection<?> coll = relations.get(key);
      //noinspection DataFlowIssue
      assert (coll instanceof Collection);
      HashSet<String> ids = new HashSet<>();
      for(Object co : coll){
        ids.add(String.valueOf(co));
      }
      Rel.Key relKey = new Rel.Key(key);
      Rel rel = getRel(relKey);
      System.out.println("before: "+rel.getRelatedIds());
      rel.setRelatedIds(ids);
      System.out.println("after: "+rel.getRelatedIds());
      res.put(relKey,rel);
    }
    return res;
  }

  private void merge(ParseObject obj, JSONObject jsonObject)
  {
    ParseOperationSet set = obj.operationSetQueue.getFirst();
    for(String key : obj.keySet()){
      Object val=obj.get(key);
      if(eq(val,jsonObject.opt(key))){
        continue;
      }
      obj.put(key,val);
    }
  }

  static boolean eq(Object val, Object opt)
  {
    if (val==opt)
      return true;
    if(val==null || opt==null)
      return false;
    return val.equals(opt);
  }

//  public void loadFromXet(Collection<ParseObject> objects) {
//    Map<String, String> params = new HashMap<>();
//    ArrayList<Object> result = ParseCloud.run("relations", params);
//    assert result != null;
//    acceptData(result);
//    saveToCache();
//  }

  @Override
  public void saveToCache(JSONObject json) {
    JSONArray data = new JSONArray();
    JSONObject dates = new JSONObject(mDates);
    for (String key : mDates.keySet()) {
      Date val = mDates.get(key);
      if (val != null) {
        dates.put(key, val.getTime());
      }
    }
    data.put(dates);
    for (Key key : mToSave) {
      Rel rel = mHolder.getRel(key);
      if(rel instanceof AggregateRel)
        continue;
      JSONArray relArray = rel.toJSON();
      if(relArray==null){
        System.out.println("Key: "+key);
        System.out.println("   : null");
      } else {
        data.put(relArray);
      }
    }
    jsonToFile(data);
  }

  @Override
  public void clear() {
    mDates.clear();
    mHolder.clear();
  }

  private void onCreate(XRelationshipUpdate update) {
//    synchronized (mRelMap)
//    {
//      String op = update.getString("op");
//
//      if (op == null)
//        throw new RuntimeException("op is null");
//
////      owningField  relatedClass  relatedField  owningId
//      key = Key.create(
//        update.getString("owningClass"),
//        update.getString("relatedClass"),
//        update.getString("owningField"),
//        update.getString("relatedField"),
//        update.getString("owningId")
//      );
//      String relatedId = update.getString("relatedId");
//      if (mRelMap.containsKey(key.toString()))
//      {
//        Rel rel = getRel(key);
//        if (rel != null)
//        {
//          if (op.equals("AddRelation"))
//          {
//            rel.add(relatedId);
//          } else if (op.equals("RemoveRelation"))
//          {
//            rel.add(relatedId);
//          } else
//          {
//            showToast("Idk what " + op + " means");
//          }
//        }
//      } else {
//        System.out.println(key);
//      }
//    }
  }

  synchronized void acceptData(List<Object> result) {
    if (result == null || result.size() == 0) {
      return;
    }
    Object rawDates = result.remove(0);
    Map<String,Date> objDates=decodeDates(rawDates);
    for (Object o : result) {
      // This writes directly into mHolder's rels.
      decodeRel(o);
    }
    synchronized (this) {
      mDates.putAll(objDates);
    }
  }

  @Nullable
  private Rel decodeRel(Object o)
  {
    if(o instanceof Iterable<?>) {
      Iterable<?> list = (Iterable<?>) o;
      Iterator<?> itr = list.iterator();
      if(!itr.hasNext())
        return null;
      String[] strs = new String[5];
      for (int i = 0; i < 5; i++)
        strs[i] = String.valueOf(itr.next());
      Integer count = (Integer) itr.next();
      HashSet<String> ids = new HashSet<>();
      for (int i = 0; i < count; i++)
        ids.add(String.valueOf(itr.next()));
      Rel rel = mHolder.getRel(strs);
      rel.setRelatedIds(ids);
      return rel;
    } else {
      throw new RuntimeException("Expected Iterable");
    }
  }

  private Map<String, Date> decodeDates(Object remove)
  {
//    for(Object oKey : dates.keySet()) {
//      String key = String.valueOf(oKey);
//      Long val = (Long) dates.get(key);
//      assert val!=null;
//      objDates.put(key,new Date(val));
//    }
    Map<String,Date> res = new HashMap<>();
    if(remove instanceof Map) {
      Map<?,?> rawDates = (java.util.Map<?, ?>) remove;
      for(Object id : rawDates.keySet()) {
        String key = String.valueOf(id);
        Long val = (Long) rawDates.get(id);
        if(val!=null)
          res.put(key,new Date(val));
      }
    } else if ( remove instanceof JSONObject ) {
      JSONObject jsonDates = (JSONObject) remove;
      for(String id : jsonDates.keySet()) {
        long num = jsonDates.getLong(id);
        res.put(id,new Date(num));
      }
    } else {
      throw new IllegalArgumentException("Expected map or json");
    }
    return res;
  }

  public Date getDate(String objectId) {
    return mDates.get(objectId);
  }

  public void setDate(String objectId, Date date) {
    mDates.put(objectId, date);
  }

  public Rel getFriends() {
    return mHolder.mFriends;
  }

  public void removeCellMember(XPublicCell cell, XUser user) {
    getExecutor().execute(() ->
    {
      ParseRelation<XUser> members = cell.getRelation("members");
      members.remove(user);
    });
  }

  @Override
  public Collection<String> getMembers(XBaseCell cell) {
    Rel rel = getMemberRel(cell);
    return rel.getRelatedIds();
  }

  @Override
  public boolean setMembers(XBaseCell cell, Collection<String> ids) {
    Rel rel = getMemberRel(cell);
    return rel.setRelatedIds(ids);
  }

  public Rel getMemberRel(XBaseCell cell) {
    return mHolder.getCellMembers(cell);
  }


  @Override
  public void loadFromCache() {
    if(getCacheFile().delete())
      System.out.println("Killed: "+getCacheFile());
    if(!getCacheFile().exists())
      return;
    try {
      String text = fileToString();
      text=text.trim();
      JSONArray json;
      if(text.startsWith("[") && text.endsWith("]")){
        json=new JSONArray(text);
      } else if ( text.startsWith("{") && text.endsWith("}")) {
        JSONObject object = new JSONObject(text);
        json=object.getJSONArray("__wrapped");
      } else {
        throw new IllegalArgumentException("Bad json: "+text);
      }
      List<Object> list = new ArrayList<>();
      for(Object object : json) {
        list.add(object);
      }
      acceptData(list);
    } catch ( Exception e )  {
      System.out.println(""+e);
    }
  }

  private JSONArray fileToJSONArray()
  {
    JSONObject wrapper = fileToJSONObject();
    return wrapper.getJSONArray("__wrapped");
  }

  private JSONObject fileToJSONObject()
  {
    return new JSONObject(fileToString());
  }

  public Rel getOwnedCells(String className) {
    if (className.equals("PrivateCell")) {
      return mHolder.mPriCellsOwned;
    } else if (className.equals("PublicKey")) {
      return mHolder.mPriCellsOwned;
    } else {
      throw new RuntimeException("Expected PublicCell or PrivateCell");
    }
  }

  @Nonnull
  public Rel getJoinedCells(String className) {
    if (className.equals("PublicCell")) {
      return mHolder.mPubCellsJoined;
    } else if (className.equals("PrivateKey")) {
      return mHolder.mPriCellsOwned;
    } else {
      throw new RuntimeException("Expected PublicCell or PrivateCell");
    }
  }

  public String curId() {
    if (mUserId == null) {
      XUser user = XUser.getCurrentUser();
      mUserId = user.getObjectId();
    }
    return mUserId;
  }

  public Rel getBlocksMe() {
    return mHolder.mBlocksMe;
  }

  public Rel getMyBlocks() {
    return mHolder.mMyBlocks;
  }

  public Rel getReceivedRequests() {
    return mHolder.mRequestsSentTo;
  }

  public Rel getOwnedRequests() {
    return mHolder.mRequestsOwned;
  }

  @Override
  public void onEvents(final ObjectEvent<XRelationshipUpdate> event) {
    switch (event.getEventId()) {
      case CREATE:
        onCreate(event.getObject());
      case ENTER:
      case UPDATE:
      case LEAVE:
      case DELETE:
        break;
    }
  }


  public Rel getAudienceAlerts() {
    return mHolder.mAlertsSentTo;
  }

  public Rel getOwnedAlerts() {
    return mHolder.mAlertsOwned;
  }

  public Rel getCurrentUserRel() {
    return mHolder.mCurrentUser;
  }

  public Rel getCounterParties() {
    return mHolder.mCounterParties;
  }

  public Rel getAllUsers() {
    return mHolder.mAllUsers;
  }

  public Rel getRel(Key key) {
    return mHolder.getRel(key);
  }
  public Rel getRel(String key) {
    return getRel(new Rel.Key(key));
  }

//  public AggregateRel getAggregateRel(String key, Class<? extends ParseObject> type)
//  {
//    return (AggregateRel) mRelMap.mMap.computeIfAbsent(key,
//        (k) -> new AggregateRel(key));
//  }

//  public Rel getCounterParties() {
//    return getRel(
//      Collect.asList("_User", "counterParties"));
//  }
}

