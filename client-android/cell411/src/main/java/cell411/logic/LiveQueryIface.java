package cell411.logic;

import com.parse.ParseQuery;
import com.parse.livequery.ParseLiveQueryClient;
import com.parse.model.ObjectEventsCallback;
import com.parse.model.ParseObject;

public interface LiveQueryIface
{
  <T extends ParseObject>
  ParseObjectSubscription<T> subscribe(
    ParseQuery<T> query,
    ObjectEventsCallback<T> callback
  );

  default
  <T extends ParseObject>
  ParseObjectSubscription<T> subscribe(ParseQuery<T> query) {
    return subscribe(query,null);
  }

  ParseLiveQueryClient getClient();

  <T extends ParseObject> void unsubscribe(ParseQuery<T> query);

  RelationWatcher getRelationWatcher();

  void onUI(Runnable runnable);
  void onUI(Runnable runnable,long delay);

  MultiCacheObject getCache(boolean b);
}
