package cell411.logic;

import com.parse.ParseQuery;

import cell411.parse.XAlert;
import cell411.parse.XUser;
import cell411.utils.XTAG;
import com.parse.model.ParseObject;

import javax.annotation.Nonnull;
import java.util.Collection;

public class AlertWatcher extends Watcher<XAlert> {
  private static final XTAG TAG = new XTAG();

  public AlertWatcher(@Nonnull RelationWatcher relationWatcher) {
    super(relationWatcher, "Alerts", XAlert.class);
  }

  @Override

  public ParseQuery<XAlert> directQuery() {
    ParseQuery<XAlert> query1 = ParseQuery.getQuery(mType);
    XUser user = XUser.getCurrentUser();
    query1.whereEqualTo("owner", user);
    ParseQuery<XAlert> query2 = ParseQuery.getQuery(mType);
    query2.whereEqualTo("audience", user);
    ParseQuery<XAlert> query3 = ParseQuery.getQuery(mType);
    query3.whereEqualTo("audience", '"' + user.getObjectId() + '"');
    ParseQuery<XAlert> query4 = ParseQuery.getQuery(mType);
    query4.whereEqualTo("audience", user.getObjectId());
    ParseQuery<XAlert> query5 = ParseQuery.getQuery(mType);
    query5.whereEqualTo("owner", user.getObjectId());
    return ParseQuery.or(query1, query2, query3, query4, query5);
  }

  @Override
  public synchronized void loadFromNet(Collection<ParseObject> extras)
  {
    super.loadFromNet(extras);
  }

}
