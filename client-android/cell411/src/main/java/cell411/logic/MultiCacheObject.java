package cell411.logic;

import cell411.json.JSONObject;
import cell411.libcell.ConfigDepot;
import cell411.parse.XPrivateCell;
import cell411.parse.XPublicCell;
import cell411.utils.ObservableValue;
import cell411.utils.PrintString;
import cell411.utils.Reflect;
import cell411.utils.TimeLog;
import cell411.utils.Util;
import cell411.utils.ValueObserver;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import cell411.utils.concurrant.STPExecutorPlus;
import com.parse.Parse;
import com.parse.model.ParseObject;
import com.parse.model.ParseUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.Callable;
import java.util.function.Function;

public class MultiCacheObject
  extends CacheObject
  implements Callable<Throwable>,
             Runnable
{
  private final static XTAG TAG = new XTAG();
  static int smId = 0;
  static SetList<CacheObject> mQueue = new SetList<>();
  final ArrayList<CacheObject> mAllCacheObjects = new ArrayList<>();
  private final RelationWatcher mRelationWatcher = new RelationWatcher(this);
  //  private final ArrayList<CacheObject> mQueue = new ArrayList<>();
  private final ClassLoader mLoader = getClass().getClassLoader();
  private final CurrentUserObserver mCurrentUserObserver;
  private final CellWatcher<XPrivateCell> mPrivateCellWatcher =
    new CellWatcher<>(mRelationWatcher, XPrivateCell.class);
  private final RequestWatcher mRequestWatcher =
    new RequestWatcher(mRelationWatcher);
  private final UserWatcher mUserWatcher = new UserWatcher(mRelationWatcher);
  private final AlertWatcher mAlertWatcher = new AlertWatcher(mRelationWatcher);
  private final CellWatcher<XPublicCell> mPublicCellWatcher =
    new CellWatcher<>(mRelationWatcher, XPublicCell.class);
  private final ChatRoomWatcher mChatRoomWatcher =
    new ChatRoomWatcher(mRelationWatcher);
  int mId = smId++;
  String mName = "MultiCache: " + mId;
  ObservableValue<Boolean> mReady = new ObservableValue<>(Boolean.class, false);

  public MultiCacheObject(@Nonnull LiveQueryIface service)
  {
    super(service, "MultiCacheObject");
    mCurrentUserObserver = new CurrentUserObserver();
    Parse.addCurrentUserObserver(mCurrentUserObserver);
    ParseUser user = ParseUser.getCurrentUser();
    putObject(user);
    if (user != null)
      mCurrentUserObserver.onChange(user, null);
  }

  public static <X extends ParseObject> void queueNetLoad(Watcher<X> xWatcher)
  {
    throw new RuntimeException();
  }

  @Nonnull
  @Override
  public File getBackupFile()
  {
    return new File(getCacheFile().toString() + ".bak");
  }

  @Override
  public boolean cacheExists()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return toString();
  }

  @Override
  public File getCacheFile()
  {
    return new File("");
  }

  @Override
  public void clear()
  {
    for (CacheObject co : getAllCacheObjects()) {
      co.clear();
    }
    mQueue.clear();
  }

  ArrayList<CacheObject> getAllCacheObjects()
  {
    synchronized (this) {
      if (mAllCacheObjects.isEmpty()) {
        ArrayList<CacheObject> cacheObjects = new ArrayList<>();
        cacheObjects.add(getRelationWatcher());
        cacheObjects.add(getRequestWatcher());
        cacheObjects.add(getPublicCellWatcher());
        cacheObjects.add(getPrivateCellWatcher());
        cacheObjects.add(getAlertWatcher());
        cacheObjects.add(getChatRoomWatcher());
        cacheObjects.add(getUserWatcher());
        mAllCacheObjects.addAll(cacheObjects);
        return cacheObjects;
      } else {
        return new ArrayList<>(mAllCacheObjects);
      }
    }
  }

  public synchronized RelationWatcher getRelationWatcher()
  {
    return mRelationWatcher;
  }

  public synchronized RequestWatcher getRequestWatcher()
  {
    return mRequestWatcher;
  }

  public synchronized CellWatcher<XPublicCell> getPublicCellWatcher()
  {
    //    if (mPublicCellWatcher == null) {
    //      ThreadUtil.waitUntil(this, () -> ParseUser.getCurrentUser() !=
    //      null, 500);
    //    }
    return mPublicCellWatcher;
  }

  public synchronized CellWatcher<XPrivateCell> getPrivateCellWatcher()
  {
    return mPrivateCellWatcher;
  }

  public synchronized AlertWatcher getAlertWatcher()
  {
    return mAlertWatcher;
  }

  public synchronized ChatRoomWatcher getChatRoomWatcher()
  {
    return mChatRoomWatcher;
  }

  public synchronized UserWatcher getUserWatcher()
  {
    return mUserWatcher;
  }

  public void setInitialLoadComplete()
  {
    Reflect.announce(mName);
    for (CacheObject co : mQueue) {
      co.setInitialLoadComplete();
    }
  }

  @Override
  public void loadFromNet(Collection<ParseObject> objs)
  {
    Reflect.announce(mName);
    for (CacheObject co : mQueue) {
      co.loadFromNet(objs);
    }
  }

  @Override
  public void loadFromCache()
  {
    Reflect.announce(mName);
    for (CacheObject co : mQueue) {
      co.loadFromCache();
    }
  }

  @Override
  public void saveToCache(JSONObject json)
  {
    Reflect.announce(mName);
    for (CacheObject co : mQueue) {
      co.saveToCache(json);
    }
  }

  public void report(TimeLog ps)
  {
    Reflect.announce(mName);
    ps.pl("Starting report");
    RelationWatcher relationWatcher = getRelationWatcher();
    if (relationWatcher != null) {
      relationWatcher.report(ps);
    }
    for (CacheObject co : getAllCacheObjects()) {
      co.report(ps);
    }
    ps.pl("Report complete");
  }

  @Override
  public void createSubscription()
  {
    Reflect.announce(mName);
    for (CacheObject co : mQueue) {
      co.createSubscription();
    }
  }

  //  void loadObjects(Collection<ParseObject> objects)
  //  {
  //    if (objects.size() > 0) {
  //      @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
  //      FactoryMap<String, HashSet<ParseObject>> factoryMap =
  //        new FactoryMap<>(this::makeHashSet);
  //      for (ParseObject obj : objects) {
  //        if (obj != null) {
  //          factoryMap.get(obj.getClassName()).add(obj);
  //        }
  //      }
  //      for (String className : factoryMap.keySet()) {
  //        HashSet<ParseObject> objs = factoryMap.get(className);
  //        ArrayList<ParseObject> list = new ArrayList<>(objs);
  //        list.removeIf(ParseObject::isComplete);
  //        if (list.size() == 0) {
  //          continue;
  //        }
  //        ParseSyncUtils.fetchAll(list);
  //      }
  //    }
  //  }

  public MultiCacheObject getMultiCacheObject()
  {
    return CacheObject.get();
  }

//  public synchronized Throwable call1()
//  {
//    try {
//      if (!Parse.isInitialized() || ParseUser.getCurrentUser() == null) {
//        return null;
//      }
//      MultiCacheObject cache = mService.getCache(false);
//      if (cache != this) {
//        return null;
//      }
//      mQueue.clear();
//      for (CacheObject co : getAllCacheObjects()) {
//        if (co != mRelationWatcher)
//          mQueue.add(co);
//      }
//      processCacheObject(mRelationWatcher);
//      processCacheObject(this);
//      setReady(true);
//      return null;
//    } catch (Throwable t) {
//      return t;
//    }
//  }

  public void setReady(boolean value)
  {
    mReady.set(value);
  }

  public synchronized Throwable call()
  {
    try {
      ArrayList<ParseObject> objs = new ArrayList<>();
      mQueue.addAll(getAllCacheObjects());
      loadFromCache();
      PrintString ps = new PrintString();
      TimeLog log = new TimeLog(ps);
      XLog.i(TAG, "%s\n", objs);
      report(log);
      log.flush();
      XLog.i(TAG, "%s\n", ps.reset());
      return null;
    } catch (Throwable t) {
      return t;
    }
  }

  private HashSet<ParseObject> makeHashSet(final String s)
  {
    return new HashSet<>();
  }

  public void queue()
  {
    getExecutor().execute(this);
  }

  public STPExecutorPlus getExecutor()
  {
    return ConfigDepot.getExecutor();
  }

  @Override
  public void run()
  {
    try {
      XLog.i(TAG, "%s\n", Thread.currentThread());
      XLog.i(TAG, "%s\n", this);
      Throwable ex = getExecutor().submit((Callable<Throwable>) this).get();
      if (ex == null) {
        System.out.println("All is well!");
      } else {
        ex.printStackTrace();
      }
    } catch (Throwable e) {
      throw Util.rethrow(e);
    }
  }

  static class SetList<X>
    extends AbstractList<X>
  {
    ArrayList<X> mData = new ArrayList<>();

    @Override
    public X get(int index)
    {
      return mData.get(index);
    }

    @Override
    public synchronized void add(int index, X element)
    {
      if (!mData.contains(element))
        mData.add(index, element);
    }

    @Override
    public int size()
    {
      return mData.size();
    }
  }

  static class IntegerFactory
    implements Function<String, int[]>
  {
    @Override
    public int[] apply(String s)
    {
      return new int[]{0};
    }
  }

  class CurrentUserObserver
    implements ValueObserver<ParseUser>
  {

    @Override
    public void onChange(@Nullable ParseUser newValue,
                         @Nullable ParseUser oldValue)
    {
      if (newValue == null) {
        MultiCacheObject i = getMultiCacheObject();
        if (i != null)
          i.clear();
      } else {
        queue();
      }
    }
  }
}
