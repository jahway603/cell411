package cell411.logic;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;
import static androidx.core.app.NotificationManagerCompat.IMPORTANCE_DEFAULT;
import static androidx.core.app.NotificationManagerCompat.IMPORTANCE_MAX;
import static com.parse.livequery.LiveQueryEventId.CREATE;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;

import androidx.core.app.NotificationChannelCompat;
import androidx.core.app.NotificationChannelCompat.Builder;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.graphics.drawable.IconCompat;

import com.parse.model.ObjectEvent;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseObject;
import com.safearx.cell411.Cell411Activity;
import com.safearx.cell411.R;

import java.util.Date;
import java.util.HashMap;
import java.util.StringJoiner;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

import cell411.android.RingtoneData;
import cell411.enums.ProblemType;
import cell411.parse.XAlert;
import cell411.parse.XChatMsg;
import cell411.parse.XPublicCell;
import cell411.parse.XRequest;
import cell411.parse.XUser;
import cell411.services.LocationService;
import cell411.ui.base.BaseActivity;
import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseContext;
import cell411.utils.ImageUtils;
import cell411.utils.MethodRunnable;
import cell411.utils.PrintString;
import cell411.utils.Util;

public class NotificationAgent implements BaseContext {
  private static final String REQUEST_CHANNEL = "bREQUEST_CHANNEL";
  private static final String ALERT_CHANNEL = "bALERT_CHANNEL";
  private static final String CHAT_CHANNEL = "bCHAT_CHANNEL";

  private final HashMap<String, ChData> mChData = new HashMap<>();
  private final NotificationManagerCompat mManager =
    NotificationManagerCompat.from(BaseApp.req());
  private final LQListener<XRequest> mRequestListener = new LQListener<XRequest>() {
    @Override
    public void onEvents(ObjectEvent<XRequest> event) {
      if (event.getEventId() == CREATE && !event.getObject().isSelfAlert()) {
        sendRequestNotification(event.getObject());
      }
    }

    public void change() {

    }
  };
  private final LQListener<XPublicCell> mPublicCellListener = new LQListener<XPublicCell>() {
    @Override
    public void change() {

    }

    @Override
    public void onEvents(ObjectEvent<XPublicCell> event) {
      LQListener.super.onEvents(event);
      if (event.getObject().ownedByCurrent()) {
        return;
      }
      LocationService ls = ls();
      if (ls != null) {
        ParseGeoPoint currentLocation = ls.getParseGeoPoint();
        ParseGeoPoint cellLocation = event.getObject().getLocation();
        if (currentLocation.distanceInMilesTo(cellLocation) >= 50) {
          return;
        }
      }
      sendPublicCellNotification(event.getObject());
    }

  };
  private final LiveQueryService mService;
  //Keys not yet used.
  //builder.setConversationId(null);
  //builder.setGroup(null);
  //builder.setLightColor(null);
  //builder.setLightsEnabled(null);
  //builder.setVibrationEnabled(null);
  //builder.setVibrationPattern(null);
  LQListener<XAlert> mAlertListener = new LQListener<XAlert>() {
    public void onEvents(ObjectEvent<XAlert> event) {
      sendAlertNotification(event.getObject());
    }

    public void change() {
    }
  };
  MethodRunnable mSubscribeToQueries =
    MethodRunnable.forVirtual(this, "subscribeToQueries");
  private AudioAttributes mAudioAttributes;

  public NotificationAgent(LiveQueryService service) {
    mService = service;
    createChannels();
  }

  public void sendLocationNotification(ParseGeoPoint geoPoint) {
    BaseApp.getExecutor().execute(new Notifier(geoPoint));
  }

  public void sendRequestNotification(XRequest request) {
    BaseApp.getExecutor().execute(new Notifier(request));
  }

  public void sendAlertNotification(XAlert alert) {
    BaseApp.getExecutor().execute(new Notifier(alert));
  }

  private ChData getChData(final ParseObject object) {
    String className = object.getClassName();
    switch (className) {
      case "Request":
        return mChData.get(REQUEST_CHANNEL);
      case "Alert":
        return mChData.get(ALERT_CHANNEL);
      default:
        return mChData.get(CHAT_CHANNEL);
    }
  }

  void createChannels() {
    TreeMap<String, RingtoneData> tones = RingtoneData.getRingtoneData();
    ChData data;
    RingtoneData toneData;

    // ALERT_CHANNEL
    data = new ChData(ALERT_CHANNEL);
    toneData = tones.get("ALERT");
    if (toneData != null) {
      data.mRingtone = toneData.getUri();
    }
    NotificationChannelCompat.Builder builder;
    builder = new Builder(ALERT_CHANNEL, IMPORTANCE_MAX);
    builder.setName("Emergency Alerts");
    builder.setDescription("The channel for emergencies");
    builder.setShowBadge(true);
    builder.setSound(data.mRingtone, audioAttributes());
    builder.setVibrationEnabled(true);
    data.mChannel = builder.build();
    mManager.createNotificationChannel(data.mChannel);

    // REQUEST_CHANNEL
    data = new ChData(REQUEST_CHANNEL);
    //    UrlUtils.toUri(tones.get("REQUEST.uri"));
    toneData = tones.get("REQUEST");
    if (toneData != null) {
      data.mRingtone = toneData.getUri();
    }

    builder = new Builder(REQUEST_CHANNEL, IMPORTANCE_DEFAULT);
    builder.setName("Requests");
    builder.setDescription("A channel for friend and cell join requests");
    builder.setShowBadge(true);
    builder.setSound(data.mRingtone, audioAttributes());
    data.mChannel = builder.build();
    mManager.createNotificationChannel(data.mChannel);

    // CHAT_CHANNEL
    data = new ChData(CHAT_CHANNEL);
    toneData = tones.get("CHAT");
    if (toneData != null) {
      data.mRingtone = toneData.getUri();
    }
    builder = new Builder(CHAT_CHANNEL, IMPORTANCE_DEFAULT);
    builder.setName("Chat Messages");
    builder.setDescription("The channel for chats");
    builder.setShowBadge(true);
    builder.setSound(data.mRingtone, audioAttributes());
    builder.setVibrationEnabled(true);
    data.mChannel = builder.build();

    mManager.createNotificationChannel(data.mChannel);
  }

  public void subscribeToQueries() {
    LiveQueryService lqs = mService;
    if (lqs == null) {
      getExec().schedule(mSubscribeToQueries, 500, TimeUnit.MILLISECONDS);
      return;
    }
    AlertWatcher alertWatcher = lqs.getAlertWatcher();
    RequestWatcher requestWatcher = lqs.getRequestWatcher();
    CellWatcher<XPublicCell> cellWatcher = lqs.getPublicCellWatcher();
    if (alertWatcher == null || requestWatcher == null || cellWatcher == null) {
      getExec().schedule(mSubscribeToQueries, 500, TimeUnit.MILLISECONDS);
      return;
    }

  }

  public void restore() {
    ChData data;
    TreeMap<String, RingtoneData> tones = RingtoneData.getRingtoneData();
    RingtoneData tone;

    data = mChData.get(ALERT_CHANNEL);
    assert data != null;
    tone = tones.get("ALERT");
    if (tone != null) {
      data.mRingtone = tone.getUri();
    }

    data = mChData.get(REQUEST_CHANNEL);
    assert data != null;
    tone = tones.get("REQUEST");
    if (tone != null) {
      data.mRingtone = tone.getUri();
    }
  }

  AudioAttributes audioAttributes() {
    if (mAudioAttributes == null) {
      mAudioAttributes =
        new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
          .setUsage(AudioAttributes.USAGE_ALARM).build();
    }
    return mAudioAttributes;
  }

  private void sendPublicCellNotification(XPublicCell ignoredObject) {

  }

  @Nonnull
  private Intent createIntent(final BaseApp context) {
    Context intentContext = context;
    BaseActivity currentActivity = context.getCurrentActivity();
    if (currentActivity != null) {
      intentContext = currentActivity;
    }

    int flags = 0;
    if (currentActivity == null) {
      flags = FLAG_ACTIVITY_NEW_TASK;
    }
    Intent notificationIntent = new Intent(intentContext, Cell411Activity.class);
    flags |= FLAG_ACTIVITY_SINGLE_TOP;
    notificationIntent.addFlags(flags);
    notificationIntent.putExtra("fragment", "FriendRequestFragment");
    return notificationIntent;
  }

  @Nonnull
  public String toString() {
    StringJoiner joiner = new StringJoiner("");
    joiner.add("TAG").add("[");
    joiner.add(getClass().getSimpleName());
    if (Util.theGovernmentIsHonest()) {
      joiner.add(mManager.toString());
    }
    joiner.add("]");
    return joiner.toString();
  }

  public void sendChatNotification(final XChatMsg chatMsg) {
    BaseApp.getExecutor().execute(new Notifier(chatMsg));
  }

  static class Counter {
    int mValue;

    Counter() {
      this(0);
    }

    Counter(int value) {
      mValue = value;
    }

    public int next() {
      return mValue++;
    }
  }

  class Subscriber implements Runnable {

    @Override
    public void run() {
      boolean done = false;
      try {
        LiveQueryService service = LiveQueryService.opt();
        if (service == null)
          return;
        if (!service.isReady())
          return;
        subscribeToQueries();
        done = true;
      } finally {
        if (!done) {
          getExec().schedule(this, 1000, TimeUnit.MILLISECONDS);
        }
      }
    }
  }

  class Notifier implements Runnable {
    final ParseObject mObject;

    Notifier(final ParseObject object) {
      mObject = object;
    }

    public Notifier(ParseGeoPoint geoPoint) {
      mObject = new ParseObject("Holder");
      mObject.put("location", geoPoint);
    }

    public void run() {
      ChData chData = getChData(mObject);
      assert chData != null;
      String message = chData.genMessage(mObject);
      if (message == null)
        return;
      NotificationChannelCompat nc = chData.mChannel;
      NotificationCompat.Builder builder;
      BaseApp context = BaseApp.req();
      builder = new NotificationCompat.Builder(context, nc.getId());
      builder.setAutoCancel(true);
      builder.setSound(chData.mRingtone);
      Bitmap avatar = chData.getAvatar(mObject);
      if (avatar != null) {
        IconCompat compat = IconCompat.createWithBitmap(avatar);
        builder.setSmallIcon(compat);
        builder.setLargeIcon(avatar);
      } else {
        Bitmap bitmap = null;
        if (mObject instanceof XAlert) {
          XUser owner = (XUser) mObject.get("owner", false);
          if (owner != null) {
            bitmap = owner.getAvatarPic();
          }
        }
        if (bitmap == null) {
          bitmap = ImageUtils.getLargeIconBitmap(R.mipmap.ic_launcher);
        }
        builder.setLargeIcon(bitmap);
        builder.setSmallIcon(R.mipmap.ic_launcher);
      }
      builder.setPriority(chData.getPriority());
      Date createdAt = mObject.getCreatedAt();
      if (createdAt != null) {
        builder.setShowWhen(true);
        builder.setWhen(createdAt.getTime());
      }
      Counter counter = chData.mCounter;

      Intent intent = createIntent(context);
      PendingIntent pendingIntent =
        PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE);
      builder.setContentIntent(pendingIntent);

      int notificationId =
        counter == null ? (int) (Math.random() * Integer.MAX_VALUE) : counter.next();

      String title = chData.genTitle(mObject);
      builder.setContentTitle(title + "  #" + notificationId);
      builder.setChannelId(nc.getId());
      builder.setContentText(message);
      builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));

      builder.setSound(chData.mRingtone);

      Notification notification = builder.build();

      mManager.notify(notificationId, notification);
    }
  }

  class ChData {
    String mId;
    NotificationChannelCompat mChannel;
    Uri mRingtone;
    Counter mCounter = new Counter();

    ChData(String id) {
      mId = id;
      mChData.put(id, this);
    }

    public String genTitle(final ParseObject object) {
      if (object instanceof XAlert) {
        return "Emergency Alert Received";
      }
      return "Title:  " + object;
    }

    public Bitmap getAvatar(final ParseObject object) {
      XUser user = object.has("owner") ? (XUser) object.get("owner") : null;
      if (user == null) {
        return null;
      }
      return user.getAvatarPic();
    }

    public int getPriority() {
      return mChannel.getImportance();
    }

    public String genMessage(final ParseObject object) {
      if (object instanceof XAlert) {
        XAlert alert = (XAlert) object;
        XUser user = alert.getOwner();
        ProblemType problemType = alert.getProblemType();
        String note = alert.getNote();
        if (Util.isNoE(note)) {
          note = "";
        } else {
          note = " '" + note + "'";
        }
        return Util.format("%s issued a %s alert%s", user.getName(),
          problemType, note);
      } else if (object instanceof XRequest) {
        XRequest request = (XRequest) object;
        XPublicCell cell = request.getCell();
        XUser owner = request.getOwner();
        XUser sentTo = request.getSentTo();
        XUser curr = XUser.getCurrentUser();
        PrintString ps = new PrintString();
        String oName, sName, gNote = "", rType;
        if (owner == curr) {
          oName = "You";
          sName = owner.getName();
        } else if (sentTo == curr) {
          oName = sentTo.getName();
          sName = "You";
        } else {
          return null;
        }
        if (cell == null) {
          rType = "friend request";
        } else {
          rType = "cell join request";
          gNote = "\n\nFor cell " + cell.getName();
        }
        ps.p(oName).p(" sent a ").p(rType).p(" to ").pl(sName)
          .pl(gNote);
        return ps.toString();
      } else if (object instanceof XPublicCell) {
        XPublicCell cell = (XPublicCell) object;
        return "A new public cell called " + cell + "\n"
          + "was created near you";
      } else if (object instanceof XChatMsg) {
        return "A new chat message arrived";
      } else {
        return null;
      }
    }
  }
  //  public void showNotification(String title, String message, XObject object)
  //  {
  //    NotificationManagerCompat nm = NotificationManagerCompat.from(mContext);
  //
  //    Intent notificationIntent = new Intent(mContext,
  //      MainActivity.class);
  //    //FIXME!    mCurrentUser.addPendingEvent(object);
  //
  //    notificationIntent.putExtra("object", object.getClassName() + "::" + object.getObjectId());
  //
  //    PendingIntent pendingIntent =
  //      PendingIntent.getActivity(mContext, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE);
  //
  //    NotificationCompat.Builder notificationBuilder =
  //      new NotificationCompat.Builder(mContext, CHANNEL);
  //    notificationBuilder.setSmallIcon(cell411.services.R.mipmap.ic_launcher);
  //    notificationBuilder.setOngoing(true);
  //    notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
  //    notificationBuilder.setShowWhen(true);
  //    notificationBuilder.setWhen(object.getCreatedAt().getTime());
  //    notificationBuilder.setContentTitle(title + "  #" + mLastId);
  //    notificationBuilder.setChannelId(CHANNEL);
  //    Bitmap bitmap = BitmapFactory.decodeResource(Bro411.get().getResources(), R.mipmap
  //    .ic_launcher);
  //
  //    notificationBuilder.setLargeIcon(bitmap);
  //
  //    if (message == null)
  //      message = "The service is running";
  //
  //    notificationBuilder.setContentText(message);
  //
  //    notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
  //    if (pendingIntent != null)
  //      notificationBuilder.setContentIntent(pendingIntent);
  //
  //    Notification notification = notificationBuilder.build();
  //    notification.defaults =
  //      Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
  //    ++mLastId;
  //    nm.notify(mLastId, notification);
  //  }

}
