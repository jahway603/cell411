package cell411.logic;

import com.parse.model.ParseObject;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class ObjectCache
{
  protected final LiveQueryIface mService;

  public void onUI(Runnable runnable) {
    onUI(runnable,0);
  }
  public void onUI(Runnable runnable, long delay) {
    mService.onUI(runnable,delay);
  }
  public LiveQueryIface getService(){
    return mService;
  }
  public ObjectCache(LiveQueryIface service)
  {
    mService=service;
  }

//  public static final class Mapper
//  {
//    HashMap<String, ParseObject> map = new HashMap<>();
//
//    public ParseObject get(String objectId) {
//      ParseObject object = map.get(objectId);
//      System.out.printf("GET: %-20s => %s\n", objectId, object);
//      return object;
//    }
//
//    void put(ParseObject newObject) {
//      String objectId = newObject.getObjectId();
//      ParseObject oldObject = map.get(objectId);
//      if (oldObject == null) {
//        map.put(objectId, newObject);
//        System.out.printf("PUT: %-20s => %s\n", objectId, newObject);
//      } else {
//        System.out.printf("UPD: %-20s => %s %s\n",
//          objectId, oldObject.getUpdatedAt(), oldObject);
//        System.out.printf("     %-20s => %s %s\n",
//          objectId, newObject.getUpdatedAt(), newObject);
//        assert newObject.getClass().isInstance(oldObject);
//        oldObject.setState(newObject.getState());
//      }
//    }
//
//    public void remove(String objectId) {
//      System.out.printf("REM: %-20s => %s\n", objectId, null);
//      System.out.println("Not removing shit, don't know how yet.");
//    }
//
//    @Nonnull
//    public <X extends ParseObject>
//    X getOrStub(String objectId, Class<X> type) {
//      X object = type.cast(get(objectId));
//      if (object == null) {
//        JSONObject pointer = new JSONObject();
//        pointer.put("__type", "Pointer");
//        pointer.put("objectId", objectId);
//        pointer.put("className", ParseSyncUtils.getTableForClass(type));
//        Object obj = ParseDecoder.get().decode(pointer);
//        X x=type.cast(obj);
//        if(x!=null)
//          put(x);
//      }
//      return require(object,type);
//    }
//
//    private <X extends ParseObject>
//    X require(X object,Class<X> type)
//    {
//      if(object==null)
//        throw new NullPointerException();
//      else
//        return type.cast(object);
//    }
//
//    public <X extends ParseObject>
//    X merge(String id, JSONObject json, Class<X> type)
//    {
//      X res = getOrStub(id,type);
//      System.out.println("getOrStub("+id+","+type+") => "+res);
//      System.out.println(res.toJSON().toString(2));
//      ParseOperationSet set = res.operationSetQueue.getFirst();
//      if(set.isEmpty()){
//        for(String key : json.keySet()){
//          Object val = json.opt(key);
//          Object obj = ParseDecoder.get().decode(val);
//          assert (val==null)==(obj==null);
//          if(obj==null){
//            res.remove(key);
//          } else {
//            res.put(key,val);
//          }
//        }
//      }
//      System.out.println(set);
//      System.out.println(res);
//      return type.cast(res);
//    }
//  }

  static final Map<String, ParseObject> smMapper = ParseObject.smCache;

  static protected
  <X extends ParseObject, C extends Collection<X>>
  C getValues(C co, Iterable<String> it, Class<X> ty)
  {
    for(String id : it)
      co.add(getObject(id,ty));
    return co;
  }

  static protected
  <X extends ParseObject>
  ArrayList<X> getValues(Iterable<String> it, Class<X> ty){
    return getValues(new ArrayList<>(), it, ty);
  }


  static public <X extends ParseObject>
  X getObject(String objectId, Class<X> type)
  {
    return ParseObject.createWithoutData(type,objectId);
  }

//  static public <X extends ParseObject>
//  X requireObject(String objectId) {
//    return Objects.requireNonNull(getObject(objectId));
//    X x = getObject(objectId);
//    if (x == null) {
//      throw new NoSuchElementException("No object found for key '" + objectId + "'");
//    }
//    return x;
//  }

  @SuppressWarnings("unchecked")
  static public <X extends ParseObject>
  X getObject(String objectId) {
    return (X) smMapper.get(objectId);
  }

//  static public void remObject(String objectId) {
//    smMapper.remove(objectId);
//  }

  static public void putObject(@Nullable ParseObject newObject) {
    if(newObject==null)
      return;
    synchronized (smMapper) {
      assert smMapper.get(newObject.getObjectId()) == newObject;
    }
//    if(newObject==null)
//      return;
//    smMapper.put(newObject.getObjectId(),newObject);
  }
}
