package cell411.services;

import android.Manifest.permission;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseContext;
import cell411.utils.DroidUtil;
import cell411.utils.LocationUtil;
import cell411.utils.MethodRunnable;
import cell411.utils.ObservableValue;
import cell411.utils.Reflect;
import cell411.utils.ThreadUtil;
import cell411.utils.TimeLog;
import cell411.utils.Util;
import cell411.utils.ValueObserver;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseUser;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.safearx.cell411.Cell411.opt;

@SuppressWarnings("unused")
public class LocationService extends ContextWrapper
  implements BaseContext, OnSharedPreferenceChangeListener {
  final static public String LS_MIN_TIME_KEY =
    "LocationService.minDistanceMetres";
  final static public String LS_MIN_DIST_KEY = "LocationService.minTimeMs";
  // final, static members
  final private static ObservableValue<Location> mLocation;
  private static final LocationListener mListener;
  private static final XTAG TAG = new XTAG();
  private static LocationService smInstance;

  static {
    mLocation = new ObservableValue<>(Location.class, null);
    mListener = new LocationServiceListener();
    mLocation.set(LocationUtil.getLocation(0, 0));
  }

  private final MethodRunnable mLoadLocationRunnable;

  private final MethodRunnable mInitRunnable;

  {
    mLoadLocationRunnable = MethodRunnable.forVirtual(this, "loadLocation");
    mInitRunnable = MethodRunnable.forVirtual(this,"init");
  }
  boolean mIsWatching = false;
  private long smLastUpdateMillis = 0;

  {
    smInstance = this;
  }

  public LocationService(Context context) {
    super(context);
    getExec().execute(mInitRunnable);
  }
  static final ByteArrayOutputStream baos=new ByteArrayOutputStream();
  public void init() {
    boolean done=false;
    try {
      assert !DroidUtil.isMainThread();
      TimeLog timeLog=new TimeLog(baos);
      timeLog.pl("Waiting for BaseAoo");
      ThreadUtil.waitUntil(this, () -> BaseApp.opt() != null);
      timeLog.pl("Waiting for MainActivity");
      ThreadUtil.waitUntil(this, () -> BaseApp.req().getMainActivity() != null);
      timeLog.pl("Waiting for ParseUser.getCurrentUser");
      ThreadUtil.waitUntil(this, () -> ParseUser.getCurrentUser() != null);
      timeLog.pl("Starting location service");
      XLog.i(TAG,"%s",timeLog);
      getExec().execute(mLoadLocationRunnable);
    } catch ( Throwable t ) {
      t.printStackTrace();
    } finally {
      getExec().execute(mLoadLocationRunnable);
    }
  }

  public static void addObserver(ValueObserver<Location> locationValueObserver) {
    mLocation.addObserver(locationValueObserver);
    smInstance.startWatching();
  }

  public static void removeObserver(ValueObserver<Location> locationValueObserver) {
    mLocation.removeObserver(locationValueObserver);
    if (mLocation.countObservers() == 0 && smInstance != null) {
      smInstance.stopWatching();
    }
  }

  public static LocationService get() {
    if (smInstance == null) {
      smInstance = new LocationService(BaseApp.req());
    }
    return smInstance;
  }

  public static void getPermRequests(Set<String> perms) {
    perms.add(permission.ACCESS_COARSE_LOCATION);
    perms.add(permission.ACCESS_FINE_LOCATION);
  }

  public void loadLocation() {
    if (BaseApp.opt() == null) {
      Reflect.announce("Deferring:  app()=null");
      getExec().schedule(mLoadLocationRunnable, 30000, TimeUnit.MILLISECONDS);
      return;
    }
    try {
      SharedPreferences latLngPref = getAppPrefs();
      double lat =
        latLngPref.getFloat("Location.Lat", (float) 42.93383800273033);
      double lng =
        latLngPref.getFloat("Location.Lng", (float) -72.27850181930877);
      long locationTime = latLngPref.getLong("Location.Date", 0);
      Location location = LocationUtil.getLocation(lat, lng);
      smLastUpdateMillis = locationTime;
      mLocation.set(location);
    } catch (Exception e) {
      System.out.println(Util.format("Exception: %s", e));
    }
  }

  public void storeLocation() {
    if (opt() == null) {
      Reflect.announce("Deferring:  app()=null");
      getExec().schedule(mLoadLocationRunnable, 5000, TimeUnit.MILLISECONDS);
      return;
    }
    SharedPreferences latLngPref = getAppPrefs();
    SharedPreferences.Editor edit = latLngPref.edit();
    Location location = mLocation.get();
    if (location != null) {
      edit.putFloat("Location.Lat", (float) location.getLatitude());
      edit.putFloat("Location.Lng", (float) location.getLongitude());
      edit.putLong("Location.Time", smLastUpdateMillis);
      edit.apply();
    }
  }

  public long locationAge() {
    return System.currentTimeMillis() - smLastUpdateMillis;
  }

  private boolean startWatching(String provider) {
    Context context = getApplicationContext();
    LocationManager lm = context.getSystemService(LocationManager.class);
    try {
      SharedPreferences prefs = getAppPrefs();
      // Default to 4 updates per hour
      long minDelay = prefs.getLong(LS_MIN_TIME_KEY, Util.millis(15, TimeUnit.MINUTES));

      // Default to one update per mile.
      long minDistance = prefs.getLong(LS_MIN_DIST_KEY, 1600);

      lm.requestLocationUpdates(provider, minDelay, minDistance, mListener);
      return true;
    } catch (SecurityException se) {
      BaseApp.req().handleException("Requesting location updates", se);
    }
    return false;
  }

  void startWatching() {
    if (mIsWatching) {
      return;
    }
    BaseApp app = app();
    if (!isOnRF()) {
      onRF(this::startWatching, 500);
      return;
    }
    Set<String> missingPermissions = app.getMissingPermissions();
    boolean locOk = !missingPermissions.contains(permission.ACCESS_COARSE_LOCATION);
    if (!locOk) {
      locOk = !missingPermissions.contains(permission.ACCESS_FINE_LOCATION);
    }
    if (!locOk) {
      onRF(this::startWatching, 1000);
      return;
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
      mIsWatching = mIsWatching || startWatching(LocationManager.FUSED_PROVIDER);
    }
    mIsWatching = mIsWatching || startWatching(LocationManager.GPS_PROVIDER);
    if (!mIsWatching) {
      ds().showToast("The location manager is not available.  I don't know where we are!");
    }
  }

  private void stopWatching() {
    Context context = ds().getApplicationContext();
    LocationManager lm = context.getSystemService(LocationManager.class);
    try {
      lm.removeUpdates(mListener);
    } catch (SecurityException se) {
      BaseApp.req().handleException("Requesting location updates", se);
    }
  }

  public ParseGeoPoint getParseGeoPoint() {
    return LocationUtil.getGeoPoint(getLocation());
  }

  @Nonnull
  public Location getLocation() {
    Location res = mLocation.get();
    if (res == null) {
      res = LocationUtil.getLocation(0, 0);
    }
    return res;
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    switch (key) {
      case LS_MIN_DIST_KEY:
      case LS_MIN_TIME_KEY:

    }
  }

  private static class LocationServiceListener implements LocationListener {
    @Override
    public void onLocationChanged(@Nonnull final Location location) {
      mLocation.set(location);
    }

    @Override
    public void onFlushComplete(final int requestCode) {
      Reflect.announce("Flush Complete");
    }

    @Override
    public void onProviderEnabled(@Nonnull final String provider) {
      Reflect.announce(provider);
    }

    @Override
    public void onProviderDisabled(@Nonnull final String provider) {
      Reflect.announce(provider);
    }
  }
}

