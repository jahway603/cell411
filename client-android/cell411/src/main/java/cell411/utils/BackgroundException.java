package cell411.utils;

import java.util.Objects;

import javax.annotation.Nonnull;

/**
 * Wrapper class for exceptions caught in the background
 */
public class BackgroundException extends RuntimeException {

  final int tid;
  final String threadName;

  /**
   * @param e          original exception
   * @param tid        id of the thread where exception occurred
   * @param threadName name of the thread where exception occurred
   */
  public BackgroundException(Throwable e, int tid, String threadName) {
    super(Objects.requireNonNull(e));
    this.tid = tid;
    this.threadName = threadName;
  }

  @Nonnull
  public Throwable getCause() {
    return Objects.requireNonNull(super.getCause());
  }
}
