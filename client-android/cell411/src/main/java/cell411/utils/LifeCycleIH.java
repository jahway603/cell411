package cell411.utils;

import android.app.Activity;
import cell411.ui.base.*;

import java.lang.reflect.*;

public class LifeCycleIH implements InvocationHandler {
  static private final Class<LifeCycleIH> mIHClass = LifeCycleIH.class;
  static private final ClassLoader mLoader = mIHClass.getClassLoader();
  static private final Class<?>[] mTypes = new Class<?>[]{
    AALC.class
  };

  private LifeCycleIH() {
  }

  public static AALC create() {
    Reflect.announce();
    LifeCycleIH self = new LifeCycleIH();
    return (AALC) Proxy.newProxyInstance(mLoader, mTypes, self);
  }

  public Object invoke(Object proxy, Method method, Object[] args) {
    String name = method.getName();

    switch (name) {
      case "equals":
        return args[0] == this;
      case "hashCode":
        return 42;
      case "toString":
        return proxy.toString();

      case "onActivityResumed":
      case "onActivityPaused":
        if (args[0] instanceof Activity) {
          BaseActivity baseActivity = (BaseActivity) args[0];
          BaseApp app = BaseApp.req();
          if(app==null)
            return null;
          switch (name) {
            case "onActivityResumed":
              app.pushActivity(baseActivity);
              break;
            case "onActivityPaused":
              app.popActivity(baseActivity);
              break;
          }
        }
      case "onActivityCreated":
      case "onActivityStarted":
      case "onActivityStopped":
      case "onActivitySaveInstanceState":
      case "onActivityDestroyed":
      case "onActivityPostResumed":
        break;
    }

    return null;
  }
}