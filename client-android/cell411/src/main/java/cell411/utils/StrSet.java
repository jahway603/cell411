package cell411.utils;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

import cell411.json.TypedIterator;

public class StrSet extends TreeSet<String> {
  final static Comparator<String> smComparator = String::compareToIgnoreCase;

  @SafeVarargs
  public StrSet(Collection<String>... copy) {
    super(smComparator);
    for (Collection<String> coll : copy) {
      if (coll != null) {
        addAll(coll);
      }
    }
  }

  public StrSet(TypedIterator<String> itr) {
    addAll(itr);
  }

  private void addAll(Iterator<String> itr) {
    while (itr.hasNext()) {
      String next = itr.next();
      if (next != null) {
        add(next);
      }
    }
  }

  @SafeVarargs
  final StrSet replaceAll(Collection<String>... copy) {
    clear();
    addAll(copy);
    return this;
  }

  @SafeVarargs
  final StrSet addAll(Collection<String>... copy) {
    for (Collection<String> coll : copy) {
      super.addAll(coll);
    }
    return this;
  }

  @SafeVarargs
  final StrSet removeAll(Collection<String>... copy) {
    for (Collection<String> coll : copy) {
      super.removeAll(coll);
    }
    return this;
  }
}
