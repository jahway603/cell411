package cell411.utils;

import android.os.Handler;
import android.os.Looper;

import javax.annotation.Nonnull;

import cell411.ui.base.RealExceptionHandler;


public class TheWolf {
  static final Handler smLikeQuiChangCaine = new Handler(Looper.getMainLooper());
  private static final XTAG TAG = XLog.getTag();
  private static RealExceptionHandler smRealExceptionHandler;
  public static void setRealExceptionHandler(RealExceptionHandler handler){
    smRealExceptionHandler=handler;
  }

  static {
    Thread.setDefaultUncaughtExceptionHandler(new Vincent());
  }

  public static void showAlertDialog(String message, OnCompletionListener listener) {
    RealExceptionHandler app = smRealExceptionHandler;
    if (app != null) {
      app.realShowAlertDialog(message, listener);
      return;
    }
    new Jules("showAlertDialog", message, null, null, listener).walkTheEarth();
  }

  public static void showToast(String title) {
    RealExceptionHandler app = smRealExceptionHandler;
    if (app != null) {
      app.realShowToast(title);
      return;
    }
    new Jules("showToast", title, null,
      null, null).walkTheEarth();
  }

  public static void handleException(String title, Throwable throwable,
                                     OnCompletionListener listener, final boolean dialog) {
    RealExceptionHandler app = smRealExceptionHandler;
    if (app != null) {
      app.realHandleException(title, throwable, listener, dialog);
      return;
    }
    new Jules("handleException", null,
      title, throwable, listener).walkTheEarth();
  }

  public static void showYesNoDialog(
    String title, String text, OnCompletionListener listener) {
    RealExceptionHandler app = smRealExceptionHandler;
    if (app == null) {
      new Jules("showYesNoDialog", title,
        text, null, listener).walkTheEarth();
    } else {
      app.realShowYesNoDialog(title, text, listener);
    }
  }

  static class Vincent implements Thread.UncaughtExceptionHandler {
    public void uncaughtException(@Nonnull Thread t, @Nonnull Throwable e) {
      TheWolf.handleException("Uncaught exception in thread: " + t, e, null, false);
    }
  }

  private static class Jules implements Runnable {
    private final String mMethod;
    private final String mMessage;
    private final String mText;
    private final OnCompletionListener mListener;
    private final Throwable mThrowable;

    public Jules(
      String method, String message,
      String text, Throwable throwable,
      OnCompletionListener listener) {
      mMethod = method;
      mMessage = message;
      mListener = listener;
      mThrowable = throwable;
      mText = text;
    }

    @Override
    public void run() {
      RealExceptionHandler app = smRealExceptionHandler;
      if( app==null || app.isNotReady()){
        walkTheEarth();
        return;
      }
      switch (mMethod) {
        default:
        case "showAlertDialog":
          showAlertDialog(mMessage, mListener);
          break;
        case "showYesNoDialog":
          showYesNoDialog(mMessage, mText, mListener);
          break;
        case "showToast":
          showToast(mMessage);
          break;
        case "handleException":
          handleException(mMessage, mThrowable, mListener, false);
          break;
      }
    }

    public void walkTheEarth() {
      smLikeQuiChangCaine.postDelayed(this, 100);
    }
  }

}
