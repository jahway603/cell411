package cell411.utils;

import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HttpLogInterceptor implements Interceptor {
  public static final XTAG TAG = new XTAG();

  final static String msg1 =
    "" + "#%d: time:   %d\n" + "#%d: thread: %s\n" + "#%d: method: %s\n" + "#%d: url:    %s\n" +
      "#%d: code:   %d\n" + "#%d: msg:    %s\n";
  //  static TimeLog log = new TimeLog();
  final boolean smVerbose = true;
  int mReqNum = 0;

  @Nonnull
  @Override
  public Response intercept(@Nonnull Chain chain) throws IOException {
    Request req = chain.request();
    if (smVerbose) {
      Reflect.announce("Intercept");
      try (PrintString ps = new PrintString()) {

        Response res = chain.proceed(req);

        int reqNo = ++mReqNum;

        ps.pl("HTTP:   " + msg1, reqNo, System.currentTimeMillis(), reqNo,
          Thread.currentThread().getName(), reqNo, req.method(), reqNo, req.url(), reqNo,
          res.code(), reqNo, res.message());
        Log.i("HttpLogInter", ps.toString());
        return res;
      } finally {
        System.out.println("exit");
      }
    } else {
      //      Log.i("HttpLog:", ""+req.url());
      //      Log.i("HttpLog: ", ""+req.body());
      Reflect.announce(req.url());
      final Response res = chain.proceed(req);
      Reflect.announce(res.code());
      //      Log.i("HttpLog:", ""+res);
      return res;
    }
  }

  private void dumpHeaders(Request req, PrintString ps) {
    Headers headers = req.headers();
    Set<String> names = headers.names();
    int maxLen = 0;
    for (String name : names) {
      maxLen = Math.max(maxLen, name.length());
    }
    String format = Util.format("%%-%ds: %%s", maxLen + 3);
    for (String name : names) {
      List<String> values = headers.values(name);
      for (String value : values) {
        ps.pl(Util.format(format, name, value));
        name = "";
      }
    }
  }
}
