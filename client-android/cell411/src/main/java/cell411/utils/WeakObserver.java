package cell411.utils;

import java.lang.ref.WeakReference;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class WeakObserver<ValueType> implements ValueObserver<ValueType> {
  @Nonnull
  final WeakReference<ValueObserver<ValueType>> mReference;

  WeakObserver(@Nonnull ValueObserver<ValueType> observer) {
    mReference = new WeakReference<>(observer);
  }

  @Override
  public void onChange(@Nullable ValueType newValue, @Nullable ValueType oldValue) {
  }

  @Override
  public boolean done() {
    return mReference.get() == null;
  }
}
