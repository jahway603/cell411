package cell411.utils;

import javax.annotation.Nullable;

public interface ValueObserver<ValueType> {
  void onChange(@Nullable ValueType newValue, @Nullable ValueType oldValue);

  default boolean done() {
    return false;
  }
}
