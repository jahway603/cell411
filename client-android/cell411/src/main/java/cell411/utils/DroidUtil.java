package cell411.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewParent;
import android.widget.ImageView;
import androidx.annotation.BoolRes;
import androidx.annotation.IntegerRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import cell411.imgstore.ImageStore;
import cell411.parse.XUser;
import cell411.ui.base.BaseApp;
import cell411.ui.profile.ProfileImageFragment;
import cell411.utils.concurrant.STPExecutorPlus;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class DroidUtil {
  public static final NullHandler NULL_HANDLER = new NullHandler();
  static final ScheduledExecutorService smExecutor =
    new STPExecutorPlus(5);
  private static final ArrayList<Wrap<?>> smSave = new ArrayList<>();
  static Handler smHandler = new Handler(Looper.getMainLooper());

  public static boolean isMainThread() {
    return Looper.getMainLooper().isCurrentThread();
  }

  public static void onMain(Callable<?> callable) {
    onMain(callable,0);
  }
  final static XTAG TAG = new XTAG();
  public static void onMain(Callable<?> callable, long delay) {
    smHandler.postDelayed(() -> {
      try {
        callable.call();
      } catch (Exception e) {
        XLog.i(TAG, "Exception: %s",e);
        throw new RuntimeException(e);
      }
    }, delay);
  }

  public static void onMain(Runnable callable) {
    smHandler.post(callable);
  }

  public static void onMain(Runnable callable, long delay) {
    smHandler.postDelayed(callable, delay);
  }

  public static <B, A1 extends B, A2 extends B>
  B getBoolean(@BoolRes int resId, A1 tv, A2 fv) {
    return getBoolean(resId) ? tv : fv;
  }

  private static boolean getBoolean(int resId) {
    return false;
  }

  public static int getColor(Activity activity, int color) {
    Resources resources = activity.getResources();
    Resources.Theme theme = activity.getTheme();
    return ResourcesCompat.getColor(resources, color, theme);
  }

  public static int getColor(Resources resources, int color, Resources.Theme theme) {
    return ResourcesCompat.getColor(resources, color, theme);
  }

  public static void onExec(Callable<?> r) {
    smExecutor.submit(r);
//    smExecutor.schedule(wrap(r,null),0,TimeUnit.MILLISECONDS);
  }

  public static void onExec(Runnable r) {
    smExecutor.submit(r);
    //   smExecutor.schedule(wrap(r,null),0,TimeUnit.MILLISECONDS);
  }

  public static void onExec(Callable<?> r, long delay) {
    smExecutor.schedule(r, delay, TimeUnit.MILLISECONDS);
    //  smExecutor.schedule(wrap(r,null),delay, TimeUnit.MILLISECONDS);
  }

  public static void onExec(Runnable r, long delay) {
    smExecutor.schedule(r, delay, TimeUnit.MILLISECONDS);
    // smExecutor.schedule(wrap(r,null),delay,TimeUnit.MILLISECONDS);
  }

  public static ScheduledExecutorService getExec() {
    return smExecutor;
  }

  public static Handler getHand() {
    return smHandler;
  }

  public static void removeCallbacks(Runnable r) {
    smHandler.removeCallbacks(r);
  }

  public static <T>
  void onEither(boolean main, Callable<T> callable, long delay) {
    if (main) {
      onMain(callable, delay);
    } else {
      onExec(callable, delay);
    }
//    Wrap<T> wrap = wrap(callable, null);
//    if(main)
//      getHand().postDelayed(wrap,delay);
//    else
//      getExec().schedule(wrap,delay,TimeUnit.MILLISECONDS);
//    return wrap;
  }

  public static void onEither(boolean main, Runnable callable, long delay) {
    if (main) {
      onMain(callable, delay);
    } else {
      onExec(callable, delay);
    }
//    Wrap<?> wrap = wrap(callable, null);
//    if(main)
//      getHand().postDelayed(wrap,delay);
//    else
//      getExec().schedule(wrap,delay,TimeUnit.MILLISECONDS);
//    return wrap;
  }

  public synchronized static void save(Wrap<?> w) {
    synchronized (smSave) {
      smSave.add(w);
      XLog.i(TAG,"%10d: %s saved", smSave.size(), w);
    }
  }
//  public static class Wrap<T> implements RunnableFuture<T> {
//    static WrapMap smWrapMap = new WrapMap();
//    private final Wrap<?> mNext;
//    Throwable mCreation = new Throwable("Wrap Created Here");
//    long mCreated =System.currentTimeMillis();
//    long mTime;
//    long mBegin;
//    Runnable mRunnable;
//    Callable<T> mCallable;
//    T mResult;
//    Throwable mException;
//    private StateType mState=New;
//    private boolean mCancel=false;
//    private String mString;
//
//    public static List<Wrap<?>> getWraps(Runnable r) {
//      return smWrapMap.get(r);
//    }
//    public static List<Wrap<?>> getWraps(Callable<?> r) {
//      return smWrapMap.get(r);
//    }
//
//    public T get() {
//      return mResult;
//    }
//
//
//    enum StateType {
//      New(0), Started(1), Next(2), Done(3), Success(4),
//      Cancel(-1), Fail(-2);
//      final int mValue;
//      StateType(int value)
//      {
//        mValue = value;
//      }
//      int getValue(){
//        return mValue;
//      }
//    }
//    static boolean smFirstTime=true;
//    static void firstTime() {
//      if(smFirstTime){
//        smFirstTime=false;
//        StateType[]values=StateType.values();
//        for(int i=0;i<values.length;i++){
//          StateType type=values[i];
//          LogEvent.Log("%d %s %d",i++,type,type.getValue());
//        }
//      }
//    }
//    public Wrap(Runnable runnable,Wrap<?> next){
//      mRunnable = runnable;
//      mNext=next;
//      smWrapMap.add(runnable,this);
//    }
//    public Wrap(Callable<T> callable, Wrap<?> next) {
//      mCallable = callable;
//      mNext=next;
//      smWrapMap.add(callable,this);
//    }
//
//    @Override
//    public void run() {
//      mBegin=System.currentTimeMillis()- mCreated;
//      if(mCancel) {
//        mState=Cancel;
//        return;
//      }
//      if (call())
//        return;
//      if(mNext!=null) {
//        mState = Next;
//        mNext.run();
//      }
//      mState=Success;
//      mTime =System.currentTimeMillis()- mCreated;
//      save(this);
//    }
//
//    public boolean call() {
//      assert (mState== New): Util.format("State: %s",mState);
//      mState=Started;
//      assert (mRunnable ==null)!=(mCallable ==null);
//      try {
//        if (mRunnable != null) {
//          mRunnable.run();
//        } else {
//          mResult = mCallable.call();
//        }
//      } catch (Throwable e) {
//        return !fail(e);
//      }
//      return !mCancel;
//    }
//
//    private boolean fail(Throwable e) {
//      mState=Fail;
//      mException = e;
//      while(e.getCause()!=null){
//        e = e.getCause();
//      }
//      e.initCause(mCreation);
//      return false;
//    }
//
//    boolean cancel(){
//      // This looks weird.  Even if we did not actually
//      // cancel the task, we still use the flag to prevent
//      // any next tasks from running.
//      //
//      // Note that this cancels the wrapper.  The task is stateless.
//      // It may run many times, but each wrapper runs but once.
//      mCancel=true;
//      if(mState== New){
//        mState = Cancel;
//        return true;
//      } else {
//        return false;
//      }
//    }
//
//    @NonNull
//    public String toString() {
//      return Util.format("Wrap[%s,%s,%s,%s,%s]",mRunnable,mCallable,mState,
//        mNext==null?"!next":"next",mException);
//    }
//  }

  private synchronized static List<Wrap<?>> reset() {
    synchronized (smSave) {
      ArrayList<Wrap<?>> list = new ArrayList<>(smSave);
      smSave.clear();
      return list;
    }
  }

  public static int getInteger(@IntegerRes int resId) {
    return BaseApp.req().getResources().getInteger(resId);
  }

  public static String getString(int res, Object... args) {
    String str = "No Strings Available";
    Context context = BaseApp.opt();
    if (context != null)
      str = context.getString(res, args);
    return str;
  }


//  private static
//  Wrap<Void> wrap(Runnable r, Wrap<?> next){
//    return new Wrap<>(r,next);
//  }
//  private static <T>
//  Wrap<T> wrap(Callable<T> r, Wrap<?> next) {
//    return new Wrap<>(r,next);
//  }
//  private static <T>
//  Wrap<?> wrap(Object r, Wrap<?> next){
//    if(r instanceof Runnable){
//      return wrap((Runnable) r,next);
//    } else if (r instanceof Callable<?>){
//      return wrap((Callable<?>)r, next);
//    } else {
//      String fmt="Error:  %s is neither Runnable, nor Callable, it is %s";
//      String msg=Util.format(fmt,r,Util.className(r));
//      throw new IllegalArgumentException(msg);
//    }
//  }
//  private static Wrap<?>[] wrap(Object[] runOrCall){
//    Wrap<?>[] res=new Wrap[runOrCall.length];
//    Wrap<?> last=null;
//    for(int i=res.length-1;i>=0;i--){
//      res[i] = wrap(runOrCall[i], last);
//      last=res[i];
//    }
//    return res;
//  }

  public static String getFuzzyTimeOrDay(long millis) {
    Calendar calCurrentTime = Calendar.getInstance();
    calCurrentTime.setTimeInMillis(System.currentTimeMillis());
    calCurrentTime.set(Calendar.HOUR, 0);
    calCurrentTime.set(Calendar.MINUTE, 0);
    calCurrentTime.set(Calendar.SECOND, 0);
    calCurrentTime.set(Calendar.MILLISECOND, 0);
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(millis);
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH) + 1;
    int day = cal.get(Calendar.DAY_OF_MONTH);
    int hour = cal.get(Calendar.HOUR);
    int minute = cal.get(Calendar.MINUTE);
    int ampm = cal.get(Calendar.AM_PM);
    String ap;
    if (ampm == 0)
      ap = "AM";
    else
      ap = "PM";
    String hourMinute = "" + hour;
    if (minute > 0 && minute < 10)
      hourMinute += ":0" + minute;
    else if (minute > 9)
      hourMinute += ":" + minute;
    String time;
    if (cal.getTimeInMillis() <
      calCurrentTime.getTimeInMillis()) // Check if the alert was issued yesterday
      time = month + "/" + day + "/" + year;
    else
      time = hourMinute + " " + ap;
    return time;
  }

  public static void post(Runnable runnable) {
    DroidUtil.onEither(true, runnable, 0);
  }

  public static boolean hasPerm(@Nonnull Activity activity, String accessFineLocation) {
    assert activity != null;
    int res = ActivityCompat.checkSelfPermission(activity, accessFineLocation);
    return res == PackageManager.PERMISSION_GRANTED;
  }

  public static List<String> missingPerms(Activity activity, List<String> perms) {
    ArrayList<String> res = new ArrayList<>();
    for (String perm : perms) {
      if (!hasPerm(activity, perm))
        res.add(perm);
    }
    return res;
  }

  public static ArrayList<View> ancestors(View view) {
    ArrayList<View> res;
    ViewParent parent = view.getParent();
    if (parent instanceof View) {
      res = ancestors((View) parent);
    } else {
      res = new ArrayList<>();
    }
    res.add(view);
    return res;
  }

  @Nonnull
  public static String cleanFileName(@Nonnull String folderName) {
    char[] chars = new char[folderName.length()];
    folderName.getChars(0, chars.length, chars, 0);
    for (int i = 0; i < chars.length; i++) {
      char ch = chars[i];
      if (Character.isSpaceChar(ch) || Character.isISOControl(ch)) {
        ch = '_';
      }
      switch (ch) {
        case '/':
        case ':':
        case '\\':
          ch = '_';
        default:
          break;
      }
      chars[i] = ch;
    }
    folderName = new String(chars);
    return folderName;
  }

  public static void sendToast(Throwable e) {
    BaseApp.req().sendToast(e);
  }

  public static void sendToast(String format, Object... args) {
    BaseApp.req().sendToast(format, args);
  }

  public static void sendToast(int format, Object... args) {
    BaseApp.req().sendToast(format, args);
  }

//  public static void setUpMap(GoogleMap map) {
//    map.getUiSettings().setAllGesturesEnabled(true);
//    map.getUiSettings().setCompassEnabled(true);
//    map.getUiSettings().setMyLocationButtonEnabled(false);
//    map.getUiSettings().setRotateGesturesEnabled(true);
//    map.getUiSettings().setScrollGesturesEnabled(true);
//    map.getUiSettings().setTiltGesturesEnabled(true);
//    map.getUiSettings().setZoomControlsEnabled(false);
//    map.getUiSettings().setZoomGesturesEnabled(true);
//  }
//
  public static Resources getResources() {
    return app().getResources();
  }

  @Nonnull
  public static BaseApp app() {
    return BaseApp.req();
  }
  public static ImageStore is() {
    return ImageStore.get();
  }
  public static void setupImage(XUser user, ImageView avatar)
  {
    avatar.setImageBitmap(null);
    avatar.setImageDrawable(null);
    avatar.setOnClickListener(null);
    if(user.hasProfileImage()) {
      avatar.setImageBitmap(ImageStore.get().getUserPlaceHolder());
      is().loadImage(user, avatar);
      avatar.setOnClickListener(ProfileImageFragment.getOpener(user));
    }
  }

  static class WrapMap {
    HashMap<Object, ArrayList<Wrap<?>>> mData = new HashMap<>();

    ArrayList<Wrap<?>> createList(Object key) {
      return new ArrayList<>();
    }

    public synchronized void add(Object key, Wrap<?> val) {
      mData.computeIfAbsent(key, this::createList).add(val);
    }

    public List<Wrap<?>> get(Callable<?> r) {
      return mData.get(r);
    }

    public List<Wrap<?>> get(Runnable r) {
      return mData.get(r);
    }
  }

  public static class Wrap<T> {

  }

  static class NullHandler
    implements DialogInterface.OnClickListener,
    DialogInterface.OnCancelListener {
    @Override
    public void onClick(DialogInterface dialog, int which) {
    }

    @Override
    public void onCancel(DialogInterface dialog) {
    }
  }


}
