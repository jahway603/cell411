package cell411.utils;

import com.parse.controller.ClassTool;

import java.util.function.Supplier;

public class Suppliers
{
  public static class NewInstanceSupplier<X>
    implements Supplier<X>
  {
    private final Class<? extends X> mType;

    public <Y extends X> NewInstanceSupplier(Class<Y> type)
    {
      mType = type;
    }

    @Override
    public X get()
    {
      try {
        return mType.newInstance();
      } catch (Exception e) {
        throw Util.rethrow(e);
      }
    }
  }

  public static <X, Y extends X> Supplier<X> createSupplier(
    Class<Y> parseUserClass)
  {
    return new NewInstanceSupplier<>(parseUserClass);
  }
}
