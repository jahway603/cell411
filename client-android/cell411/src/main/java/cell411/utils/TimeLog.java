package cell411.utils;

import android.util.Log;

import java.io.OutputStream;
import java.time.Instant;

public class TimeLog
  extends SimplePrintStream
  implements PrintHelper
{
  private static final XTAG TAG = new XTAG();

  public TimeLog(OutputStream out) {
    super(out);
  }

  @Override
  public void startLine()
  {
    super.startLine();
    Instant instant = Instant.now();
    printf("%s: ",instant);
  }

  @Override
  protected void finalize()
  throws Throwable
  {
    Log.i(TAG.toString(),out.toString());
    super.finalize();
  }
}
