package cell411.utils;

import androidx.annotation.NonNull;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class LazyMap<K, V>
  extends AbstractMap<K, V>
{
  final Class<K> mKClass;
  final Class<V> mVClass;

  public LazyMap(Class<K> kClass, Class<V> vClass)
  {
    mKClass = kClass;
    mVClass = vClass;
  }
  public LazyMap(@Nonnull K k, @Nonnull V v ){
    this(Collect.getClass(k),Collect.getClass(v));
  }

  class EntryImpl
    implements Entry<K, V>
  {
    final K mKey;
    final V mVal;

    EntryImpl(K key, V val)
    {
      mKey=key;
      mVal=val;
    }

    @Override
    public K getKey()
    {
      return mKey;
    }

    @Override
    public V getValue()
    {
      return Util.getRef(mVal);
    }

    @Override
    public V setValue(V value)
    {
      return null;
    }
  }

  Entry<K, V> newEntry(K key, V val)
  {
    return new EntryImpl(key, val);
  }

  TreeSet<EntryImpl> mEntries;
  AbstractSet<Entry<K,V>> mWrap = new AbstractSet<Entry<K, V>>()
  {
    @NonNull
    @NotNull
    @Override
    public Iterator<Entry<K, V>> iterator()
    {
      Iterator<EntryImpl> iterator=mEntries.iterator();
      return new Iterator<Entry<K, V>>()
      {
        @Override
        public boolean hasNext()
        {
          return iterator.hasNext();
        }

        @Override
        public Entry<K, V> next()
        {
          return iterator.next();
        }
      };
    }

    @Override
    public int size()
    {
      return 0;
    }
  };

  @NonNull
  @NotNull
  @Override
  public Set<Entry<K, V>> entrySet()
  {
    return mWrap;
  }
}
