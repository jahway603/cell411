package cell411.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import cell411.json.JSONObject;
import cell411.libcell.ConfigDepot;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@SuppressWarnings("unused")
public class IOUtil
{
  private static final XTAG TAG = new XTAG();
  static int count = 0;

  public static String bin2hex(byte[] array)
  {
    try (PrintString sb = new PrintString()) {
      for (byte b : array) {
        sb.printf("%02x", (b & 0xff));
      }
      return sb.toString();
    }
  }

  public static String md5Hex(@Nonnull String message)
  {
    try {
      return md5Hex(message.getBytes("CP1252"));
    } catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }

  public static String md5Hex(@Nonnull byte[] message)
  {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] digest = md.digest(message);
      byte[] hexDigest = new byte[digest.length * 2];
      int p = 0;
      for (byte b : digest) {
        hexDigest[p++] = hexDigit(b, true);
        hexDigest[p++] = hexDigit(b, false);
      }
      return new String(hexDigest);
    } catch (NoSuchAlgorithmException ex) {
      throw new RuntimeException("digesting text", ex);
    }
  }

  private static byte hexDigit(byte b, boolean high)
  {
    if (high)
      b = (byte) (b << 4);
    if (b < 9) {
      return (byte) (b + '0');
    } else {
      return (byte) (b + 'a' - 10);
    }
  }

  public static byte[] streamToBytes(InputStream is)
  {
    try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
      int res;
      byte[] bytes = new byte[1024];
      while ((res = is.read(bytes)) != -1) {
        os.write(bytes, 0, res);
      }
      return os.toByteArray();
    } catch (IOException e) {
      throw Util.rethrow(e);
    }
  }

  public static String fileToString(File file)
  {
    try (FileInputStream is = new FileInputStream(file)) {
      return streamToString(is);
    } catch (IOException e) {
      throw Util.rethrow(e);
    }
  }

  private static String streamToString(FileInputStream is)
  {
    return new String(streamToBytes(is));
  }

  public static int stringToFile(File file, String save)
  {
    return bytesToFile(file, save.getBytes());
  }

  public static int bytesToFile(File file, byte[] bytes)
  {
    try (FileOutputStream fos = new FileOutputStream(file)) {
      return bytesToStream(fos, bytes);
    } catch (IOException e) {
      throw Util.rethrow(e);
    }
  }

  /**
   * Closes {@code closeable}, ignoring any checked exceptions. Does nothing
   * if {@code closeable} is
   * null.
   */
  public static void closeQuietly(Closeable closeable)
  {
    if (closeable != null) {
      try {
        closeable.close();
      } catch (RuntimeException rethrown) {
        throw rethrown;
      } catch (Exception ignored) {
      }
    }
  }

  public static void delete(File cacheFile)
  {
    if (cacheFile.exists() && cacheFile.delete()) {
      ++count;
    } else {
      --count;
    }
    if (count == 0) {
      XLog.i(TAG, "ZERO!");
    }
  }

  public static boolean createNewFile(File photoFile)
  {
    try {
      return photoFile.createNewFile();
    } catch (IOException e) {
      throw Util.rethrow("creating file", e);
    }
  }

  public static void close(AutoCloseable out)
  {
    if (out != null) {
      try {
        out.close();
      } catch (Exception e) {
        XLog.i(TAG, "Exception: " + e);
        XLog.i(TAG, "  Closing: " + out);
      }
    }
  }

  public static OutputStream newOutputStream(File file)
  {
    try {
      return Files.newOutputStream(file.toPath());
    } catch (Exception e) {
      throw new RuntimeException("opening " + file, e);
    }
  }

  public static InputStream fileToStream(final File file)
  {
    try {
      return new FileInputStream(file);
    } catch (FileNotFoundException e) {
      throw Util.rethrow(e);
    }
  }

  public static void renameTo(final File file, final File backupFile)
  {
    //noinspection ResultOfMethodCallIgnored
    file.renameTo(backupFile);
  }

  public static byte[] fileToBytes(File file)
  {
    try {
      return streamToBytes(Files.newInputStream(file.toPath()));
    } catch (IOException ioe) {
      throw Util.rethrow(ioe);
    }
  }

  public static int bytesToStream(OutputStream fos, byte[] bytes)
  throws IOException
  {
    try {
      fos.write(bytes);
      return bytes.length;
    } finally {
      closeQuietly(fos);
    }
  }

  public static File mkdirs(File images)
  {
    if(images.mkdirs()){
      System.out.println("made '"+images+"'");
    }
    return images;
  }

  public static int storeBitmap(File file, Bitmap bitmap)
  {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    String str = file.getName();
    CompressFormat cf;
    if(str.endsWith(".jpeg") || str.endsWith(".jpg")) {
      cf=CompressFormat.JPEG;
    } else if ( str.endsWith(".png")) {
      cf = CompressFormat.PNG;
    } else {
      throw new RuntimeException("No file type for name "+file);
    }
    byte[] bytes = ImageUtils.imageToBytes(bitmap, cf);
    return bytesToFile(file,bytes);
  }

  public static int storeBitmap(String s, Bitmap bitmap)
  {
    File file;
    if(s.startsWith("/")) {
      file = new File(s);
    } else {
      file=new File(ConfigDepot.getProfileDir(),s);
    }
    return storeBitmap(file, bitmap);
  }

  public static int jsonToFile(File file, JSONObject object)
  {
    return stringToFile(file, object.toString(2));
  }

  public static class Catching
  {
    Catching()
    {
    }

    public String fileToString(File file)
    {
      try {
        byte[] bytes = streamToBytes(Files.newInputStream(file.toPath()));
        return new String(bytes);
      } catch (IOException ex) {
        throw new RuntimeException("reading file: " + file, ex);
      }
    }

    public int stringToFile(File cacheFile, String string)
    {
      return bytesToFile(cacheFile, string.getBytes());
    }

    private RuntimeException rethrowException(String s, Throwable ex)
    {
      throw new RuntimeException(s, ex);
    }

    public byte[] streamToBytes(InputStream is)
    {
      try (ByteArrayOutputStream os = new ByteArrayOutputStream(1024)) {
        byte[] buffer = new byte[1024 * 1024];
        int len;
        while ((len = is.read(buffer)) >= 0) {
          os.write(buffer, 0, len);
        }
        return os.toByteArray();
      } catch (Exception ex) {
        throw rethrowException("reading InputStream: " + is, ex);
      } finally {
        closeQuietly(is);
      }
    }

    public int bytesToFile(File file, byte[] bytes)
    {
      try (OutputStream os = newOutputStream(file)) {
        return bytesToStream(os, bytes);
      } catch (Exception ex) {
        throw rethrowException("writing file: " + file, ex);
      }
    }
  }
}
