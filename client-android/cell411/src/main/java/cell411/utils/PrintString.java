package cell411.utils;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class PrintString
  extends SimplePrintStream
  implements PrintHelper
{
  ArrayList<byte[]> mStack = new ArrayList<>();
  final ByteArrayOutputStream mBuf;
  public PrintString() {
    super(new ByteArrayOutputStream());
    mBuf = (ByteArrayOutputStream) out;
  }

  public int size() {
    return mBuf.size();
  }

  @Nonnull
  public byte[] toByteArray() {
    return mBuf.toByteArray();
  }

  @Nonnull
  public String toString() {
    flush();
    return mBuf.toString();
  }

  @Nonnull
  public String reset() {
    String ret = out.toString();
    mBuf.reset();
    return ret;
  }

}
