package cell411.utils;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import cell411.parse.XUser;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.ModelBaseFragment;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ImageViewUpdater
  implements Runnable, ValueObserver<Bitmap>, View.OnAttachStateChangeListener {
  private final BaseFragment mFragment;
  private final XUser mUser;
  private final ImageView mView;

  public ImageViewUpdater(ModelBaseFragment fragment, XUser user, ImageView view) {
    mFragment = fragment;
    mUser = user;
    mView = view;
    mUser.addAvatarObserver(this);
    mView.addOnAttachStateChangeListener(this);
    mFragment.onUI(this);
  }

  @Override
  public void onChange(@Nullable Bitmap newValue, @Nullable Bitmap oldValue) {
    mFragment.onUI(this);
  }

  @Override
  public void run() {
    Bitmap bitmap = mUser.getAvatarPic();
    mView.setImageBitmap(bitmap);
  }

  @Override
  public void onViewAttachedToWindow(@Nonnull View v) {
    Reflect.announce(v);
  }

  @Override
  public void onViewDetachedFromWindow(@Nonnull View v) {
    Reflect.announce(v);
  }
}
