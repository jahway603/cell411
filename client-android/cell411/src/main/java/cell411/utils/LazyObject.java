package cell411.utils;

import cell411.utils.func.Func0;

import java.util.function.Supplier;

public class LazyObject<X>
  implements Func0<X>
{
  final Supplier<X> mCreator;
  private final Class<X> mType;
  X mValue;

  private LazyObject(Class<X> type, Supplier<X> creator) {
    mType=type;
    mCreator=creator;
    mValue=null;
  }

  public LazyObject(Class<X> type, X value)
  {
    mType=type;
    mValue=value;
    mCreator=null;
  }

  public static <R>
  LazyObject<R> create(Class<R> type, Supplier<R> sup)
  {
    return new LazyObject<>(type,sup);
  }

  public synchronized X apply() {
    return (mCreator==null?mValue: mCreator.get());
  }
}
