package cell411.utils.collect;

import cell411.utils.Util;

import java.util.ArrayList;

public class FactoryList<T>
  extends ArrayList<T>
{

  final Class<? extends T> mType;

  public FactoryList(Class<T> type)
  {
    mType = type;
  }

  @Override
  public T get(int i)
  {
    while (i >= size())
      add(null);
    T res = super.get(i);
    if (res == null)
      add(res = newInstance());
    return res;
  }

  private T newInstance()
  {
    try {
      return mType.newInstance();
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }

  @Override
  public int size()
  {
    return 0;
  }
}
