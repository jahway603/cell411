package cell411.utils.func;

public interface Func3V<A1, A2, A3> {
  void apply(A1 a1, A2 a2, A3 a3);

  default <B1 extends A1, B2 extends A2, B3 extends A3>
  Func2V<B2,B3> getClosure1(B1 b1) {
    return (b2,b3)->apply(b1,b2,b3);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3>
  Func2V<B1,B3> getClosure2(B2 b2) {
    return (b1,b3)->apply(b1,b2,b3);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3>
  Func2V<B1,B2> getClosure3(B3 b3) {
    return (b1,b2) -> apply(b1, b2, b3);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3>
  Func1V<B3> getClosure12(B1 b1,B2 b2) {
    return (b3)->apply(b1,b2,b3);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3>
  Func1V<B2> getClosure13(B1 b1,B3 b3) {
    return (b2)->apply(b1,b2,b3);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3>
  Func1V<B1> getClosure23(B2 b2, B3 b3) {
    return (b1) -> apply(b1, b2, b3);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3>
  Func0V getClosure(B1 b1, B2 b2, B3 b3) {
    return () -> apply(b1, b2, b3);
  }
}
