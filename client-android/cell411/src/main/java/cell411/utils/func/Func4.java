package cell411.utils.func;

public interface Func4<R, A1, A2, A3, A4>
{
  R apply(A1 a1, A2 a2, A3 a3, A4 a4);

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func3<R, B2, B3, B4> getClosure1( B1 b1)
  {
    return (b2, b3, b4) -> apply(b1, b2, b3, b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func3<R, B1, B3, B4> getClosure2( B2 b2)
  {
    return (b1, b3, b4) -> apply(b1, b2, b3, b4);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func3<R, B1, B2, B4> getClosure3( B3 b3 )
  {
    return (b1, b2, b4)-> apply(b1,b2,b3,b4);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func2<R, B3, B4> getClosure12( B1 b1, B2 b2)
  {
    return (b3,b4)->apply(b1,b2,b3,b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func2<R, B2, B4> getClosure13( B1 b1, B3 b3)
  {
    return (b2,b4)->apply(b1,b2,b3,b4);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func2<R, B2, B3> getClosure14( B1 b1, B4 b4 )
  {
    return (b2,b3)->apply(b1,b2,b3,b4);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func2<R, B1, B4> getClosure23( B2 b2, B3 b3)
  {
    return (b1,b4)->apply(b1,b2,b3,b4);
  }

  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func2<R, B1, B3> getClosure24( B2 b2, B4 b4)
  {
    return (b1,b3)->apply(b1,b2,b3,b4);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func2<R, B1, B2> getClosure34( B3 b3, B4 b4 )
  {
    return (b1,b2)->apply(b1,b2,b3,b4);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func1<R,A4> getClosure123(B1 b1, B2 b2, B3 b3)
  {
    return (b4)->apply(b1,b2,b3,b4);
  }
  default <B1 extends A1, B2 extends A2, B3 extends A3, B4 extends A4>
  Func0<R> getClosure( B1 b1, B2 b2, B3 b3, B4 b4)
  {
    return () -> apply(b1, b2, b3, b4);
  }
}
