package cell411.utils.func;

public interface Func2<R, A1, A2> {
  R apply(A1 a1, A2 a2);

  default Func1<R,A1> getClosure2(A2 a2) {
    Func2<R,A1,A2> orig = this;
    return a1 -> orig.apply(a1,a2);
  }
  default Func1<R,A2> getClosure1(A1 a1) {
    Func2<R,A1,A2> orig=this;
    return a2 -> orig.apply(a1,a2);
  }
  default <B1 extends A1, B2 extends A2> Func0<R> getClosure(B1 b1, B2 b2) {
    return () -> apply(b1,b2);
  }
}