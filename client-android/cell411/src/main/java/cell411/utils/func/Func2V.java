package cell411.utils.func;

public interface Func2V<A1, A2> {
  void apply(A1 a1, A2 a2);

  default <B1 extends A1, B2 extends A2>
  Func1V<B1> getClosure1(B2 b2) {
    Func2V<A1,A2> orig=this;
    return (b1)->apply(b1,b2);
  }
  default <B1 extends A1, B2 extends A2>
  Func1V<B1> getClosure2(B2 b2) {
    return (b1) -> apply(b1, b2);
  }
  default <B1 extends A1, B2 extends A2>
  Func0V getClosure(B1 b1, B2 b2) {
    return () -> apply(b1, b2);
  }
}