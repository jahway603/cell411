package cell411.utils;

import android.util.Log;

@SuppressWarnings("unused")
public final class XLog implements ExceptionHandler {
  public static final int VERBOSE = Log.VERBOSE;
  public static final int ASSERT = Log.ASSERT;
  public static final int DEBUG = Log.DEBUG;
  public static final int ERROR = Log.ERROR;
  public static final int INFO = Log.INFO;
  public static final int WARN = Log.WARN;
  public static final int smMaxCode = Util.max(ASSERT, DEBUG, ERROR, INFO, WARN, VERBOSE);
  public static final char[] smCodes = new char[smMaxCode + 1];
  static final long smStart = System.currentTimeMillis();
  public static long smLast = 0;
  static Throwable NULL = null;
  static PrintString smOutErr;

  static {
    smCodes[ASSERT] = 'A';
    smCodes[DEBUG] = 'D';
    smCodes[ERROR] = 'E';
    smCodes[INFO] = 'I';
    smCodes[WARN] = 'W';
    smCodes[VERBOSE] = 'V';
  }

  //  static
//  {
//    if (Util.theGovernmentIsHonest())
//    {
//      smOutErr = new PrintString();
//    }
//    if (smOutErr != null)
//    {
//      System.setOut(smOutErr);
//      System.setErr(smOutErr);
//      System.out.println("Installed PrintString");
//    }
//  }
//
  public static void d(XTAG tag, String msg, Throwable tr, Object... args) {
    LogMessage.send(DEBUG, tag, msg, tr, args);
  }

  public static void e(XTAG tag, String msg, Object... args) {
    LogMessage.send(ERROR, tag, msg, NULL, args);
  }

  public static void e(XTAG tag, String msg, Throwable tr, Object... args) {
    LogMessage.send(ERROR, tag, msg, NULL, args);
  }

  public static void i(XTAG tag, String msg, Object... args) {
    LogMessage.send(INFO, tag, msg, NULL, args);
  }

  public static void i(String tag, String msg, Object... args) {
    LogMessage.send(INFO, new XTAG(tag), msg, NULL, args);
  }

  public static void i(XTAG tag, String msg, Throwable tr, Object... args) {
    LogMessage.send(INFO, tag, msg, tr, args);
  }

  public static void v(XTAG tag, String msg, Object... args) {
    LogMessage.send(VERBOSE, tag, msg, NULL, args);
  }

  public static void v(XTAG tag, String msg, Throwable tr, Object... args) {
    LogMessage.send(VERBOSE, tag, msg, NULL, args);
  }

  public static void w(String tag, String msg, Object... args) {
    LogMessage.send(WARN, tag, msg, NULL, args);
  }

  public static void w(XTAG tag, String msg, Object... args) {
    LogMessage.send(WARN, tag, msg, NULL, args);
  }

  public static void w(XTAG tag, String msg, Throwable tr, Object... args) {
    LogMessage.send(WARN, tag, msg, NULL, args);
  }

  public static void d(String tag, String format) {
    LogMessage.send(DEBUG, new XTAG(tag), format, NULL, null);
  }

  public static void d(XTAG tag, String format) {
    LogMessage.send(DEBUG, tag, format, NULL, null);
  }

  public static void log(final int messageLogLevel, final String tag, final String message,
                         final Throwable tr) {
    LogMessage.send(messageLogLevel, tag, message, tr, null);
  }

  public static XTAG getTag() {
    return new XTAG(1);
  }

  public interface ILog extends ExceptionHandler {
    default boolean isLoggable(XTAG tag, int level) {
      return true;
    }

    void log(String prefix, String message);
  }
}