package cell411.utils;

import java.util.Arrays;

public interface PrintHelper
{

  byte[] smBuf = new byte[256];

  void write(int ch);
  void write(byte[] bs);
  void write(byte[] bs,int off,int len);
  void write(int ch,boolean nl);
  void write(byte[] bs,boolean nl);
  void write(byte[] bs,int off,int len,boolean nl);

  default void close()
  {
  }



  default void flush()
  {
  }

  default void print(char c)
  {
    write(c, false);
  }

  default void print(char[] v)
  {
    for (final char c : v) {
      write(c, false);
    }
  }

  default void print(double v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default void print(float v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default void print(int v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default void print(long v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default void print(String v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default void println()
  {
    write(10, false);
  }

  default void println(char x)
  {
    write( x, false);
    write( 10, false);
  }

  default void println(int x)
  {
    byte[] bs = Integer.toString(x).getBytes();
    write(bs, true);
  }

  default void println(long x)
  {
    byte[] bs = Long.toString(x).getBytes();
    write(bs, true);
  }

  default void println(float x)
  {
    byte[] bs = Float.toString(x).getBytes();
    write(bs, true);
  }

  default void println(double x)
  {
    byte[] bs = Double.toString(x).getBytes();
    write(bs, true);
  }

  default void println(char[] x)
  {
    for (final char c : x) {
      write( c, false);
    }
    write( 10, false);
  }

  default PrintHelper p(String fmt, Object... args)
  {
    if (args.length > 0) {
      return p(Util.format(fmt, args));
    } else {
      return p(fmt);
    }
  }

  default PrintHelper p(Object o)
  {
    print(o);
    return this;
  }

  default void print(Object v)
  {
    byte[] bs = String.valueOf(v).getBytes();
    write(bs, false);
  }

  default PrintHelper pl(String fmt, Object... args)
  {
    if (args.length > 0) {
      return pl(Util.format(fmt, args));
    } else {
      return pl(fmt);
    }
  }

  default PrintHelper pl(Object o)
  {
    println(o);
    return this;
  }

  default void println(Object x)
  {
    byte[] bs = String.valueOf(x).getBytes();
    write(bs, true);
  }

  default PrintHelper pl()
  {
    println("");
    return this;
  }

  default PrintHelper p(int i, String name)
  {
    assert name.length() < i - 4;
    assert i < smBuf.length;
    byte[] bytes = name.getBytes();
    System.arraycopy(bytes, 0, smBuf, 0, bytes.length);
    Arrays.fill(smBuf, bytes.length, i, (byte) '.');
    return this;
  }

  default PrintHelper pfl(String fmt, Object ...args) {
    pl(Util.format(fmt,args));
    return this;
  }
  default PrintHelper pf(String fmt, Object ...args) {
    p(Util.format(fmt,args));
    return this;
  }
}
