package cell411.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.database.Cursor;
import android.graphics.*;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Size;
import androidx.annotation.DrawableRes;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.util.Pair;
import androidx.exifinterface.media.ExifInterface;
import cell411.libcell.ConfigDepot;
import cell411.ui.base.BaseApp;
import com.parse.ParseCloud;
import com.parse.model.ParseFile;
import com.safearx.cell411.R;

import javax.annotation.Nullable;
import java.io.*;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static cell411.libcell.ConfigDepot.getExecutor;
import static cell411.libcell.ConfigDepot.mkname;

public class ImageUtils
{
  public static final String JPEG_FILE_SUFFIX = ".jpg";
  public static final DisplayMetrics mDisplayMetrics;
  public static final Resources res;
  public static final BaseApp app;
  static {
    app=BaseApp.req();
    res = app.getResources();
    mDisplayMetrics =res.getDisplayMetrics();
  }
  static Bitmap smPlaceHolder = createPlaceHolder();
  public static Bitmap getPlaceHolder() {
    if(smPlaceHolder==null){
      smPlaceHolder=createPlaceHolder();
    }
    return smPlaceHolder;
  }
  public static Bitmap createPlaceHolder()
  {
    Bitmap placeHolder = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(placeHolder);

    int id = R.drawable.ic_placeholder_user;
    Paint whitePaint = new Paint();
    whitePaint.setColor(0xff000000);
    whitePaint.setStyle(Paint.Style.FILL);
    canvas.drawRect(0, 0, 300, 300, whitePaint);

    OvalShape ovalShape = new OvalShape();
    final Drawable background = new ShapeDrawable(ovalShape);
    final int primaryColor = BaseApp.req().getPrimaryColor();

    background.setTint(0xff000000);
    background.setBounds(0, 0, 300, 300);
    background.draw(canvas);

    background.setTint(primaryColor);
    background.setBounds(20, 20, 280, 280);
    background.draw(canvas);

    final Drawable drawable = AppCompatResources.getDrawable(BaseApp.req(), id);
    int extra = 300 - 262;
    if (drawable != null) {
      drawable.setBounds(extra / 2, extra / 2, 300 - extra / 2, 300 - extra / 2);
      drawable.draw(canvas);
    }
    return placeHolder;
  }
  public static int getOrientation(final String filePath)
  {
    if (Util.isNoE(filePath)) {
      return 0;
    }
    try {
      final ExifInterface exifInterface = new ExifInterface(filePath);
      int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
        ExifInterface.ORIENTATION_NORMAL);
      return exifOrientationToDegrees(orientation);
    } catch (final IOException ignored) {
    }
    return 0;
  }


  public static Pair<File, Uri> createUriAndFile(String name, String authority)
  {
    int tries = 0;
    while (true) {
      try {
        BaseApp app = BaseApp.req();
        File storageDir =
          ConfigDepot.getPictureDir();
        File imageF = File.createTempFile(name, JPEG_FILE_SUFFIX, storageDir);
        Uri uri = FileProvider.getUriForFile(app, authority, imageF);
        return new Pair<>(imageF, uri);
      } catch (IOException e) {
        if (tries++ > 3) {
          throw Util.rethrow("getting uri", e);
        }
        System.out.println("" + e);
      }
    }
  }
  //  public static void takePicture(String name) {
  //    try {
  //      if (mUri != null)
  //        throw new IllegalStateException("Uri already pending");
  //
  //      try {
  //        File storageDir = getApp().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
  //        File imageF     = File.createTempFile(name, JPEG_FILE_SUFFIX, storageDir);
  //        UploadPicture.mCurrentPhotoPath = imageF.getAbsolutePath();
  //        mUri                            = IOUtil.getUriForFile(getApp().getCurrentActivity(),
  //        imageF);
  //      } catch (IOException e) {
  //        e.printStackTrace();
  //        XLog.i(TAG, "IOException: " + e.getMessage());
  //      }
  //      //      smTPLauncher.launch(mPendingOperation);
  //    } catch (Exception ex) {
  //      handleException("takingPicture", ex);
  //      mUri = null;
  //    }
  //  }

  public static Bitmap getCroppedBitmap(@androidx.annotation.Nullable Bitmap bmp)
  {
    if (bmp == null) {
      return null;
    }
    if (bmp.getWidth() == bmp.getHeight()) {
      return bmp;
    }
    int size = Math.min(bmp.getWidth(), bmp.getHeight());
    Bitmap output = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);
    canvas.drawARGB(0, 0, 0, 0);
    Rect rectDest;
    Rect rectSrc;
    if (bmp.getWidth() < bmp.getHeight()) {
      double diff = bmp.getHeight() - bmp.getWidth();
      rectSrc = new Rect(0, (int) diff / 2, bmp.getWidth(),
        bmp.getHeight() - ((int) diff / 2));
      rectDest = new Rect(0, 0, bmp.getWidth(), bmp.getWidth());
    } else {
      double diff = bmp.getWidth() - bmp.getHeight();
      rectSrc = new Rect((int) diff / 2, 0, bmp.getWidth() - ((int) diff / 2),
        bmp.getHeight());
      rectDest = new Rect(0, 0, bmp.getHeight(), bmp.getHeight());
    }
    canvas.drawBitmap(bmp, rectSrc, rectDest, null);
    return output;
  }

  public static  File saveImage(File file, Bitmap bitmap)
  {
    return saveImage(file, bitmap, CompressFormat.PNG);
  }

  public static byte[] imageToBytes(Bitmap bitmap, CompressFormat format)
  {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    bitmap.compress(format, 100, stream);
    bitmap.compress(format, 100, stream);
    System.out.println(stream.size());
    byte[] bytes = stream.toByteArray();
    System.out.println("encoded");
    return bytes;
  }

  public static File saveImage(File file, Bitmap bitmap, CompressFormat format)
  {
    if (bitmap == null) {
      return file;
    }

    if (!file.isAbsolute()) {
      BaseApp app = BaseApp.req();
      assert app != null;
      File dir = mkname(ConfigDepot.getDocDir(), "Cell411",
        ConfigDepot.getFlavor());
      ConfigDepot.mkdirs(dir);
      file = new File(dir, file.getName());
    }
    {
      File dir = file.getParentFile();
      if (dir != null && dir.mkdirs())
        BaseApp.req().showToast("Made new directory: " + dir);
    }
    try (OutputStream out = IOUtil.newOutputStream(file)) {
      bitmap.compress(format, 100, out);
      out.flush();
    } catch (Throwable throwable) {
      throw Util.rethrow("saving image", throwable);
    }
    return file;
  }


  //  public static  String getBaseName(URL url)
  //  {
  //    String fileName = url.toString();
  //    int pos = fileName.lastIndexOf('/');
  //    fileName = fileName.substring(pos + 1);
  //    return fileName;
  //  }

  public static Bitmap makeThumbnail(@androidx.annotation.Nullable Bitmap bmp)
  {
    bmp = getCroppedBitmap(bmp);
    if (bmp == null)
      return null;
    Bitmap output = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);
    Rect rectDest;
    Rect rectSrc;
    rectSrc = new Rect(0, 0, bmp.getHeight(), bmp.getHeight());
    rectDest = new Rect(0, 0, output.getWidth(), output.getHeight());
    canvas.drawBitmap(bmp, rectSrc, rectDest, null);
    return output;
  }

  public static float rotationForImage(Uri uri)
  {
    try {
      int res = -1;
      if (uri.getScheme().equals("content")) {
        //From the media gallery
        String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
        Cursor c = BaseApp.req().getContentResolver()
          .query(uri, projection, null, null, null);
        if (c.moveToFirst()) {
          res = c.getInt(0);
        }
        c.close();
      } else if (uri.getScheme().equals("file")) {
        //From a file saved by the camera
        ExifInterface exif = new ExifInterface(uri.getPath());
        res = exifOrientationToDegrees(
          exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL));
        exif.setAttribute(ExifInterface.TAG_ORIENTATION,
          "" + ExifInterface.ORIENTATION_NORMAL);
        exif.saveAttributes();
      }
      return res;
    } catch (IOException e) {
      Reflect.announce("Failed: "+e);
      return 0;
    }
  }

  public static int exifOrientationToDegrees(int exifOrientation)
  {
    if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
      return 90;
    } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
      return 180;
    } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
      return 270;
    }
    return 0;
  }

  @Nullable
  public static Bitmap rotateBitmap(Bitmap bmp, float rotation)
  {
    if (rotation != 0) {
      //New rotation matrix
      Matrix matrix = new Matrix();
      matrix.preRotate(rotation);
      bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(),
        matrix, true);
    }
    return bmp;
  }

  public static Bitmap loadCameraImage(File file)
  {
    String path = file.getAbsolutePath();
    Bitmap bitmap = BitmapFactory.decodeFile(path);
    float rotation = ImageUtils.getOrientation(path);
    if (rotation != 0) {
      Matrix matrix2 = new Matrix();
      matrix2.preRotate(rotation);
      double w1 = bitmap.getWidth();
      double h1 = bitmap.getHeight();
      Bitmap newBitmap =
        Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix2, true);
      double h2 = newBitmap.getHeight();
      double w2 = newBitmap.getWidth();
      PrintString ps = new PrintString();
      ps.printf("bitmap: %f x %f\n", w1, h1);
      ps.printf("newMap: %f x %f\n", w2, h2);
      ps.println(newBitmap);
      ps.println(bitmap);
      String text = ps.toString();
      System.out.println(text);
      bitmap = newBitmap;
    }
    return bitmap;
  }

  public static float getDensity()
  {
    return mDisplayMetrics.density;
  }

  public static int getScreenWidth()
  {
    return mDisplayMetrics.widthPixels;
  }

  public static int getScreenHeight()
  {
    return mDisplayMetrics.heightPixels;
  }

  public static Drawable getDrawable(int id)
  {
    Theme theme = getTheme();
    return ResourcesCompat.getDrawable(res, id, theme);
  }

  private static Theme getTheme()
  {
    return BaseApp.req().getTheme();
  }

  private Resources getResources()
  {
    return BaseApp.req().getResources();
  }

  public static Size getLargeIconSize()
  {
    Resources res;
    BaseApp app = BaseApp.req();
    int width = app.getNotificationWidth();
    int height = app.getNotificationHeight();

    return new Size(width, height);
  }

  static Bitmap createBitmap(Size size)
  {
    int width = size.getWidth();
    int height = size.getHeight();
    Config argb8888 = Config.ARGB_8888;
    return Bitmap.createBitmap(width, height, argb8888);
  }

  static Drawable setSize(Drawable drawable, Size size)
  {
    return setSize(drawable, size.getWidth(), size.getHeight());
  }

  public static Bitmap getLargeIconBitmap(@DrawableRes int drawableId)
  {
    Size size = getLargeIconSize();
    Drawable drawable = setSize(getDrawable(drawableId), size);
    Bitmap bitmap = createBitmap(size);
    Canvas canvas = new Canvas(bitmap);
    drawable.draw(canvas);
    return bitmap;
  }

  static Drawable setSize(Drawable drawable, int width, int height)
  {
    drawable.setBounds(0, 0, width, height);
    return drawable;
  }

  public static Bitmap decodeSampledBitmapFromResource(String imageFile, int reqWidth,
                                                       int reqHeight)
  {
    //    XLog.i(TA//G, "Decoding Bitmap to " + reqWidth + " * " + reqHeight);
    // First decodeObject with inJustDecodeBounds=true to check dimensions
    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inJustDecodeBounds = true;
    BitmapFactory.decodeFile(imageFile, options);
    // Calculate inSampleSize
    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false;
    //    XLog.i(T//AG, "Decoding Bitmap to sample size = " + options.inSampleSize);
    return BitmapFactory.decodeFile(imageFile, options);
  }

  public static Bitmap getBitmap(byte[] bytes)
  {
    if (bytes == null) {
      return null;
    }
    return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
  }

  public static Bitmap loadGalleryImage(Uri uri)
  {
    try (InputStream is = BaseApp.req().getContentResolver()
      .openInputStream(uri))
    {
      Bitmap bmp = BitmapFactory.decodeStream(is);
      float rotation = rotationForImage(uri);
      return ImageUtils.rotateBitmap(bmp, rotation);
    } catch (Throwable throwable) {
      throw Util.rethrow("saving Image", throwable);
    }
  }

  public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
                                          int reqHeight)
  {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;
    if (height > reqHeight || width > reqWidth) {
      final int halfHeight = height / 2;
      final int halfWidth = width / 2;
      // Calculate the largest inSampleSize value that is a power of 2 and
      // keeps both
      // height and width larger than the requested height and width.
      while ((halfHeight / inSampleSize) > reqHeight &&
        (halfWidth / inSampleSize) > reqWidth) {
        inSampleSize *= 2;
      }
    }
    return inSampleSize;
  }

  public static Bitmap getBitmap(ParseFile file)
  {
    return getBitmap(file.getData());

  }

  public static Bitmap getBitmap(File file)
  {
    return getBitmap(IOUtil.fileToBytes(file));
  }

  public static ParseFile createParseFile(String fileName, Bitmap image)
  {
    CompressFormat format = CompressFormat.JPEG;
    return createParseFile(fileName, image, format);
  }

  public static ParseFile createParseFile(String fileName, Bitmap image,
                                          CompressFormat format)
  {
    Reflect.announce("fileName:   " + fileName);
    Reflect.announce("format:  " + format);
    String mimeType;
    switch (format) {
      case PNG:
        fileName = fileName + ".png";
        mimeType = "image/png";
        break;
      case JPEG:
        fileName = fileName + ".jpeg";
        mimeType = "image/jpeg";
        break;
      case WEBP_LOSSLESS:
      case WEBP_LOSSY:
      case WEBP:
        fileName = fileName + ".webp";
        mimeType = "image/webp";
        break;
      default:
        throw new RuntimeException("What the fuck?");
    }
    byte[] imageBits = ImageUtils.imageToBytes(image, format);
    Reflect.announce("bytes:   " + imageBits.length);
    return new ParseFile(fileName, imageBits, mimeType);
  }

  public static void queueUpload(File avatarFile, String objectId, String purpose)
  {
    try {
      BaseApp cell411 = BaseApp.req();
      SharedPreferences prefs = cell411.getSharedPreferences("files", Context.MODE_PRIVATE);
      Editor editor = prefs.edit();
      editor.putString("file", avatarFile.toString());
      editor.putString("objectId", objectId);
      editor.putString("purpose", purpose);
      editor.apply();
      sendFile();
    } catch (Throwable t) {
      t.printStackTrace();
    }
  }

  public static void sendFile()
  {
    BaseApp cell411 = BaseApp.req();
    SharedPreferences prefs = cell411.getSharedPreferences("files", Context.MODE_PRIVATE);
    if (prefs.getString("file", null) != null)
      cell411.getExec().execute(new SendFile(prefs));
  }

  public static void removePrefs(SharedPreferences prefs)
  {
    Editor editor = prefs.edit();
    editor.remove("file");
    editor.remove("purpose");
    editor.remove("objectId");
    editor.apply();
  }

  public static class SendFile
    implements Runnable,
               OnCompletionListener
  {
    SharedPreferences mPrefs;

    SendFile(SharedPreferences prefs)
    {
      mPrefs = prefs;
    }

    public void run()
    {
      BaseApp cell411 = BaseApp.req();
      try {
        File file = getFileInfo();
        if (file == null)
          return;
        HashMap<String, Object> params = getParams();
        if (params == null)
          return;
        params = ParseCloud.run("sendUpload", params);
        int port = Integer.parseInt(String.valueOf(params.get("port")));
        NetUtil.sendUpload(port, file);
        cell411.showYesNoDialog("Again?", "That was fun, do it again?", this);
      } catch (Exception e) {
        e.printStackTrace();
        cell411.showYesNoDialog("Failed",
          "Failed to upload file.  Try again later?", this);
      }
    }

    private HashMap<String, Object> getParams()
    {
      HashMap<String, Object> params = new HashMap<>();
      for (String key : new String[]{"purpose", "objectId"}) {
        String val = mPrefs.getString(key, null);
        if (val == null) {
          removePrefs(mPrefs);
          return null;
        }
        params.put(key, val);
      }
      return params;
    }


    private File getFileInfo()
    {
      String name = mPrefs.getString("file", null);
      if (name == null)
        return null;
      File file = new File(name);
      if (!file.exists()) {
        removePrefs(mPrefs);
        return null;
      }
      return file;
    }

    @Override
    public void done(boolean success)
    {
      if (success) {
        removePrefs(mPrefs);
      } else {
        getExecutor().schedule(this, 5000, TimeUnit.MILLISECONDS);
      }
    }
  }
}
