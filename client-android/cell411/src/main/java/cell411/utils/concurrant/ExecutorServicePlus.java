//package cell411.utils.concurrant;
//
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Future;
//
//import cell411.utils.WrapperException;
//
//public interface ExecutorServicePlus extends ExecutorService
//{
//  default void post(Runnable runnable)
//  {
//    execute(runnable);
//  }
//  default <T>
//  T get(Future<T> future) {
//    try
//    {
//      return future.get();
//    } catch (Exception e) {
//      throw new WrapperException("calling get",e);
//    }
//  }
//}
//