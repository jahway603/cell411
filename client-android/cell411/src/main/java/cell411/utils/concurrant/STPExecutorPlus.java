package cell411.utils.concurrant;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class STPExecutorPlus
  extends ScheduledThreadPoolExecutor
  implements ScheduledExecutorService
{
  static class SingleThreadFactory
    implements ThreadFactory
  {
    Thread mThread;
    SingleThreadFactory(Thread thread){
      mThread=thread;
    }

    @Override
    public Thread newThread(Runnable r) {
      Thread temp=mThread;
      mThread=null;
      return temp;
    }
  }
  public STPExecutorPlus(Thread thread) {
    this(1, new SingleThreadFactory(thread));
  }

  public STPExecutorPlus(int corePoolSize)
  {
    super(corePoolSize, Executors.defaultThreadFactory());
  }
  public STPExecutorPlus(int corePoolSize, ThreadFactory threadFactory)
  {
    super(corePoolSize, threadFactory);
  }

  public Future<Void> post(Runnable command) {
    return submit(command,null);
  }
  public ScheduledFuture<?> postDelayed(Runnable command, long delay){
    return schedule(command,delay, TimeUnit.MILLISECONDS);
  }
}
