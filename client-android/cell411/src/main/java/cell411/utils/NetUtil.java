package cell411.utils;

import androidx.exifinterface.media.ExifInterface;

import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.util.Objects;

import javax.annotation.Nonnull;

import cell411.libcell.ConfigDepot;
import okhttp3.*;

public class NetUtil {
  private static final XTAG TAG = new XTAG();

  public static String sendUpload(int port, byte[] upBytes) {
    try {
      String str = ConfigDepot.getParseUrl();
      assert str != null;
      URL url = UrlUtils.toURL(str);
      assert url != null;
      String host = url.getHost();
      assert host != null;
      Socket socket = new Socket(host, port);
      ExifInterface iFace = new ExifInterface(new ByteArrayInputStream(upBytes),
        ExifInterface.STREAM_TYPE_FULL_IMAGE_DATA);
      System.out.println(iFace.getAttribute("mimeType"));
      String result;
      try (OutputStream stream = socket.getOutputStream()) {
        InputStream input = socket.getInputStream();
        stream.write(upBytes);
        byte[] bytes = new byte[1024];
        socket.shutdownOutput();
        int len = input.read(bytes);
        if (len <= 0) {
          throw new RuntimeException("Expected response");
        }

        while (bytes[len - 1] == '\n') {
          --len;
        }

        int beg = 0;
        for (int pos = 0; pos < len; pos++) {
          if (bytes[pos] == ' ') {
            beg = pos + 1;
          }
        }
        result = new String(bytes, beg, len - beg);
        XLog.i(TAG, result);
        socket.close();
        return result;
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw Util.rethrow(ex);
    }
  }

  public static String get(final String url) {
    OkHttpClient client = ConfigDepot.getHttpClient();
    Request.Builder builder = new Request.Builder();
    builder.url(url);
    Request request = builder.build();
    Call call = client.newCall(request);
    try (
      Response response = call.execute()
    ) {
      if (!response.isSuccessful()) {
        throw new RuntimeException("Failed to connect to '" + url + "'");
      }
      ResponseBody body = Objects.requireNonNull(response.body());
      return Objects.requireNonNull(body.string());
    } catch (IOException e) {
      throw new RuntimeException("Connecting to '" + url + "'", e);
    }
  }

  @Nonnull
  public static String sendUpload(int port, final File file) {
    return sendUpload(port, IOUtil.fileToStream(file));
  }

  @Nonnull
  public static String sendUpload(int port, final InputStream fileInputStream) {
    return sendUpload(port, IOUtil.streamToBytes(fileInputStream));
  }

  public static void sendMail(String f, String s, String t) {
    SMTPSender sender = new SMTPSender(f, s, t);
    ConfigDepot.getExecutor().execute(sender);
  }

}

