package cell411.utils;

import android.os.Handler;
import cell411.libcell.ConfigDepot;
import cell411.utils.concurrant.STPExecutorPlus;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class MessageService {
  private static MessageService xmService;
  //  final HashSet<MessageListener> listeners = new HashSet<>();
  final ArrayList<LogMessage> recycled = new ArrayList<>();
  final ArrayList<LogMessage> smAllMessages = new ArrayList<>();
  int mGetMessageCalls = 0;
  int mCreated = 0;
  private Handler smHandler;

  public MessageService() {
  }

  public synchronized static MessageService getService() {
    if (xmService == null) {
      xmService = new MessageService();
    }
    return xmService;
  }

  public void post(Runnable runnable) {
    getHandler().execute(runnable);
  }

  public synchronized STPExecutorPlus getHandler() {
    return ConfigDepot.getExecutor();
  }

  //  public void postMessage(String string) {
  //    for (MessageListener listener : listeners) {
  //      listener.accept(string);
  //    }
  //  }

  //  public void addListener(MessageListener listener) {
  //    getHandler().post(() -> {
  //      listeners.add(listener);
  //    });
  //  }
  //
  //  public void removeListener(MessageListener listener) {
  //    getHandler().post(() -> {
  //      while (listeners.remove(listener))
  //        Util.nop();
  //    });
  //  }

  public LogMessage getMessage() {
    synchronized (recycled) {
      LogMessage result = null;
      while (result == null) {
        ++mGetMessageCalls;
        if (recycled.isEmpty()) {
          ++mCreated;
          result = new LogMessage();
        } else {
          result = recycled.remove(recycled.size() - 1);
        }
      }
      //      System.out.println("result: "+result.toString());
      return result;
    }
  }

  public void send(int code, XTAG tag, String format, Throwable tr, Object[] args) {
    LogMessage m = getMessage();
    m.mThread = Thread.currentThread();
    m.mCode = code;
    m.mTag = tag;
    m.mArgs = args;
    m.mFormat = format;
    m.mExcept = tr;
    m.mTime = System.currentTimeMillis();
    switch (m.mCode) {
      case XLog.INFO:
      case XLog.DEBUG:
      case XLog.ERROR:
      case XLog.WARN:
      case XLog.VERBOSE:
      case XLog.ASSERT:
        break;
      default:
        throw new RuntimeException("Bad log level value: " + m.mCode);
    }
    m.run();
    //getHandler().execute(m);
  }

  public void recycle(final LogMessage logMessage) {
    logMessage.reset();
    recycled.add(logMessage);
  }

  public void postDelayed(final Runnable runnable, final int i) {
    getHandler().schedule(runnable, i, TimeUnit.MILLISECONDS);
  }

  //  public interface MessageListener {
  //    void accept(String logmsg);
  //  }
}
