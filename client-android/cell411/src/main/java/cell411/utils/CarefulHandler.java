package cell411.utils;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

public class CarefulHandler
  extends Handler
  implements ExceptionHandler, ExecutorService
{
  //  final ArrayList<Message> mMessages = new ArrayList<>();
  int mDispatched = 0;
  int mSent = 0;

  public CarefulHandler(Looper looper) {
    super(looper);//, new LoggingCallback());
  }

  public CarefulHandler(HandlerThread thread) {
    this(thread.getLooper());
  }

  @Override
  public void dispatchMessage(@Nonnull Message msg) {
//    boolean removed = mMessages.remove(msg);
    //    assert removed;
    try {
      mDispatched++;
      //      Runnable runnable = msg.getCallback();
      //      String string = String.valueOf(runnable);
      //      System.out.printf("%40s (D) => %40s\n",Thread.currentThread(), string);
      super.dispatchMessage(msg);
    } catch (Throwable t) {
      handleException("" + msg, t);
    }
  }

  @Override
  public boolean sendMessageAtTime(@Nonnull Message msg, long uptimeMillis) {
    ++mSent;
    if (Util.theGovernmentIsHonest()) {
      PrintString ps = new PrintString();
      new Throwable().printStackTrace(ps);
      assert msg.obj == null;
      msg.obj = ps.toString();
//      mMessages.add(msg);
    }
    return super.sendMessageAtTime(msg, uptimeMillis);
  }

  @Nonnull
  public String toString() {
    return Util.format("[CarefulHandler: %s; %d s, %d d]", getLooper().getThread(), mSent,
      mDispatched);
  }

  public boolean isCurrentThread() {
    return getLooper().isCurrentThread();
  }

  @Override
  public void execute(Runnable command) {
    post(command);
  }

  @Override
  public void shutdown() {

  }

  @Override
  public List<Runnable> shutdownNow() {
    return null;
  }

  @Override
  public boolean isShutdown() {
    return false;
  }

  @Override
  public boolean isTerminated() {
    return false;
  }

  @Override
  public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
    return false;
  }

  public <T> Future<T> submit(Callable<T> task) {
    if (task == null) throw new NullPointerException();
    RunnableFuture<T> ftask = newTaskFor(task);
    execute(ftask);
    return ftask;
  }


  /**
   * @throws RejectedExecutionException {@inheritDoc}
   * @throws NullPointerException       {@inheritDoc}
   */
  public <T> Future<T> submit(Runnable task, T result) {
    if (task == null) throw new NullPointerException();
    RunnableFuture<T> ftask = newTaskFor(task, result);
    execute(ftask);
    return ftask;
  }

  protected <T> RunnableFuture<T>
  newTaskFor(Callable<T> task) {
    return new FutureTask<>(task);
  }

  protected <T> RunnableFuture<T>
  newTaskFor(Runnable runnable, T value) {
    return new FutureTask<>(runnable, value);
  }

  /**
   * @throws RejectedExecutionException {@inheritDoc}
   * @throws NullPointerException       {@inheritDoc}
   */
  public Future<?> submit(Runnable task) {
    if (task == null) throw new NullPointerException();
    RunnableFuture<Void> futureTask = newTaskFor(task, null);
    execute(futureTask);
    return futureTask;
  }

  public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks)
      throws InterruptedException {
    if (tasks == null)
      throw new NullPointerException();
    ArrayList<Future<T>> futures = new ArrayList<>(tasks.size());
    try {
      for (Callable<T> t : tasks) {
        RunnableFuture<T> f = newTaskFor(t);
        futures.add(f);
        execute(f);
      }
      for (int i = 0, size = futures.size(); i < size; i++) {
        Future<T> f = futures.get(i);
        if (!f.isDone()) {
          try { f.get(); }
          catch (CancellationException | ExecutionException ignore) {}
        }
      }
      return futures;
    } catch (Throwable t) {
      cancelAll(futures);
      throw t;
    }
  }

  public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks,
                                       long timeout, TimeUnit unit)
      throws InterruptedException {
    if (tasks == null)
      throw new NullPointerException();
    final long nanos = unit.toNanos(timeout);
    final long deadline = System.nanoTime() + nanos;
    ArrayList<Future<T>> futures = new ArrayList<>(tasks.size());
    int j = 0;
    timedOut: try {
      for (Callable<T> t : tasks)
        futures.add(newTaskFor(t));

      final int size = futures.size();

      // Interleave time checks and calls to execute in case
      // executor doesn't have any/much parallelism.
      for (int i = 0; i < size; i++) {
        if (((i == 0) ? nanos : deadline - System.nanoTime()) <= 0L)
          break timedOut;
        execute((Runnable)futures.get(i));
      }
      for (; j < size; j++) {
        Future<T> f = futures.get(j);
        if (!f.isDone()) {
          try { f.get(deadline - System.nanoTime(), NANOSECONDS); }
          catch (CancellationException | ExecutionException ignore) {}
          catch (TimeoutException timedOut) {
            break timedOut;
          }
        }
      }
      return futures;
    } catch (Throwable t) {
      cancelAll(futures);
      throw t;
    }
    // Timed out before all the tasks could be completed; cancel remaining
    cancelAll(futures, j);
    return futures;
  }

  @Override
  public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
      throws ExecutionException
  {
    throw new ExecutionException(new Exception("Not Implemented"));
  }
  public <T> T invokeAny(Collection<? extends Callable<T>> tasks)
      throws ExecutionException
  {
    throw new ExecutionException(new Exception("Not Implemented"));
  }
  private static <T> void cancelAll(ArrayList<Future<T>> futures) {
    cancelAll(futures,0);
  }
  /** Cancels all futures with index at least j. */
  private static <T> void cancelAll(ArrayList<Future<T>> futures, int j) {
    for (int size = futures.size(); j < size; j++)
      futures.get(j).cancel(true);
  }
}
