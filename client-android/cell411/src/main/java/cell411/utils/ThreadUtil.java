package cell411.utils;

import cell411.utils.func.Func0;

public class ThreadUtil {

  public static void waitUntil(Object o, Func0<Boolean> cond) {
    waitUntil(o, cond, 2000);
  }

  public static void waitUntil(Object o, Func0<Boolean> cond, long timeout) {
    //noinspection SynchronizationOnLocalVariableOrMethodParameter
    synchronized (o) {
      while (!cond.apply()) {
        wait(o, timeout);
      }
    }
  }

  @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")

  public static void sleep(long i) {
    if (i < 1) {
      throw new IllegalArgumentException("Sleep should have a duration of at least 1");
    }
    sleepUntil(System.currentTimeMillis() + i);
  }

  public static void sleepUntil(final long endTime) {
    long millis = System.currentTimeMillis();
    while (millis < endTime) {
      try {
        Thread.sleep(endTime - System.currentTimeMillis());
      } catch (InterruptedException ignored) {
      }
      millis = System.currentTimeMillis();
    }
  }

  public static void join(Thread thread) {
    join(thread, 0);
  }

  public static void join(Thread thread, long time) {
    long startTime = System.currentTimeMillis();
    try {
      thread.join(time);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("Waited: " + (System.currentTimeMillis() - startTime));
  }

  @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
  public static void notify(final Object o, boolean all) {
    synchronized (o) {
      if (all) {
        o.notifyAll();
      } else {
        o.notify();
      }
    }
  }

  public static void sleep(Object o, int t) {
    try {
      Thread.sleep(t);
    } catch (Exception e) {
      System.out.println("ex: " + e);
    }
  }

  public static boolean wait(Object o)
  {
    return wait(o,-1);
  }
  @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
  public static boolean wait(Object o, long timeout) {
    long endTime = System.currentTimeMillis() + timeout;
    synchronized (o) {
      try {
        if(timeout<0){
          o.wait();
        } else {
          o.wait(timeout);
        }
        return true;
      } catch (InterruptedException e) {
        e.printStackTrace();
        return false;
      }
    }
  }
}