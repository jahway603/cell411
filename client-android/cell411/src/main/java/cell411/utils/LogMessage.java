package cell411.utils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@SuppressWarnings("FieldMayBeFinal")
class LogMessage implements Runnable {
  public static int mThreadWidth = 0;
  static int mTagWidth = 0;
  private static String mMsgFormat;
  private static long mLastMessage = System.currentTimeMillis();
  private static Runnable smHeartBeat;

  public long mTime = System.currentTimeMillis();
  public Object[] mArgs;
  public int mCode;
  public Throwable mExcept;
  public String mFormat;
  public XTAG mTag;
  public Thread mThread;

  LogMessage() {
    getService().smAllMessages.add(this);
  }

  public static MessageService getService() {
    return MessageService.getService();
  }

  //  static XTAG mHeartBeatTag;
  public static void send(int code, @Nonnull XTAG tag, @Nonnull String format,
                          @Nullable Throwable tr, @Nullable Object[] args) {
    mLastMessage = System.currentTimeMillis();
    //    final long finalLastMessage = mLastMessage;
    //    smHeartBeat=new Runnable()
    //    {
    //      @Override
    //      public void run()
    //      {
    //        if(smHeartBeat!=this)
    //          return;
    //        if(mLastMessage==finalLastMessage) {
    //          if(mHeartBeatTag==null)
    //            mHeartBeatTag=new XTAG("HeartBeat");
    //          XLog.i(mHeartBeatTag, "HeartBeat");
    //        }
    //      }
    //    };
    //    getService().postDelayed(smHeartBeat, 29997);
    getService().send(code, tag, format, tr, args);
  }

  public static void send(int code, @Nonnull String tag, @Nonnull String format,
                          @Nullable Throwable tr, @Nullable Object[] args) {
    send(code, new XTAG(tag), format, tr, args);
  }

  void reset() {
    mArgs = null;
    mCode = 0;
    mExcept = null;
    mFormat = null;
    mTag = null;
    mThread = null;
    mTime = 0;
  }

  @Nonnull
  public String toString() {
    String prefix = tag();
    String msg = message();
    return mCode + "/" + prefix + ": " + msg;
  }

  private String tag() {
    String str;
    if (mTag == null || mTag.s == null) {
      str = "null";
    } else {
      str = mTag.s;
    }
    if (str.length() + 3 > mTagWidth) {
      mTagWidth = str.length() + 3;
    }
    byte[] buffer = new byte[mTagWidth];
    System.arraycopy(str.getBytes(), 0, buffer, 0, str.length());
    for (int i = str.length(); i < buffer.length; i++) {
      buffer[i] = '.';
    }
    return new String(buffer);
  }

  @Nonnull
  private String message() {
    String xPrefix;
    try {
      long time = mTime - XLog.smStart;
      long eTime = mTime - XLog.smLast;
      XLog.smLast = mTime;
      String thread = String.valueOf(mThread);
      if (thread.length() > mThreadWidth) {
        mMsgFormat = "%-" + thread.length() + "s: %8d: %8d: %s";
        mThreadWidth = thread.length();
      }
      String msg = Util.format(mFormat, mArgs);
      xPrefix = Util.format(mMsgFormat, mThread, time, eTime, msg);
    } catch (Exception e) {
      xPrefix = "<Exception Making Message>";
    }
    return xPrefix;
  }

  @Override
  public void run() {
    String prefix = tag();
    String message = message();
    switch (mCode) {
      case XLog.INFO:
      case XLog.DEBUG:
      case XLog.ERROR:
      case XLog.WARN:
      case XLog.VERBOSE:
      case XLog.ASSERT:
        if(!message.endsWith("\n"))
          message=message+"\n";
        System.out.printf("%s: %s", prefix, message);
//        Log.println(mCode, prefix, message);
        break;
      default:
        throw new RuntimeException("No code matches: "+mCode);
    }
    getService().recycle(this);
  }
}
