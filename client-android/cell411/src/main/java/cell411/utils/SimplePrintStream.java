package cell411.utils;

import androidx.annotation.CallSuper;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.time.Instant;
import java.util.Locale;

public class SimplePrintStream
  extends PrintStream
  implements ExceptionHandler
{
  final protected OutputStream out;
  static class TLData {
    static int smSerial=100;
    final int mSerial=++smSerial;
    boolean mFirstLine=true;
    ByteArrayOutputStream mBuf = new ByteArrayOutputStream();
  }
  final ThreadLocal<TLData> smTLData =
    ThreadLocal.withInitial(TLData::new);

  public SimplePrintStream(OutputStream out)
  {
    super(new TrapStream());
    this.out =out;
  }

  public SimplePrintStream(OutputStream out, boolean autoFlush)
  {
    super(new TrapStream(),true);
    this.out =out;
  }

  @Override
  public void flush()
  {
  }

  @Override
  public void close()
  {
  }

  public boolean checkError()
  {
    return false;
  }

  protected void setError()
  {
  }

  protected void clearError()
  {
  }

  public void write(final byte[] buf)
  {
    for (byte value : buf)
      write(value);
  }

  public void write(byte[] bs, boolean nl)
  {
    write(bs);
    if(nl)
      write(10);
  }

  public void write(byte[] bs, int off, int len)
  {
    for(int i=0;i<len;i++)
      write(bs[off+i]);
  }

  byte[] smTrue = "true\n".getBytes();
  byte[] smNull = "null\n".getBytes();
  byte[] smFalse = "false\n".getBytes();
  public void print(boolean b)
  {
    byte[] str = (b ? smTrue : smFalse);
    write(str, 0,str.length-1);
  }
  public void println(boolean x)
  {
    write(x?smTrue:smFalse);
  }

  @Override
  public void print(char c)
  {
    write(c);
  }

  @Override
  public void print(int i)
  {
    printf("%d",i);
  }

  @Override
  public void print(long l)
  {
    printf("%d",l);
  }

  @Override
  public void print(float f)
  {
    printf("%f",f);
  }

  @Override public void print(double d)
  {
    printf("%lf",d);
  }

  @Override
  public void print(char[] s)
  {
    for (char c : s) {
      write(c);
    }
  }

  @Override
  public void print(String s)
  {
    if(s==null)
      write(smNull);
    else
      write(s.getBytes());
  }

  @Override
  public void print(Object obj)
  {
    printf("%s",obj);
  }

  @Override
  public void println()
  {
    write(10);
  }


  @Override
  public void println(char x)
  {
    write(x);
    write(10);
  }

  @Override
  public void println(int x)
  {
    printf("%d\n",x);
  }

  @Override
  public void println(long x)
  {
    printf("%d\n",x);
  }

  @Override
  public void println(float x)
  {
    printf("%f\n",x);
  }

  @Override
  public void println(double x)
  {
    printf("%d",x);
  }

  @Override
  public void println(char[] x)
  {
    for (char c : x) {
      write(c);
    }
  }

  @Override
  public void println(String x)
  {
    write((x+"\n").getBytes());
  }

  @Override
  public void println(Object x)
  {
    printf("%s\n",x);
  }

  @Override
  public PrintStream printf(String format, Object... args)
  {
    print(Util.format(format,args));
    return this;
  }

  @Override
  public PrintStream printf(Locale l, String format, Object... args)
  {
    return format(l,format,args);
  }

  @Override
  public PrintStream format(String format, Object... args)
  {
    return super.format(format, args);
  }

  @Override
  public PrintStream format(Locale l, String format, Object... args)
  {
    return super.format(l, format, args);
  }

  @NotNull
  @Override
  public PrintStream append(CharSequence csq)
  {
    return super.append(csq);
  }

  @NotNull
  @Override
  public PrintStream append(CharSequence csq, int start, int end)
  {
    return super.append(csq, start, end);
  }

  @NotNull
  @Override
  public PrintStream append(char c)
  {
    return super.append(c);
  }

  public void write(byte[] bs, int off, int len, boolean nl)
  {
    write(bs,off,len);
    if(nl)
      write(10);
  }

  public void write(int b)
  {
    if(b==0)
      return;
    TLData tld = smTLData.get();
    assert tld!=null;
    if(tld.mBuf.size()==0){
      tld.mBuf.write('|');
      String str = Util.format(" %s | %4d | ",Instant.now(), tld.mSerial);
      if(tld.mFirstLine){
        tld.mFirstLine=false;
        str=str+" | Thread="+Thread.currentThread()+"\n|"+str;
        try {
          tld.mBuf.write(str.getBytes());
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
      startLine();
    }
    tld.mBuf.write(b);
    if (b != 10)
      return;
    try {
      endLine();
      synchronized(out) {
        out.write(tld.mBuf.toByteArray());
        out.flush();
      }
      tld.mBuf.reset();
    } catch (IOException e) {
      throw Util.rethrow(e);
    }
  }

  @CallSuper
  public void endLine()
  {
  }

  @CallSuper
  public void startLine()
  {

  }

  public void write(int ch, boolean nl)
  {
    write(ch);
    if(nl)
      write(10);
  }

  private static class TrapStream
    extends OutputStream
  {
    @Override
    public void write(int b)
    {
      throw new RuntimeException("It's a trap!");
    }
  }
}
