package cell411.utils;

import android.os.Handler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

@SuppressWarnings("unused")
public class ObservableValue<X> implements ValueObserver<X> {
  //  private final WeakList<ValueObserver<ValueType>> mObservers = new WeakList<>();
  private final HashSet<ValueObserver<? super X>> mObservers = new HashSet<>();
  private final Class<X> mType;
  private X mValue;
  private Handler mHandler;

  public ObservableValue()
  {
    this(null,null,null);
  }
  public ObservableValue(@Nonnull X value) {
    this(null,null,value);
  }

  public ObservableValue(Class<X> type) {
    this(null,type,null);
  }

  public ObservableValue(Class<X> type, X value) {
    this(null,type,value);
  }

  public ObservableValue(Handler handler, Class<X> type, X value) {
    mHandler = handler;
    mType = type;
    mValue = value;
  }

  void setHandler(Handler handler){
    mHandler=handler;
  }

  public boolean set(X newValue) {
    if (mValue == newValue) {
      return false;
    }
    if (mValue != null && mValue.equals(newValue)) {
      return false;
    }
    X oldValue = mValue;
    mValue = newValue;
    fireStateChange(oldValue);
    return true;
  }

  @Nonnull
  public String toString() {
    return "Observable[" + mValue + "]";
  }

  public synchronized void addObserver(ValueObserver<? super X> observer) {
    mObservers.add(observer);
  }

  public synchronized void removeObserver(ValueObserver<? super X> observer) {
    mObservers.remove(observer);
  }

  public ValueObserver<X> getObserver() {
    return (newValue, oldValue) -> set(newValue);
  }

  public X get() {
    return mValue;
  }

  synchronized boolean observed() {
    return !mObservers.isEmpty();
  }

  synchronized List<ValueObserver<? super X>> copyObservers() {
    ArrayList<ValueObserver<? super X>> result = null;
    Iterator<ValueObserver<? super X>> iterator = mObservers.iterator();
    while (iterator.hasNext()) {
      ValueObserver<? super X> object = iterator.next();
      if (object == null) {
        iterator.remove();
      } else {
        if (result == null) {
          result = new ArrayList<>();
        }
        result.add(object);
      }
    }
    if (result == null) {
      return new ArrayList<>();
    }
    return result;
  }

  private void fireStateChange(X oldValue) {
    if (mHandler != null && !mHandler.getLooper().isCurrentThread()) {
      mHandler.post(() -> fireStateChange(oldValue));
    } else {
      X newValue = get();
      List<ValueObserver<? super X>> observers = copyObservers();
      for (ValueObserver<? super X> observer : observers) {
        if (observer != null) {
          observer.onChange(newValue, oldValue);
        }
      }
    }
  }

  public int countObservers() {
    return mObservers.size();
  }

  @Override
  public void onChange(@Nullable X newValue, @Nullable X oldValue) {
    set(newValue);
  }

  /**
   * Just like addObserver, except you get a notification right away
   * with the value at subscription time.
   * </p>
   * If you need to, you can recognize the initial event because it
   * carries the same value for oldValue and newValue.
   *
   * @param immediate want that immediate notification?
   * @param bitmapValueObserver who wants to know?
   */
  public void addObserver(boolean immediate,
                          ValueObserver<X> bitmapValueObserver)
  {
    addObserver(bitmapValueObserver);
    bitmapValueObserver.onChange(get(),get());
  }
}
