package cell411.utils;

import cell411.utils.func.Func1V;

public interface OnCompletionListener
  extends Func1V<Boolean>
{
  void done(boolean success);

  default Runnable getRunnable(boolean success) {
    return getClosure(success);
  }

  @Override
  default void apply(Boolean aBoolean) {
    done(aBoolean);
  };
}
