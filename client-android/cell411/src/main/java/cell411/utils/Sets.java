package cell411.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Sets {
  public static <T>
  ArrayList<T> findCommon(Set<T> lhs, Set<T> rhs) {
    HashSet<T> com = union(lhs, rhs);
    com.retainAll(lhs);
    com.retainAll(rhs);
    lhs.removeAll(com);
    rhs.removeAll(com);
    ArrayList<T> res = new ArrayList<>(com);
    res.add(null);
    res.addAll(lhs);
    res.add(null);
    res.addAll(rhs);
    return res;
  }

  private static <T>
  HashSet<T> union(Set<T> lhs, Set<T> rhs) {
    return Collect.addAll(new HashSet<>(lhs), rhs);
  }
}
