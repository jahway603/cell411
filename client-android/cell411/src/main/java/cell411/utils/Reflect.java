package cell411.utils;

import android.os.PowerManager;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nonnull;

@SuppressWarnings("unused")
public class Reflect {
  private static final List<Class<?>> smBoxers = Collections.unmodifiableList(
    Arrays.asList(Boolean.class, Boolean.TYPE, Byte.class, Byte.TYPE, Character.class,
      Character.TYPE, Double.class, Double.TYPE, Float.class, Float.TYPE, Integer.class,
      Integer.TYPE, Long.class, Long.TYPE, Short.class, Short.TYPE, Void.class,
      Void.TYPE));
  static Map<Class<?>, Class<?>> smPrimitive = createPrimMap();
  static Comparator<Class<?>> smSimpleNameCompare =
    Comparator.comparing(Class::getSimpleName);
  static Comparator<Class<?>> smNameCompare =
    Comparator.comparing(Class::getName);
  static Comparator<Class<?>> smFullClassCompare;

  static {
    smFullClassCompare = smSimpleNameCompare.thenComparing(smNameCompare);
    smFullClassCompare = smFullClassCompare.thenComparing(Objects::hashCode);
  }

  public static void log(XTAG tag, String msg) {
    System.out.println(tag + ": " + msg);
  }

  public static void announce(boolean b) {
    String text = announceStr(1, b);
    XTAG tag = getTag(1);
    log(tag, text);
  }

  public static String announce() {
    String text = announceStr(1, null);
    XTAG tag = getTag(1);
    log(tag, text);
    return text;
  }

  public static void announce(Object obj) {
    log(getTag(1), announceStr(1, obj));
  }

  public static String announceStr(Object obj) {
    String announceStr = announceStr(1, null);
    return announceStr + "  " + obj;
  }

  public static String announceStr(int i, Object obj) {
    String announceStr = announceStr(1 + i, null);
    return announceStr + "  " + obj;
  }

  public static String announceStr(int i, Boolean b) {
    StackTraceElement pos = Reflect.currentStackPos(i + 1);
    String prefix;
    if (b == null) {
      prefix = "X ";
    } else if (b) {
      prefix = "I ";
    } else {
      prefix = "O ";
    }
    String posStr = String.valueOf(pos);
    int index = posStr.lastIndexOf('(');
    index = posStr.lastIndexOf('.', index);
    posStr = posStr.substring(index);
    return prefix + posStr;
  }

  public static String announceStr(Boolean b) {
    return announceStr(1, b);
  }

  public static String currentSimpleClassName() {
    return currentSimpleClassName(1);
  }

  public static String currentMethodName() {
    return currentMethodName(1);
  }

  private static String currentClassName(int i) {
    return currentStackPos(i + 1).getClassName();
  }

  public static String currentSimpleClassName(int i) {
    String fullClassName = currentClassName(i + 1);
    int pos = fullClassName.lastIndexOf('.') + 1;
    String res = fullClassName.substring(pos);
    assert res.length() > 0;
    return res;
  }

  public static String currentMethodName(int i) {
    String res = currentStackPos(i + 1).getMethodName();
    if (res.equals("<init>")) {
      return currentSimpleClassName(i + 1);
    } else {
      return res;
    }
  }

  public static StackTraceElement currentStackPos(int i) {
    Exception ex = new Exception();
    StackTraceElement[] trace = ex.getStackTrace();
    if (trace.length < 2 + i) {
      throw new RuntimeException("trace.length<" + (3 + i) + "!");
    }
    return trace[1 + i];
  }

  public static StackTraceElement currentStackPos() {
    return currentStackPos(1);
  }

  public static Method findStaticMethod(Class<?> clazz, String name) {
    for (Method method : clazz.getDeclaredMethods()) {
      if (!method.getName().equals(name)) {
        continue;
      }
      if (method.getTypeParameters().length != 0) {
        continue;
      }
      int modifiers = method.getModifiers();
      if ((modifiers & Modifier.STATIC) == 0) {
        continue;
      }
      return method;
    }
    return null;
  }

  public static String getTag() {
    return currentSimpleClassName(1);
  }

  public static XTAG getTag(int i) {
    return new XTAG(i + 1);
  }

  public static void stackTrace(PrintString ps, StackTraceElement[] trace) {
    stackTrace(ps, "", trace);
  }

  public static void stackTrace(PrintString ps, String firstLine, StackTraceElement[] trace) {
    if (firstLine != null && !firstLine.isEmpty()) {
      ps.pl(firstLine);
    }
    for (StackTraceElement traceElement : trace) {
      ps.p("\tat ").pl(traceElement);
    }
  }

  // for static fields
  public static int getInt(Class<PowerManager> type, String name) {
    return getInt(type, null, name);
  }

  public static int getInt(Class<PowerManager> type, Object obj, String name) {
    try {
      Field field = type.getField(name);
      return field.getInt(obj);
    } catch (Exception ex) {
      throw new RuntimeException("reading field: " + name, ex);
    }
  }

  static public boolean hasDeclaredMethod(Class<?> type, String name) {
    Method[] methods = type.getDeclaredMethods();
    for (Method method : methods) {
      if (method.getName().equals(name)) {
        return true;
      }
    }
    return false;

  }

  static public boolean hasDeclaredMethod(Object object, String name) {
    return object != null && hasDeclaredMethod(object.getClass(), name);
  }

  public static String getSourceLoc(int i) {
    Throwable throwable = new Throwable();
    StackTraceElement[] stackTrace = throwable.getStackTrace();
    return "\tat " + stackTrace[i];
  }

  static Class<?> getClass(Object x) {
    if (x == null) {
      throw new NullPointerException("target must be Class (for static method) or Object");
    }
    if (x instanceof Class<?>) {
      return (Class<?>) x;
    } else {
      return x.getClass();
    }
  }

  static public Class<?>[] getTypes(Object[] args) {
    Class<?>[] types = new Class<?>[args.length];
    int i = 0;
    for (Object arg : args) {
      if (arg == null) {
        types[i++] = Void.TYPE;
      } else {
        types[i++] = arg.getClass();
      }
    }
    return types;
  }

  public static Method getMethod(Class<?> type, Object target, final String name, Object[] args) {
    if (args == null) {
      return getMethod(type, target, name, new Object[0]);
    }
    Exception ex = null;
    ArrayList<Method> methods = new ArrayList<>();
    Collect.addAll(methods, type.getMethods());
    methods.removeIf(method -> !matches(method, target, name, args));
    if (methods.isEmpty()) {
      PrintString ps = new PrintString();
      ps.pl("Cannot find any method in class " + type);
      ps.pl("    or for target " + target);
      ps.pl("    named '" + name + "'");
      ps.pl("    matching: " + args.length + " args");
      for (int i = 0; i < args.length; i++) {
        ps.pl("        #" + i + ": " + args[i]);
      }
      throw Util.rethrow(new NoSuchMethodException(ps.toString()));
    }
    return methods.get(0);
  }

  public static boolean matches(Method method, Object object, String name, Object[] args) {
    if (!method.getName().equals(name)) {
      return false;
    }
    if (Modifier.isStatic(method.getModifiers()) != (object == null)) {
      return false;
    }
    if (!Modifier.isPublic(method.getModifiers())) {
      return false;
    }
    if (method.getParameterCount() != args.length) {
      return false;
    }
    Class<?>[] types = method.getParameterTypes();
    for (int i = 0; i < types.length; i++) {
      if (canConvert(args[i], types[i])) {
        continue;
      }
      if (canConvert(args[i], smPrimitive.get(types[i]))) {
        continue;
      }
      return false;
    }
    return true;
  }

  static Map<Class<?>, Class<?>> createPrimMap() {
    HashMap<Class<?>, Class<?>> back = new HashMap<>();
    Map<Class<?>, Class<?>> res = Collections.unmodifiableMap(back);
    for (int i = 0; i < smBoxers.size(); i += 2) {
      Class<?> boxed = smBoxers.get(i);
      Class<?> unboxed = smBoxers.get(i + 1);
      back.put(boxed, unboxed);
      back.put(unboxed, boxed);
    }
    return res;
  }

  public static boolean canConvert(Object arg, Class<?> type) {
    if (arg == null) {
      return Object.class.isAssignableFrom(type);
    } else if (smPrimitive.get(type) == arg.getClass()) {
      return true;
    } else {
      return type.isAssignableFrom(arg.getClass());
    }
  }

  public static void announce(String s, Object... args) {
    if (args.length > 0) {
      s = Util.format(s, args);
    }
    log(getTag(1), announceStr(1, s));
  }

  public static String[] getFieldNames(Class<?> core) {
    Field[] fields = core.getFields();
    String[] names = new String[fields.length];
    for (int i = 0; i < fields.length; i++) {
      names[i] = fields[i].getName();
    }
    return names;
  }

  public static <X>
  X readField(Object obj, String name, Class<X> type) {
    try {
      Field field = type.getField(name);
      return type.cast(field.get(obj));
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }

  public static <X>
  GetSet<X> getGetSet(Object obj, String name, Class<X> type) {
    try {
      Class<?> owningType = obj.getClass();
      Field field = owningType.getField(name);
      return new GetSet<>(obj, field, type);
    } catch (Exception e) {
      throw Util.rethrow(e);
    }
  }

  public static class GetSet<X> {
    private final Object mObject;
    private final Class<X> mType;
    private final Field mField;

    public GetSet(Object object, Field field, Class<X> type) {
      mObject = object;
      mType = type;
      mField = field;
    }

    public X get() {
      try {
        return mType.cast(mField.get(mObject));
      } catch (IllegalAccessException e) {
        throw Util.rethrow(e);
      }
    }

    public void set(X res) {
      try {
        mField.set(mObject, res);
      } catch (IllegalAccessException e) {
        throw Util.rethrow(e);
      }
    }

    @Nonnull
    public String toString() {
      return "GetSet<" + mType.getSimpleName() + ">." + mField.getName();
    }

    public String getName() {
      return mField.getName();
    }

    @SuppressWarnings("unchecked")
    public Class<? super X> getType() {
      return (Class<? super X>) mField.getType();
    }
  }
}

