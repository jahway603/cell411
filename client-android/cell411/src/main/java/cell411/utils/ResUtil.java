package cell411.utils;

import cell411.ui.base.BaseApp;

public class ResUtil
{
  public static int getColor(final int resId) {
    BaseApp app = BaseApp.req();
    if (app == null) {
      return 0xffffff;
    } else {
      return app.getColor(resId);
    }
  }
}
