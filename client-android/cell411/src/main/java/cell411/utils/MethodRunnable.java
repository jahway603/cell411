package cell411.utils;

import cell411.utils.func.Func0;
import cell411.utils.func.Func0V;
import cell411.utils.func.Func1;
import cell411.utils.func.Func1V;
import cell411.utils.func.Func2;
import cell411.utils.func.Func2V;
import cell411.utils.func.Func3;
import cell411.utils.func.Func3V;
import cell411.utils.func.Func4;
import cell411.utils.func.Func4V;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.function.Supplier;

public class MethodRunnable
  implements Func0V
{
  private final Object mTarget;
  private final Object[] mArgs;
  private final Method mMethod;
  private final Class<?> mType;
  private Object mRes=Void.class;

  private MethodRunnable(Object target, Method method,
                         Object... args)
  {
    mType = Reflect.getClass(target);
    mTarget = (Reflect.getClass(target) == target) ? null : target;
    mArgs = args;
    mMethod = method;
  }
  private MethodRunnable(Class<?> type, Method method, Object... args)
  {
    mType = type;
    mTarget = null;
    mArgs = args;
    mMethod = method;
  }


  public static
  MethodRunnable forStatic(Class<?> type, String name, Object ... args)
  {
    Method method = getMethod(type,null,name,args);
    return new MethodRunnable(type,method);
  }
  public static <X>
  MethodRunnable forVirtual(Object target, String name, Object ... args)
  {
    Method method = getMethod(target.getClass(),target,name,args);
    return new MethodRunnable(target,method,args);
  }

  static Method getMethod(@Nonnull Class<?> type,
                          @Nullable Object target,
                          @Nonnull String name,
                          Object ... args)
  {
    return Reflect.getMethod(type, target, name, args);
  }
  public static <X>
  MethodRunnable forSupplier(Supplier<X> supplier)
  {
    Method method=getMethod(Supplier.class,supplier,"get");
    return new MethodRunnable(supplier,method);
  }
  public static <X>
  MethodRunnable forRunnable(Runnable runnable, X res)
  {
    Method method = getMethod(Runnable.class,runnable,"run");
    MethodRunnable mr = new MethodRunnable(runnable,method);
    mr.setRes(res);
    return mr;
  }

  private void setRes(Object res)
  {
    mRes=res;
  }
  public Object getRes() {
    if(mRes==Void.class)
      run();
    assert mRes!=Void.class;
    return mRes;
  }

  public static <X>
  MethodRunnable forFunc(Func0V func, X res){
    return forFunc(func.returning(res));
  }
  public static <X,A1>
  MethodRunnable forFunc(Func1V<A1> func, A1 a1){
    return forFunc(func.getClosure(a1),(X)null);
  }
  public static <X>
  MethodRunnable forFunc(Func0<X> func){
    return forSupplier(func);
  }
  public static <X,A1>
  MethodRunnable forFunc(Func1<X,A1> func,
                                                 A1 arg)
  {
    return forFunc(func.getClosure(arg));
  }
  public static <X,A1,A2>
  MethodRunnable forFunc(Func2<X,A1,A2> func,
                                                    A1 a1, A2 a2)
  {
    return forFunc(func.getClosure(a1,a2));
  }
  public static <A1,A2>
  MethodRunnable forFunc(Func2V<A1,A2> func, A1 a1,
                                                     A2 a2)
  {
    return forFunc(func.getClosure(a1,a2),func);
  }
  public static <X,A1,A2,A3>
  MethodRunnable forFunc(Func3<X,A1,A2,A3> func,
                                                    A1 a1, A2 a2, A3 a3)
  {
    return forFunc(func.getClosure(a1,a2,a3));
  }
  public static <A1,A2,A3>
  MethodRunnable forFunc(Func3V<A1,A2,A3> func, A1 a1, A2 a2, A3 a3)
  {
    return forFunc(func.getClosure(a1,a2,a3),func);
  }
  public static <X,A1,A2,A3,A4>
  MethodRunnable forFunc( Func4<X,A1,A2,A3, A4> func, A1 a1, A2 a2, A3 a3, A4 a4 )
  {
    return forFunc(func.getClosure(a1,a2,a3,a4));
  }
  public static <A1,A2,A3,A4>
  MethodRunnable forFunc(
    Func4V<A1,A2,A3,A4> func,
    A1 a1, A2 a2, A3 a3, A4 a4
  )
  {
    return forFunc(func.getClosure(a1,a2,a3,a4),func);
  }

  @Nonnull
  public String toString() {
    return Util.format("%s.%s(%s)", mTarget, mMethod.getName(), argsToString());
  }

  private String argsToString() {
    String[] strArgs = new String[mArgs.length];
    int i = 0;
    for (Object arg : mArgs) {
      strArgs[i++] = String.valueOf(arg);
    }
    return "[" + String.join("], [", strArgs) + "]";
  }

  @Override
  public void apply()
  {
    mRes=invoke();
  }

  private Object invoke()
  {
    try {
      return mMethod.invoke(mTarget, mArgs);
    } catch (Exception e) {
      e.printStackTrace();
      TheWolf.handleException("running " + mMethod, e, null, true);
      return null;
    }
  }

  @Override
  public void run()
  {
    mRes=invoke();
  }
}
