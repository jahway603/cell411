package cell411.utils;

import javax.annotation.Nonnull;

public final class XTAG {
  public final String s;

  public XTAG() {
    this(1);
  }

  public XTAG(int i) {
    this(Reflect.currentSimpleClassName(i + 1));
  }

  public XTAG(String tag) {
    this.s = tag;
  }

  @Nonnull
  public String toString() {
    return s;
  }
}
