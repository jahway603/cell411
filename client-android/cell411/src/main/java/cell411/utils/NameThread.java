package cell411.utils;

public class NameThread implements Runnable {
  String mName;

  public NameThread(String name) {
    mName = name;
  }

  public void run() {
    Thread.currentThread().setName(mName);
  }
}
