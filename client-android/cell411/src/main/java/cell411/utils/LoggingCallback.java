package cell411.utils;

import android.os.Handler;
import android.os.Message;

import javax.annotation.Nonnull;

public class LoggingCallback implements Handler.Callback {
  public static final XTAG TAG = new XTAG();

  @Override
  public boolean handleMessage(@Nonnull Message msg) {
    XLog.i(TAG, "msg: " + msg);
    return false;
  }
}
