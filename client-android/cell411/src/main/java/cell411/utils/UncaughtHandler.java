package cell411.utils;

import android.os.Handler;
import android.os.Looper;

import com.safearx.cell411.Cell411;

import javax.annotation.Nonnull;

import cell411.ui.base.BaseApp;

public class UncaughtHandler
  implements Thread.UncaughtExceptionHandler,
    Runnable, ExceptionHandler
{
  private final Thread.UncaughtExceptionHandler mSystemUncaughtHandler;

  private UncaughtHandler(Handler handler)
  {
    mHandler = handler;
    mSystemUncaughtHandler = Thread.getDefaultUncaughtExceptionHandler();
  }
  final Handler mHandler ;
  public static void startCatcher() {
    CarefulHandler handler = Cell411.getUIHandler();
    handler.post(new UncaughtHandler(handler));
  }

  public void uncaughtException(Thread thread, @Nonnull final Throwable e) {
    final int tid = 0;// = Process.myTid();
    final String threadName = thread.getName();
    mHandler.post(() ->
    {
      throw new BackgroundException(e, tid, threadName);
    });
  }

  @Override
  public void run() {
    Thread thread = Thread.currentThread();
    String name = thread.getName();
    try {
      thread.setName("UI Thread");
      // the following handler is used to catch exceptions thrown in background threads
      Thread.setDefaultUncaughtExceptionHandler(this);

      while (!BaseApp.req().isDone()) {
        try {
          Looper.loop();
          Thread.setDefaultUncaughtExceptionHandler(mSystemUncaughtHandler);
          throw new RuntimeException("Main thread loop unexpectedly exited");
        } catch (BackgroundException e) {
          handleException("Running main loop", e.getCause());
        } catch (Throwable e) {
          handleException("Running main loop", e);
        }
      }
    } finally {
      thread.setName(name);
    }
  }
}
