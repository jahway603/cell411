package cell411.utils;

import cell411.utils.func.Func0;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Method;

public class MethodCallable<X>
  implements Func0<X>
{
  final Class<? extends X> mRetType;
  private final Object mTarget;
  private final Object[] mArgs;
  private final Method mMethod;
  private final Class<?> mType;

  private MethodCallable(Class<X> retType, Object target, Method method,
                         Object... args)
  {
    mRetType=retType;
    mType = Reflect.getClass(target);
    mTarget = (Reflect.getClass(target) == target) ? null : target;
    mArgs = args;
    mMethod = method;
  }
  private MethodCallable(Class<X> retType, Class<?> type, Method method,
                         Object... args)
  {
    mRetType=retType;
    mType = type;
    mTarget = null;
    mArgs = args;
    mMethod = method;
  }


  public static <X,T>
  MethodCallable<X> forVirtual(Class<X> retType,
                               T target,
                               String name,
                               Object ... args)
  {
    Class<?> type = target.getClass();
    Method method = getMethod(retType, type,null,name,args);
    return new MethodCallable<X>(retType,type,method);
  }

  public static <X>
  MethodCallable<X> forStatic(Class<X> retType,
                              Class<?> type, String name,
                              Object ... args)
  {
    Method method = getMethod(retType, type,null,name,args);
    return new MethodCallable<X>(retType,type,method);
  }

  static <X>
  Method getMethod(@Nonnull Class<X> retType,
                          @Nonnull Class<?> type,
                          @Nullable Object target,
                          @Nonnull String name,
                          Object ... args)
  {
    return Reflect.getMethod(type, target, name, args);
  }

  @Nonnull
  public String toString() {
    return Util.format("%s.%s(%s)", mTarget, mMethod.getName(), argsToString());
  }

  private String argsToString() {
    String[] strArgs = new String[mArgs.length];
    int i = 0;
    for (Object arg : mArgs) {
      strArgs[i++] = String.valueOf(arg);
    }
    return "[" + String.join("], [", strArgs) + "]";
  }

  @Override
  public X apply()
  {
    return invoke();
  }

  private X invoke()
  {
    try {
      return mRetType.cast(mMethod.invoke(mTarget, mArgs));
    } catch (Exception e) {
      e.printStackTrace();
      TheWolf.handleException("running " + mMethod, e, null, true);
      return null;
    }
  }

}
