package cell411.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

public class NumberInfo<N extends Number> {
  public final static NumberInfo<Double> mDouble;
  public final static NumberInfo<Float> mFloat;
  public final static NumberInfo<Long> mLong;
  public final static NumberInfo<Integer> mInteger;
  public final static NumberInfo<Short> mShort;
  public final static NumberInfo<Byte> mByte;
  private static final Map<Class<?>, NumberInfo<?>> smLookup = new HashMap<>();

  static {
    try {
      mDouble = new NumberInfo<>(Double.class);
      mFloat = new NumberInfo<>(Float.class);
      mLong = new NumberInfo<>(Long.class);
      mInteger = new NumberInfo<>(Integer.class);
      mShort = new NumberInfo<>(Short.class);
      mByte = new NumberInfo<>(Byte.class);
    } catch (Exception ex) {
      throw Util.rethrow(ex);
    }
  }

  public final Class<N> mType;
  public final Field mMinValueField;
  public final N mMinValue;
  public final Field mMaxValueField;
  public final N mMaxValue;
  public final Field mPrimField;
  public final Class<?> mPrim;
  private final Constructor<?>[] mConstructors;
  private Method mUnbox;

  NumberInfo(final Class<N> type) throws Exception {
    mType = type;
    mPrimField = type.getDeclaredField("TYPE");
    mPrim = (Class<?>) mPrimField.get(null);
    assert mPrim != null;

    System.out.println("Constructors");
    mConstructors = type.getConstructors();
    for (final Constructor<?> constructor : mConstructors) {
      System.out.println(constructor);
    }
    System.out.println("Methods");
    for (final Method method : Number.class.getMethods()) {
      String name = method.getName();
      if (!name.endsWith("Value")) {
        continue;
      }
      if (!name.startsWith(mPrim.getName())) {
        continue;
      }
      if (method.getReturnType() != mPrim) {
        continue;
      }
      mUnbox = method;
    }
    if (mUnbox == null) {
      System.out.println("Warning:  failed to find unbox function");
    }

    mMinValueField = type.getDeclaredField("MIN_VALUE");
    mMinValue = mType.cast(mMinValueField.get(null));
    mMaxValueField = type.getDeclaredField("MAX_VALUE");
    mMaxValue = mType.cast(mMaxValueField.get(null));
    smLookup.put(mType, this);
    smLookup.put(mPrim, this);
    System.out.println(smLookup.get(mType));
  }

  public N valueOf(Number number) {
    try {
      return mType.cast(mUnbox.invoke(number));
    } catch (IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException("unboxing Number", e);
    }
  }

  @Nonnull
  public String toString() {
    PrintString ps = new PrintString();
    ps.p("NumberInfo<").p(mType.getSimpleName()).p(">").pl();
    return ps.toString();
  }

  public N getMinValue() {
    return mMinValue;
  }

  public N getMaxValue() {
    return mMaxValue;
  }

  private N get(final Field field) {
    try {
      return mType.cast(field.get(null));
    } catch (IllegalAccessException e) {
      throw Util.rethrow(e);
    }
  }
}
