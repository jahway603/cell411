package com.parse.decoder;

import cell411.utils.LazyObject;
import cell411.utils.Util;
import com.parse.model.ParseObject;

import javax.annotation.Nullable;

public interface ParseDecoder
{
  LazyObject<ParseDecoder> smInstance =
    LazyObject.create(ParseDecoder.class,ParseDecoderImpl::new);
  static ParseDecoder get()
  {
    return Util.getRef(smInstance);
  }

  ParseObject decodePointer(String className, String objectId);

  Object decode(@Nullable Object object);
}
