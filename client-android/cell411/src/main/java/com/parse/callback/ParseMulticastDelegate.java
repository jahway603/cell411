/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.callback;

import com.parse.ParseException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ParseMulticastDelegate<T> implements ParseCallback2<T> {
  private final List<ParseCallback2<T>> callbacks;

  public ParseMulticastDelegate() {
    callbacks = new LinkedList<>();
  }

  public void subscribe(ParseCallback2<T> callback) {
    callbacks.add(callback);
  }

  public void unsubscribe(ParseCallback2<T> callback) {
    callbacks.remove(callback);
  }

  public void done(T result, ParseException exception) {
    List<ParseCallback2<T>> solidCallbacks = getCallbacks();

    for (ParseCallback2<T> callback : new ArrayList<>(callbacks)) {
      callback.done(result, exception);
    }
  }

  public synchronized List<ParseCallback2<T>> getCallbacks() {
    return new ArrayList<>(callbacks);
  }

  public void clear() {
    callbacks.clear();
  }

}

