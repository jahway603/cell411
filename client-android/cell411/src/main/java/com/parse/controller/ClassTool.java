package com.parse.controller;

import com.parse.http.ParseSyncUtils;
import com.parse.model.ParseObject;

import javax.annotation.Nonnull;
import java.util.function.Supplier;

import static cell411.utils.Suppliers.createSupplier;

public class ClassTool<T extends ParseObject> {
  private final Class<T> mType;
  private final Supplier<T> mSupplier;
  private final String mName;

  public ClassTool(Class<T> type, Supplier<T> supplier) {
    mType = type;
    mSupplier = supplier;
    mName = ParseSyncUtils.getClassName(type);
  }

  public ClassTool(Class<T> parseUserClass)
  {
    this(parseUserClass,createSupplier(parseUserClass));
  }

  public T newInstance() {
    return getSupplier().get();
  }

  public Class<T> getType()
  {
    return mType;
  }

  public Supplier<T> getSupplier()
  {
    return mSupplier;
  }

  @Nonnull
  public String getName()
  {
    return mName;
  }
}
