package com.parse.rest;

import com.parse.http.ParseHttpMethod;

public class ParseFileRequest extends ParseRequest {
  public ParseFileRequest(ParseHttpMethod method, String url) {
    super(method, url);
  }
}

