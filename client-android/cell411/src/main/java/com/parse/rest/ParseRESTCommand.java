/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.rest;

import cell411.json.JSONObject;
import cell411.utils.Reflect;
import com.parse.callback.ProgressCallback;
import com.parse.encoder.NoObjectsEncoder;
import com.parse.http.ParseHttpBody;
import com.parse.http.ParseHttpHeader;
import com.parse.http.ParseHttpMethod;
import com.parse.http.ParseHttpRequest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import static com.parse.Parse.getServer;

public class ParseRESTCommand extends ParseRequest {
  final JSONObject jsonParameters;
  private final String sessionToken;
  public String masterKey;
  public String httpPath;
  private String installationId;

  public ParseRESTCommand(String httpPath, ParseHttpMethod httpMethod, Map<String, ?> parameters,
                          String sessionToken) {
    this(httpPath, httpMethod, encodeParams(parameters), sessionToken);

  }

  public ParseRESTCommand(String httpPath, ParseHttpMethod httpMethod, JSONObject jsonParameters,
                          String sessionToken)
  {
    super(httpMethod, createUrl(httpPath));
    this.httpPath = httpPath;
    this.jsonParameters = jsonParameters;
    this.sessionToken = sessionToken;
    Reflect.announce("httpPath: " + httpPath);
    Reflect.announce("jsonParameters: " + jsonParameters);
    Reflect.announce("sessionToken==null: "+(sessionToken==null));
  }

  public ParseRESTCommand(Init<?> builder) {
    this(builder.httpPath, builder.method, builder.jsonParameters, builder.sessionToken);
    installationId = builder.installationId;
    masterKey = builder.masterKey;
    Reflect.announce("installationId: "+installationId);
    Reflect.announce("masterKey==null: "+(masterKey==null));
  }

  static JSONObject encodeParams(Map<String, ?> params) {
    if (params == null) {
      return null;
    } else {
      return (JSONObject) NoObjectsEncoder.get().encode(params);
    }
  }

  public static String createUrl(String httpPath) {
    // We send all parameters for GET/HEAD/DELETE requests in a post body,
    // so no need to worry about query parameters here.
    if (httpPath == null) {
      return getServer();
    }

    try {
      return new URL(new URL(getServer()), httpPath).toString();
    } catch (MalformedURLException ex) {
      throw new RuntimeException(ex);
    }
  }

  protected void addAdditionalHeaders(ParseHttpRequest.Builder requestBuilder) {
    if (installationId != null) {
      requestBuilder.addHeader(ParseHttpHeader.HEADER_INSTALLATION_ID, installationId);
    }
    if (sessionToken != null) {
      requestBuilder.addHeader(ParseHttpHeader.HEADER_SESSION_TOKEN, sessionToken);
    }
    if (masterKey != null) {
      requestBuilder.addHeader(ParseHttpHeader.HEADER_MASTER_KEY, masterKey);
    }
  }

  @Override
  protected ParseHttpBody newBody(ProgressCallback uploadProgressCallback) {
    if (jsonParameters == null) {
      String message =
        String.format("Trying to execute a %s command without body parameters.",
          method.toString());
      throw new IllegalArgumentException(message);
    }

    try {
      JSONObject parameters = jsonParameters;
      if (method == ParseHttpMethod.GET || method == ParseHttpMethod.DELETE) {
        // The request URI may be too long to include parameters in the URI.
        // To avoid this problem we send the parameters in a POST request json-encoded body
        // and add a http method override parameter.
        parameters = new JSONObject(jsonParameters.toString());
        parameters.put(ParseHttpHeader.PARAMETER_METHOD_OVERRIDE, method.toString());
      }
      return new ParseByteArrayHttpBody(parameters.toString(), "application/json", this);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  @Override
  public ParseHttpRequest newRequest(ParseHttpMethod method, String url,
                                     ProgressCallback uploadProgressCallback) {
    ParseHttpRequest request;
    if (jsonParameters != null && method != ParseHttpMethod.POST && method != ParseHttpMethod.PUT) {
      // The request URI may be too long to include parameters in the URI.
      // To avoid this problem we send the parameters in a POST request json-encoded body
      // and add a http method override parameter in newBody.
      request = super.newRequest(ParseHttpMethod.POST, url, uploadProgressCallback);
    } else {
      request = super.newRequest(method, url, uploadProgressCallback);
    }
    ParseHttpRequest.Builder requestBuilder = new ParseHttpRequest.Builder(request);
    addAdditionalHeaders(requestBuilder);
    return requestBuilder.build();
  }

  public String getSessionToken() {
    return sessionToken;
  }

  /* package */ static abstract class Init<T extends Init<T>> {
    public String masterKey;
    private String sessionToken;
    private String installationId;
    private ParseHttpMethod method = ParseHttpMethod.GET;
    private String httpPath;
    private JSONObject jsonParameters;

    private String operationSetUUID;
    private String localId;

    /* package */
    abstract T self();

    public T sessionToken(String sessionToken) {
      this.sessionToken = sessionToken;
      return self();
    }

    public T installationId(String installationId) {
      this.installationId = installationId;
      return self();
    }

    public T masterKey(String masterKey) {
      this.masterKey = masterKey;
      return self();
    }

    public T method(ParseHttpMethod method) {
      this.method = method;
      return self();
    }

    public T httpPath(String httpPath) {
      this.httpPath = httpPath;
      return self();
    }

    public T jsonParameters(JSONObject jsonParameters) {
      this.jsonParameters = jsonParameters;
      return self();
    }

    public T operationSetUUID(String operationSetUUID) {
      this.operationSetUUID = operationSetUUID;
      return self();
    }

    public T localId(String localId) {
      this.localId = localId;
      return self();
    }
  }

  public static class Builder extends Init<Builder> {
    @Override
      /* package */ Builder self() {
      return this;
    }

    public ParseRESTCommand build() {
      return new ParseRESTCommand(this);
    }
  }
}

