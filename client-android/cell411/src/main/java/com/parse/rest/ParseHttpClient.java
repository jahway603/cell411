/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.rest;

import com.parse.http.ParseHttpRequest;
import com.parse.http.ParseHttpResponse;

import java.io.IOException;

import javax.annotation.Nonnull;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Internal http client which wraps an {@link OkHttpClient}
 */
public class ParseHttpClient {

  OkHttpClient mOkHttpClient;
  private boolean hasExecuted;

  public ParseHttpClient(OkHttpClient okHttpClient) {
    mOkHttpClient = okHttpClient;

  }

  @Nonnull
  public static Request getRequest(ParseHttpRequest parseRequest) {
    return ParseHttpRequest.getRequest(parseRequest);
  }

  public final ParseHttpResponse execute(ParseHttpRequest request) throws IOException {
    if (!hasExecuted) {
      hasExecuted = true;
    }
    return executeInternal(request);
  }

  /**
   * Execute internal. Keep default protection for tests
   *
   * @param parseRequest request
   * @return response
   * @throws IOException exception
   */
  ParseHttpResponse executeInternal(ParseHttpRequest parseRequest) throws IOException {
    Request okHttpRequest = getRequest(parseRequest);
    Call okHttpCall = getHttpClient().newCall(okHttpRequest);

    Response okHttpResponse = okHttpCall.execute();

    return ParseHttpResponse.getResponse(okHttpResponse);
  }

  private OkHttpClient getHttpClient() {
    return mOkHttpClient;
  }

}

