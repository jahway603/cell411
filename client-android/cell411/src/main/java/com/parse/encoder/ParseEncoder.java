package com.parse.encoder;

import cell411.json.JSONObject;
import cell411.utils.Collect;
import cell411.utils.func.Func1;
import com.parse.ParsePolygon;
import com.parse.model.ParseACL;
import com.parse.model.ParseFile;
import com.parse.model.ParseGeoPoint;
import com.parse.model.ParseObject;
import com.parse.model.ParseRelation;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public interface ParseEncoder
  extends Func1<Object,Object>
{
  HashSet<Class<?>> smClasses = new HashSet<>( Collect.asList(
    String.class,         Number.class,               Boolean.class,
    Date.class,           List.class,                 Map.class,
    byte[].class,         ParseObject.class,          ParseACL.class,
    ParseFile.class,      ParseGeoPoint.class,        ParsePolygon.class,
    ParseRelation.class,  JSONObject.NULL.getClass()
  ));
  static boolean isValidType(Object value)
  {
    return smClasses.contains(value.getClass());
  }

  Object encode(Object object);

  JSONObject encodeRelatedObject(ParseObject object);

  @Override
  default Object apply(Object object) {
    return encode(object);
  };
}
