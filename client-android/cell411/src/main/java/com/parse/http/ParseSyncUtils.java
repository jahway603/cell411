package com.parse.http;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.NetworkOnMainThreadException;
import cell411.json.JSONArray;
import cell411.json.JSONException;
import cell411.json.JSONObject;
import cell411.libcell.ConfigDepot;
import cell411.utils.CarefulHandler;
import cell411.utils.IOUtil;
import cell411.utils.LazyObject;
import cell411.utils.PrintString;
import cell411.utils.Reflect;
import cell411.utils.Util;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import cell411.utils.concurrant.STPExecutorPlus;
import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.callback.CountCallback;
import com.parse.callback.LogInCallback;
import com.parse.callback.ParseCallback1;
import com.parse.callback.ParseCallback2;
import com.parse.decoder.KnownParseObjectDecoder;
import com.parse.decoder.ParseDecoder;
import com.parse.encoder.ParseEncoder;
import com.parse.encoder.AbstractParseEncoder;
import com.parse.encoder.ParseObjectCoder;
import com.parse.encoder.PointerEncoder;
import com.parse.controller.ClassTool;
import com.parse.controller.ParseCorePlugins;
import com.parse.controller.ParseObjectSubclassingController;
import com.parse.model.CurrentData;
import com.parse.model.ParseFile;
import com.parse.model.ParseInstallation;
import com.parse.model.ParseObject;
import com.parse.model.ParseRole;
import com.parse.model.ParseSession;
import com.parse.model.ParseUser;
import com.parse.operation.ParseFieldOperation;
import com.parse.operation.ParseOperationSet;
import com.parse.operation.ParseSetOperation;
import com.parse.rest.ParseFileRequest;
import com.parse.rest.ParseHttpClient;
import com.parse.rest.ParseRESTCloudCommand;
import com.parse.rest.ParseRESTCommand;
import com.parse.rest.ParseRESTFileCommand;
import com.parse.rest.ParseRESTObjectCommand;
import com.parse.rest.ParseRESTQueryCommand;
import com.parse.rest.ParseRESTUserCommand;
import com.parse.rest.ParseRequest;
import com.parse.utils.ParseDateFormat;
import com.parse.utils.ParseFileUtils;
import com.parse.utils.ParseIOUtils;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Supplier;

public class ParseSyncUtils
{
  public static final Map<String, ClassTool<? extends ParseObject>> smByName
    = new HashMap<>();

  private static final Object mutex = new Object();

  public static String userAgent() {
    return "Parse Android SDK API Level " + Build.VERSION.SDK_INT;
  }

  public static String getClassName(Class<? extends ParseObject> clazz) {
    ParseClassName info = clazz.getAnnotation(ParseClassName.class);
    if (info == null) {
      throw new IllegalArgumentException("No ParseClassName annotation provided on " + clazz);
    }
    return info.value();
  }

  public static boolean isSubclassValid(String className,
                                        Class<? extends ParseObject> clazz)
  {
    ClassTool<? extends ParseObject> classTool;

    synchronized (mutex) {
      classTool = smByName.get(className);
      if (classTool == null)
        return clazz == ParseObject.class;
      else
        return classTool.getType().isAssignableFrom(clazz);

    }

  }

  public static <T extends ParseObject>
  void registerSubclass(ClassTool<T> classTool) {
    Class<T> type = classTool.getType();
    Supplier<T> supplier = classTool.getSupplier();

    if (!ParseObject.class.isAssignableFrom(type)) {
      throw new IllegalArgumentException(
        "Cannot register a type that is not a subclass of ParseObject");
    }

    String className = getClassName(type);
    ClassTool<? extends ParseObject> prevClassTool;
    synchronized (mutex) {
      prevClassTool = smByName.get(className);
      if (prevClassTool != null) {
        Class<? extends ParseObject> prevType = prevClassTool.getType();
        XLog.i(TAG,"%s isAssignableFrom %s: %s\n",
          prevType, type, prevType.isAssignableFrom(type));
        XLog.i(TAG,"%s isAssignableFrom %s: %s\n",
          type, prevType, type.isAssignableFrom(prevType));
        if (type.isAssignableFrom(prevType)) {
          return;
        }
      }
      smByName.put(className, classTool);
    }

//    synchronized (mutex) {
//      previousConstructor = registeredSubclasses.get(className);
//      if (previousConstructor != null) {
//        Class<? extends ParseObject> previousClass =
//          previousConstructor.getDeclaringClass();
//        if (clazz.isAssignableFrom(previousClass)) {
//          // Previous subclass is more specific or equal to the current type, do nothing.
//          return;
//        } else if (previousClass.isAssignableFrom(clazz)) {
//          // Previous subclass is parent of new child subclass, fallthrough and actually
//          // register this class.
//          /* Do nothing */
//        } else {
//          throw new IllegalArgumentException()
//            "Tried to register both "
//              + previousClass.getName()
//              + " and "
//              + clazz.getName()
//              + " as the ParseObject subclass of "
//              + className
//              + ". "
//              + "Cannot determine the right "
//              + "class to use because neither inherits from the other.");
//        }
//      }
//
//      try {
//        registeredSubclasses.put(className, getSupplier(clazz));
//      } catch (NoSuchMethodException ex) {
//        throw new IllegalArgumentException(
//          "Cannot register a type that does not implement the default constructor!");
//      } catch (IllegalAccessException ex) {
//        throw new IllegalArgumentException(
//          "Default constructor for " + clazz + " is not accessible.");
//      }
//    }

    if (prevClassTool != null) {
      // TODO: This is super tightly coupled. Let's remove it when automatic registration is
      // in.
      // NOTE: Perform this outside of the mutex, to prevent any potential deadlocks.
      if (className.equals(getClassName(ParseUser.class))) {
        CurrentData.saveAndLoad();
      } else if (className.equals(getClassName(ParseInstallation.class))) {
        CurrentData.saveAndLoad();
      }
    }
  }

  public static void unregisterSubclass(Class<? extends ParseObject> clazz) {
    String className = getClassName(clazz);

    synchronized (mutex) {
      smByName.remove(className);
    }
  }

  /* package */
  public static ParseObject newInstance(String className) {
    ClassTool<? extends ParseObject> classTool;

    synchronized (mutex) {
      classTool = smByName.get(className);
    }

    try {
      return classTool != null ? classTool.newInstance() : new ParseObject(className);
    } catch (RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException("Failed to create instance of subclass.", e);
    }
  }

  public static String getTableForClass(Class<? extends ParseObject> aClass)
  {
    return getClassName(aClass);
  }

  public static Class<? extends ParseObject> getClassForTable(String targetClassName)
  {
    ClassTool<? extends ParseObject> tool = smByName.get(targetClassName);
    if(tool==null)
      return null;
    else
      return tool.getType();
  }

  /* package */

  static class LogT {

  }
  static final LogT Log = new LogT();
  private static final XTAG TAG = new XTAG();
  public static ParseDateFormat smParseDateFormat = ParseDateFormat.get();
  ScheduledExecutorService smService = Executors.newSingleThreadScheduledExecutor();

  private static final LazyObject<ParseHttpClient> smRestClient =
    LazyObject.create(ParseHttpClient.class,ParseSyncUtils::createRestClient);

  private static final LazyObject<ParseHttpClient> smFileClient =
    LazyObject.create(ParseHttpClient.class,ParseSyncUtils::createFileClient);

  static ParseHttpClient createFileClient()
  {
    return new ParseHttpClient(ConfigDepot.getHttpClient());
  }
  static ParseHttpClient createRestClient()
  {
    XLog.i(TAG, String.valueOf(OkHttpClient.Builder.class));
    OkHttpClient.Builder clientBuilder = ConfigDepot.getHttpClientBuilder();
    clientBuilder.pingInterval(Duration.ofSeconds(10));

    //add it as the first interceptor
    clientBuilder.interceptors().add(0, chain ->
    {
      Request request = chain.request();
      request = ParseHttpRequest.customizeRequest(request, Parse.getConfiguration());

      return chain.proceed(request);
    });
    return new ParseHttpClient(clientBuilder.build());
  }
  private static final ParseObjectCoder coder = ParseObjectCoder.get();
  private static final byte[] dashed_line;
  private static Handler smUIHandler;

  static {
    dashed_line = new byte[70];
    Arrays.fill(dashed_line, (byte) '_');
    dashed_line[69] = 10;
  }

  public static JSONObject getJSON(ParseHttpResponse response) {
    InputStream responseStream = null;
    try {
      responseStream = response.getContent();
      byte[] bytes = ParseIOUtils.toByteArray(responseStream);
      String content = new String(bytes).trim();
      if (contentIsHtml(content)) {
        if (content.contains("<title>502 Bad Gateway")) {
          throw new ParseException(ParseException.CONNECTION_FAILED, "502 Bad Gateway");
        } else {
          throw new ParseException(ParseException.OTHER_CAUSE, content);
        }
      } else {
        return new JSONObject(content);
      }
    } catch (JSONException e) {
      throw new RuntimeException("Parsing query response", e);
    } finally {
      ParseIOUtils.closeQuietly(responseStream);
    }
  }

  private static ParseHttpBody dumpCopy(ParseHttpRequest request,
                                        ParseHttpBody parseBody,
                                        PrintString ps) {
    try {
      ps.write(dashed_line);
      ps.write(dashed_line);
      ps.write(dashed_line);
      ps.pl("\n\n");
      ps.println("Method:   " + request.getMethod());
      ps.println("Url:      " + request.getUrl());
      ps.println("CLength:  " + parseBody.getContentLength());
      ps.println("CType:    " + parseBody.getContentType());
      ps.println("\n");
      ps.println("Content:");
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      parseBody.writeTo(baos);
      byte[] bytes = IOUtil.streamToBytes(parseBody.getContent());
      String text = new String(bytes);
      JSONObject object = new JSONObject(text);
      ps.println(object.toString(2));
      ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
      ps.write(dashed_line);
      ps.write(dashed_line);
      ps.write(dashed_line);
      ps.pl("\n\n");

      parseBody = new ParseHttpBody(parseBody.getContentType(),
        parseBody.getContentLength()) {
        @Override
        public InputStream getContent() {
          return bais;
        }

        @Override
        public void writeTo(OutputStream out) {
          try {
            out.write(bytes);
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      };
    } catch (IOException e) {
      e.printStackTrace();
    }

    return parseBody;
  }

  public static JSONObject executeRequest(ParseHttpRequest request) {
    ParseHttpResponse response;
    PrintString ps = new PrintString();
    dumpCopy(request, request.getBody(), ps);
    try {
      response = getRestClient().execute(request);
    } catch (IOException ioException) {
      throw ParseException.wrap(ioException);
    }
    JSONObject jsonResponse = getJSON(response);
    ps.write(dashed_line);
    ps.println(jsonResponse.toString(2));
    ps.write(dashed_line);
    System.out.println(ps);

    int statusCode = response.getStatusCode();
    switch (statusCode / 100) {
      case 4:
      case 5: {
        int code = jsonResponse.optInt("code");
        String message = jsonResponse.optString("error", null);
        // Internal error server side, or something, we'll
        // flag that it might be worth trying again.
        if (message == null) {
          message = jsonResponse.optString("message", null);
        }
        if (message == null) {
          message = "No Message Supplied";
        }
        ParseRequest.ParseRequestException e =
          new ParseRequest.ParseRequestException(code, message);
        e.mIsPermanentFailure = statusCode < 500;
        throw e;
      }
      case 2:
        return jsonResponse;
      default:
        throw new ParseException(ParseException.OTHER_CAUSE, "Unexpected http code");
    }
  }

  public static <T extends ParseObject> List<T> find(ParseQuery.State<T> state)
    throws ParseException {
    JSONObject jsonResponse = runQuery(state, false);
    JSONArray jsonResults = jsonResponse.getJSONArray("results");
    String resultClassName = jsonResponse.optString("className");
    if (resultClassName.isEmpty()) {
      resultClassName = state.className();
    }
    List<T> results = new ArrayList<>(jsonResults.length());
    for (int i = 0; i < jsonResults.length(); i++) {
      JSONObject jsonResult = jsonResults.getJSONObject(i);
      T result =
        ParseObject.fromJSON(jsonResult, resultClassName, ParseDecoder.get(),
          state.selectedKeys());
      results.add(result);
    }
    return results;
  }

  private static ParseEncoder minParseEncoder() {
    return new AbstractParseEncoder() {
      @Override
      public JSONObject encodeRelatedObject(final ParseObject po) {
        JSONObject object = new JSONObject();
        object.put("data", po.getClassName() + "." + po.getObjectId());
        return object;
      }
    };
  }

  public static <T extends ParseObject> JSONObject runQuery(ParseQuery.State<T> state,
                                                            boolean count) {
    ParseUser currentUser = CurrentData.getCurrentUser();
    if (currentUser == null) {
      throw new ParseException(ParseException.NOT_LOGGED_IN, "session missing");
    }

    String sessionToken = currentUser.getSessionToken();
    String httpPath = String.format("classes/%s", state.className());
    Map<String, String> parameters = ParseRESTQueryCommand.encode(state, count);
    ParseRESTQueryCommand command =
      new ParseRESTQueryCommand(httpPath, ParseHttpMethod.GET, parameters, sessionToken);
    ParseHttpMethod method = command.method;
    String url = command.url;
    ParseHttpRequest request = command.newRequest(method, url, null);
    return executeRequest(request);
  }

  public static <T extends ParseObject> int count(ParseQuery.State<T> state) {
    JSONObject result = runQuery(state, true);
    return result.optInt("count", 0);
  }

  public static Object decodeObject(Object o) {
    return ParseDecoder.get().decode(o);
  }

  public static <T> T run(@Nonnull String name, @Nonnull Map<String, ?> params) {
    String sessionToken = ParseUser.getCurrentSessionToken();
    ParseRESTCommand command =
      ParseRESTCloudCommand.callFunctionCommand(name, params, sessionToken);
    ParseHttpRequest request = command.newRequest();
    JSONObject jsonResponse = executeRequest(request);
    Object result = jsonResponse.opt("result");
    //noinspection unchecked
    return (T) ParseSyncUtils.decodeObject(result);
  }

  @SuppressWarnings("unused")
  public static <X extends ParseObject>
  JSONObject encodeQuery(ParseQuery<X> query) {
    PointerEncoder encoder = PointerEncoder.get();
    return query.getBuilder().build().toJSON(encoder);
  }

  public static void logIn(String username, String password, LogInCallback listener) {
    try {
      Parse.checkInit();
      if (Looper.getMainLooper().isCurrentThread()) {
        getExec().post(() -> logIn(username, password, listener));
        return;
      }
      if (username == null) {
        throw new IllegalArgumentException("Must specify a username for the user to log in with");
      }
      if (password == null) {
        throw new IllegalArgumentException("Must specify a password for the user to log in with");
      }

      ParseRESTCommand command = ParseRESTUserCommand.logInUserCommand(username, password);
      ParseHttpRequest request = command.newRequest();
      if (ParseUser.getCurrentUser() != null) {
        throw new RuntimeException("Somebody is already logged in");
      }
      JSONObject jsonResponse = executeRequest(request);
      ParseUser.State.Builder builder = new ParseUser.State.Builder();

      ParseDecoder decoder = ParseDecoder.get();
      builder.isComplete(false);

      ParseObjectCoder objectCoder = ParseObjectCoder.get();
      ParseUser.State state =
        objectCoder.decode(builder, jsonResponse, decoder).isComplete(true).build();

      ParseUser currentUser = ParseUser.from(state);
      CurrentData.setCurrentUser(currentUser);
      if (listener != null) {
        ParseException pe = null;
        if (Util.theGovernmentIsHonest())
          pe = new ParseException(null);
        getUIHandler().post(ParseCallback2.wrap(listener, currentUser, pe));
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      Reflect.announce(false);
    }
  }

  public static Handler getUIHandler() {
    if (smUIHandler == null) {
      smUIHandler = new CarefulHandler(Looper.getMainLooper());
    }

    return smUIHandler;
  }

  public static ParseHttpClient getRestClient() {
    return smRestClient.get();
  }
  private static final Object lock=new Object();
  public static ParseHttpClient getFileClient() {
    return smFileClient.get();
  }

  public static ParseUser signUp(ParseUser user) {
    HashMap<String, Object> parameters = new HashMap<>(user.getEstimatedData());
    HashMap<String, Object> result = ParseCloud.run("signUp", parameters);
    if (Boolean.TRUE.equals(result.get("success"))) {
      user = (ParseUser) result.get("user");
      CurrentData.setCurrentUser(user);
      return CurrentData.getCurrentUser();
    } else {
      String message = (String) result.get("message");
      if (message == null) {
        message = "No Message";
      }
      throw new ParseException(ParseException.OTHER_CAUSE, message);
    }
  }

  public static void save(ParseObject object) {
    if (onMainThread()) {
      throw new ParseException(ParseException.OTHER_CAUSE, "On Main Thread",
        new NetworkOnMainThreadException());
    }

    String sessionToken = ParseUser.getCurrentSessionToken();
    HashSet<ParseObject> dirtyChildren = new HashSet<>();
    HashSet<ParseFile> dirtyFiles = new HashSet<>();
    ParseObject.collectDirtyChildren(object, dirtyChildren, dirtyFiles);
    ParseOperationSet operations;
    for (ParseFile pf : dirtyFiles) {
      pf.save();
    }
    for (ParseObject po : dirtyChildren) {
      po.updateBeforeSave();
      po.validateSave();
      operations = po.startSave();
      ParseObject.State state = po.getState();
      JSONObject objectJSON = coder.encode(state, operations, PointerEncoder.get());

      ParseRESTObjectCommand command =
        ParseRESTObjectCommand.saveObjectCommand(state, objectJSON, sessionToken);
      JSONObject result = executeRequest(command.newRequest());
      final Map<String, ParseObject> fetched = po.collectFetchedObjects();
      ParseDecoder decoder = new KnownParseObjectDecoder(fetched);
      ParseObject.State.Init<?> builder = po.getState().newBuilder();
      builder.apply(operations);
      builder.isComplete(false);
      builder = ParseObjectCoder.get().decode(builder, new JSONObject(), decoder);
      ParseOperationSet operationSet = new ParseOperationSet();
      for (String key : result.keySet()) {
        Object val = decoder.decode(result.opt(key));
        ParseFieldOperation operation;
        if (val instanceof ParseFieldOperation) {
          operation = (ParseFieldOperation) val;
        } else {
          operation = new ParseSetOperation(val);
        }
        operationSet.put(key, operation);
      }
      builder.apply(operationSet);
      ListIterator<ParseOperationSet> it = po.operationSetQueue.listIterator();
      it.next();
      it.remove();
      builder.isComplete(true);
      ParseObject.State newState = builder.build();
      po.setState(newState);
      po.afterSave();
    }
  }

  public static <T extends ParseObject> T fetch(T object) {
    assert !onMainThread();
    ParseQuery<T> query = ParseQuery.getQuery(object.getClassName());
    return query.get(object.getObjectId());
  }

  public static void post(Runnable runnable, ParseCallback1 callback) {
    getExec().post(PostedOp.create(runnable, callback));
  }

  public static int identityHashCode(ParseObject object) {
    return System.identityHashCode(object);
  }


  public static <T, CB extends ParseCallback2<T>>
  void post(Callable<T> call, CB callback) {
    getExec().post(PostedOp.create(call, callback));
  }

  private static STPExecutorPlus getExec() {
    return ConfigDepot.getExecutor();
  }

  public static void post(Callable<Integer> call, CountCallback callback) {
    post(call, (ParseCallback2<Integer>) (count, exception) ->
    {
      if (count == null) {
        count = -1;
      }
      callback.done(count, exception);
    });
  }

  public static <X>
  void post(Callable<X> call, ParseCallback1 callback) {
    getExec().post(PostedOp.create(call, callback));
  }

  static boolean onMainThread() {
    return Looper.getMainLooper().isCurrentThread();
  }

  public static void saveFile(final ParseFile parseFile) {
    assert !onMainThread();
    ParseFile.State state = parseFile.getState();
    ParseUser user = CurrentData.getCurrentUser();
    String sessionToken = null;
    if (user != null) {
      sessionToken = user.getSessionToken();
    }
    byte[] fileData = parseFile.data;
    File fileFile = parseFile.file;

    ParseRESTCommand command;
    if (fileData != null) {
      command = new ParseRESTFileCommand.Builder().fileName(state.name()).data(fileData)
        .contentType(state.mimeType())
        .sessionToken(sessionToken).build();
    } else if (fileFile != null) {
      command = new ParseRESTFileCommand.Builder().fileName(state.name()).file(fileFile)
        .contentType(state.mimeType())
        .sessionToken(sessionToken).build();
    } else {
      throw new IllegalStateException("Either file or data must be non-null");
    }
    JSONObject result = executeRequest(command.newRequest());
    ParseFile.State newState =
      new ParseFile.State.Builder(state).name(result.getString("name"))
        .url(result.getString("url"))
        .build();
    try {
      ParseFileUtils.writeByteArrayToFile(getCacheFile(newState), fileData);
    } catch (IOException e) {
      System.out.println("Error saving ParseFile: " + e);
    }
    parseFile.state = newState;
  }

  private static File getCacheFile(ParseFile.State state) {
    File fileCacheDir = ConfigDepot.getExtDir("cache");
    return new File(fileCacheDir, state.name());
  }

  public static void requestPasswordReset(String email) {
    ParseRESTCommand command = ParseRESTUserCommand.resetPasswordResetCommand(email);
    JSONObject result = executeRequest(command.newRequest());
    XLog.i(TAG, result.toString(2));
  }

  public static void delete(ParseObject object) {
    ParseObject.State state = object.getState();
    String sessionToken = ParseUser.getCurrentSessionToken();
    ParseRESTObjectCommand command =
      ParseRESTObjectCommand.deleteObjectCommand(state, sessionToken);
    executeRequest(command.newRequest());
  }

  public static byte[] getFileData(@Nonnull final ParseFile parseFile) {
    return IOUtil.fileToBytes(getFileFile(parseFile));
  }

  public static InputStream getFileDataStream(@Nonnull final ParseFile parseFile)
  {
    return IOUtil.fileToStream(getFileFile(parseFile));
  }

  public static File getFileFile(final ParseFile parseFile) {
    File cacheFile;
    File tempFile;
    ParseHttpResponse response;
    try {
      ParseFile.State state = parseFile.getState();

      cacheFile = getCacheFile(state);
      if (cacheFile.exists()) {
        Reflect.announce("Loading '" + cacheFile + "' from Cache");
        return cacheFile;
      } else {
        Reflect.announce("Loading from Net");
      }
      // Generate the temp file path for caching ParseFile content based on ParseFile's url
      // The reason we do not write to the cacheFile directly is because there is no way we can
      // verify if a cacheFile is complete or not. If download is interrupted in the middle, next
      // time when we download the ParseFile, since cacheFile has already existed, we will return
      // this incomplete cacheFile
      tempFile = parseFile.getTempFile();

      // network
      final ParseFileRequest request = new ParseFileRequest(ParseHttpMethod.GET, state.url());

      ParseHttpClient client = getFileClient();
      response = client.execute(request.newRequest());
    } catch (IOException e) {
      throw new ParseException(ParseException.IO_EXCEPTION, e.getMessage(), e);
    }
    try (
      InputStream iStream = response.getContent();
      OutputStream oStream = IOUtil.newOutputStream(tempFile)
    )
    {
      long copied = 0;
      final long conSize = response.getTotalSize();
      byte[] buf = new byte[1 + ((int) (conSize + 99) / 100)];
      int percent = 0;
      while (true) {
        int read = iStream.read(buf);
        if (read < 0) {
          break;
        }
        oStream.write(buf, 0, read);
        copied += read;
        int newPer = (int) (100 * copied / conSize);
        if (newPer == percent) {
          continue;
        }
        percent = newPer;
      }

      ParseFileUtils.deleteQuietly(cacheFile);
      ParseFileUtils.moveFile(tempFile, cacheFile);
      return cacheFile;
    } catch (IOException e) {
      throw new ParseException(ParseException.IO_EXCEPTION, e.getMessage(), e);
    }
    //        }
    //        return null;
  }

  private static boolean contentIsHtml(String content) {
    int p = 0;
    while (Character.isSpaceChar(content.charAt(p))) {
      p++;
    }
    return content.charAt(p) == '<';
  }

  public static void runQuery(String textQuery,
                              ParseCallback2<String> runnableQuery) {
    try {
      JSONObject jsonObject = new JSONObject(textQuery);
      String className = jsonObject.getString("className");
      ParseQuery<ParseObject> query = ParseQuery.getQuery(className);
      JSONObject response = runQuery(query.getState(), false);
      runnableQuery.done(response.toString(), null);
    } catch (ParseException pe) {
      runnableQuery.done(null, pe);
    }
  }

  public static void dumpObjects(ParseObject oldObject, ParseObject newObject) {
    try(
      PrintString ps = new PrintString()
    ) {
      if (oldObject == null) {
        ps.println("New Object: " + newObject);
        ps.println("Content: ");
        ps.println(newObject.toJSON().toString(200));
      } else if (newObject == null) {
        ps.println("Old Object: " + oldObject);
      } else {
        HashSet<String> keys = new HashSet<>(oldObject.availableKeys());
        keys.addAll(newObject.availableKeys());
        for (String key : keys) {
          boolean oldHas = oldObject.has(key);
          boolean newHas = newObject.has(key);
          if (oldHas && newHas) {
            Object oldValue = oldObject.get(key);
            Object newValue = newObject.get(key);
            if (oldValue == null && newValue == null)
              continue;
            if (oldValue != null && oldValue.equals(newValue))
              continue;
            ps.println("Key: " + key);
            ps.println("  O: " + oldValue);
            ps.println("  N: " + newValue);
          }
        }
      }
    }
  }

  /**
   * Fetches all the objects in the provided list.
   *
   * @param objects The list of objects to fetch.
   * @throws ParseException Throws an exception if the server returns an error or is inaccessible.
   */
  public static <X extends ParseObject, C extends Collection<X>> void fetchAll(C objects)
    throws ParseException
  {
    HashMap<String, HashSet<String>> mSets = new HashMap<>();
    for (ParseObject t : objects) {
      if (t == null) {
        continue;
      }
      HashSet<String> set = mSets.computeIfAbsent(t.getClassName(), (className) -> new HashSet<>());
      set.add(t.getObjectId());
    }
    List<ParseObject> needed = new ArrayList<>();
    for (String className : mSets.keySet()) {
      HashSet<String> ids = mSets.get(className);
      ParseQuery<X> query = ParseQuery.getQuery(className);
      query.whereContainedIn("objectId", ids);
      List<X> batch = query.find();
    }
  }

  /**
   * Registers the Parse-provided {@code ParseObject} subclasses. Do this here in a real method
   * rather than
   * as part of a static initializer because doing this in a static initializer can lead to
   * deadlocks
   */

  public static void registerParseSubclasses() {
    registerSubclass(ParseUser.class);
    registerSubclass(ParseRole.class);
    registerSubclass(ParseInstallation.class);
    registerSubclass(ParseSession.class);

  }

  public static <X extends ParseObject>
  void registerSubclass(Class<X> parseUserClass)
  {
    registerSubclass(new ClassTool<>(parseUserClass));
  }

  public static void unregisterParseSubclasses() {
    unregisterSubclass(ParseUser.class);
    unregisterSubclass(ParseRole.class);
    unregisterSubclass(ParseInstallation.class);
    unregisterSubclass(ParseSession.class);
  }

  static public String format(Date date) {
    if (date == null) {
      return "null";
    } else {
      return smParseDateFormat.format(date);
    }
  }

  public static ParseObjectSubclassingController getSubclassingController() {
    return ParseCorePlugins.get().getSubclassingController();
  }

  public static <T extends ParseObject> T fromJSON(JSONObject json, ParseQuery.State<T> state,
                                                   ParseDecoder decoder) {
    if (decoder == null) {
      decoder = ParseDecoder.get();
    }
    String className = null;
    Set<String> keys = null;
    if (state != null) {
      keys = state.selectedKeys();
      className = state.className();
    }
    return ParseObject.fromJSON(json, className, decoder, keys);
  }

  public ParseObject.State save(ParseObject.State state, final ParseOperationSet operations,
                                String sessionToken, final ParseDecoder decoder)
  {
    /*
     * Get the JSON representation of the object, and use some to * construct
     * the command.
     */
    JSONObject objectJSON = coder.encode(state, operations, PointerEncoder.get());

    ParseRESTObjectCommand command =
      ParseRESTObjectCommand.saveObjectCommand(state, objectJSON, sessionToken);
    JSONObject result = executeRequest(command.newRequest());
    ParseObject.State.Init<?> builder = state.newBuilder().clear();
    return coder.decode(builder, result, decoder).isComplete(false).build();
  }

  public ParseObject.State fetch(final ParseObject.State state, String sessionToken,
                                 final ParseDecoder decoder) {
    final ParseRESTCommand command =
      ParseRESTObjectCommand.getObjectCommand(state.objectId(), state.className(), sessionToken);

    ParseObject.State.Init<?> builder = state.newBuilder().clear();
    JSONObject result = executeRequest(command.newRequest());

    return coder.decode(builder, result, decoder).isComplete(true).build();
  }

  public static class PostedOp implements Runnable {
    Runnable mRunnable;
    Runnable mCallback;
    ParseException mException;
    RuntimeException mCreated = new RuntimeException("created here");
    int mState = 0;

    public PostedOp() {
      Reflect.announce("building PostedOp");
    }

    static <V>
    PostedOp create(@Nonnull Callable<V> callable,
                    @Nullable ParseCallback2<V> callback2) {
      PostedOp op = new PostedOp();
      ArrayList<V> result = new ArrayList<>();
      result.add(null);
      Runnable runnable = () ->
      {
        try {
          result.set(0, callable.call());
        } catch (Throwable t) {
          op.mException = ParseException.wrap(t);
        }
      };
      Runnable callback = null;
      if (callback2 != null) {
        callback = () ->
          callback2.done(result.get(0), op.mException);
      }

      PostedOp op1 = new PostedOp();
      op1.mRunnable = runnable;
      op1.mCallback = callback;
      return op1;
    }

    public static <X> PostedOp create(@Nonnull Callable<X> callable,
                                      @Nullable ParseCallback1 callback) {
      PostedOp op = new PostedOp();
      op.mRunnable = () ->
      {
        try {
          callable.call();
        } catch (Throwable t) {
          op.mException = ParseException.wrap(t);
        }
      };
      op.mCallback = callback == null ? null : () ->
        callback.done(op.mException);
      return op;
    }

    public static PostedOp create(@Nonnull Runnable runnable, @Nullable ParseCallback1 callback1) {
      PostedOp op = new PostedOp();
      op.mRunnable = () ->
      {
        try {
          runnable.run();
        } catch (Throwable t) {
          op.mException = ParseException.wrap(t);
        }
      };
      if (callback1 != null) {
        op.mCallback = () ->
          callback1.done(op.mException);
      }
      return op;
    }

    @Override
    public void run() {
      switch (mState++) {
        case 0:
          assert !onMainThread();
          mRunnable.run();
          if (mCallback == null) {
            return;
          }
          Throwable exception = mException;
          if (exception != null) {
            Throwable cause = exception.getCause();
            while (cause != null) {
              exception = cause;
              cause = exception.getCause();
            }
            exception.initCause(mCreated);
          }
          getUIHandler().post(this);
          break;
        case 1:
          assert onMainThread();
          mCallback.run();
          break;
        default:
          throw new IllegalStateException("PostedOp called " + mState + " times");
      }
    }
  }

  private static class ExecutorHandler {
  }
}
