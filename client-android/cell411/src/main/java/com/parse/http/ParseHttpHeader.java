package com.parse.http;
/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * A helper object to send requests to the server.
 */

public class ParseHttpHeader {
  public static final String HEADER_APPLICATION_ID = "X-Parse-Application-Id";
  public static final String HEADER_CLIENT_KEY = "X-Parse-Client-Key";
  public static final String HEADER_APP_BUILD_VERSION = "X-Parse-App-Build-Version";
  public static final String HEADER_APP_DISPLAY_VERSION = "X-Parse-App-Display-Version";
  public static final String HEADER_OS_VERSION = "X-Parse-OS-Version";
  public static final String HEADER_INSTALLATION_ID = "X-Parse-Installation-Id";
  public static final String USER_AGENT = "User-Agent";
  public static final String HEADER_SESSION_TOKEN = "X-Parse-Session-Token";
  public static final String HEADER_MASTER_KEY = "X-Parse-Master-Key";
  public static final String PARAMETER_METHOD_OVERRIDE = "_method";
}
