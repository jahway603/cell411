/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse;

import android.content.Context;
import android.util.Log;
import cell411.libcell.ConfigDepot;
import cell411.utils.ValueObserver;
import com.parse.controller.ParseCorePlugins;
import com.parse.http.ParseSyncUtils;
import com.parse.model.CurrentData;
import com.parse.model.ParseUser;
import com.parse.operation.ParseFieldOperations;
import com.parse.rest.ParseRequest;
import com.parse.utils.PLog;
import com.parse.utils.ParseFileUtils;
import okhttp3.OkHttpClient;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The {@code Parse} class contains static functions that handle global configuration for the Parse
 * library.
 */
@SuppressWarnings("unused")
public class Parse {
  public static final int LOG_LEVEL_VERBOSE = Log.VERBOSE;
  public static final int LOG_LEVEL_DEBUG = Log.DEBUG;
  public static final int LOG_LEVEL_INFO = Log.INFO;
  public static final int LOG_LEVEL_WARNING = Log.WARN;
  public static final int LOG_LEVEL_ERROR = Log.ERROR;
  //region LDS
  public static final int LOG_LEVEL_NONE = Integer.MAX_VALUE;
  final static Object lock = new Object();
  static final AtomicBoolean mInitialized =
    new AtomicBoolean(false);
  private static final String TAG = "com.parse.Parse";
  private static final int DEFAULT_MAX_RETRIES = ParseRequest.DEFAULT_MAX_RETRIES;
  private static final Object MUTEX = new Object();
  //region ParseCallbacks
  private static final Object MUTEX_CALLBACKS = new Object();
  static File parseDir;
  static File cacheDir;
  static File filesDir;
  static Configuration mConfiguration;

  //endregion
  //region Server URL
  private static Context applicationContext;
  private static boolean isLocalDatastoreEnabled;
  String mFlavor;

  // Suppress constructor to prevent subclassing
  private Parse() {
    throw new AssertionError();
  }

  public static synchronized void initialize(Configuration configuration) {
    synchronized (mInitialized) {
      if (isInitialized()) {
        PLog.w(TAG, "Parse is already initialized");
        new Exception("Parse is already initialized!").printStackTrace();
        return;
      }
      mConfiguration = configuration;


      ParseSyncUtils.registerParseSubclasses();

      checkCacheApplicationId();
      ParseFieldOperations.registerDefaultDecoders();
      CurrentData.loadInitialData();
      mInitialized.compareAndSet(false, true);
    }
  }

  /**
   * Returns the current server URL.
   */
  public static @Nonnull
  String getServer() {
    String res = getConfiguration().server;
    return Objects.requireNonNull(res);
  }

  /**
   * Validates the server URL.
   *
   * @param server The server URL to validate.
   * @return The validated server URL.
   */
  private static @Nullable
  String validateServerUrl(@Nullable String server) {

    // Add an extra trailing slash so that Parse REST commands include
    // the path as part of the server URL (i.e. http://api.myhost.com/parse)
    if (server != null && !server.endsWith("/")) {
      server = server + "/";
    }

    return server;
  }

  /**
   * Destroys this client and erases its local data store.
   * Calling this after {@link Parse#initialize} allows you to re-initialize this client with a
   * new configuration. Calling this while server requests are in progress can cause undefined
   * behavior.
   */
  public static void destroy() {
    ParseSyncUtils.unregisterParseSubclasses();
    ParseCorePlugins.get().reset();
  }

  /**
   * Verifies that the data stored on disk for Parse was generated using the same application that
   * is running now.
   */
  static void checkCacheApplicationId() {
    synchronized (MUTEX) {
      String applicationId = Parse.applicationId();
      if (applicationId != null) {
        File dir = ConfigDepot.getCacheDir();

        // Make sure the current version of the cache is for this application id.
        File applicationIdFile = ConfigDepot.mkdirs(dir, "applicationId");
        if (applicationIdFile.exists()) {
          // Read the file
          boolean matches = false;
          try {
            RandomAccessFile f = new RandomAccessFile(applicationIdFile, "r");
            byte[] bytes = new byte[(int) f.length()];
            f.readFully(bytes);
            f.close();
            String diskApplicationId = new String(bytes, StandardCharsets.UTF_8);
            matches = diskApplicationId.equals(applicationId);
          } catch (IOException e) {
            // Hmm, the applicationId file was malformed or something. Assume it
            // doesn't match.
          }

          // The application id has changed, so everything on disk is invalid.
          if (!matches) {
            try {
              ParseFileUtils.deleteDirectory(dir);
            } catch (IOException e) {
              // We're unable to delete the directory...
            }
          }
        }

        // Create the version file if needed.
        applicationIdFile = new File(dir, "applicationId");
        try {
          FileOutputStream out = new FileOutputStream(applicationIdFile);
          out.write(applicationId.getBytes(StandardCharsets.UTF_8));
          out.close();
        } catch (IOException e) {
          // Nothing we can really do about it.
        }
      }
    }
  }

  /**
   * Used by Parse LiveQuery
   */
  public static void checkInit() {
    if (Parse.applicationId() == null) {
      throw new RuntimeException(
        "applicationId is null. " + "You must call Parse.initialize(Context)" +
          " before using the Parse library.");
    }
  }

//  @SuppressWarnings("BooleanMethodIsAlwaysInverted")
//  public static boolean hasPermission(String permission) {
//    return mConfiguration.hasPermission(permission);
//  }

//  public static void requirePermission(String permission) {
//    if (!hasPermission(permission)) {
//      throw new IllegalStateException(
//        "To use this functionality, add this to your AndroidManifest.xml:\n" +
//          "<uses-permission android:name=\"" + permission + "\" />");
//    }
//  }

  /**
   * Returns the level of logging that will be displayed.
   */
  public static int getLogLevel() {
    return PLog.getLogLevel();
  }

  /**
   * Sets the level of logging to display, where each level includes all those below it. The default
   * level is {@link #LOG_LEVEL_NONE}. Please ensure this is set to {@link #LOG_LEVEL_ERROR}
   * or {@link #LOG_LEVEL_NONE} before deploying your app to ensure no sensitive information is
   * logged. The levels are:
   * <ul>
   * <li>{@link #LOG_LEVEL_VERBOSE}</li>
   * <li>{@link #LOG_LEVEL_DEBUG}</li>
   * <li>{@link #LOG_LEVEL_INFO}</li>
   * <li>{@link #LOG_LEVEL_WARNING}</li>
   * <li>{@link #LOG_LEVEL_ERROR}</li>
   * <li>{@link #LOG_LEVEL_NONE}</li>
   * </ul>
   *
   * @param logLevel The level of logcat logging that Parse should do.
   */
  public static void setLogLevel(int logLevel) {
    PLog.setLogLevel(logLevel);
  }

  public static boolean isInitialized() {
    synchronized (mInitialized) {
      return mInitialized.get();
    }
  }

  public static Configuration getConfiguration() {
    return Objects.requireNonNull(mConfiguration);
  }

  public static String applicationId() {
    return getConfiguration().applicationId;
  }

  public static String clientKey() {
    return getConfiguration().clientKey;
  }


//  public static BaseApp getApp() {
//    if(mBaseApp==null)
//      mBaseApp=BaseApp.require();
//    return mBaseApp;
//  }

  public static int getVersionCode()
  {
    return ConfigDepot.getVersionCode();
  }

  public static String getVersionName()
  {
    return ConfigDepot.getVersionName();
  }

  public static void addCurrentUserObserver(ValueObserver<? super ParseUser> observer)
  {
    CurrentData.addCurrentUserObserver(observer);

  }

  /**
   * Represents an opaque configuration for the {@code Parse} SDK configuration.
   */
  public static final class Configuration {
    public final String applicationId;
    public final String clientKey;
    public final String server;
    public final int maxRetries;
    public String versionCode;
    public String versionName;

    private Configuration(Builder builder) {
      this.applicationId = builder.applicationId;
      this.clientKey = builder.clientKey;
      this.server = builder.server;
      this.maxRetries = builder.maxRetries;
    }

    /**
     * Allows for simple constructing of a {@code Configuration} object.
     */
    public static final class Builder {
      public String versionCode;
      private String applicationId;
      private String clientKey;
      private String server;
      private boolean localDataStoreEnabled;
      private OkHttpClient.Builder clientBuilder;
      private int maxRetries = DEFAULT_MAX_RETRIES;

      /**
       * Set the application id to be used by Parse.
       *
       * @param applicationId The application id to set.
       * @return The same builder, for easy chaining.
       */
      public Builder applicationId(String applicationId) {
        this.applicationId = applicationId;
        return this;
      }

      /**
       * Set the client key to be used by Parse.
       *
       * @param clientKey The client key to set.
       * @return The same builder, for easy chaining.
       */
      public Builder clientKey(String clientKey) {
        this.clientKey = clientKey;
        return this;
      }

      /**
       * Set the server URL to be used by Parse.
       *
       * @param server The server URL to set.
       * @return The same builder, for easy chaining.
       */
      public Builder server(String server) {
        this.server = validateServerUrl(server);
        return this;
      }

      /**
       * Enable pinning in your application. This must be called before your application can use
       * pinning.
       *
       * @return The same builder, for easy chaining.
       */
      public Builder enableLocalDataStore() {
        localDataStoreEnabled = true;
        return this;
      }

      private Builder setLocalDatastoreEnabled(boolean enabled) {
        localDataStoreEnabled = enabled;
        return this;
      }

      /**
       * Set the {@link okhttp3.OkHttpClient.Builder} to use when communicating with the Parse
       * REST API
       * <p>
       *
       * @param builder The client builder, which will be modified for compatibility
       * @return The same builder, for easy chaining.
       */
      public Builder clientBuilder(OkHttpClient.Builder builder) {
        clientBuilder = builder;
        return this;
      }

      /**
       * Set the max number of times to retry Parse operations before deeming them a failure
       * <p>
       *
       * @param maxRetries The maximum number of times to retry. <=0 to never retry commands
       * @return The same builder, for easy chaining.
       */
      public Builder maxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
        return this;
      }

      /**
       * Construct this builder into a concrete {@code Configuration} instance.
       *
       * @return A constructed {@code Configuration} object.
       */
      public Configuration build() {
        return new Configuration(this);
      }
    }
  }


  @Nullable
  public static File ensureParent(@Nullable File file){
    if(file==null)
      return null;
    ConfigDepot.mkdirs(file.getParentFile());
    return file;
  }

  @Nonnull
  public static File getFilesDir() {
    return ConfigDepot.getCacheDir();
  }
}

