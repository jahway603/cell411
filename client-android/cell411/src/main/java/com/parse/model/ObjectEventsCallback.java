package com.parse.model;

public interface ObjectEventsCallback<T extends ParseObject> {
  void onEvents(ObjectEvent<T> event);

}
