/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package com.parse.model;

import cell411.utils.ValueObserver;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.callback.LogInCallback;
import com.parse.callback.LogOutCallback;
import com.parse.callback.SignUpCallback;
import com.parse.http.ParseSyncUtils;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@code ParseUser} is a local representation of user data that can be saved and retrieved from
 * the Parse cloud.
 */
@SuppressWarnings("unused")
@ParseClassName("_User")
public class ParseUser extends ParseObject {

  private static final String KEY_SESSION_TOKEN = "sessionToken";
  private static final String KEY_AUTH_DATA = "authData";
  private static final String KEY_USERNAME = "username";
  private static final String KEY_PASSWORD = "password";
  private static final String KEY_EMAIL = "email";

  private static final List<String> READ_ONLY_KEYS =
    Collections.unmodifiableList(Arrays.asList(KEY_SESSION_TOKEN, KEY_AUTH_DATA));

  private static final String PARCEL_KEY_IS_CURRENT_USER = "_isCurrentUser";
  //  private static final Object isAutoUserEnabledMutex = new Object();

  /**
   * Constructs a new ParseUser with no data in it. A ParseUser constructed in this way will not
   * have an objectId and will not persist to the database until {@link #signUp} is called.
   */
  public ParseUser() {
  }

  /**
   * Constructs a query for {@code ParseUser}.
   *
   * @see ParseQuery#getQuery(Class)
   */
  public static ParseQuery<ParseUser> getQuery() {
    return ParseQuery.getQuery(ParseUser.class);
  }

  //  /* package for tests */
  //  static ParseUserController getUserController()
  //  {
  //    return ParseCorePlugins.get().getUserController();
  //  }

  /* package for tests */
  //  public static ParseCurrentUserController getCurrentUserController() {
  //    return ParseCorePlugins.getInstance().getCurrentUserController();
  //  }

  /* package for tests */
  //  static ParseAuthenticationManager getAuthenticationManager() {
  //    return ParseCorePlugins.getInstance().getAuthenticationManager();
  //  }

  //  /**
  //   * Logs in a user with a username and password. On success, this saves the session to disk,
  //   so you
  //   * can retrieve the currently logged in user using {@link #getCurrentUser}.
  //   * <p/>
  //   * This is preferable to using {@link #logIn}, unless your code is already running from a
  //   * background thread.
  //   *
  //   * @param username The username to log in with.
  //   * @param password The password to log in with.
  //   * @return A Task that is resolved when logging in completes.
  //   */
  //  public static Task<ParseUser> logInInBackground(String username, String password)
  //  {
  //    if (username == null) {
  //      throw new IllegalArgumentException("Must specify a username for the user to log in with");
  //    }
  //    if (password == null) {
  //      throw new IllegalArgumentException("Must specify a password for the user to log in with");
  //    }
  //
  //    return getUserController().logInAsync(username, password).onSuccessTask(new
  //    Continuation<State, Task<ParseUser>>()
  //    {
  //      @Override
  //      public Task<ParseUser> then(Task<State> task)
  //      {
  //        State result = task.getResult();
  //        final ParseUser newCurrent = from(result);
  //        return saveCurrentUserAsync(newCurrent).onSuccess(new Continuation<Void, ParseUser>()
  //        {
  //          @Override
  //          public ParseUser then(Task<Void> task)
  //          {
  //            return newCurrent;
  //          }
  //        });
  //      }
  //    });
  //  }

  //  /**
  //   * Logs in a user with a username and password. On success, this saves the session to disk,
  //   so you
  //   * can retrieve the currently logged in user using {@link #getCurrentUser}.
  //   * <p/>
  //   * This is preferable to using {@link #logIn}, unless your code is already running from a
  //   * background thread.
  //   *
  //   * @param username The username to log in with.
  //   * @param password The password to log in with.
  //   * @param callback callback.done(user, e) is called when the login completes.
  //   */
  //  public static void logInInBackground(
  //    final String username, final String password, LogInCallback callback
  //  )
  //  {
  //    ParseTaskUtils.callbackOnMainThreadAsync(logInInBackground(username, password), callback);
  //  }

  //  /**
  //   * Authorize a user with a session token. On success, this saves the session to disk, so
  //   you can
  //   * retrieve the currently logged in user using {@link #getCurrentUser}.
  //   * <p/>
  //   * This is preferable to using {@link #become}, unless your code is already running from a
  //   * background thread.
  //   *
  //   * @param sessionToken The session token to authorize with.
  //   * @return A Task that is resolved when authorization completes.
  //   */
  //  public static Task<ParseUser> becomeInBackground(String sessionToken)
  //  {
  //    if (sessionToken == null) {
  //      throw new IllegalArgumentException("Must specify a sessionToken for the user to log in
  //      with");
  //    }
  //
  //    return getUserController().getUserAsync(sessionToken).onSuccessTask(task ->
  //    {
  //      State result = task.getResult();
  //
  //      final ParseUser user = from(result);
  //      return saveCurrentUserAsync(user).onSuccess(task1 -> user);
  //    });
  //  }

  //  /**
  //   * Authorize a user with a session token. On success, this saves the session to disk, so
  //   you can
  //   * retrieve the currently logged in user using {@link #getCurrentUser}.
  //   * <p/>
  //   * Typically, you should use {@link #becomeInBackground} instead of this, unless you are
  //   managing
  //   * your own threading.
  //   *
  //   * @param sessionToken The session token to authorize with.
  //   * @return The user if the authorization was successful.
  //   * @throws ParseException Throws an exception if the authorization was unsuccessful.
  //   */
  //  public static ParseUser become(String sessionToken) throws ParseException
  //  {
  //    return ParseTaskUtils.wait(becomeInBackground(sessionToken));
  //  }

  //  /**
  //   * Authorize a user with a session token. On success, this saves the session to disk, so
  //   you can
  //   * retrieve the currently logged in user using {@link #getCurrentUser}.
  //   * <p/>
  //   * This is preferable to using {@link #become}, unless your code is already running from a
  //   * background thread.
  //   *
  //   * @param sessionToken The session token to authorize with.
  //   * @param callback     callback.done(user, e) is called when the authorization completes.
  //   */
  //  public static void becomeInBackground(final String sessionToken, LogInCallback callback)
  //  {
  //    ParseTaskUtils.callbackOnMainThreadAsync(becomeInBackground(sessionToken), callback);
  //  }

  //  //TODO (grantland): Publicize
  //  /* package */
  //  public static Task<ParseUser> getCurrentUserAsync()
  //  {
  //    return Task.forResult(getCurrentUser());
  //  }

  /**
   * This retrieves the currently logged in ParseUser with a valid session, either from memory or
   * disk if necessary.
   *
   * @return The currently logged in ParseUser
   */

  public static ParseUser getCurrentUser() {
    return CurrentData.getCurrentUser();
  }

  public static String getCurrentSessionToken() {
    return CurrentData.getCurrentSessionToken();
  }

  //region Getter/Setter helper methods

  //  // Persists a user as currentUser to disk, and into the singleton
  //  private static Task<Void> saveCurrentUserAsync(ParseUser user)
  //  {
  //    CurrentData.setCurrentUser(user);
  //    return Task.forResult(null);
  //  }

  public static void logIn(String username, String password) {
    logIn(username, password, null);
  }

  public static void logIn(final String username, final String password, LogInCallback listener) {
    ParseSyncUtils.logIn(username, password, listener);
  }

  /**
   * Logs out the current user. This will remove the session from disk, log out
   * of linked services, and future calls to {@link #getCurrentUser()} will return {@code null}.
   * <p/>
   * This is preferable to using {@link #logOut}, unless your code is already running from a
   * background thread.
   */
  public static void logOutInBackground(LogOutCallback callback) {
    ParseSyncUtils.post(CurrentData::logOut, callback);
  }

  /**
   * Logs the current user out. This will remove the session from disk, log out
   * of linked services, and future calls to {@link #getCurrentUser()} will return {@code null}.
   * <p/>
   * Typically, you should use {@link # logOutInBackground()} instead of this,
   * unless you are managing your own threading.
   * <p/>
   * <strong>Note:</strong>: Any errors in the flow will be swallowed due to
   * backward-compatibility reasons. Please use {@link # logOutInBackground()}
   * if you'd wish to handle them.
   */
  public static void logOut() {
    CurrentData.logOut();
  }

  /**
   * Requests a password reset email to be sent to the specified email address associated with the
   * user account. This email allows the user to securely reset their password on the Parse site.
   * <p/>
   * Typically, you should use {@link # requestPasswordResetInBackground} instead of this, unless
   * you
   * are managing your own threading.
   *
   * @param email The email address associated with the user that forgot their password.
   * @throws ParseException Throws an exception if the server is inaccessible, or if an account
   *                        with that email
   *                        doesn't exist.
   */
  public static void requestPasswordReset(String email) throws ParseException {
    ParseSyncUtils.requestPasswordReset(email);
  }

  //  /**
  //   * Requests a password reset email to be sent in a background thread to the specified email
  //   * address associated with the user account. This email allows the user to securely reset
  //   their
  //   * password on the Parse site.
  //   * <p/>
  //   * This is preferable to using {@link #requestPasswordReset(String)}, unless your code is
  //   already
  //   * running from a background thread.
  //   *
  //   * @param email    The email address associated with the user that forgot their password.
  //   * @param callback callback.done(e) is called when the request completes.
  //   */
  //  public static void requestPasswordResetInBackground(
  //    final String email, RequestPasswordResetCallback callback
  //  )
  //  {
  //    Callable<Void> call = () ->
  //    {
  //      ParseUser.requestPasswordReset(email);
  //      return null;
  //    };
  //    ParseSyncUtils.post(call, callback);
  //  }

  //  /**
  //   * Registers a third party authentication callback.
  //   * <p/>
  //   * <strong>Note:</strong> This shouldn't be called directly unless developing a third party
  //   authentication
  //   * library.
  //   *
  //   * @param authType The name of the third party authentication source.
  //   * @param callback The third party authentication callback to be registered.
  //   * @see AuthenticationCallback
  //   */
  //  public static void registerAuthenticationCallback(
  //    String authType, AuthenticationCallback callback
  //  )
  //  {
  ////    getAuthenticationManager().register(authType, callback);
  //  }

  //  /**
  //   * Logs in a user with third party authentication credentials.
  //   * <p/>
  //   * <strong>Note:</strong> This shouldn't be called directly unless developing a third party
  //   authentication
  //   * library.
  //   *
  //   * @param authType The name of the third party authentication source.
  //   * @param authData The user credentials of the third party authentication source.
  //   * @return A {@code Task} is resolved when logging in completes.
  //   * @see AuthenticationCallback
  //   */
  //  public static Task<ParseUser> logInWithInBackground(
  //      final String authType, final Map<String, String> authData
  //  )
  //  {
  //    if (authType == null) {
  //      throw new IllegalArgumentException("Invalid authType: " + null);
  //    }
  //
  //    final Continuation<Void, Task<ParseUser>> logInWithTask = new Continuation<Void,
  //    Task<ParseUser>>() {
  //      @Override
  //      public Task<ParseUser> then(Task<Void> task) {
  //        return getUserController().logInAsync(authType, authData)
  //                                  .onSuccessTask(new Continuation<ParseUser.State,
  //                                  Task<ParseUser>>() {
  //                                    @Override
  //                                    public Task<ParseUser> then(Task<ParseUser.State> task) {
  //                                      ParseUser.State result = task.getResult();
  //                                      final ParseUser user = from(result);
  //                                      return saveCurrentUserAsync(user).onSuccess(new
  //                                      Continuation<Void, ParseUser>() {
  //                                        @Override
  //                                        public ParseUser then(Task<Void> task) {
  //                                          return user;
  //                                        }
  //                                      });
  //                                    }
  //                                  });
  //      }
  //    };
  //
  //    // Handle claiming of user.
  //    return getCurrentUserController().getAsync(false).onSuccessTask(new
  //    Continuation<ParseUser, Task<ParseUser>>() {
  //      @Override
  //      public Task<ParseUser> then(Task<ParseUser> task) {
  //        final ParseUser user = task.getResult();
  //        return Task.<Void>forResult(null).continueWithTask(logInWithTask);
  //      }
  //    });
  //  }

  //  /**
  //   * Enables automatic creation of anonymous users. After calling this method,
  //   * {@link #getCurrentUser()} will always have a value. The user will only be created on the
  //   server
  //   * once the user has been saved, or once an object with a relation to that user or an ACL that
  //   * refers to the user has been saved.
  //   * <p/>
  //   * <strong>Note:</strong> {@link ParseObject# saveEventually()} will not work if an item being
  //   * saved has a relation to an automatic user that has never been saved.
  //   */
  //  public static void enableAutomaticUser()
  //  {
  //    synchronized (isAutoUserEnabledMutex) {
  //      autoUserEnabled = true;
  //    }
  //  }
  //
  //  /* package */
  //  static void disableAutomaticUser()
  //  {
  //    synchronized (isAutoUserEnabledMutex) {
  //      autoUserEnabled = false;
  //    }
  //  }
  //
  //  /* package */
  //  public static boolean isAutomaticUserEnabled()
  //  {
  //    synchronized (isAutoUserEnabledMutex) {
  //      return autoUserEnabled;
  //    }
  //  }

  //  /**
  //   * Enables revocable sessions. This method is only required if you wish to use
  //   * {@link ParseSession} APIs and do not have revocable sessions enabled in your application
  //   * settings on your com.parse server.
  //   * <p/>
  //   * Upon successful completion of this {@link Task}, {@link ParseSession} APIs will be
  //   available
  //   * for use.
  //   *
  //   * @return A {@link Task} that will resolve when enabling revocable session
  //   */
  //  public static Task<Void> enableRevocableSessionInBackground()
  //  {
  //    return Task.forResult(null);
  //    // TODO(mengyan): Right now there is no way for us to add interceptor for this client,
  //    // so maybe we should move add interceptor steps to restClient()
  //    ParseCorePlugins.getInstance()
  //                    .registerUserController(new NetworkUserController(ParsePlugins.get()
  //                    .restClient(), true));
  //
  //    return getCurrentUserController().getAsync(false).onSuccessTask(new
  //    Continuation<ParseUser, Task<Void>>() {
  //      @Override
  //      public Task<Void> then(Task<ParseUser> task) {
  //        ParseUser user = task.getResult();
  //        if (user == null) {
  //          return Task.forResult(null);
  //        }
  //        return user.upgradeToRevocableSessionAsync();
  //      }
  //    });
  //  }

  public static void clearCurrentUser() {
    CurrentData.setCurrentUser(null);
  }

  public static String getCurrentId() {
    ParseUser user = getCurrentUser();
    if (user == null) {
      return null;
    } else {
      return user.getObjectId();
    }
  }

  public static void addCurrentUserObserver(ValueObserver<ParseUser> observer) {
    CurrentData.addCurrentUserObserver(observer);
  }

  public static void removeCurrentUserObserver(ValueObserver<ParseUser> observer) {
    CurrentData.removeCurrentUserObserver(observer);
  }

  //  public static Future<ParseUser> getFutureUser() {
  //    return new FutureParseUser();
  //  }
  //
  //
  //  static class FutureParseUser
  //    implements Future<ParseUser> , ValueObserver<ParseUser>
  //  {
  //    ParseUser mResult;
  //
  //    @Override
  //    public boolean cancel(boolean mayInterruptIfRunning) {
  //      return false;
  //    }
  //
  //    @Override
  //    public boolean isCancelled() {
  //      return false;
  //    }
  //
  //    @Override
  //    public boolean isDone() {
  //      return false;
  //    }
  //
  //    @Override
  //    public ParseUser get() throws ExecutionException, InterruptedException {
  //      ParseUser res;
  //      while(true){
  //        res=ParseUser.getCurrentUser();
  //        if(res!=null)
  //          return res;
  //        synchronized(this) {
  //          wait();
  //        }
  //      }
  //    }
  //
  //    @Override
  //    public ParseUser get(long timeout, TimeUnit unit) throws ExecutionException,
  //    InterruptedException, TimeoutException {
  //      return null;
  //    }
  //
  //
  //    @Override
  //    public void onChange(@Nullable @javax.annotations.Nullable ParseUser newValue,
  //                         @Nullable @javax.annotations.Nullable ParseUser oldValue)
  //    {
  //
  //    }
  //
  //  }

  //endregion
  @Override
  public State.Builder newStateBuilder(String className) {
    return new State.Builder();
  }

  @Override
  public
    /* package */ State getState() {
    return (State) super.getState();
  }

  @Override
  public void setState(ParseObject.State newState) {
    String token = getSessionToken();
    if (token != null) {
      Object newToken = newState.get(KEY_SESSION_TOKEN);
      if (newToken == null) {
        State.Builder newStateBuilder = newState.newBuilder();
        newStateBuilder.put(KEY_SESSION_TOKEN, token);
        newState = newStateBuilder.build();
      }
    }

    super.setState(newState);
  }

  //  @Override
  //  public JSONObject toRest(
  //    ParseObject.State state, List<ParseOperationSet> operationSetQueue, ParseEncoder
  //    objectEncoder
  //  )
  //  {
  //    // Create a sanitized copy of operationSetQueue with `password` removed if necessary
  //    List<ParseOperationSet> cleanOperationSetQueue = operationSetQueue;
  //    for (int i = 0; i < operationSetQueue.size(); i++) {
  //      ParseOperationSet operations = operationSetQueue.get(i);
  //      if (operations.containsKey(KEY_PASSWORD)) {
  //        if (cleanOperationSetQueue == operationSetQueue) {
  //          cleanOperationSetQueue = new LinkedList<>(operationSetQueue);
  //        }
  //        ParseOperationSet cleanOperations = new ParseOperationSet(operations);
  //        cleanOperations.remove(KEY_PASSWORD);
  //        cleanOperationSetQueue.set(i, cleanOperations);
  //      }
  //    }
  //    return super.toRest(state, cleanOperationSetQueue, objectEncoder);
  //  }

  //  @Override
  //  public Task<Void> handleSaveResultAsync(
  //    ParseObject.State result, ParseOperationSet operationsBeforeSave
  //  )
  //  {
  //    boolean success = result != null;
  //    if (success) {
  //      operationsBeforeSave.remove(KEY_PASSWORD);
  //    }
  //
  //    return super.handleSaveResultAsync(result, operationsBeforeSave);
  //  }
  //
  @Override
  public void validateSave() {
    synchronized (mutex) {
//      if (Util.theGovernmentIsLying()) {
//        return;
//      }
      if (getObjectId() == null) {
        throw new IllegalArgumentException(
          "Cannot save a ParseUser until it has been signed up. Call signUp first.");
      }

      if (isAuthenticated() || !isDirty() || isCurrentUser()) {
        return;
      }
    }

    throw new IllegalArgumentException("Cannot save a ParseUser that is not authenticated.");
  }

  //  @Override
  //  public void validateSaveEventually() throws ParseException
  //  {
  //    if (isDirty(KEY_PASSWORD)) {
  //      // TODO(mengyan): Unify the exception we throw when validate fails
  //      throw new ParseException(ParseException.OTHER_CAUSE,
  //        "Unable to saveEventually on a ParseUser with dirty password");
  //    }
  //  }

  @Override
  public void remove(@Nonnull String key) {
    if (KEY_USERNAME.equals(key)) {
      throw new IllegalArgumentException("Can't remove the username key.");
    }
    super.remove(key);
  }

  @Override
  public boolean isKeyMutable(String key) {
    return !READ_ONLY_KEYS.contains(key);
  }

  @Override
  public boolean needsDefaultACL() {
    return false;
  }

  /**
   * Whether the ParseUser has been authenticated on this device. This will be true if the ParseUser
   * was obtained via a logIn or signUp method. Only an authenticated ParseUser can be saved (with
   * altered attributes) and deleted.
   */
  public boolean isAuthenticated() {
    if (getState().sessionToken() == null) {
      return false;
    }
    ParseUser current = ParseUser.getCurrentUser();
    if (current == null) {
      return false;
    }
    return current.hasSameId(this);

  }

  //  /* package for tests */ Task<Void> cleanUpAuthDataAsync()
  //  {
  //    ParseAuthenticationManager controller = getAuthenticationManager();
  //    Map<String, Map<String, String>> authData;
  //    synchronized (mutex) {
  //      authData = getState().authData();
  //      if (authData.size() == 0) {
  //        return Task.forResult(null); // Nothing to see or do here...
  //      }
  //    }
  //
  //    List<Task<Void>> tasks = new ArrayList<>();
  //    Iterator<Map.Entry<String, Map<String, String>>> i = authData.entrySet().iterator();
  //    while (i.hasNext()) {
  //      Map.Entry<String, Map<String, String>> entry = i.next();
  //      if (entry.getValue() == null) {
  //        i.remove();
  //        tasks.add(controller.restoreAuthenticationAsync(entry.getKey(), null).makeVoid());
  //      }
  //    }
  //
  //    State newState = getState().newBuilder().authData(authData).build();
  //    setState(newState);
  //
  //    return Task.whenAll(tasks);
  //  }

  /* package */
  public boolean isCurrentUser() {
    synchronized (mutex) {
      ParseUser user = getCurrentUser();
      return user != null && user.getObjectId().equals(getObjectId());
    }
  }

  /**
   * @return the session token for a user, if they are logged in.
   */
  public String getSessionToken() {
    return getState().sessionToken();
  }

  //  /* package for testes */ Map<String, Map<String, String>> getAuthData()
  //  {
  //    synchronized (mutex) {
  //      Map<String, Map<String, String>> authData = getMap(KEY_AUTH_DATA);
  //      if (authData == null) {
  //        // We'll always return non-null for now since we don't have any null checking in place.
  //        // Be aware not to get and set without checking size or else we'll be adding a value
  //        that
  //        // wasn't there in the first place.
  //        authData = new HashMap<>();
  //      }
  //      return authData;
  //    }
  //  }
  //
  //  private Map<String, String> getAuthData(String authType)
  //  {
  //    return getAuthData().get(authType);
  //  }
  //
  //  /* package */
  //  public void putAuthData(String authType, Map<String, String> authData)
  //  {
  //    synchronized (mutex) {
  //      Map<String, Map<String, String>> newAuthData = getAuthData();
  //      newAuthData.put(authType, authData);
  //      performPut(KEY_AUTH_DATA, newAuthData);
  //    }
  //  }
  //
  //  private void removeAuthData(String authType)
  //  {
  //    synchronized (mutex) {
  //      Map<String, Map<String, String>> newAuthData = getAuthData();
  //      newAuthData.remove(authType);
  //      performPut(KEY_AUTH_DATA, newAuthData);
  //    }
  //  }

  /**
   * Retrieves the username.
   */
  public String getUsername() {
    return getString(KEY_USERNAME);
  }

  /**
   * Sets the username. Usernames cannot be null or blank.
   *
   * @param username The username to set.
   */
  public void setUsername(String username) {
    put(KEY_USERNAME, username);
  }

  /* package for tests */ String getPassword() {
    return getString(KEY_PASSWORD);
  }

  /**
   * Sets the password.
   *
   * @param password The password to set.
   */
  public void setPassword(String password) {
    put(KEY_PASSWORD, password);
  }

  /**
   * Retrieves the email address.
   */
  public String getEmail() {
    return getString(KEY_EMAIL);
  }

  /**
   * Sets the email address.
   *
   * @param email The email address to set.
   */
  public void setEmail(String email) {
    put(KEY_EMAIL, email);
  }

  //region Third party authentication

  /**
   * Indicates whether this {@code ParseUser} was created during this session through a call to
   * {@link #signUp()} or by logging in with a linked service such as Facebook.
   */
  public boolean isNew() {
    return getState().isNew();
  }

  //  private void stripAnonymity()
  //  {
  //
  //  }

  //  // TODO(grantland): Can we replace this with #revert(String)?
  //  private void restoreAnonymity(Map<String, String> anonymousData)
  //  {
  //  }

  //  /* package for tests */ Task<Void> saveAsync(String sessionToken, boolean isLazy,
  //  Task<Void> toAwait)
  //  {
  //    Task<Void> task;
  //    task = super.saveAsync(sessionToken, toAwait);
  //
  //    if (isCurrentUser()) {
  //      // If the user is the currently logged in user, we persist all data to disk
  //      return task.onSuccessTask(new Continuation<Void, Task<Void>>()
  //      {
  //        @Override
  //        public Task<Void> then(Task<Void> task)
  //        {
  //          return cleanUpAuthDataAsync();
  //        }
  //      }).onSuccessTask(new Continuation<Void, Task<Void>>()
  //      {
  //        @Override
  //        public Task<Void> then(Task<Void> task)
  //        {
  //          return saveCurrentUserAsync(ParseUser.this);
  //        }
  //      });
  //    }
  //
  //    return task;
  //  }

  //  /**
  //   * Signs up a new user. You should call this instead of {@link #save} for new ParseUsers. This
  //   * will create a new ParseUser on the server, and also persist the session on disk so that
  //   you can
  //   * access the user using {@link #getCurrentUser}.
  //   * <p/>
  //   * A username and password must be set before calling signUp.
  //   * <p/>
  //   * This is preferable to using {@link #signUp}, unless your code is already running from a
  //   * background thread.
  //   *
  //   * @return A Task that is resolved when sign up completes.
  //   */
  //  public Task<Void> signUpInBackground()
  //  {
  //    return taskQueue.enqueue(new Continuation<Void, Task<Void>>()
  //    {
  //      @Override
  //      public Task<Void> then(Task<Void> task)
  //      {
  //        return signUpAsync(task);
  //      }
  //    });
  //  }

  //  /* package for tests */ Task<Void> signUpAsync(Task<Void> toAwait)
  //  {
  //    final ParseUser user = getCurrentUser(); //TODO (grantland): convert to async
  //    synchronized (mutex) {
  //      final String sessionToken = user != null ? user.getSessionToken() : null;
  //      if (ParseTextUtils.isEmpty(getUsername())) {
  //        return Task.forError(new IllegalArgumentException("Username cannot be missing or
  //        blank"));
  //      }
  //
  //      if (ParseTextUtils.isEmpty(getPassword())) {
  //        return Task.forError(new IllegalArgumentException("Password cannot be missing or
  //        blank"));
  //      }
  //
  ////      if (getObjectId() != null) {
  ////        // For anonymous users, there may be an objectId. Setting the
  ////        // userName will have removed the anonymous link and set the
  ////        // value in the authData object to JSONObject.NULL, so we can
  ////        // just treat it like a save operation.
  ////        Map<String, Map<String, String>> authData = getAuthData();
  ////
  ////
  ////        // Otherwise, throw.
  ////        return Task.forError(new IllegalArgumentException("Cannot sign up a user that has
  // already signed up."));
  ////      }
  //
  //      // If the operationSetQueue is has operation sets in it, then a save or signUp is in
  //      progress.
  //      // If there is a signUp or save already in progress, don't allow another one to start.
  //      if (operationSetQueue.size() > 1) {
  //        return Task.forError(new IllegalArgumentException("Cannot sign up a user that is
  //        already signing up."));
  //      }
  //
  //      // If the current user is an anonymous user, merge this object's data into the
  //      anonymous user
  //      // and save.
  //
  //      final ParseOperationSet operations = startSave();
  //
  //      return toAwait.onSuccessTask(new Continuation<Void, Task<Void>>()
  //      {
  //        @Override
  //        public Task<Void> then(Task<Void> task)
  //        {
  //          return getUserController().signUpAsync(getState(), operations, sessionToken)
  //            .continueWithTask(new Continuation<ParseUser.State, Task<Void>>()
  //            {
  //              @Override
  //              public Task<Void> then(final Task<ParseUser.State> signUpTask)
  //              {
  //                ParseUser.State result = signUpTask.getResult();
  //                return handleSaveResultAsync(result, operations).continueWithTask(
  //                  new Continuation<Void, Task<Void>>()
  //                  {
  //                    @Override
  //                    public Task<Void> then(Task<Void> task)
  //                    {
  //                      if (!signUpTask.isCancelled() && !signUpTask.isFaulted()) {
  //                        return saveCurrentUserAsync(ParseUser.this);
  //                      }
  //                      return signUpTask.makeVoid();
  //                    }
  //                  });
  //              }
  //            });
  //        }
  //      });
  //    }
  //  }

  /**
   * Signs up a new user. You should call this instead of {@link #save} for new ParseUsers. This
   * will create a new ParseUser on the server, and also persist the session on disk so that you can
   * access the user using {@link #getCurrentUser}.
   * <p/>
   * A username and password must be set before calling signUp.
   * <p/>
   * your own threading.
   *
   * @throws ParseException Throws an exception if the server is inaccessible, or if the username
   *                        has already
   *                        been taken.
   */
  public ParseUser signUp() throws ParseException {

    return ParseSyncUtils.signUp(this);
  }

  /**
   * Signs up a new user. You should call this instead of {@link #save} for new ParseUsers. This
   * will create a new ParseUser on the server, and also persist the session on disk so that you can
   * access the user using {@link #getCurrentUser}.
   * <p/>
   * A username and password must be set before calling signUp.
   * <p/>
   * This is preferable to using {@link #signUp}, unless your code is already running from a
   * background thread.
   *
   * @param callback callback.done(user, e) is called when the signUp completes.
   */
  public void signUpInBackground(SignUpCallback callback) {
    ParseSyncUtils.post(this::signUp, callback);
  }

  //endregion

  //  /**
  //   * Indicates whether this user is linked with a third party authentication source.
  //   * <p/>
  //   * <strong>Note:</strong> This shouldn't be called directly unless developing a third party
  //   authentication
  //   * library.
  //   *
  //   * @param authType The name of the third party authentication source.
  //   * @return {@code true} if linked, otherwise {@code false}.
  //   * @see AuthenticationCallback
  //   */
  //  public boolean isLinked(String authType)
  //  {
  //    Map<String, Map<String, String>> authData = getAuthData();
  //    return authData.containsKey(authType) && authData.get(authType) != null;
  //  }

  //  //region Legacy/Revocable Session Tokens
  //  /* package */ Task<Void> upgradeToRevocableSessionAsync()
  //  {
  //    return taskQueue.enqueue(new Continuation<Void, Task<Void>>()
  //    {
  //      @Override
  //      public Task<Void> then(Task<Void> toAwait)
  //      {
  //        return upgradeToRevocableSessionAsync(toAwait);
  //      }
  //    });
  //  }

  //  private Task<Void> upgradeToRevocableSessionAsync(Task<Void> toAwait)
  //  {
  //    final String sessionToken = getSessionToken();
  //    return toAwait.continueWithTask(new Continuation<Void, Task<String>>()
  //    {
  //      @Override
  //      public Task<String> then(Task<Void> task)
  //      {
  //        return ParseSession.upgradeToRevocableSessionAsync(sessionToken);
  //      }
  //    }).onSuccessTask(new Continuation<String, Task<Void>>()
  //    {
  //      @Override
  //      public Task<Void> then(Task<String> task)
  //      {
  //        String result = task.getResult();
  //        return setSessionTokenInBackground(result);
  //      }
  //    });
  //  }

  //endregion
  @Override
  public void afterSave() {
    if (getCurrentUser() == this) {
      CurrentData.saveCurrentUser();
    }
  }

  public static class State extends ParseObject.State {

    private final boolean isNew;

    private State(Builder builder) {
      super(builder);
      isNew = builder.isNew;
    }

    //    /* package */
    //    public State(Parcel source, String className, ParseParcelDecoder decoder)
    //    {
    //      super(source, className, decoder);
    //      isNew = source.readByte() == 1;
    //    }

    @SuppressWarnings("unchecked")
    @Override
    public Builder newBuilder() {
      return new Builder(this);
    }

    //    @Override
    //    protected void writeToParcel(Parcel dest, ParseParcelEncoder encoder)
    //    {
    //      super.writeToParcel(dest, encoder);
    //      dest.writeByte(isNew ? (byte) 1 : 0);
    //    }

    public String sessionToken() {
      return (String) get(KEY_SESSION_TOKEN);
    }

    @SuppressWarnings("unchecked")
    public Map<String, Map<String, String>> authData() {
      Map<String, Map<String, String>> authData =
        (Map<String, Map<String, String>>) get(KEY_AUTH_DATA);
      if (authData == null) {
        // We'll always return non-null for now since we don't have any null checking in place.
        // Be aware not to get and set without checking size or else we'll be adding a value that
        // wasn't there in the first place.
        authData = new HashMap<>();
      }
      return authData;
    }

    public boolean isNew() {
      return isNew;
    }

    public static class Builder extends Init<Builder> {

      private boolean isNew;

      public Builder() {
        super("_User");
      }

      /* package */ Builder(State state) {
        super(state);
        isNew = state.isNew();
      }

      @Override
      public Builder self() {
        return this;
      }

      @Override
      public State build() {
        return new State(this);
      }

      @Override
      public Builder apply(ParseObject.State other) {
        isNew(((State) other).isNew());
        return super.apply(other);
      }

      public Builder sessionToken(String sessionToken) {
        return put(KEY_SESSION_TOKEN, sessionToken);
      }

      public Builder authData(Map<String, Map<String, String>> authData) {
        return put(KEY_AUTH_DATA, authData);
      }

      @SuppressWarnings("unchecked")
      public Builder putAuthData(String authType, Map<String, String> authData) {
        Map<String, Map<String, String>> newAuthData =
          (Map<String, Map<String, String>>) serverData.get(KEY_AUTH_DATA);
        if (newAuthData == null) {
          newAuthData = new HashMap<>();
        }
        newAuthData.put(authType, authData);
        serverData.put(KEY_AUTH_DATA, newAuthData);
        return this;
      }

      public Builder isNew(boolean isNew) {
        this.isNew = isNew;
        return this;
      }
    }
  }
}

