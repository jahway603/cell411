package com.parse.model;

import cell411.utils.Reflect;
import cell411.utils.ValueObserver;
import com.parse.Parse;
import com.parse.encoder.ParseObjectCurrentCoder;
import com.parse.encoder.ParseUserCurrentCoder;
import com.parse.offline.FileObjectStore;
import com.parse.offline.InstallationId;

import java.io.File;
import java.util.ArrayList;

import static cell411.libcell.ConfigDepot.getParseDir;

public class CurrentData {
  public static final String FILENAME_CURRENT_USER = "currentUser";
  public static final String FILENAME_CURRENT_INSTALLATION = "currentInstallation";
  public static final String FILENAME_CURRENT_CONFIG = "currentConfig";
  public static final String INSTALLATION_ID_LOCATION = "installationId";
  private static final Data mData = new Data();

  public static InstallationId getOfflineInstallationId() {
    return mData.getOfflineInstallationid();
  }

  public static String getInstallationId() {
    return mData.getInstallationId().get();
  }

  public static ParseInstallation getCurrentInstallation() {
    return mData.mCurrentInstallationStore.get();
  }

  public static void setCurrentInstallation(ParseInstallation installation) {
    mData.setCurrentInstallation(installation);
  }

  public static void addCurrentUserObserver(ValueObserver<? super ParseUser> observer) {
    mData.addCurrentUserObserver(observer);
  }

  public static void removeCurrentUserObserver(ValueObserver<ParseUser> observer) {
    mData.removeCurrentUserObserver(observer);
  }

  public static String getCurrentSessionToken() {
    return mData.getCurrentSessionToken();
  }

  public static void logOut() {
    mData.logOut();
  }

  public static <T extends ParseObject> boolean isCurrent(T t) {
    return mData.isCurrent(t);
  }

  public static void loadInitialData() {
    mData.loadInitialData();
  }

  public static ParseUser getCurrentUser() {
    Parse.checkInit();
    return mData.getCurrentUser();
  }

  public static void setCurrentUser(ParseUser user) {
    Parse.checkInit();
    mData.setCurrentUser(user);
  }

  public static void saveCurrentUser() {
    mData.getCurrentUserStore().save();
  }

  public static void saveAndLoad()
  {
    saveCurrentUser();
    saveCurrentInstallation();
    loadInitialData();
  }

  public static void saveCurrentInstallation()
  {
    mData.getCurrentInstallationStore().save();
  }

  static class Data {
    private FileObjectStore<ParseUser> mCurrentUserStore;
    private FileObjectStore<ParseInstallation> mCurrentInstallationStore;
    private InstallationId installationId;
    private final ArrayList<ValueObserver<? super ParseUser>> mCurrentUserObservers
        = new ArrayList<>();

    public FileObjectStore<ParseUser> getCurrentUserStore() {
      if (mCurrentUserStore == null) {
        mCurrentUserStore=createCurrentUserStore();
      }
      return mCurrentUserStore;
    }

    private static FileObjectStore<ParseUser> createCurrentUserStore() {
      FileObjectStore<ParseUser> res;
      ParseUserCurrentCoder coder = ParseUserCurrentCoder.get();
      File file = new File(getParseDir(), FILENAME_CURRENT_USER);
      Reflect.announce("currentUserStore: "+file);
      res = new FileObjectStore<>(ParseUser.class , file, coder);
      System.out.println("initialize from: "+file);
      for(ValueObserver<? super ParseUser> observer : mData.mCurrentUserObservers){
        res.addObserver(observer);
      }
      return res;
    }


    // We initialize this lazily, but we only try to read the disk when Parse
    // calls init as it gets initialized.  That way, you can put an observer on
    // the value before we are open for business, and find out as soon as we
    // open up.
    public FileObjectStore<ParseInstallation> getCurrentInstallationStore() {
      if (mCurrentInstallationStore == null) {
        mCurrentInstallationStore=createCurrentInstallationStore();
      }
      return mCurrentInstallationStore;
    }

    private FileObjectStore<ParseInstallation> createCurrentInstallationStore() {
      FileObjectStore<ParseInstallation> res;
      File file = new File(getParseDir(), FILENAME_CURRENT_INSTALLATION);
      ParseObjectCurrentCoder coder = ParseObjectCurrentCoder.get();
      res = new FileObjectStore<>(ParseInstallation.class, file, coder);
      System.out.println("initialize from: "+file);
      return res;
    }

    public InstallationId getInstallationId() {
      if (installationId == null) {
        installationId = new InstallationId(new File(
            getParseDir(), INSTALLATION_ID_LOCATION));
      }
      return installationId;
    }

    public InstallationId getOfflineInstallationid() {
      return installationId;
    }

    public void setCurrentInstallation(ParseInstallation installation) {
      mCurrentInstallationStore.set(installation);
    }

    public void logOut() {
      // Anybody caching objects needs to be observing this value
      // and clearing their caches when it changes, unless they don't
      // mind the next user inheriting it.
      setCurrentUser(null);
    }

    public void loadInitialData() {
      getCurrentInstallationStore().load();
      getCurrentUserStore().load();
    }

    public ParseUser getCurrentUser() {
      return getCurrentUserStore().get();
    }

    public void setCurrentUser(ParseUser user) {
      getCurrentUserStore().set(user);
    }

    public <T extends ParseObject> boolean isCurrent(T t) {
      if (t == null) {
        throw new NullPointerException("Don't give me null");
      } else {
        Class<? extends ParseObject> aClass = t.getClass();
        if (ParseUser.class.isAssignableFrom(aClass)) {
          return getCurrentUser().hasSameId(t);
        } else if (ParseInstallation.class.isAssignableFrom(aClass)) {
          return getCurrentInstallation().hasSameId(t);
        } else {
          throw new IllegalArgumentException("I don't know what you're saying.  Who sent you?");
        }
      }
    }

    public String getCurrentSessionToken() {
      ParseUser user = getCurrentUser();
      if (user == null) {
        return null;
      }
      return user.getSessionToken();
    }

    public void addCurrentUserObserver(ValueObserver<? super ParseUser> observer) {
      mCurrentUserObservers.add(observer);
      if(mCurrentUserStore!=null)
        mCurrentUserStore.addObserver(observer);
    }

    public void removeCurrentUserObserver(ValueObserver<ParseUser> observer) {
      mCurrentUserObservers.remove(observer);
    }
  }

}
