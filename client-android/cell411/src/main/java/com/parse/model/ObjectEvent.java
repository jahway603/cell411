package com.parse.model;

import cell411.json.JSONObject;
import cell411.utils.Util;
import com.parse.ParseQuery;
import com.parse.http.ParseSyncUtils;
import com.parse.livequery.LiveQueryEventId;
import com.parse.livequery.Subscription;

import java.util.Collection;
import java.util.Collections;

public class ObjectEvent<T extends ParseObject> {
  private final ParseRelation<T> mRelation;
  private final ParseQuery<T> mQuery;
  private final LiveQueryEventId mEventId;
  private final JSONObject mInput;
  private final ParseQuery.State<T> mState;
  private T mObject;
  private T mOriginal;
  StackTraceElement[] mStackTraceElements1;
  StackTraceElement[] mStackTraceElements2;

  public ObjectEvent(Subscription<T> subscription, JSONObject jsonObject,
                     StackTraceElement[] stackTrace)
  {
    mStackTraceElements1=stackTrace;
    mStackTraceElements2=new Exception().getStackTrace();
    mInput = jsonObject;
    mQuery = subscription.getQuery();
    mEventId = LiveQueryEventId.valueOf(Util.uc(jsonObject.getString("op")));
    mState = subscription.getQueryState();
    mObject = getObject();//object;
    mRelation = null;
  }

  public ObjectEvent(ParseRelation<T> relation, JSONObject jsonObject) {
    mInput = jsonObject;
    mQuery = relation.getQuery();
    mState = relation.getQueryState();
    mEventId = LiveQueryEventId.valueOf(Util.uc(jsonObject.getString("op")));
    mObject = getObject();//object;
    mRelation = null;
  }

  public T getOriginal() {
    if (mOriginal == null) {
      JSONObject origPO = mInput.optJSONObject("original");
      if (origPO != null) {
        mOriginal = ParseSyncUtils.fromJSON(origPO, mState, null);
      }
    }
    return mOriginal;
  }

  public T getObject() {
    if (mObject == null) {
      JSONObject jsonPO = mInput.getJSONObject("object");
      mObject = ParseSyncUtils.fromJSON(jsonPO, mState, null);
    }
    return mObject;
  }

  public ParseQuery<T> getQuery() {
    return mQuery;
  }

  public LiveQueryEventId getEventId() {
    return mEventId;
  }

  public Dispatcher<T> dispatcher(final ObjectEventsCallback<T> callback) {
    return new Dispatcher<>(this, Collections.singletonList(callback));
  }

  public Dispatcher<T> dispatcher(
    final Collection<? extends ObjectEventsCallback<T>> handleEventsCallbacks) {
    return new Dispatcher<>(this, handleEventsCallbacks);
  }
}
