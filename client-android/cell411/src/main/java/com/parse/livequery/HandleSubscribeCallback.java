package com.parse.livequery;

import com.parse.ParseQuery;
import com.parse.model.ParseObject;

public interface HandleSubscribeCallback<T extends ParseObject> {
  void onSubscribe(ParseQuery<T> query);
}
