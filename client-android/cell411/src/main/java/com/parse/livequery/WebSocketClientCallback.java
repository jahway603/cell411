package com.parse.livequery;

public interface WebSocketClientCallback {
  void onOpen();

  void onMessage(String message);

  void onClose();

  void onError(Throwable exception);

  void stateChanged();
}
