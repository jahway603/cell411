//package com.parse.livequery;
//
//import java.lang.reflect.InvocationHandler;
//import java.lang.reflect.Method;
//import java.lang.reflect.Proxy;
//
//import cell411.utils.CarefulHandler;
//import cell411.utils.HandlerThreadPlus;
//import cell411.utils.Reflect;
//import cell411.utils.Util;
//import okhttp3.WebSocketListener;
//
//public class WebSocketListenerProxy {
//  WebSocketClientCallback mTarget;
//  public WebSocketListenerProxy(WebSocketClientCallback target) {
//    mTarget=target;
//  }
//
//  public static WebSocketClientCallback createProxy(
//    final WebSocketClientCallback webSocketClientCallback)
//  {
//    WebSocketListenerProxy proxy = new WebSocketListenerProxy(webSocketClientCallback);
//    return proxy.createProxy();
//  }
//
//  class WSInvocationHandler implements InvocationHandler
//  {
//    @Override
//    public Object invoke(final Object proxy, final Method method, final Object[] args)
//      throws Throwable
//    {
//      Class<?>[] argTypes;
//      switch(method.getName()) {
//        case "equals":
//          if (method.getParameterCount() != 1)
//            break;
//          argTypes = method.getParameterTypes();
//          if (!argTypes[0].equals(Object.class))
//            break;
//          return method.invoke(mTarget, args);
//        case "toString":
//          if(method.getParameterCount()!=0)
//            break;
//          return mTarget.toString();
//      }
//      mHandler.post(new Invocation(proxy,method,args));
//      return null;
//    }
//  }
//  class Invocation implements Runnable {
//    private Throwable mException;
//    final Object mProxy;
//    final Method mMethod;
//    final Object[] mArgs;
//    boolean mInvoked=false;
//    Object mReturn;
//
//    Invocation(final Object proxy,
//               final Method method, final Object[] args )
//    {
//      mProxy=proxy;
//      mMethod=method;
//      mArgs=args;
//    }
//
//    @Override
//    public void run()
//    {
//      try {
//        mReturn = mMethod.invoke(mTarget, mArgs);
//      } catch ( Throwable t ) {
//        mException = t;
//      }
//    }
//  }
//
//  WebSocketClientCallback createProxy() {
//    try {
//      InvocationHandler handler = new WSInvocationHandler();
//      Class<?> proxyClass = Proxy.getProxyClass(
//        WebSocketListener.class.getClassLoader(),
//        WebSocketListener.class);
//      WebSocketListenerProxy wsl = (WebSocketListener)
//        proxyClass.getConstructor(InvocationHandler.class).newInstance(handler);
//      Reflect.announce("Created Proxy: "+wsl);
//      return wsl;
//    } catch (Exception e) {
//      throw Util.rethrow(e);
//    }
//  }
//  HandlerThreadPlus mHandlerThreadPlus = new HandlerThreadPlus("WebSocketClient Thread");
//  CarefulHandler mHandler = mHandlerThreadPlus.getHandler();;
//
//}
