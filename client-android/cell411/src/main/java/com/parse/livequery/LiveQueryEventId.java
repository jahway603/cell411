package com.parse.livequery;

public enum LiveQueryEventId {
  CREATE,
  ENTER,
  UPDATE,
  LEAVE,
  DELETE
}
