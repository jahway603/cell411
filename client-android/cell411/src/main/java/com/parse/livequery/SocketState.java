package com.parse.livequery;

public enum SocketState {
  NONE,
  CONNECTING,
  CONNECTED,
  DISCONNECTING,
  DISCONNECTED
}
