package com.safearx.cell411;

import android.content.Context;
import android.os.Bundle;
import cell411.android.RingtoneData;
import cell411.libcell.ConfigDepot;
import cell411.logic.LiveQueryConnection;
import cell411.logic.LiveQueryService;
import cell411.logic.NotificationAgent;
import cell411.parse.XEntity;
import cell411.services.LocationService;
import cell411.ui.base.BaseActivity;
import cell411.ui.base.BaseApp;
import cell411.ui.base.MainActivity;
import cell411.utils.ResultListener;
import cell411.utils.Util;
import cell411.utils.XLog;
import cell411.utils.XTAG;
import com.parse.model.ParseGeoPoint;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.RECEIVE_BOOT_COMPLETED;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS;

public class Cell411 extends BaseApp {
  final static XTAG TAG = new XTAG();
  public static final long TIME_TO_LIVE_FOR_CHAT_ON_ALERTS = 86400 * 1000 * 3;

  static {
    XLog.i(TAG,"Loading Class %s",Cell411.class);
  }

  LiveQueryConnection mLiveQueryConnection;
  NotificationAgent mNotificationAgent;
  private boolean mDarkModeChanged = false;

  public Cell411() {
  }

  static public Cell411 opt() {
    return (Cell411) BaseApp.opt();
  }
  static public Cell411 req() {
    return (Cell411) BaseApp.req();
  }

  public boolean isDone() {
    return false;
  }


  //    R.dimen.compat_notification_large_icon_max_width;
  public int getNotificationWidth() {
    return getResources().getDimensionPixelSize(
        R.dimen.compat_notification_large_icon_max_width
    );
  }

  public int getNotificationHeight() {
    return getResources().getDimensionPixelSize(
        R.dimen.compat_notification_large_icon_max_height
    );
  }

//  public ObservableValue<Bitmap> getAvatar() {
//    return mAvatar;
//  }

  @Nonnull
  public Context createWindowContext(int type, @Nullable Bundle options) {
    return super.createWindowContext(type, options);
  }

  public int getPrimaryColor() {
    return getResources().getColor(R.color.colorPrimary, getTheme());
  }

  public BaseActivity getCurrentActivity() {
    return super.getCurrentActivity();
  }


  public Cell411Activity getMainActivity() {
    return (Cell411Activity) super.getMainActivity();
  }

  public String getVersion() {
    return ConfigDepot.getAppVersion();
  }

  public LiveQueryService getLiveQueryService() {
    return LiveQueryService.req();
  }

  public Future<Throwable> startLiveQueryService() {
    Context context=getBaseContext();
    XLog.i(TAG, "startLiveQueryService() context=%s",context);
    assert context!=null;
    if(mLiveQueryConnection==null)
      mLiveQueryConnection = new LiveQueryConnection(this,context);
    return BaseApp.getExecutor().submit((Callable<Throwable>) mLiveQueryConnection);
  }

  public void clearCache() {
    LiveQueryService liveQueryService = LiveQueryService.opt();
    if (liveQueryService != null)
      liveQueryService.clear();
  }

  public PrintStream getLogStream() {
    return System.out;
  }

  public File getExternalDir(String type) {
    File dir = new File(getExternalFilesDir(type),
      "cell411/" + ConfigDepot.getFlavor());

    if (!dir.mkdirs() && !dir.exists()) {
      showAlertDialog(
        "dir " + dir + " does not exist, and creation failed");
      return null;
    }
    return dir;
  }

  protected Class<Cell411Activity> getMainActivityClass() {
    return Cell411Activity.class;
  }

  public void onCreate() {
    super.onCreate();
    //if (Util.theGovernmentIsHonest())
  }
//  {
//    System.setOut(getPS());
//  }
//  public PrintStream getPS() {
//    try {
//      File file = getExternalDir(DIRECTORY_DOCUMENTS);
//      file = new File(file, "AndStuff");
//      FileOutputStream fos = new FileOutputStream(file);
//      return new SimplePrintStream();
//    } catch (Exception ex) {
//      ex.printStackTrace();
//    }
//    return System.out;
//  }

  public Set<String> getAllPermissions() {
    if (mAllPermissions != null) {
      return mAllPermissions;
    }

    Set<String> perms = new HashSet<>();

    perms.add(ACCESS_NETWORK_STATE);
    perms.add(CAMERA);
    perms.add(INTERNET);
    perms.add(RECEIVE_BOOT_COMPLETED);
    perms.add(RECORD_AUDIO);
    perms.add(REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);

    LocationService.getPermRequests(perms);
    LiveQueryService.getPermRequests(perms);
    return perms;
  }

  public Set<String> getMissingRingtones() {
    return new HashSet<>();
  }

  protected void sendLocationNotification(ParseGeoPoint geoPoint) {
    NotificationAgent agent = getNotificationAgent();
    if (agent == null)
      return;
    agent.sendLocationNotification(geoPoint);
  }

  public void openChat(final XEntity cell) {
    MainActivity mainActivity = getMainActivity();
    if (mainActivity == null)
      return;
    mainActivity.openChat(cell);
  }

  public TreeMap<String, RingtoneData> getTones() {
    return RingtoneData.getRingtoneData();
  }

  public NotificationAgent getNotificationAgent() {
    if (mNotificationAgent == null) {
      if (getLiveQueryService() == null)
        return null;
      mNotificationAgent = new NotificationAgent(LiveQueryService.opt());
    }
    return mNotificationAgent;
  }

  public void requestPermission(String[] perms, ResultListener<String[]> results) {
    Runnable runnable = () -> results.result(perms, null);
    getExecutor().schedule(runnable, 500, TimeUnit.MILLISECONDS);
  }

  public boolean getDarkModeChanged() {
    return mDarkModeChanged;
  }

  public void setDarkModeChanged(boolean changed) {
    mDarkModeChanged = changed;
  }

  @Override
  public boolean isNotReady()
  {
    return false;
  }

  static class ThreadStream
    extends Thread
  {
    Socket mSock;

    ThreadStream(Socket sock) {
    }

    public OutputStream getNetStream() {
      try {
        mSock = new Socket("dev.copblock.app", 6666);
        return new PrintStream(mSock.getOutputStream());
      } catch (IOException e) {
        throw Util.rethrow(e);
      }
    }
  }

  public static class ByteSink implements Consumer<byte[]> {
    public void accept(byte[] bytes) {
      try {
        System.out.write(bytes);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  final Set<String> mAllPermissions = getAllPermissions();
}
