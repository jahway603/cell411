package com.safearx.cell411;

import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import cell411.methods.Dialogs;
import cell411.parse.XEntity;
import cell411.ui.base.FragmentFactory;
import cell411.ui.base.FragmentFrame;
import cell411.ui.base.MainActivity;
import cell411.utils.Reflect;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class Cell411Activity extends MainActivity {
  private final Closer mCloser = new Closer();
  private DrawerLayout mDrawer;
  private Cell411NavView mNavigationView;
  private ActionBarDrawerToggle mToggle;
  private Toolbar mActionBar;

  public Cell411Activity() {
    super(R.layout.activity_main);
  }

  public void setDrawerIndicatorEnabled(boolean b) {
    mToggle.setDrawerIndicatorEnabled(b);
  }

  public void closeDrawer() {
    onUI(mCloser);
  }

  long mDebounceTime=0;
  @Override
  public void onBackPressed() {
    long now = System.currentTimeMillis();
    if(now<mDebounceTime)
      return;
    mDebounceTime=now+1000;
    if (mDrawer.isOpen()) {
      mCloser.run();
    } else {
      FragmentFrame result;
      synchronized (this) {
        result = getFragmentFrame();
        if (result.getDepth() >= 2) {
          result.onBackPressed();
          return;
        }
      }
      Dialogs.showYesNoDialog("Do you want to exit?", success ->
      {
        if (success) {
          super.onBackPressed();
        }
      });
    }
  }

  @Override
  protected void onCreate(@Nullable final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mActionBar = findViewById(R.id.toolbar);

    mDrawer = findViewById(R.id.drawer_layout);
    setSupportActionBar(mActionBar);

    int navOpen = R.string.navigation_drawer_open;
    int navClose = R.string.navigation_drawer_close;
    mToggle = new ActionBarDrawerToggle(
      this, mDrawer, mActionBar, navOpen, navClose);
    for (int i = 0; i < mActionBar.getChildCount(); i++) {
      View view = mActionBar.getChildAt(i);
      if (!(view instanceof TextView)) {
        continue;
      }
      view.setOnClickListener(this::onTitleClick);
      break;
    }

    mDrawer.addDrawerListener(mToggle);
    mToggle.syncState();
    mNavigationView = findViewById(R.id.nav_view);
    mNavigationView.setup(this);
    FragmentSelector.start();
    mCloser.run();
  }

  private void onTitleClick(View view) {
    Reflect.announce("Title Clicked");
    FragmentFactory top = getFragmentFrame().getTopFactory();
    System.out.println(top);
  }

  @Override
  public void setVisible(boolean visible) {
    System.out.println("setVisible(" + visible + ")");
    super.setVisible(visible);
  }

  @Override
  protected void onResume() {
    if (Cell411.req().getDarkModeChanged()) {
      Cell411.req().setDarkModeChanged(false);
    }

    super.onResume();
  }

  @Override
  public void openChat(XEntity cell) {
    FragmentSelector.openChat(cell);
  }

  @Override
  public boolean onOptionsItemSelected(@Nonnull MenuItem item) {
    return super.onOptionsItemSelected(item);
  }

  @Override
  public ApplicationInfo getApplicationInfo() {
    if (getBaseContext() == null) {
      return null;
    }
    return super.getApplicationInfo();
  }

  @Override
  public void onFragmentPopped() {
    super.onFragmentPopped();
  }

  class Closer implements Runnable {
    @Override
    public void run() {
      if (!isOnUI()) {
        onUI(this, 200);
      } else {
        mDrawer.close();
      }
    }
  }
}