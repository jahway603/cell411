package com.safearx.cell411;

import android.view.View;
import cell411.libcell.ConfigDepot;
import cell411.logic.LiveQueryService;
import cell411.parse.XEntity;
import cell411.parse.XUser;
import cell411.ui.MainFragment;
import cell411.ui.base.BaseApp;
import cell411.ui.base.BaseFragment;
import cell411.ui.base.FragmentFactory;
import cell411.ui.welcome.AccountFragment;
import cell411.ui.welcome.PermissionFragment;
import cell411.ui.welcome.StartFragment;
import cell411.ui.welcome.UserConsentFragment;
import cell411.utils.ExceptionHandler;
import cell411.utils.NetUtil;
import cell411.utils.OnCompletionListener;
import cell411.utils.Reflect;
import cell411.utils.ThreadUtil;
import cell411.utils.Util;
import cell411.utils.ValueObserver;
import cell411.utils.func.Func0;
import com.parse.Parse;
import com.parse.model.ParseUser;

import javax.annotation.Nullable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static cell411.ui.base.BaseApp.getExecutor;
import static cell411.utils.Reflect.announce;

public class FragmentSelector
  implements ValueObserver<Object>, ExceptionHandler, Runnable
{
  private static final boolean smSendReports = Util.theGovernmentIsHonest();

  static {
    announce();
  }
  {
    announce("Created: "+this);
  }

  private Cell411Activity mActivity;
  static ExecutorService mExecutor =
    Executors.newSingleThreadExecutor();
  FragmentFactory mStartFactory =
    FragmentFactory.fromClass(StartFragment.class);
  FragmentFactory mPermissionFactory =
    FragmentFactory.fromClass(PermissionFragment.class);
  FragmentFactory mUserConsentFactory =
    FragmentFactory.fromClass(UserConsentFragment.class);
  FragmentFactory mMainFactory = FragmentFactory.fromClass(MainFragment.class);
  FragmentFactory mAccountFactory =
    FragmentFactory.fromClass(AccountFragment.class);

  FragmentSelector()
  {
  }

  public static void start()
  {
    getExecutor().execute(
      new FragmentSelector()
    );
  }

  private static Boolean userHasConsented() {
    ParseUser parseUser = ParseUser.getCurrentUser();
    if (!XUser.class.isInstance(parseUser)) {
      return false;
    }
    XUser user = (XUser) parseUser;
    return user.getConsented();
  }

  static void tryInitParse() {
    announce();
    BaseApp.req().initParse();
  }

  void tryStartLiveQueryService() {
    announce();
    try {
      if (LiveQueryService.opt() != null)
        return;
      mActivity.onUI(() -> mActivity.setDrawerIndicatorEnabled(false));
      Future<Throwable> future = Cell411.req().startLiveQueryService();
      LiveQueryService service = LiveQueryService.opt();
      while (service==null){
        try {
          Throwable except = future.get();
          if (except != null) {
            throw except;
          }
          service = LiveQueryService.opt();
        } catch ( TimeoutException te ) {
          System.out.println(""+te);
        }
      }
      if (service == null) {
        throw new NullPointerException("Service is null");
      }
      service.addReadyObserver(this);
      while (!service.isReady()) {
        announce("Waiting for ready");
        ThreadUtil.wait(this, 2000);
      }
    } catch (Throwable e) {
      OnCompletionListener onCompletionListener =
        success -> getExecutor().execute(this);
      mActivity.handleException(
        "staring live query service", e, onCompletionListener ,true);
    }
  }
  void poll(Future<Throwable> future){
    while(true) {
      try {
        Throwable except = future.get(1000, TimeUnit.MILLISECONDS);
        break;
      } catch (TimeoutException te) {
        System.out.println(""+te);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }

  }
  void tryPushWait(FragmentFactory factory, Func0<Boolean> func)
  {
    announce(factory);
    while (!func.apply()) {
      BaseFragment frag = factory.get(false);
      assert(frag==null || !frag.isAdded());

      mActivity.push(factory, true, true);

      while ((frag = factory.get(false)) == null || !frag.isAdded()) {
        ThreadUtil.wait(this, 2000);
      }

      while ((frag = factory.get(false)) != null && frag.isAdded()) {
        View view = frag.getView();
        if (view == null) {
          ThreadUtil.wait(this, 500);
          continue;
        }

        if (func.apply()) {
          break;
        }
        ThreadUtil.wait(this,5000);
      }
    }
  }

  @Override
  public synchronized void run() {
    Thread thread = Thread.currentThread();
    String oldName = thread.getName();
    mActivity= Cell411.req().getMainActivity();
    if(mActivity==null) {
      getExecutor().postDelayed(this, 500);
      return;
    }
    try {
      Reflect.announce(this);
      thread.setName("FragmentSelector");
      announce();
      while(ConfigDepot.get()==null){
        Reflect.announce("Waiting for Config");
        ThreadUtil.wait(this,500);
      }
      Parse.addCurrentUserObserver(this);
      mActivity.push(mStartFactory);

      BaseApp app = BaseApp.req();
      assert app!=null;
      tryPushWait(mPermissionFactory, app::hasPermissions);
      tryInitParse();
      tryPushWait(mAccountFactory, () -> ParseUser.getCurrentUser() != null);
      tryStartLiveQueryService();
      tryPushWait(mUserConsentFactory, FragmentSelector::userHasConsented);

      maybeSendReport();
      uploadCachedFiles();

      mActivity.onUI(() -> {
        mActivity.setDrawerIndicatorEnabled(true);
        mActivity.push(mMainFactory,true,true);
      });
      Reflect.announce("FS done");
    } catch ( Throwable e ) {
      Reflect.announce(e);
      e.printStackTrace();
      Reflect.announce(e);
    } finally {
      Reflect.announce(this);
      thread.setName(oldName);
    }
  }

  private void uploadCachedFiles()
  {
  }

  public static class UploadCachedFiles
    implements Runnable
  {
    // This runs unconditionally, the func will just
    // return if it has nothing to do.
    public void run() {
      getExecutor()
        .schedule(this, 1000, TimeUnit.MILLISECONDS);
    }
  }

  private void maybeSendReport() {
    if (smSendReports) {
      LiveQueryService service = LiveQueryService.opt();
      assert service.isReady();
      String header = "Do you want to send a copy to the\n" + "developers?";

      String report = service.report();
      ReportAccepted accepted = new ReportAccepted(header);
      showYesNoDialog(header, report, accepted);
    }
  }

  public void pushStart() {
    mActivity.push(mStartFactory);
  }

  public static void openChat(XEntity cell) {
    announce();
  }
  @Override
  public void onChange(@Nullable Object newValue, @Nullable Object oldValue) {
    announce();
    ThreadUtil.notify(this, true);
  }

  static class ReportAccepted implements OnCompletionListener
  {
    String mReport;

    public ReportAccepted(String report) {
      mReport = report;
    }

    @Override
    public void done(boolean success) {
      if (success) {
        NetUtil.sendMail("dev@copblock.app", "load report", mReport);
      }
    }
  }
}
