package com.safearx.cell411;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;

import androidx.core.content.res.ResourcesCompat;

import com.google.android.material.navigation.NavigationView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cell411.ui.base.BaseContext;
import cell411.ui.nav.NavHeaderMain;
import cell411.ui.nav.NavListener;

public class Cell411NavView extends NavigationView
  implements BaseContext {
  private Menu mNavMenu;
  private Cell411Activity mActivity;

  public Cell411NavView(@Nonnull Context context) {
    this(context, null);
  }

  public Cell411NavView(@Nonnull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
  }

  public Cell411NavView(@Nonnull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  public void setup(Cell411Activity activity) {
    NavHeaderMain navHeaderMain = (NavHeaderMain) inflateHeaderView(R.layout.nav_header_main);
    mActivity = activity;
    mNavMenu = getMenu();
    setNavigationItemSelectedListener(new NavListener(mActivity));
    setActionView(mNavMenu, R.id.nav_share_this_app);
    setActionView(mNavMenu, R.id.nav_faq_and_tutorials);
    setActionVisible(mNavMenu, R.id.nav_notifications);
    setActionVisible(mNavMenu, R.id.nav_know_your_rights);
    setupLogout();
    onUI(navHeaderMain);
  }

  public void setupLogout() {
    MenuItem miLogout = mNavMenu.findItem(R.id.nav_logout);
    if (miLogout == null) {
      return;
    }
    SpannableString s = new SpannableString(miLogout.getTitle());
    s.setSpan(new TextAppearanceSpan(getContext(),
      R.style.TextAppearanceLogout), 0, s.length(), 0);
    miLogout.setTitle(s);
    Drawable mDrawableLogout =
      ResourcesCompat.getDrawable(getResources(), R.drawable.nav_logout,
        mActivity.getTheme());
    if (mDrawableLogout == null) {
      return;
    }
    mDrawableLogout.setColorFilter(
      new PorterDuffColorFilter(mActivity.getColor(R.color.highlight_color_dark),
        PorterDuff.Mode.MULTIPLY));
    miLogout.setIcon(mDrawableLogout);
  }

  private void setActionVisible(Menu navMenu, int itemId) {
    if (navMenu == null) {
      return;
    }
    MenuItem item = navMenu.findItem(itemId);
    if (item == null) {
      return;
    }
    item.setVisible(true);
  }

  private void setActionView(Menu navMenu, int nav_share_this_app) {
    if (navMenu == null) {
      return;
    }
    MenuItem item = navMenu.findItem(nav_share_this_app);
    if (item == null) {
      return;
    }
    item.setActionView(R.layout.layout_outside_link);
  }
}
