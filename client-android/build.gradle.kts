val debugApplicationIdSuffix by extra(".dev")
buildscript {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:7.3.1")
    }
}

tasks.register("hello") {
    doLast {
        println("Hello, World!");
    }
}
