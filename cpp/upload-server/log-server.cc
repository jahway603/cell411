#include "system.hh"
#include "checkret.hh"
#include "fixed_buf.hh"
#include "md5.h"
#include "md5.hh"
#include "unixpp.hh"
#include "util.hh"


using namespace checkret;
using unixpp::range_t;
using unixpp::xmmap_file;
using std::string;
extern "C" {
  int dprintf(int, const char *, ...);
  int atoi(const char *);
}


static std::array<char,1024*1024> buf;
typedef int fd_t;

int main(int argc, char**argv){
  int port=9999;
  dprintf(2,"listening on %d\n", port);
  fd_t ifd=bind_accept_fork("0.0.0.0",port);
  dup2(ifd,0);
  close(ifd);
  ifd=0;
  char fname[128];
  mkdir("log",0700);
  snprintf(fname,sizeof(fname)-1,"log/log.%s.log",fmt_now());
  dprintf(2,"output: %s\n",fname);
  fd_t ofd=xopen(fname,O_WRONLY|O_APPEND|O_CREAT,0700);
  while(true){
    size_t res=xread(ifd,buf.begin(),sizeof(buf));
    char *beg=buf.begin();
    char *end=beg+res;
    while(beg<end){
      size_t wes=xwrite(ofd,beg,end-beg);
      beg+=wes;
    }
    beg=buf.begin();
    while(beg<end){
      size_t wes=xwrite(2,beg,end-beg);
      beg+=wes;
    }
  }
  dprintf(2,"Done\n");
  return 0;
}
